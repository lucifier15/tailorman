/**
 * Fabric design
 * @{param} -> 
 * #(design) - array of records || undefined
 * $ purpose -> If any disable dependency and selected
 *              then make dependencies disable
 */
export function fabricDesignDependency(design) {
    if(design) {
		let duplicateDesign = _.cloneDeep(design);

		duplicateDesign.map((item, index) => {
            let allDependencies = [];
            let enableDependencies = [];

            _.find(item.Designs, (value) => {
            	if(value.disable_dependency) {
            		allDependencies.push(value);
            	}

            	if(value.enable_dependency) {
            		enableDependencies.push(value);
            	}
            });

            //Disable/enable based on disable_dependency
            if(allDependencies && allDependencies.length > 0) {
            	_.find(allDependencies, (type) => {
            		let selected = type.selected;
            		let dependency = type.disable_dependency;

					if(selected || selected === 0 || selected != -1) {
						let dependentObj = dependency[type.values[selected]];

						if(dependentObj && dependentObj.length > 0) {
							dependentObj.map((value, index) => {
								let typeValue = _.find(item.Designs, { id: value });

								if(typeValue) {
	                                typeValue.selected = -1;
	                                typeValue.disabled = true;
	                            }

							});
						} else {
							_.forIn(dependency, (values, key) => {
								values.map((value, i) => {
									let typeValue = _.findIndex(item.Designs, { id: value });

									if(typeValue != -1 && item.Designs[typeValue]) {
		                                item.Designs[typeValue].selected = (item.Designs[typeValue].disabled) ? 0 : item.Designs[typeValue].selected;
		                                item.Designs[typeValue].disabled = false;
		                            }
								});
							});
						}
					}
            	});
            }

            //Enable only when dependent is selected
            //if else it will be in disabled mode
            if(enableDependencies && enableDependencies.length > 0) {
            	_.find(enableDependencies, (type) => {
            		let selected = type.selected;
            		let dependency = type.enable_dependency;
            		
					if(selected || selected === 0 || selected != -1) {
						let dependentObj = dependency[type.values[selected]];

						if(dependentObj && dependentObj.length > 0) {
							dependentObj.map((value, index) => {
								let typeValue = _.find(item.Designs, { id: value });

								if(typeValue) {
	                                typeValue.disabled = false;
	                            }

							});
						} else {
							_.forIn(dependency, (values, key) => {
								values.map((value, i) => {
									let typeValue = _.findIndex(item.Designs, { id: value });

									if(typeValue != -1 && item.Designs[typeValue]) {
										item.Designs[typeValue].selected = -1;
		                                item.Designs[typeValue].disabled = true;
		                            }
								});
							});
						}
					}
            	});
            }
		});

		return duplicateDesign;
	} else {
		return design;
	}
}