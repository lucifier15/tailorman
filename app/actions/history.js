import * as types from 'types';
import {
	polyfill
} from 'es6-promise';
import {
	push
} from 'react-router-redux';
import request from 'axios';
import moment from 'moment';

polyfill();

function makeRequest(method, data, api) {
	return request[method](api, data);
}

function getOrderItems(order) {
	return makeRequest('get', null, '/order/itemList?order_id=' + order.order_id).then((response) => {
		if (response.status === 200) {
			const order_items = response.data.map((sale) => {
				sale.style = sale.style_id;
				sale.fiton_date = (sale.fiton_date) ? moment(sale.fit_on_date).format('YYYY-MM-DD') : '';
				sale.sku = sale.style;
				sale.delivery_date = (sale.delivery_date) ? moment(sale.delivery_date).format('YYYY-MM-DD') : '';
				return sale;
			});
			order.sales = order_items || [];
			return order;
		} else {
			console.log("Error fetching order items");
		}
	});
}


export function populateOrders(customer_id) {

	return dispatch => {
		makeRequest('get', null, '/orders?customer_id=' + customer_id).then(response => {
			if (response.status === 200) {
				var customerOrders = response.data.map((order) => {
					order.id = order.order_id;
					order.name = order.display_name || [order.order_id, moment(order.modified_time).format('LL')].join('--');
					order.order_date = moment(order.order_date).format('YYYY-MM-DD');
					//TODO: Fix these typos in the procs
					order.occasion_date = order.occation_date ? moment(order.occation_date).format('YYYY-MM-DD') : '';
					order.benificiary_phone = order.benficiary_mobile;
					order.benificiary_name = order.benficiary_name;
					return order;
				});
				Promise.all(customerOrders.map((order) => {
					return getOrderItems(order);
				})).then((orders) => {
					dispatch({
						type: types.HISTORY_POPULATE_ORDERS,
						orders
					});
				});
			} else {
				console.log("Error receiving list of orders for customer", customer_id);
			}
		});
	}
}

export function setActiveKey(key) {
	return {
		type: types.TOGGLE_ACCORDION,
		key
	}
}