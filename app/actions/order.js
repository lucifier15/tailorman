import * as types from 'types';
import {
	polyfill
} from 'es6-promise';
import {
	push
} from 'react-router-redux';
import request from 'axios';
import moment from 'moment';
polyfill();


function makeRequest(method, data, api) {
	return request[method](api, data);
}
export function updateMessage(type, message) {
	return {
		type,
		message
	}
}

export function updateCustomerGender(value) {
	return {
		type: types.ORDER_CUSTOMER_UPDATE_GENDER,
		gender: value[0]
	}
}
export function updateCustomerSource(value) {
	return {
		type: types.ORDER_CUSTOMER_UPDATE_SOURCE,
		source_id: value
	}
}

export function updateCustomerIsNew(value) {
	return {
		type: types.ORDER_CUSTOMER_UPDATE_IS_NEW_CUSTOMER,
		isNewCustomer: value[0]
	}
}

export function updateCustomerPincode(pincode) {
	return {
		type: types.ORDER_CUSTOMER_UPDATE_PINCODE,
		pincode
	}
}

export function updateCustomerPhone(phone) {
	return {
		type: types.ORDER_CUSTOMER_UPDATE_PHONE,
		phone
	}
}

export function updateOrderTotal(total_amount) {
	return {
		type: types.ORDER_DETAILS_UPDATE_TOTAL_AMOUNT,
		total_amount
	}
}

export function selectOrder(order, redirect) {
	return dispatch => {
		dispatch({
			type: types.SELECT_ORDER,
			order
		});
		if (redirect)
			dispatch(push('/order/details'));
	}
}

export function saveMeasurementProfile(profile, measurements, index) {
	return dispatch => {
		return makeRequest('post', profile, '/order/customer/profile').then((response) => {
			if (response.status === 200) {
				profile.profile_id = response.data.profile_id;
				return profile;
			}
		}).then(function (profile) {
			dispatch({
				type: types.ORDER_SALES_UPDATE_SALE_ITEM_ENTRY,
				payload: {
					value: profile,
					field: 'profile',
					index
				}
			});
			return saveMeasurements(profile.profile_id, measurements);
		})
	}
}

export function saveProfile(profile, measurements) {
	var profileSave = (profile) => {
		let url = null;
		if (profile.profile_id && profile.profile_id > 0) {
			url = '/order/customer/profile/update';
		} else {
			url = '/order/customer/profile';
		}
		return makeRequest('post', profile, url).then((response) => {
			if (response.status === 200) {
				profile.profile_id = profile.profile_id || response.data.profile_id;
				return profile;
			}
		})
	}
	return profileSave(profile).then(function (profile) {
		return saveMeasurements(profile.profile_id, measurements);
	});
}
export function getThumbnails(order_id) {
	return dispatch => {
		return makeRequest('get', null, '/details/image?order_id=' + order_id + "&image_id=" + types.IMAGE_TYPE_MEASUREMENT).then((response) => {
			let imageDataURLs = [];
			if (response.status === 200) {
				if (response.data && response.data.length > 0) {
					imageDataURLs = response.data.map((image) => {
						var convertImage = (image) => {
							let dataURL = '';
							image.forEach((charCode) => {
								dataURL += String.fromCharCode(charCode);
							})
							return dataURL;
						}
						return convertImage(image.image.data);
					});
				}
			}
			dispatch({
				type: types.ORDER_POPULATE_THUMBNAILS,
				imageDataURLs
			})
		});
	}
}

export function showThumbnail(thumbnail, show) {
	return {
		type: types.SHOW_THUMBNAIL,
		show,
		thumbnail
	}
}


export function getOrderItems(order_id, customer_id) {
	return dispatch => {
		makeRequest('get', null, '/order/itemList?order_id=' + order_id).then((response) => {
			if (response.status === 200) {
				var sales = response.data.map((sale) => {
					sale.style = sale.style_id || sale.sku_id;
					sale.fiton_date = (sale.fit_on_date) ? moment(sale.fit_on_date).format('YYYY-MM-DD') : '';
					sale.sku = sale.sku || sale.style;
					sale.name = sale.display_name;
					sale.id = sale.order_item_id;
					sale.discount_value = sale.discount_value || sale.discount;
					sale.delivery_date = (sale.delivery_date) ? moment(sale.delivery_date).format('YYYY-MM-DD') : '';
					sale.finish_type_id = sale.finish_type_id || sale.finish_type;
					sale.is_new_measurements = (sale.profile_id > -1) ? false : true;
					sale.order_flag = sale.order_flag || 1;
					sale.upcharges = sale.upcharge || []
					return sale;
				});
				Promise.all(sales.map((sale) => {
					return makeRequest('get', null, '/order/item/fabricDesign?order_item_id=' + sale.order_item_id).then((response) => {
						if (response.status === 200) {
							return response.data;
						}
					});
				})).then((fabricDesigns) => {
					for (var i = 0; i < sales.length; i++) {
						if (fabricDesigns[i][0]) {
							sales[i].fabric_designs = fabricDesigns[i][0].fabric_design;
							sales[i].fabric_design_comment = fabricDesigns[i][0].comment;
						}
					}
					return;
				}).then(() => {
					return Promise.all(sales.map((sale) => {
						return makeRequest('get', null, '/profiles?item_type_id=' + sale.item_type_id + "&customer_id=" + customer_id).then((profiles) => {
							if (sale.profile_id > -1) {
								profiles = profiles.data;
								const profile = _.find(profiles, {
									profile_id: parseInt(sale.profile_id)
								});

								profile.name = profile.profile_name;
								profile.profile_id = sale.profile_id;
								const measurement_upcharge = sale.upcharges ? _.find(sale.upcharges, {
									type: 'Higher Size Upcharge'
								}) : false;
								return getUpchargedMeasurementProfile(profile, sale.item_type_id, measurement_upcharge).then((profile) => {
									sale.profile = profile;
									return sale;
								});
							} else {
								return sale;
							}
						});
					}))
				}).then((sales) => {
					dispatch({
						type: types.ORDER_ITEMS_POPULATE,
						sales
					});
				});
			}
		});
	}
}

function getUpchargedMeasurementProfile(profile, type_id, has_measurement_upcharge) {
	return makeRequest('get', null, '/profile/values?profile_id=' + profile.profile_id).then((response) => {
		if (response.status === 200) {
			const values = response.data;
			return makeRequest('get', null, '/list/measurementField?item_type_id=' + type_id + "&measurement_type_id=" + profile.measurement_source_id).then(response => {
				if (response.status === 200) {
					const fields = response.data;
					profile.measurements = values.map((value) => {
						const field = _.find(fields, {
							measurement_type_id: value.measurement_type_id
						});
						if (!has_measurement_upcharge) {
							if (field && field.upcharge_limit > 0 && field.upcharge_limit <= value.value) {
								value.upcharge = {
									type: 'Higher Size Upcharge',
									value: (field.upcharge_persentage > 0) ? field.upcharge_persentage : field.upcharge_value,
									unit: (field.upcharge_persentage > 0) ? types.UPCHARGE_UNIT_PERCENTAGE : types.UPCHARGE_UNIT_VALUE,
								}
							}
						}
						value.id = value.measurement_type_id;
						return value;
					});
					profile.values = profile.measurements;
					return profile;
				} else {
					return profile;
				}
			});
		} else {
			return profile;
		}
	});
}
export function updateSaleItemProfile(profile, item_type, saleItemIndex) {
	return dispatch => {
		getUpchargedMeasurementProfile(profile, item_type).then((profile) => {
			dispatch(updateSaleItemEntry(profile, 'profile', saleItemIndex));
		})
	}
}

export function getUpCharge(fabricDesign) {
	let upcharge = [];
	fabricDesign.forEach((type) => {
		type.Designs.forEach((design) => {
			if (design.selected > 0 && design.upcharge && design.upcharge > 0) {
				upcharge.push({
					type: design.name,
					value: design.upcharge
				});
			}
		});
	});
	return upcharge;
}

export function updateCustomer(customer) {
	customer.mobile = customer.phone;
	console.log(customer, parseInt(customer.customer_id) > 0, customer.id > 0, customer.customer_id > 0 || customer.id > 0);
	let url = '/order/customer';
	if (customer.customer_id > 0 || customer.id > 0)
		url = '/order/customer/update';
	return dispatch => {
		makeRequest('post', customer, url).then((response) => {
			if (response.status === 200) {
				if (!(customer.customer_id > 0 || customer.id > 0))
					customer.customer_id = response.data.customer_id;
				dispatch({
					type: types.ORDER_CUSTOMER_UPDATE,
					customer
				});
				dispatch(updateMessage(types.ORDER_CUSTOMER_SAVE_SUCCESS, 'Customer ' + customer.name + ' has been updated'));
			} else {
				console.log("Something went wrong with the request", err);
			}
		});
		dispatch(push('/order/details'));
	}
}

export function saveImage(imageDataURL, order_id, type) {
	return dispatch => {
		makeRequest('post', {
			dataURL: imageDataURL,
			order_id: order_id,
			type: type
		}, '/order/saveImage').then(response => {
			if (response.status === 200) {
				dispatch({
					type: types.ORDER_ADD_IMAGE_THUMBNAIL,
					imageDataURL
				})
			} else {
				console.log("Something went wrong with the request", err);
			}
		});
	}
}

//An Image to be uploaded in AWS s3
//and update in DB
export function saveImageAWS(file, order_id, type) {
	return dispatch => {
		getSignedUrl(file, order_id, type)
			.then(data => {
				return uploadImagetoAWS(file, data.data.requestUrl, data.data.imageUrl);
			})
			.then(imageUrl => {
				return updateImageInDB(imageUrl, order_id, type)
			})
			.then(imageDataURL => {
				dispatch(updateMessage('IMAGE_UPLOADED_SUCCESS', 'Image uploaded successfully!'));
				dispatch({
					type: types.ORDER_ADD_IMAGE_THUMBNAIL,
					imageDataURL
				})
			})
			.catch(error => {
				console.log('Error to upload the image: ', error);
				dispatch(updateMessage('IMAGE_UPLOADED_FAILURE', 'Failed to upload the image!'));
			});
	}
}

//To get the s3 signed url
export function getSignedUrl(file, order_id, type) {
	var fileObject = {
		type: file.type,
		size: file.size,
		name: file.name,
		order_id: order_id
	};
	console.log('fileObject: ', fileObject);

	return makeRequest('post', {
		file: fileObject
	}, '/s3/image/getsignedurl')
	.then((response) => {
		let data = response.data;

		return data;
	})
	.catch((error) => {
		console.log('Error on getting signed request : ',error);
		throw new Error(error.message);
	});
}

//To upload into AWS s3 image using signed url
export function uploadImagetoAWS(file, signed_request, response_url) {
	var options = {
		headers: {
			'Content-Type': file.type
		}
	};

	return request
			.put(signed_request, file, options)
					.then((data) => {
						return response_url;
					})
					.catch((error) => {
						throw new Error(error.message);
					});
}

//To update in DB
export function updateImageInDB(imageDataURL, order_id, type) {
	return makeRequest('post', {
		dataURL: imageDataURL,
		order_id: order_id,
		type: type
	}, '/order/saveImage')
	.then((response) => {
		return imageDataURL;
	})
	.catch((error) => {
		throw new Error(error.message);
	});
}

export function updatePriorityType(priority_type) {
	return {
		type: types.ORDER_DETAILS_UPDATE_PRIORITY_TYPE,
		priority_id: priority_type
	}
}
export function updateFullPaymentFlag(value) {
	return {
		type: types.ORDER_DETAILS_UPDATE_FULL_PAYMENT_FLAG,
		value
	}
}

export function updateTailor(tailor) {
	return {
		type: types.ORDER_DETAILS_UPDATE_TAILOR,
		tailor_id: tailor
	}
}

export function updateSalesman(salesman_id) {
	return {
		type: types.ORDER_DETAILS_UPDATE_SALESMAN,
		salesman_id
	}
}

export function updateOrderPIN(pin) {
	return {
		type: types.ORDER_DETAILS_UPDATE_PIN,
		pin
	}
}
export function updateOrderPINDate(pindate) {
	return {
		type: types.ORDER_DETAILS_UPDATE_PIN_Date,
		pindate
	}
}
export function updateOccasion(occasion) {
	return {
		type: types.ORDER_DETAILS_UPDATE_OCCASION,
		occasion
	}
}

export function updatePaymentType(payment_type_id) {
	return {
		type: types.ORDER_DETAILS_UPDATE_PAYMENT_TYPE,
		payment_type_id
	}
}

export function updateBenificiaryPhone(benificiary_phone) {
	return {
		type: types.ORDER_DETAILS_UPDATE_BENIFICIARY_PHONE,
		benificiary_phone
	}
}

function openFormSaleItem(close) {
	return {
		type: types.ORDER_SALES_OPEN_FORM,
		close
	}
}

function openFormMeasurements(close) {
	return {
		type: types.ORDER_SALE_ITEM_OPEN_MEASUREMENTS,
		close
	}
}

function openSignature(close) {
	return {
		type: types.OPEN_SIGNATURE,
		close
	}
}

function openFormStyles(close) {
	return {
		type: types.ORDER_SALE_ITEM_OPEN_STYLES,
		close
	}
}

export function initNewSaleItem(item_type, customer_id, sales) {
	let existComment = false;
	item_type = parseInt(item_type);
	sales.map((item,index)=>{
		if(item.item_type_id == item_type ){
			existComment = item.comment || true;
		}
	});
	
	return dispatch => {
		dispatch({
			type: types.FETCH_LIST_MEASUREMENT_FIELDS,
			measurement_fields: []
		});

		dispatch({
			type: types.FETCH_LIST_FABRIC_DESIGN_FIELDS,
			fabric_design_fields: []
		});

		dispatch({
			type: types.ORDER_SALES_INIT_SALE_ITEM,
			item_type,
			comment: (typeof existComment === 'string') ? existComment : ''
		});

		if(!existComment) {
			makeRequest('get', null, `/order/latestcomment?customer_id=${customer_id}&item_type_id=${item_type}`)
				.then((res) => {
					let data = res.data && res.data.data;

					let comment = (data && data.comment) || '';

					dispatch({
						type: types.UPDATE_ORDER_SALES_INIT_SALE_ITEM,
						item_type,
						comment
					});
				})
				.catch((error) => {
					console.error(error);
				})
		}
	};
}

export function deleteSaleItem(order_item_id, index) {
	return dispatch => {
		if (order_item_id && parseInt(order_item_id) > 0) {
			makeRequest('get', null, '/order/item/delete?order_item_id=' + order_item_id).then((response) => {
				if (response.status === 200) {
					dispatch(updateMessage(types.DELETE_ORDER_ITEM_SUCCESS, 'The order item has been deleted the records'));
				} else {
					console.log("something went wrong with the deletion of order item");
				}
			});
		}
		dispatch({
			type: types.ORDER_SALE_ITEM_DELETE,
			index
		});
	}
}

export function selectSaleItem(index) {
	return {
		type: types.ORDER_SALE_ITEM_SELECT,
		index
	}
}
export function updateSaleItemEntry(value, field, index) {
	return {
		type: types.ORDER_SALES_UPDATE_SALE_ITEM_ENTRY,
		payload: {
			value,
			field,
			index
		}
	}
}

export function savePhoto(imageURL, order_id) {
	makeRequest('post', {
		order_id,
		imageURL
	}, '/order/photos').then((response) => {
		if (response.status === 200) {
			return response.data;
		} else {
			console.log("something went wrong with the saving of the photographs");
		}
	})
}

export function saveThumbnail(dataURL) {
	return {
		type: types.ORDER_ADD_IMAGE_THUMBNAIL,
		dataURL
	}
}

function saveOrderDetails(order_details) {
	return {
		type: types.ORDER_DETAILS_UPDATE,
		order_details
	}
}
export function openForm(type, close) {
	return dispatch => {
		switch (type) {
			case 'measurements':
				dispatch(openFormMeasurements(close));
				break;
			case 'styles':
				dispatch(openFormStyles(close));
				break;
			case 'saleItem':
				dispatch(openFormSaleItem(close));
				break;
			case 'signature':
				dispatch(openSignature(close));
				break;
		}
	}
}

export function saveOrderItem(order_item, is_update, index) {
	return dispatch => {
		var url = (is_update) ? '/order/item/update' : '/order/item';
		if (!is_update && order_item.order_item_id > 0 && order_item.workflow_id === 2) {
			url = '/order/altered/item';
		}
		makeRequest('post', order_item, url).then((saleResponse) => {
			if (!is_update && !order_item.order_item_id)
				order_item.order_item_id = saleResponse.data.order_item_id;
			if (saleResponse.status === 200) {
				dispatch(updateMessage(types.SALE_ITEM_SAVE_SUCCESS, 'Sale Item details have been saved'));
				dispatch({
					type: types.ORDER_SALES_UPDATE_SALE_ITEM,
					order_item,
					index
				})
			} else {
				console.log("something went wrong with the sale request");
			}
		})
	}
}
export function saveCommentOrderItems(order_item, order_sales_items) {
	return dispatch => {
		var url = '/order/item/update';
		order_sales_items.map((item,index)=>{
			if( item.order_item_id && item.order_item_id != order_item.order_item_id && order_item.item_type_id == item.item_type_id ){
				item.comment = order_item.comment;
				/*
					Representing the Upcharges in the Array Instead of Object
				*/
				item.upcharges = (item.upcharges && _.isArray(item.upcharges)) ? item.upcharges : []; 
				makeRequest('post', item , url).then((saleResponse) => {
					if(saleResponse.status === 200){
						dispatch({
							type: types.ORDER_SALES_UPDATE_SALE_ITEM,
							order_item : item,
							index
						});
					}
				});		
			}
		});
	}
}
export function saveMeasurements(profile_id, measurements) {
	if(measurements && measurements.length > 0) {
		const _measurement = {
			profile_id,
			measurements	
		};

		return makeRequest('post', _measurement, '/order/item/bulkmeasurement')
			.then((measurementResponse) => {
				if (measurementResponse.status === 200)
					return measurementResponse.data;
				else
					console.log("Something went wrong when saving measurmenets");
			})
			.then(function () {
				return profile_id;
			});
	} else {
		return profile_id;
	}
	// return Promise.all(measurements.map((measurement) => {
	// 	const _measurement = {
	// 		type_id: measurement.id,
	// 		profile_id: profile_id,
	// 		value: measurement.value
	// 	};
	// 	return makeRequest('post', _measurement, '/order/item/measurement').then((measurementResponse) => {
	// 		if (measurementResponse.status === 200)
	// 			return measurementResponse.data;
	// 		else
	// 			console.log("Something went wrong when saving measurmenets");
	// 	})
	// })).then(function () {
	// 	return profile_id;
	// });
}

export function saveFabricDesign(order_item_id, fabric_design, comment) {
	return makeRequest('post', {
		fabric_design,
		comment
	}, '/order/item/fabricDesign?order_item_id=' + order_item_id).then(fabricDesignResponse => {
		if (fabricDesignResponse.status === 200)
			return fabricDesignResponse.data;
		else
			console.log("Something went wrong when saving Fabric Design");
	});
}

function saveOrder(order, isUpdate) {
	if (!order.details.total_amount)
		order.details.total_amount = 0;
	const url = isUpdate ? '/order/details/update' : '/order/details';
	return makeRequest('post', order.details, url).then((response) => {
		if (response.status === 200) {
			if (response.data.order_id)
				order.details.order_id = response.data.order_id;
			return order;
		}
	})
}
export function updateOrderDetails(details) {
	return dispatch => {
		const order = {
			details
		};
		const isUpdate = details.order_id > 0;
		saveOrder(order, isUpdate).then(function (order) {
			dispatch(saveOrderDetails(order.details));
			dispatch(push('/order/sales'));
			dispatch(updateMessage(types.ORDER_DETAILS_SAVE_SUCCESS, 'Order details have been saved'));

		});
	}
}

export function getTaxes(item_type_id, discount, store_id,completeUpchargeMrp) {
	let url = '/order/taxes?item_type_id=' + parseInt(item_type_id);
	if (parseInt(discount) > 0) {
		url += '&has_discount=true';
	}
	url += '&store_id=' + store_id;
	url += `&total=${completeUpchargeMrp}`;
	return makeRequest('get', null, url).then((response) => {
		if (response.status === 200) {
			return response.data;

		} else {
			console.log("Something went wrong when getting taxes");
		}
	});
}

export function getDiscountedTotal(sale) {
	let mrp = getUpChargedTotal(sale, sale.mrp);
	if (!sale.discount_type)
		sale.discount_type = types.UPCHARGE_UNIT_VALUE;
	if (sale.discount_type && sale.discount_value) {
		switch (sale.discount_type) {
			case types.UPCHARGE_UNIT_VALUE:
				mrp = Math.floor(parseFloat(mrp) - parseFloat(sale.discount_value));
				break;
			case types.UPCHARGE_UNIT_PERCENTAGE:
				mrp = mrp - Math.floor((mrp * sale.discount_value / 100));
				break;
		}
	}
	return parseFloat(mrp).toFixed(2);
}

export function getUpChargedTotal(sale, mrp) {
	let upChargeList = [];

	if (sale.upcharges && sale.upcharges.length > 0)
		upChargeList = sale.upcharges;
	if (sale.profile && sale.profile.measurements && sale.profile.measurements.length > 0)
		upChargeList = upChargeList.concat(_.compact(_.map(sale.profile.measurements, 'upcharge')));

	const totalUpCharge = upChargeList.reduce((_prev, _sale) => {
		if (_sale.type != types.DELIVERY_UPCHARGE) {
			if (_sale.unit == types.UPCHARGE_UNIT_VALUE)
				return _prev + parseInt(_sale.value);
			else if (_sale.unit == types.UPCHARGE_UNIT_PERCENTAGE)
				return _prev + Math.floor(sale.mrp * _sale.value / 100);
			else if (_sale.value)
				return _prev + parseInt(_sale.value);
		} else {
			return _prev;
		}
	}, 0);

	return ((parseFloat(mrp) + totalUpCharge)).toFixed(2);
}

export function calculateTaxes(sales, store_id) {
	return dispatch => {
		Promise.all(sales.map((sale) => {
			let completeUpchargeMrp = getDiscountedTotal(sale);
			return getTaxes(sale.item_type_id, sale.discount_value, store_id,completeUpchargeMrp);
		})).then((_taxes) => {
			
			const taxes = _taxes.map((tax) => {
				return _.orderBy(tax, ['order'], ['asc'])
			});
			for (var i = 0; i < sales.length; i++) {
				const discountedTotal = getDiscountedTotal(sales[i]);
				sales[i].taxes = taxes[i];
				let totalTax = 0;
				let completeTaxPercent = 0;
				sales[i].taxes && sales[i].taxes.map((tax)=>{
					completeTaxPercent += tax.percent;
				});
				let _temptax = ( parseFloat(discountedTotal) / (100+completeTaxPercent)*100 );
				sales[i].taxes = sales[i].taxes.map((tax) => {
					tax.value = ((parseFloat(tax.percent)/100) * _temptax );
					tax.mrp_before_tax = parseFloat(_temptax);
					// _temptax = _temptax - tax.value;
					totalTax += tax.value;
					return tax;
				});
				sales[i].discountedMRP = discountedTotal;
				sales[i].totalBeforeMRP = discountedTotal - totalTax;
				sales[i].upcharges = (sales[i].upcharges && _.isArray(sales[i].upcharges)) ? sales[i].upcharges : [];
				const delivery_upcharge = sales[i].upcharges.reduce((prev, next) => {
					if (next.type == types.DELIVERY_UPCHARGE) {
						return prev + next.value;
					} else {
						return prev;
					}
				}, 0);
				sales[i].subTotal = (parseFloat(discountedTotal) * sales[i].qty) + parseFloat(delivery_upcharge);
			}

			const _sales = _.cloneDeep(sales);
			dispatch({
				type: types.ORDER_ITEMS_POPULATE,
				sales: _sales
			});
			var total = _sales.reduce((prev, next) => {
				return prev + parseInt(next.subTotal);
			}, 0);
			dispatch(updateOrderTotal(total));
		});
	}

}

export function getDeliveryUpcharge(item_type_id, priority_id) {
	if (parseInt(item_type_id) > 0 && parseInt(priority_id) > 0) {
		return makeRequest('get', null, '/priority/upcharge?item_type=' + item_type_id + "&priority_id=" + priority_id).then((response) => {
			if (response.status === 200) {
				const priority = _.find(response.data, {
					priority_id: parseInt(priority_id)
				})
				if (priority && priority.delivery_upcharge > 0)
					return priority.delivery_upcharge;
				else
					return false;
			} else {
				return false;
			}
		}).catch((err) => {
			return false;
		})
	} else {
		return Promise.resolve(false);
	}
}


export function getInvoiceDetails(customer_id, order_id) {
	return dispatch => {
		const details = {};
		makeRequest('get', null, '/details/customer?customer_id=' + customer_id).then((response) => {
			if (response.status === 200) {
				details.customer = response.data;
			}
			return;
		}).then(() => {
			return makeRequest('get', null, '/details/list/order?order_id=' + order_id).then((response) => {
				if (response.status === 200) {
					details.sales = response.data;
				}
			})
		}).then(() => {
			return makeRequest('get', null, '/details/order?order_id=' + order_id).then((response) => {
				if (response.status === 200) {
					details.order = response.data;
				}
			})
		}).then(() => {
			return makeRequest('get', null, '/details/image?order_id=' + order_id + '&image_id=' + types.IMAGE_TYPE_SIGNATURE).then((response) => {
				if (response.status === 200) {
					details.signature = '';
					if (response.data[0] && response.data[0].image) {
						response.data[0].image.data.forEach((charCode) => {
							details.signature += String.fromCharCode(charCode);
						})
					}
				}
			})
		}).then(() => {
			dispatch({
				type: types.POPULATE_INVOICE_DATA,
				details
			})
		});
	}
}

export function saveCompleteOrder(order, user) {
	return dispatch => {
		//save Order Items and get Order ID
		return makeRequest('post', order.details, '/order/details/update').then((response) => {
			if (response.status === 200) {
				//order.details.order_id = response.data.order_id;
				dispatch(saveOrderDetails(order.details));
				return order;
			} else {
				console.log("Something went wrong with the request");
			}
		}).then((order) => {
			return Promise.all(order.sales.map((sale) => {
				if (sale.taxes) {
					return makeRequest('post', sale, '/order/item/update').then((response) => {
						if (response.status === 200) {
							return;
						}
					})
				} else {
					return {};
				}
			})).then((responses) => {
				return order;
			});
		}).then((order) => {
			makeRequest('get', null, '/order/invoice?customer_id=' + order.details.customer_id + "&order_id=" + order.details.order_id);
			//save Order Items and get Order Item IDs
			var fabric_design_save_calls = order.sales.map((sale) => {
				if (sale.fabric_designs && sale.fabric_designs.length > 0) {
					return saveFabricDesign(sale.order_item_id, sale.fabric_designs, sale.fabric_design_comment);
				}
			});

			fabric_design_save_calls = _.compact(fabric_design_save_calls);

			var all_save_calls = fabric_design_save_calls;
			Promise.all(all_save_calls).then((responses) => {
				//responses.forEach((response) => console.log("Saving measurements and fabric designs ", response));
			}).then(() => {
				dispatch(updateMessage(types.ORDER_SAVE_SUCCESS, 'The order has been saved'));
				dispatch({
					type: 'CLEAR_DATA'
				});
				dispatch(push('/stores'));
			});

		});
	}
}

export function reset() {
	return dispatch => {
		dispatch({
			type: 'RESET_STATES'
		});
	}
}