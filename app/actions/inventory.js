import request from 'axios';

export function updateStockUpdate(obj) {
	return dispatch => {
		return request['post']('/stockupdate', obj)
				.then((response) => {
					return response;
				})
				.catch((err) => {
					return err;
				});
	}
}