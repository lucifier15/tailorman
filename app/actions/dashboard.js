import * as types from 'types';
import {
	polyfill
} from 'es6-promise';
import {
	push
} from 'react-router-redux';
import request from 'axios';
import moment from 'moment';
import _ from 'lodash';
polyfill();

function makeRequest(method, data, api) {
	return request[method](api, data);
}

export function getQueryResults(query_string) {
	return dispatch => {
		makeRequest('post', {
			query_string : query_string
		}, '/dashboard/queryresult').then((response) => {
			if (response.status === 200) {
				dispatch({
					type: types.DASHBOARD_RESULTS_SUCCESS,
                    queryresult : response.data && response.data.data,
                    querystring : query_string
				});
			}
		}).catch((error) => {
			console.log("Error fetching workflow stages", error);
		});
	}
}
export function getDashboardQueryFilters(){
	return dispatch => {
		makeRequest('get', {}, '/dashboard/queryfilter').then((response) => {
			if (response.status === 200) {
				dispatch({
					type: types.DASHBOARD_FILTERS_SUCCESS,
					query_filters : response.data && response.data.data
                    // queryresult : response.data && response.data.data,
                    // querystring : query_string
				});
			}
		}).catch((error) => {
			console.log("Error fetching workflow stages", error);
		});
	}
}
export function setDashboardActiveKey(key){
	return {
		type: types.TOGGLE_DASHBOARD_ACCORDION,
		key
	}
}
export function getQuerySave(query_string,user_id,query_name) {
	return dispatch => {
		makeRequest('post', {
			query_string : query_string,
			user_id:user_id,
			query_name:query_name
		}, '/dashboard/querysave').then((response) => {
			if (response.status === 200) {
				dispatch({
					type: types.DASHBOARD_RESULTS_SUCCESS,
                    queryresult : response.data && response.data.data,
                    querystring : query_string
				});
			}
		}).catch((error) => {
			console.log("Error fetching workflow stages", error);
		});
	}
}

export function getUserDashboardQuerys(){
	return dispatch => {
		makeRequest('get', {}, '/dashboard/getuserqueries').then((response) => {
			if (response.status === 200) {
				console.log(response.data);
				dispatch({
					type: types.DASHBOARD_USER_QUERIES_SUCCESS,
					user_queries : response.data && response.data.data
                    // queryresult : response.data && response.data.data,
                    // querystring : query_string
				});
			}
		}).catch((error) => {
			console.log("Error fetching workflow stages", error);
		});
	}
}