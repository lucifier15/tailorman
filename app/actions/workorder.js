import * as types from 'types';
import {
	polyfill
} from 'es6-promise';
import {
	push
} from 'react-router-redux';
import request from 'axios';
import moment from 'moment';
import _ from 'lodash';
polyfill();

function makeRequest(method, data, api) {
	return request[method](api, data);
}

export function getWorkflowStages(user, workflow_id) {
	return dispatch => {
		makeRequest('post', {
			user_id: user.user_id,
			roles: user.roles.workflow,
			workflow_id: workflow_id
		}, '/list/workflowStage').then((response) => {
			if (response.status === 200) {
				const workflowStages = response.data.map((workflowStage) => {
					return {
						id: workflowStage.stage_id,
						code: workflowStage.code,
						name: workflowStage.code,
						order: workflowStage.order,
						workflow_id: workflowStage.workflow_id,
						can_skip: workflowStage.can_skip
					}
				});
				dispatch({
					type: types.POPULATE_WORKFLOW_STAGE_LIST,
					workflowStages
				});
			}
		}).catch((error) => {
			console.log("Error fetching workflow stages", error);
		});
	}
}

export function updateProfile() {
	return {
		type: types.WORKORDER_UPDATE_MEASUREMENTS
	}
}

export function saveQueryFields(query) {
	return {
		type: types.WORKORDER_SAVE_QUERY_FIELDS,
		query
	}
}
export function selectWorkOrder(stage_id) {
	return {
		type: types.SELECT_WORK_ORDER_STAGE,
		stage_id: stage_id
	}
}

export function selectWorkflow(workflow) {
	return {
		type: types.SELECT_WORKFLOW,
		workflow
	}
}

export function getWorkOrders(stage_id, from_date, to_date, customer, customer_id, order_id) {
	return dispatch => {
		makeRequest('post', {
			stage_ids: stage_id,
			from_date: from_date,
			customer: customer,
			to_date: to_date,
			customer_id,
			order_id
		}, '/workorders').then((response) => {
			if (response.status === 200) {
				const workorders = response.data;
				dispatch({
					type: types.POPULATE_WORK_ORDERS,
					workorders: _.reverse(_.sortBy(workorders, 'order_item_id'))
				})
			}
		}).catch((error) => {
			console.log("Error fetching work orders", error);
		})
	}
}

export function sortWorkOrders(type) {
	return {
		type: types.SORT_WORK_ORDERS,
		sortedBy: type
	}
}

export function setActiveKey(key) {
	return {
		type: types.TOGGLE_WORKORDER_ACCORDION,
		key
	}
}

function getOrderItemDetails(order_item_id) {
	return makeRequest('get', null, '/details/order/item?order_item_id=' + order_item_id).then((response) => {
		if (response.status === 200) {
			return response.data;
		}
	});
}

function getOrderDetailsRequest(order_id, details) {
	return makeRequest('get', null, `/details/order?order_id=${order_id}`).then(response => {
		if (response.status === 200) {
			details.order = response.data;
			return;
		}
	})
}
function getCustomerDetailsRequest(details) {
	return makeRequest('get', null, `/details/customer?customer_id=${details.order.customer_id}`).then(response => {
		if (response.status === 200) {
			details.customer = response.data;
			return;
		}
	})
}
function getOrderItemDetailsRequest(order_item_id, details) {
	return makeRequest('get', null, `/details/order/item?order_item_id=${order_item_id}`).then((response) => {
		if (response.status === 200) {
			let _sale = response.data;
			let orderItemDetails = Object.assign({}, details.order_item, _sale);
			details.order_item = {
				...orderItemDetails
			};
		}
	});
}
function order_Item_Profile_Id_values(details, is_print_sheet) {
	const _profile_id = details.order_item.profile_id;
	let url = `/profile/values?profile_id=${_profile_id}`;
	if (is_print_sheet)
		url += '&is_print=yes'
	return makeRequest('get', null, url).then((profileValues) => {
		details.order_item.profile = details.order_item.profile || {};
		details.order_item.profile.values = profileValues.data;
		details.order_item.profile.measurements = profileValues.data.map((value) => {
			return {
				id: value.measurement_type_id,
				measurement_type_id: value.measurement_type_id,
				value: value.value,
				type: value.measurement_type,
				measurement_type: value.measurement_type
			}
		});
		return;
	});
}
function order_Item_Profile_Id_Details(details) {
	return makeRequest('get', null, `/profiles?item_type_id=${details.order_item.item_type_id}&customer_id=${details.order.customer_id}`).then((profiles) => {
		profiles = profiles.data;
		const _profile_id = details.order_item.profile_id;
		var profile = _.find(profiles, {
			profile_id: parseInt(_profile_id)
		});
		if (profile)
			profile.name = profile.profile_name;
		profile = profile || {};
		let oldProfileDetails = details.order_item.profile || {};
		let newProfileDetails = Object.assign({}, oldProfileDetails, profile);
		details.order_item.profile = newProfileDetails;
	});
}
function getOrderItemImages(order_id, image_id) {
	return makeRequest('get', null, `/details/image?order_id=${order_id}&image_id=${image_id}`).then((response) => {
		if (response.status === 200) {
			let images = [];
			if (response.data && response.data.length > 0) {
				images = response.data.map((image) => {
					var convertImage = (image) => {
						let dataURL = '';
						image.forEach((charCode) => {
							dataURL += String.fromCharCode(charCode);
						})
						return dataURL;
					}
					return convertImage(image.image.data);
				});
			}
			return images;
		}
	});
}
function getOnlineOrderMeasurement(order_item_id, details) {
	return makeRequest('get', null, `/details/onlinemeasurement?order_item_id=${order_item_id}`).then(response => {
		if (response.status === 200) {
			details.onlinemeasurement = response.data;
		}
	});
}
function getOnlineOrderFabricDesign(order_item_id, details) {
	return makeRequest('get', null, `/details/onlinefabric?order_item_id=${order_item_id}`).then(response => {
		if (response.status === 200) {
			//details.onlinefabric = (response.data.length>0)?response.data[0].fabric_design : response.data;
			let onlinefabricData = (response.data.length > 0) ? response.data[0].fabric_design : response.data;
			let onlinefabricDetails = [];
			onlinefabricData && onlinefabricData.map((fabric_main) => {
				fabric_main.items.map((fabric) => {
					onlinefabricDetails.push({
						mainOptionName: fabric.mainOptionName,
						name: fabric.name
					})
				})
			})
			details.onlinefabric = onlinefabricDetails;
			return makeRequest('get', null, '/details/getstylebucket?order_item_id=' + order_item_id).then(response => {
				if (response.status === 200) {
					let bucketStyleArray = (response.data.length > 0) ? response.data[0].style : response.data;;
					bucketStyleArray && bucketStyleArray.map((bucket_main) => {
						bucket_main.items.map((fabric) => {
							let fabricIndex = _.findIndex(onlinefabricDetails, { mainOptionName: fabric.mainOptionName });
							if (fabricIndex == -1) {
								onlinefabricDetails.push({
									mainOptionName: fabric.mainOptionName,
									name: fabric.name
								})
							}
						})
					})
					details.onlinefabric = onlinefabricDetails;
					return;
				}
			})

		}
	});
}
function getOrderDetails(order_id, order_item_id, profile_id, type, is_print_sheet, customer_id, is_print_online = false) {
	var details = {
		order: {
			customer_id
		},
		order_item: {
			_profiles: []
		}
	};
	let promiseArray = [];
	if (type != 'pattern' && type != 'FGQC') {
		promiseArray.push(getOrderItemDetailsRequest(order_item_id, details));
	}
	if (is_print_sheet) {
		if (is_print_online) {
			promiseArray.push(getOnlineOrderMeasurement(order_item_id, details));
			promiseArray.push(getOnlineOrderFabricDesign(order_item_id, details));
		}
		promiseArray.push(getOrderDetailsRequest(order_id, details));
		promiseArray.push(getWorkOrderMeasurementProfile(order_item_id, 'pattern').then((patternProfile) => {
			details.order_item._profiles = details.order_item._profiles || [];
			details.order_item._profiles.push({
				profile: patternProfile,
				type: 'pattern'
			});
		}));
		promiseArray.push(getWorkOrderMeasurementProfile(order_item_id, 'FGQC').then((fgqcProfile) => {
			details.order_item._profiles = details.order_item._profiles || [];
			details.order_item._profiles.push({
				profile: fgqcProfile,
				type: 'FGQC'
			});
		}));
	}
	if (type === 'pattern' || type === 'FGQC') {
		promiseArray.push(getWorkOrderMeasurementProfile(order_item_id, type).then((profile) => {
			details.order_item.profile = profile;
			details.order_item._profiles = [{ profile, type }];
			return;
		}));
	}
	if (type === 'style' || is_print_sheet) {
		promiseArray.push(getFabricDesignDetails(order_item_id).then((design) => {
			details.order_item.design = design;
		}));
	}
	if (!is_print_sheet && type != 'style') {
		promiseArray.push(getOrderItemImages(order_id, types.IMAGE_TYPE_MEASUREMENT).then((images) => {
			details.images = images;
		}));
	}
	return Promise.all(promiseArray).then(() => {
		let innerPromiseArray = [];
		if (is_print_sheet) {
			innerPromiseArray.push(getCustomerDetailsRequest(details));
		}
		if (is_print_sheet || type === 'default') {
			innerPromiseArray.push(order_Item_Profile_Id_values(details, is_print_sheet));
			innerPromiseArray.push(order_Item_Profile_Id_Details(details));
		}
		return Promise.all(innerPromiseArray).then(() => {
			console.log(details);
			return details;
		});
	});
}
function getFabricDesignDetails(order_item_id) {
	return makeRequest('get', null, `/order/item/fabricDesign?order_item_id=${order_item_id}`).then((response) => {
		if (response.status === 200) {
			return response.data[0] || {};
		}
	})
}
export function getWorkOrderStyles(order_item_id, item_type_id) {
	var details = {
		order_item: {
			order_item_id,
			item_type_id
		}
	};
	return dispatch => {
		getFabricDesignDetails(order_item_id).then(saleDesign => {
			details.order_item.design = saleDesign;
			dispatch({
				type: types.POPULATE_ORDER_ITEM_DETAILS,
				details
			})
		});
	};
}
export function getWorkOrderMeasurementDetails(order_id, order_item_id, profile_id, type, is_print_sheet) {
	var details = {
		order_item: {
			order_item_id,
			item_type_id
		}
	};
	return dispatch => {
		getOrderItemWorkOrderMeasurementRequest().then(details => {

		});
	}
}
export function getWorkOrderDetails(order_id, order_item_id, profile_id, type, is_print_sheet, customer_id, is_print_online) {
	return dispatch => {
		getOrderDetails(parseInt(order_id), parseInt(order_item_id), profile_id, type, is_print_sheet, parseInt(customer_id), is_print_online).then((details) => {
			dispatch({
				type: types.POPULATE_ORDER_ITEM_DETAILS,
				details
			})
		})
	};
}
export function getWorkOrderItemDetails(order_item_id) {
	return dispatch => {
		var details = {};
		getOrderItemDetails(parseInt(order_item_id)).then((orderItem) => {
			orderItem.fiton_date = (orderItem.fit_on_date) ? moment(orderItem.fit_on_date).format('YYYY-MM-DD') : '';
			orderItem.delivery_date = (orderItem.delivery_date) ? moment(orderItem.delivery_date).format('YYYY-MM-DD') : '';
			details.order = orderItem;
			return makeRequest('get', null, '/details/list/order?order_id=' + orderItem.order_id).then((response) => {
				if (response.status === 200) {
					details.sales = response.data;
					dispatch({
						type: types.POPULATE_WORK_ORDER_ITEM_DETAILS,
						details
					})
				}
			});
		})
	};
}
export function toggleWorkOrderListItemMenu(isOpen, order_item_id, user, workflow_id) {
	return dispatch => {
		dispatch({
			type: types.POPULATE_ORDER_ITEM_DETAILS,
			details: {}
		})
		if (isOpen == true) {
			makeRequest('post', {
				order_item_id,
				user_id: user.user_id,
				roles: user.roles.workflow,
				workflow_id: workflow_id
			}, '/workflow/stages').then((response) => {
				if (response.status === 200) {
					dispatch({
						type: types.POPULATE_APPLICABLE_STAGE_LIST,
						stageList: response.data
					});

					dispatch({
						type: types.TOGGLE_WORKORDER_LIST_ITEM_MENU,
						isOpen
					});

					dispatch({
						type: types.SELECT_WORK_ORDER_ITEM,
						order_item_id
					})
				}
			});
		} else {
			dispatch({
				type: types.TOGGLE_WORKORDER_LIST_ITEM_MENU,
				isOpen
			});

		}
	}
}

export function updateWorkOrderItemStage(workorder, stage, comment, user_id, current_stage) {
	return makeRequest('post', {
		workflow_id: stage.workflow_id,
		workflow_stage_id: stage.stage_id,
		order_item_id: workorder.order_item_id,
		profile_id: workorder.profile_id,
		user_id: user_id,
		comment
	}, '/workflow/stage').then((response) => {
		if (response.status === 200)
			return response.data;
		else {
			return {
				status: false
			}
		}
	});
}

export function saveFabricDesign(order_item_id, fabric_design, comment, isFinal) {
	return dispatch => {
		if (isFinal) {
			makeRequest('post', {
				fabric_design,
				comment
			}, '/order/item/fabricDesign?order_item_id=' + order_item_id).then(fabricDesignResponse => {
				if (!fabricDesignResponse.status === 200)
					console.log("Something went wrong when saving Fabric Design");
			});
		}
		dispatch({
			type: types.UPDATE_WORK_ORDER_ITEM_FABRIC_DESIGN,
			fabric_design,
			comment
		})
	}
}

export function getEigthFraction(number) {
	const fraction = Number(Math.abs(number) % 1);
	let base = '';
	if (Math.floor(Math.abs(number)) === 0 || Math.ceil(Math.abs(number)) === 0) {
		if (number < 0)
			base = '-';
	} else if (Math.floor(number) > 0)
		base = Math.floor(number) + "  ";
	else if (Math.floor(number) < 0)
		base = "-" + Math.ceil(number) + "  ";
	if (fraction !== 0) {
		if (fraction > 0.5) {
			if (fraction === 0.75) {
				return base + " 3/4";
			} else if (fraction > 0.75) {
				return base + " 7/8";
			} else {
				return base + " 5/8";
			}
		} else if (fraction < 0.5) {
			if (fraction < 0.25) {
				return base + " 1/8";
			} else if (fraction == 0.25) {
				return base + " 1/4";
			} else {
				return base + " 3/8";
			}
		} else {
			return base + " 1/2";
		}
	} else {
		return base;
	}
}

function getWorkOrderMeasurementProfile(order_item_id, type) {
	let profile = {};
	return makeRequest('get', null, '/workorder/measurements?order_item_id=' + order_item_id + "&type=" + type).then((response) => {
		if (response.status == 200) {
			profile.measurement_source_id = response.data.measurement_source_id;
			profile.profile_id = response.data.profile_id;
			profile.name = profile.profile_name = response.data.profile_name;
			profile.comment = response.data.comment;
			return profile;
		} else {
			return false;
		}
	}).then((profile) => {
		if (profile) {
			return makeRequest('get', null, '/profile/values?profile_id=' + profile.profile_id).then((response) => {
				if (response.status === 200) {
					profile.measurements = profile.values = response.data.map((value) => {
						return {
							id: value.measurement_type_id,
							measurement_type_id: value.measurement_type_id,
							value: value.value,
							type: value.measurement_type,
							measurement_type: value.measurement_type,
							code: value.measurement_type
						}
					});
					return profile;
				}
			});
		} else {
			console.log("We couldn't get the profile");
		}
	}).catch((err) => {
		console.log("Something went wrong while fetching the profile or measurements", err);
	})
}
export function updateWorkOrderItemEntry(value, field) {
	return {
		type: types.WORK_ORDER_UPDATE_SALE_ITEM_ENTRY,
		payload: {
			value,
			field
		}
	}
}
export function saveWorkOrderItem(order_item, saleItems, workorder) {
	return dispatch => {
		var url = '/order/item/update';
		var promiseArray = [];
		saleItems.forEach((item, index) => {
			/*
					Representing the Upcharges in the Array Instead of Object
			*/
			order_item.upcharges = (order_item.upcharge && _.isArray(order_item.upcharge)) ? order_item.upcharge : []; 
			if (order_item.order_item_id == item.order_item_id) {
				promiseArray.push(makeRequest('post', order_item, url));
			} else if (order_item.item_type_id == item.item_type_id) {
				item.comment = order_item.comment;
				item.style = item.style_id || item.sku_id;
				promiseArray.push(makeRequest('post', item, url));
			}
		});
		Promise.all(promiseArray).then(() => {
			dispatch(push('/workorder'));
			/**
			 * Reloading the Work Orders with the Already Existing Query
			 */
			let query = workorder.query;
			if (query) {
				dispatch(getWorkOrders(...query));
			}
		});
	}
}