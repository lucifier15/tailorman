import * as types from 'types';
import {
	polyfill
} from 'es6-promise';
import {
	push
} from 'react-router-redux';
import request from 'axios';
import { updateMessage } from './order';
polyfill();

function makeRequest(method, data, api) {
	return request[method](api, data);
}

function updateCustomerValues(customer) {
	return {
		type: types.ORDER_CUSTOMER_UPDATE,
		customer
	}
}


export function selectCustomer(customer) {
	return dispatch => {
		if (customer.customer_id > 0) {
			dispatch(updateCustomerValues(customer));
			dispatch({
			type: types.CLEAR_ORDER_DATA
		})
		dispatch(push('/customer/actions'));
		} else {
			const _customer = {
				mobile: customer.phone,
				name: customer.name,
				source_id: 1,
				email: customer.email
			}
			makeRequest('post', _customer, '/order/checkcustomer')
				.then((response) => {
					if (response.status === 200) {
						customer.customer_id = response.data.customer_id;
						dispatch(push('/customer/actions'));
						dispatch(updateCustomerValues(customer));
					} else {
						console.log("Something went wrong with the request", err);
					}
				})
				.catch(error => {
					console.log("Something went wrong with the request", error);
					dispatch(updateMessage('ALREADY_EXIST_CONDITION', 'Customer already exist!'))
				});
		}
		

	}
}

export function saveCustomerPhone(phone) {
	return {
		type: types.ORDER_CUSTOMER_UPDATE_PHONE,
		phone
	}
}