import * as types from 'types';
import {
	polyfill
} from 'es6-promise';
import request from 'axios';
import moment from 'moment';

function fetchAPI(url, method = 'get', data = null) {
	return request[method](url, data).then(response => {
		if (response.status == 200)
			return response.data;
		else
			console.log("Could not fetch API", url);
	}).catch(err => {
		console.log(err);
	});
}

function postAPI(url, method = 'post', data) {
	return request[method](url, data).then(response => {
		if (response.status == 200)
			return response.data;
		else
			console.log("Could not fetch API", url);
	}).catch(err => {
		console.log(err);
	});
}

function fetchMeasurementFieldList(item_type_id, measurement_type_id) {
	return dispatch => {
		fetchAPI('/list/measurementField?item_type_id=' + item_type_id + "&measurement_type_id=" + measurement_type_id).then(response => {
			dispatch({
				type: types.FETCH_LIST_MEASUREMENT_FIELDS,
				measurement_fields: response
			})
		});
	}
}

function fetchFabricDesignFieldList(item_type_id) {
	return dispatch => {
		fetchAPI('/list/fabricDesignField?item_type=' + item_type_id).then(response => {
			dispatch({
				type: types.FETCH_LIST_FABRIC_DESIGN_FIELDS,
				fabric_design_fields: response
			})
		})
	}
}

function fetchMeasurementTypeList() {
	return dispatch => {
		fetchAPI('/list/measurementType').then(response => {
			const measurement_types = response.map((measurement_type) => {
				return {
					id: measurement_type.source_id,
					name: measurement_type.descr
				}
			})
			dispatch({
				type: types.FETCH_LIST_MEASUREMENT_TYPES,
				measurement_types: measurement_types
			})
		})
	}
}

function fetchItemTypeList() {
	return dispatch => {
		fetchAPI('/list/itemType').then(response => {
			const item_types = response.map((item_type) => {
				item_type.isMTM = (item_type.mtm_flag == 'Y') ? true : false;
				return item_type;
			});
			dispatch({
				type: types.FETCH_LIST_ITEM_TYPES,
				item_types
			})
		})
	}
}

function fetchStyles() {
	return dispatch => {
		fetchAPI('/list/style').then(response => {
			const styles = response.map((style) => {
				return {
					name: style.code,
					id: style.style_id
				}
			});
			dispatch({
				type: types.FETCH_LIST_STYLES,
				styles
			})
		})
	}
}

function fetchPriorityList() {
	return dispatch => {
		fetchAPI('/list/priority').then(response => {
			const priorities = response.map((priority) => {
				return {
					id: priority.priority_id,
					code: priority.code,
					name: priority.descr,
					delivery_upcharge: priority.delivery_upcharge
				}
			});
			dispatch({
				type: types.FETCH_LIST_PRIORITIES,
				priorities
			})
		});
	};
}

function fetchPaymentTypeList() {
	return dispatch => {
		fetchAPI('/list/paymentType').then(response => {
			const payment_types = response.map((payment_type) => {
				return {
					id: payment_type.payment_type_id,
					code: payment_type.code,
					name: payment_type.descr
				}
			});
			dispatch({
				type: types.FETCH_LIST_PAYMENT_TYPES,
				payment_types
			});
		});
	};
}

function fetchFinishTypeList() {
	return dispatch => {
		fetchAPI('/list/finishType').then(response => {
			const finish_types = response.map((finish_type) => {
				return {
					id: finish_type.finish_type_id,
					name: finish_type.descr,
					code: finish_type.code
				}
			});
			dispatch({
				type: types.FETCH_LIST_FINISH_TYPES,
				finish_types
			})
		})
	}
}

function fetchTailors() {
	return dispatch => {
		fetchAPI('/list/tailor').then(response => {
			const tailors = response.map((tailor) => {
				return {
					id: tailor.tailor_id,
					mobile: tailor.mobile,
					name: tailor.tailor_name
				}
			})
			dispatch({
				type: types.FETCH_LIST_TAILORS,
				tailors
			});
		});
	};
}

function fetchSalesMen() {
	return dispatch => {
		fetchAPI('/list/salesman').then(response => {
			const salesmen = response.map((salesman) => {
				return {
					id: salesman.sales_man_id,
					name: salesman.sales_man_name
				}
			});
			dispatch({
				type: types.FETCH_LIST_SALESMEN,
				salesmen
			});
		})
	}
}

function fetchCustomerList(keyword) {
	return dispatch => {
		fetchAPI('/list/customer?keyword=' + keyword).then(response => {
			const customers = response.map((customer) => {
				customer.id = customer.customer_id;
				customer.phone = customer.mobile;
				customer.gender = customer.gender == 'M' ? 'male' : 'female';
				customer.email = customer.email;
				customer.display_name = customer.email + " " + customer.name + " " + customer.phone;
				return customer;
			});
			dispatch({
				type: types.FETCH_LIST_CUSTOMERS,
				customers
			})
		})
	}
}

function clearCustomerList() {
	return {
		type: types.CLEAR_LIST_CUSTOMERS
	};
}

function fetchFabricList(item_type_id, keyword) {
	return dispatch => {
		fetchAPI('/list/fabric?item_type_id=' + item_type_id + "&keyword=" + keyword).then(response => {
			const fabrics = response.map((fabric) => {
				fabric.id = fabric.fabric_id;
				fabric.sku = {
					id: fabric.fabric_id,
					name: fabric.product_sku_code
				};
				return fabric;
			})
			dispatch({
				type: types.FETCH_LIST_FABRICS,
				fabrics
			});
		})
	}
}


function fetchCameras() {
	return dispatch => {
		navigator.mediaDevices.enumerateDevices()
			.then((devices) => {
				var cameras = devices.filter(device => {
					return device.kind === 'videoinput'
				});
				dispatch({
					type: types.FETCH_LIST_CAMERAS,
					cameras
				})
			});
	}
}

function fetchCustomerSourceList() {
	return dispatch => {
		fetchAPI('/list/customerSource').then(response => {
			const customerSources = response.map((source) => {
				return {
					id: source.source_id,
					code: source.code,
					name: source.descr
				}
			})
			dispatch({
				type: types.FETCH_LIST_CUSTOMER_SOURCES,
				customerSources
			})
		})
	}
}

function fetchOccasions() {
	return dispatch => {
		fetchAPI('/list/occasion').then(response => {
			const occasions = response.map((occasion) => {
				return {
					id: occasion.occation_id,
					name: occasion.descr
				}
			});
			dispatch({
				type: types.FETCH_LIST_OCCASIONS,
				occasions
			})
		});

	}
}

function fetchProfiles(item_type_id, customer_id) {
	return dispatch => {
		fetchAPI('/profiles?item_type_id=' + item_type_id + "&customer_id=" + customer_id).then(response => {
			const profiles = response.map((profile) => {
				return {
					id: profile.profile_id,
					name: profile.profile_name,
					measurement_source_id: profile.measurement_source_id,
					comment: profile.comment,
					profile_id: profile.profile_id
				}
			});
			dispatch({
				type: types.FETCH_CUSTOMER_PROFILE_LIST,
				profiles
			})
		})
	}
}

function fetchOrders(customer_id) {
	return dispatch => {
		fetchAPI('/orders?customer_id=' + customer_id).then(response => {
			var customerOrders = response.map((order) => {
				order.id = order.order_id;
				order.name = [order.order_id, order.display_name].join(' -- ');
				order.order_date = moment(order.order_date).format('YYYY-MM-DD');
				//TODO: Fix these typos in the procs
				order.occasion_date = order.occation_date ? moment(order.occation_date).format('YYYY-MM-DD') : '';
				order.benificiary_phone = order.benficiary_mobile;
				order.benificiary_name = order.benficiary_name;
				order.salesman_id = order.sales_man_id;
				return order;
			});
			dispatch({
				type: types.FETCH_CUSTOMER_ORDER_LIST,
				orders: customerOrders
			})
		})
	}
}

function fetchWorkflows() {
	return dispatch => {
		fetchAPI('/list/workflow').then(response => {
			dispatch({
				type: types.FETCH_LIST_WORKFLOW,
				workflows: response
			});
		});
	}
}

export function fetchList(type, data = null) {
	return dispatch => {
		switch (type) {
			case 'priority':
				dispatch(fetchPriorityList());
				break;
			case 'payment_type':
				dispatch(fetchPaymentTypeList());
				break;
			case 'finish_type':
				dispatch(fetchFinishTypeList());
				break;
			case 'tailor':
				dispatch(fetchTailors());
				break;
			case 'salesman':
				dispatch(fetchSalesMen());
				break;
			case 'style':
				dispatch(fetchStyles());
				break;
			case 'item_type':
				dispatch(fetchItemTypeList());
				break;
			case 'measurement_type':
				dispatch(fetchMeasurementTypeList());
				break;
			case 'measurement_field':
				if (!data.clear)
					dispatch(fetchMeasurementFieldList(data.item_type_id, data.measurement_type_id));
				else
					dispatch({
						type: types.CLEAR_LIST_MEASUREMENT_FIELDS
					})
				break;
			case 'fabric_design_field':
				dispatch(fetchFabricDesignFieldList(data.item_type_id));
				break;
			case 'fabric':
				dispatch(fetchFabricList(data.item_type_id, data.keyword));
				break;
			case 'customer':
				dispatch(fetchCustomerList(data.keyword));
				break;
			case 'cameras':
				dispatch(fetchCameras());
				break;
			case 'customer_source':
				dispatch(fetchCustomerSourceList());
				break;
			case 'profile':
				dispatch(fetchProfiles(data.item_type_id, data.customer_id));
				break;
			case 'orders':
				dispatch(fetchOrders(data.customer_id));
				break;
			case 'occasion':
				dispatch(fetchOccasions());
				break;
			case 'workflow':
				dispatch(fetchWorkflows());
				break;

		}
	}

}

export function clearList(type) {
	return dispatch => {
		switch (type) {
			case 'customer':
				dispatch(clearCustomerList());
				break;
			case 'fabric':
				return {
					type: types.CLEAR_LIST_FABRICS
				}
			case 'measurement_field':
				return {
					type: types.CLEAR_LIST_MEASUREMENT_FIELDS
				}
			default:
				break;
		}
	}
}

export function searchRTW(store_id,keyword,size,fit) {
	return dispatch => {
		fetchAPI('/list/searchRTW?keyword='+keyword+'&store_id='+store_id+'&size='+size+'&fit='+fit).then(response => {
			
			const rtwsku = response.map((rtwsku) => {
				return {
					fabric_id:rtwsku.fabric_id,
					id: rtwsku.fabric_sku_code,
					name: rtwsku.name,
					product_sku_code:rtwsku.product_sku_code,
					inventory_count:rtwsku.inventory_count,
					mrp:rtwsku.mrp,
					size:rtwsku.size,
					fit:rtwsku.fit,
					item_type_id:rtwsku.item_type_id
				}
			});
			dispatch({
				type: types.FETCH_RTW_SKU,
				rtwsku
			});
		})
	}
}

export function fetchSizeList(){
	return dispatch => {
		fetchAPI('/list/fetchSize').then(response=>{
			const rtwsku_size = response.map((rtwsku_size) => {
				return {
					name:rtwsku_size.name
				}
			});
			dispatch({
				type: types.FETCH_RTW_SIZE,
				rtwsku_size
			});
		});
	}
}

export function fetchFitList(){
	return dispatch => {
		fetchAPI('/list/fetchFit').then(response=>{
			const rtwsku_fit = response.map((rtwsku_fit) => {
				return {
					name:rtwsku_fit.name
				}
			});
			dispatch({
				type: types.FETCH_RTW_FIT,
				rtwsku_fit
			});
		});
	}
}

export function transferInventory(formValues){
	return dispatch => {
		postAPI('/list/inventoryTransfer','post',formValues).then(response=>{

			console.log("Successfully inserted details");
		});
	}
}