/* eslint consistent-return: 0, no-else-return: 0*/
import * as types from 'types';
import thunk from 'redux-thunk'

export function toggleMenu(current) {
	return {
            type: types.TOGGLE_HAMBURGER_MENU,
            status: !current
        };
    
	
}