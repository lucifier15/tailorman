import React from 'react';
import { Route, IndexRoute, IndexRedirect } from 'react-router';
import App from 'containers/App';
import LoginOrRegister from 'containers/LoginOrRegister';
import Order from 'components/Order';
import Customer from 'containers/Customer';
import OrderCustomer from 'containers/order/customer';
import OrderDetails from 'containers/order/details';
import OrderSales from 'containers/order/sales';
import OrderPayment from 'containers/order/OrderPayment';
import Stores from 'containers/Stores';
import Logout from 'containers/Logout';
import MeasurementsAndStyles from 'containers/order/MeasurementsAndStyles';
import OrderAlteration from 'containers/order/OrderAlteration';
import OrderSaleItem from 'containers/order/OrderSaleItem';
import CustomerActions from 'containers/CustomerActions';
import OrderHistory from 'containers/order/history';
import WorkOrder from 'containers/workorder';
import Page403 from 'containers/Page403';
import Landing from 'containers/Landing';
import WorkOrderPrint from 'containers/WorkOrderPrint';
import InvoicePrint from 'containers/InvoicePrint';
import WorkOrderMeasurements from 'containers/WorkOrderMeasurements';
import WorkOrderStyles from 'containers/WorkOrderStyles';
import Reset from 'containers/Reset';

import WorkOrderDetails from 'containers/WorkOrderUpdateDetails'
import WorkOrderOnlinePrint from 'containers/WorkOrderOnlinePrint';
import Dashboard from 'containers/Dashboard';
import Inventory from 'containers/Inventory';

/*
 * @param {Redux Store}
 * We require store as an argument here because we wish to get
 * state from the store after it has been authenticated.
 */

const workorder_paths = ['/workorder'];
const storeops_paths = ['/stores', '/order', '/customer', '/customer/actions', '/order/alteration', '/order/customer', '/order/sales', 'order/details', '/order/payment', '/order/history'];
const has_workorder_role = (roles) => {
	if (!roles)
		return false;
	if (roles.length > 0)
		return true;
	return false;
}

const has_storeops_role = (roles) => {
	if (!roles)
		return false;
	if (roles.length > 0)
		return true;
	return false;
}

const shouldAllowAccess = (nextState, state) => {
	if (state.user) {
		if (workorder_paths.indexOf(nextState.location.pathname) > -1 && !has_workorder_role(state.user.user.roles.workflow))
			return false;
		else if (storeops_paths.indexOf(nextState.location.pathname) > -1 && !has_storeops_role(state.user.user.roles.store))
			return false;
	}
	return true;
}

const landing = (nextState, state) => {
	const store_ops_access = has_storeops_role(state.user.user.roles.store);
	const work_order_access = has_workorder_role(state.user.user.roles.workflow);
	if (store_ops_access && work_order_access) {
		return '/landing';
	} else if (store_ops_access) {
		return '/stores';
	} else if (work_order_access) {
		return '/workorder';
	}
}

export default (store) => {
	const requireAuth = (nextState, replace, callback) => {
		const { user: { authenticated } } = store.getState();
		if (!authenticated) {
			replace({
				pathname: '/login',
				state: { nextPathname: nextState.location.pathname }
			});
		} else if (!shouldAllowAccess(nextState, store.getState())) {
			replace({
				pathname: '/page403',
				state: { nextPathname: nextState.location.pathname }
			});
		}
		callback();
	};

	const redirectAuth = (nextState, replace, callback) => {
		const { user: { authenticated } } = store.getState();
		if (authenticated) {
			replace({
				pathname: landing(nextState, store.getState())
			});
		}
		callback();
	};
	return (
		<Route path="/" component={App}>
			<IndexRedirect to="login" />
			<Route path="login" component={LoginOrRegister} onEnter={redirectAuth} />
			<Route path="invoice/customer/:customer_id/order/:order_id" component={InvoicePrint} />
			<Route path="order" component={Order} onEnter={requireAuth} />
			<Route path="customer" component={Customer} onEnter={requireAuth} />
			<Route path="customer/actions" component={CustomerActions} onEnter={requireAuth} />
			<Route path="order/customer" component={OrderCustomer} onEnter={requireAuth} />
			<Route path="order/sales" component={OrderSales} onEnter={requireAuth} />
			<Route path="order/sales/:id/type/:item_type_id" component={OrderSaleItem} onEnter={requireAuth} />
			<Route path="order/details" component={OrderDetails} onEnter={requireAuth} />
			<Route path="order/payment" component={OrderPayment} onEnter={requireAuth} />
			<Route path="order/history" component={OrderHistory} onEnter={requireAuth} />
			<Route path="order/alteration" component={OrderAlteration} onEnter={requireAuth} />
			<Route path="order/sales/:id/details" components={MeasurementsAndStyles} onEnter={requireAuth} />
			<Route path="/stores" component={Stores} onEnter={requireAuth} />
			<Route path="/reset" component={Logout} onEnter={requireAuth} />
			<Route path="/clear" component={Reset} onEnter={requireAuth} />
			<Route path="/page403" component={Page403} />
			<Route path="/landing" component={Landing} onEnter={requireAuth} />
			<Route path="/dashboard" component={Dashboard} onEnter={requireAuth} />
			<Route path="/workorder" component={WorkOrder} onEnter={requireAuth} />
			<Route path="/workorder/order/:order_id/item/:order_item_id/profile/:profile_id/print" component={WorkOrderPrint} onEnter={requireAuth} />
			<Route path="/workorder/ordernew/:order_id/item/:order_item_id/profile/:profile_id/print" component={WorkOrderOnlinePrint} onEnter={requireAuth} />
			<Route path="/workorder/update/:order_item_id/measurements/:type" component={WorkOrderMeasurements} onEnter={requireAuth} />
			<Route path="/workorder/update/:order_item_id/style" component={WorkOrderStyles} onEnter={requireAuth} />
			<Route path="/workorder/update/:order_item_id/details" component={WorkOrderDetails} onEnter={requireAuth} />
			<Route path="/inventory" component={Inventory} onEnter={requireAuth} />
		</Route>
	);
};
