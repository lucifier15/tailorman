import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/measurementForm';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import cameraicon from 'images/camera.png';

const cx = classNames.bind(styles);

export default class ImageCapture extends Component {
	constructor(props) {
		super(props);

		this.cameraCapture = this.cameraCapture.bind(this);
	}

	cameraCapture(e) {
		const self = this;

		if(e.target.files && e.target.files[0]){
			var file = e.target.files[0];

			//verifying the file type
			//file type -> only image accepted
			if(file.type.match('image.*')){
				console.log("will get signed request!");
				this.props.addImage(file);
			}else{
				console.log("Wrong file type! file type must be png, jpg or jpeg!");
			}
		}
	}

	render() {
		return (	
			<div className={cx('photo')}>
				<label htmlFor="photo"><img src={cameraicon} /></label>
				<input type="file" id="photo" accept="image/*" capture="camera" onChange={this.cameraCapture}/>
			</div>
		);
	}
}

ImageCapture.propTypes = {
	addImage: PropTypes.func
};