import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/workorder';

const cx = classNames.bind(styles);

export default class QueryResult extends Component {
    constructor(props) {
        super(props);
    }
    renderQueryResults() {
        const self = this;
        if (this.props.queryresult && this.props.queryresult.length > 0) {
            const queryresultElements = this.props.queryresult.map((query_result, index) => {
                return <div className={cx('workorder')} key={index}>
                    {this.props.columns.map((_column, index) => {
                        let style_object = {
                            'sorted': false
                        };
                        if (_column.column_width) {
                            style_object[_column.column_width] = true;
                        }
                        return <div key = {`index-${query_result.order_item_id}-${_column.dataIndex}`} className={cx(style_object)}>{query_result[_column.dataIndex]}</div>
                    })}
                </div>
            });
            return (
                <div className={cx('workorders')}>
                    <div className={cx('workorder')}>
                        {this.props.columns.map((_column, index) => {
                            let style_object = {
                                'head': true,
                                'sorted': false
                            };
                            if (_column.column_width) {
                                style_object[_column.column_width] = true;
                            }
                            return <div key = {index} className={cx(style_object)}>{_column.displayName}</div>
                        })}
                    </div>
                    {queryresultElements}
                </div>
            )
        } else {
            return (
                <h3> No Query Results Found </h3>
            )
        }
    }
    render() {
        let { queryresult } = this.props;
        return (
            <div>
                {this.renderQueryResults()}
            </div>
        );
    }
}
QueryResult.propTypes = {
};