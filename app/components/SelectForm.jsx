import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/selectForm';
import ReactDOM from 'react-dom';
import { fetchList } from '../actions/list';
import Select from 'react-select';
const cx = classNames.bind(styles);
const RenderOptions = React.createClass({
	propTypes: {
		children: React.PropTypes.node,
		className: React.PropTypes.string,
		isDisabled: React.PropTypes.bool,
		isFocused: React.PropTypes.bool,
		isSelected: React.PropTypes.bool,
		onFocus: React.PropTypes.func,
		onSelect: React.PropTypes.func,
		option: React.PropTypes.object.isRequired,
	},
	handleMouseDown(event) {
		event.preventDefault();
		event.stopPropagation();
		this.props.onSelect(this.props.option, event);
	},
	handleMouseEnter(event) {
		this.props.onFocus(this.props.option, event);
	},
	handleMouseMove(event) {
		if (this.props.isFocused) return;
		this.props.onFocus(this.props.option, event);
	},
	render() {
		return (
			<div className={this.props.className}
				onMouseDown={this.handleMouseDown}
				onMouseEnter={this.handleMouseEnter}
				onMouseMove={this.handleMouseMove}
				title={this.props.option.title}>
				<span className={cx('select-main-label')}>{this.props.option.name}</span>
				<span className={cx('select-email')}>{this.props.option.email}</span>
				<span className={cx('select-phone')}>{this.props.option.phone}</span>
			</div>
		);
	}
});

export default class SelectForm extends Component {
	constructor(props) {
		super(props);
		this.getOptions = this.getOptions.bind(this);
		this.saveValue = this.saveValue.bind(this);

	}

	saveValue(value) {
		this.props.save(value, this.props.rel);
	}

	getOptions() {
		if (this.props.options && this.props.options.length > 0) {
			return this.props.options.map((listItem) => {
				return {
					value: listItem.id,
					label: listItem.display_name || listItem.name,
					email: listItem.email,
					phone: listItem.phone,
					name: listItem.name
				}
			});
		}

	}

	render() {
		return (
			<div className={cx('selectFormContainer')} >
				<Select value={this.props.value} options={this.getOptions()} onChange={this.saveValue} optionComponent={RenderOptions} disabled={this.props.disabled} />
			</div>
		);
	}
}

SelectForm.propTypes = {
	save: PropTypes.func,
	rel: PropTypes.string,
	options: PropTypes.array,
	disabled: PropTypes.bool
};