import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import clear from 'images/clear.png';
import ReactDOM from 'react-dom';
import styles from 'css/components/workorder';
const cx = classNames.bind(styles);
import SelectForm from 'components/SelectForm';
import SQLParser from 'sql-parser-mistic/browser/sql-parser';
import _ from 'lodash';

const initializeQueryBuilder = (element, queryfilters) => {
    const filters = queryfilters;
    $(element).queryBuilder({ filters });
}
export default class QueryBuilder extends Component {
    constructor(props) {
        super(props);
        this.search = this.search.bind(this);
        this.save = this.save.bind(this);
        this.clear = this.clear.bind(this);
        this.updateQueryRules = this.updateQueryRules.bind(this);
        window.SQLParser = SQLParser;
        this.state = {
            selectedquery: ""
        }
    }
    search() {
        const rules = $(this.refs.queryBuilder).queryBuilder('getSQL', false);
        this.props.completeQueryString(rules);
    }
    save() {
        const rules = $(this.refs.queryBuilder).queryBuilder('getSQL', false);
        const query_name = ReactDOM.findDOMNode(this.refs.keyword).value;
        this.props.completeQueryStringSave(rules, query_name);
    }
    clear() {
        ReactDOM.findDOMNode(this.refs.keyword).value = '';
    }
    componentDidMount() {
        const element = this.refs.queryBuilder;
        initializeQueryBuilder(element, this.props.queryfilters);
    }
    componentWillUnmount() {
        $(this.refs.queryBuilder).queryBuilder('destroy');
    }
    updateQueryRules(_record) {
        let value;
        if (_record) {
            value = _record.value;
        }
        this.setState({
            selectedquery: value
        });
        let newQueryRule = _.find(this.props.userqueries, { id: value });
        if (newQueryRule) {
            $(this.refs.queryBuilder).queryBuilder('setRulesFromSQL', newQueryRule.query);
        } else {
            $(this.refs.queryBuilder).queryBuilder('reset');
        }
        this.forceUpdate();
    }
    shouldComponentUpdate() {
        return false;
    }
    render() {
        let saveText = '';
        let saveButton = '';
        if (!this.state.selectedquery) {
            saveText = (<div>
                <input className={cx('input-group input-query-css')} type="text" ref="keyword" placeholder='query name' />
            </div>);
            saveButton = <button className={cx('action button-query-css')} onClick={this.save}>Save</button>;
        }
        return (
            <div>
                <div className={cx('input-group')}>
                    <label htmlFor="select_order">Existing Query</label>
                    <SelectForm type="order" rel="order" options={this.props.userqueries} save={this.updateQueryRules} value={this.state.selectedquery} />
                </div>
                <div id='query-builder' ref='queryBuilder' />
                {saveText}
                {saveButton}
                <button className={cx('action button-query-css')} onClick={this.search}>Search</button>
            </div>
        );
    }
}
QueryBuilder.propTypes = {
};