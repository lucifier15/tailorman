import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/multipleChoice';
import ReactDOM from 'react-dom';
const cx = classNames.bind(styles);

export default class MultipleChoice extends Component {
	constructor(props) {
		super(props);
		this.getOptions = this.getOptions.bind(this);
		this.selectOption = this.selectOption.bind(this);
		this.isSelected = this.isSelected.bind(this);
		this.getValue = this.getValue.bind(this);

		this.selectedOptions = [];
	}

	selectOption(e) {
		const selectedOption = e;

		const { isMultiple, rel} = this.props;
		
		let selectedIndex = this.selectedOptions.indexOf(selectedOption);

		if (selectedIndex > -1) {
			this.selectedOptions.splice(selectedIndex, 1);
		} else {
			if (isMultiple)
				this.selectedOptions.push(selectedOption);
			else
				this.selectedOptions = [selectedOption];
		}

		this.props.save(this.selectedOptions, rel, isMultiple);
	}

	isSelected(option) {
		if (this.selectedOptions.length > 0) {
			return this.selectedOptions.indexOf(option) > -1;
		} else {
			return this.props.selected == option;
		}
	}

	getValue() {
		if (this.selectedOptions.length > 0)
			return this.selectedOptions;
		else
			return false;
	}

	getOptions() {
		const { options, disabled } = this.props;

		return options.map((option, index) => {
			let className = cx({
				selected: this.isSelected(option)
			});

			if(disabled) {				
				className = 'disabled';
				this.selectedOptions = [];
			}

			return <li 
						className={className} 
						onClick={this.selectOption.bind(this, option)} 
						key={index} 
						rel={option}
					>
						{option.split("_").join(" ")}
					</li>
		});
	}

	render() {
		return (
			<ul className={cx('optionsContainer')}>
				{this.getOptions()}
			</ul>
		);
	}
}

MultipleChoice.propTypes = {
	isMultiple: PropTypes.bool,
	options: PropTypes.array,
	save: PropTypes.func
};