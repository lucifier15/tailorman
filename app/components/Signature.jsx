import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/signature';
import ReactDOM from 'react-dom';
import SignaturePad from 'react-signature-pad';

const cx = classNames.bind(styles);


export default class Signature extends Component {
	constructor(props) {
		super(props);
		this.save = this.save.bind(this);
		this.close = this.close.bind(this);
		this.clear = this.clear.bind(this);
	}

	componentDidMount() {
		if (this.props.default && this.props.default.length > 0) {
			const signature = ReactDOM.findDOMNode(this.refs.signature);
			signature.fromDataURL(this.props.default);
		}
	}

	save() {
		const signature = this.refs.signature;
		if (!signature.isEmpty())
			this.props.save(signature.toDataURL())
		this.props.close();
	}

	close() {
		this.props.close();
	}

	clear() {
		const signature = this.refs.signature;
		signature.clear();

	}
	render() {
		return (
			<div className={cx('')} >
				<div className={cx('canvas-container')}>
					<SignaturePad clearButton="false" ref="signature" />
					<div className={cx('button-container')}>
						<span className={cx('disclaimer')}>By signing this, you are allowing Tailorman to send you regular updates about Tailorman via sms and email.</span>
						<button onClick={this.close} className={cx('action', 'secondary')}>Cancel</button>
						<button onClick={this.clear} className={cx('action', 'secondary')}>Clear</button>
						<button onClick={this.save} className={cx('action', 'primary')}>Save</button>
					</div>
				</div>

			</div>
		);
	}
}

Signature.propTypes = {
	save: PropTypes.func,
	rel: PropTypes.string,
	close: PropTypes.func,
	default: PropTypes.string
};