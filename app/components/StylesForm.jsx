import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/stylesForm';
import ReactDOM from 'react-dom';
import { fetchList } from '../actions/list';
import MultipleChoice from './MultipleChoice';
import Select from 'react-select';
import _ from 'lodash';
import Collapse, { Panel } from 'rc-collapse';
const cx = classNames.bind(styles);
import { UPCHARGE_UNIT_VALUE, UPCHARGE_UNIT_PERCENTAGE, ADDITIONAL_UPCHARGE, DELIVERY_UPCHARGE } from 'types';


export default class StylesForm extends Component {
	constructor(props) {
		super(props);

		this.getStylesFormFields = this.getStylesFormFields.bind(this);
		this.saveDesignSelection = this.saveDesignSelection.bind(this);
		this.saveAndClose = this.saveAndClose.bind(this);
		this.saveDesignSelectionText = this.saveDesignSelectionText.bind(this);
		this.getAdditionalUpcharge = this.getAdditionalUpcharge.bind(this);
		this.getUpcharge = this.getUpcharge.bind(this);
		this.hasUpcharge = this.hasUpcharge.bind(this);

		this.accordion = true;

		this.upCharges = this.props.upcharges || [];
		this.toggleAccordion = this.toggleAccordion.bind(this);
	}
	updateFabricDesign(nextProps){
		let formatedFabricDesign = null;
		let {fabricDesign,lists} = nextProps;

		if( fabricDesign && fabricDesign.length > 0){
			formatedFabricDesign = fabricDesign;
		}
		if(!formatedFabricDesign && !this.fabricDesign ){
			this.props.loadFields();
		}
		if(!formatedFabricDesign && lists && lists.fabric_design_fields ){
			formatedFabricDesign =  _.clone(nextProps.lists.fabric_design_fields, true) || [];
		}
		this.fabricDesign = formatedFabricDesign || [];
	}
	componentWillReceiveProps(nextProps) {
		this.updateFabricDesign(nextProps);
	}

	componentDidMount() {
		// this.props.loadFields();
		window.scrollTo(0, 0);
	}

	getUpcharge(design, index, upcharges = []) {
		if (index > -1 && (design.upcharge[index] || design["upcharge percentage"][index])) {
			const value = design.upcharge[index] || design["upcharge percentage"][index];
			const unit = (design.upcharge[index]) ? UPCHARGE_UNIT_VALUE : UPCHARGE_UNIT_PERCENTAGE;
			upcharges = _.compact(upcharges.filter((upcharge) => {
				return upcharge.type != design.name;
			}));
			upcharges.push({
				type: design.name,
				value: value,
				unit: unit
			});
		} else if (index == -1 || !(design.upcharge[index] || design["upcharge percentage"][index])) {
			upcharges = _.compact(upcharges.filter((upcharge) => {
				return upcharge.type != design.name;
			}));
		}
		return upcharges;
	}
	hasUpcharge(design, index, upcharges) {
		if (index > -1) {
			const upcharge = _.find(upcharges, { type: design.name });
			if (upcharge) {
				return {
					type: design.name,
					value: upcharge.value,
					unit: upcharge.unit,
					display_name: upcharge.value + ((upcharge.unit == UPCHARGE_UNIT_VALUE) ? ' INR' : ' % on MRP')
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	saveDesignSelection(type_id, design_id, value) {
		const self = this;
		const design_fields = this.fabricDesign;

		this.props.save(design_fields.map((type) => {
			if (type.id === type_id) {
				type.Designs.map(design => {
					if (design.id === design_id && design.values) {
						var index = design.values.indexOf(value[0]);
						design.selected = index;
						self.upCharges = self.getUpcharge(design, index, self.upCharges);
					}
					return design;
				});
			}

			return type;

		}), ReactDOM.findDOMNode(this.refs.comment).value, self.upCharges);
	}

	saveDesignSelectionText(type_id, design_id, value) {
		var design_fields = this.fabricDesign;
		this.props.save(design_fields.map((type) => {
			if (type.id === type_id) {
				type.Designs.map(design => {
					if (design.id === design_id) {
						design.text = value.target.value;
					}
					return design;
				});
			}
			return type;
		}), ReactDOM.findDOMNode(this.refs.comment).value);
	}

	getAdditionalUpcharge() {
		let additional_upchage;
		if (this.props.upcharges && this.props.upcharges.length > 0)
			additional_upchage = _.find(this.props.upcharges, { type: ADDITIONAL_UPCHARGE });
		if (additional_upchage)
			return additional_upchage.value;
		else
			return 0;
	}

	saveAndClose() {
		const _additional_upcharge = ReactDOM.findDOMNode(this.refs.upcharge).value;
		if (parseInt(_additional_upcharge) > 0) {
			let upcharges = _.compact(this.upCharges.filter((upcharge) => {
				return upcharge.type != ADDITIONAL_UPCHARGE;
			}));
			upcharges.push({
				type: ADDITIONAL_UPCHARGE,
				value: parseInt(_additional_upcharge),
				unit: UPCHARGE_UNIT_VALUE
			});
			this.upCharges = upcharges;
		}
		this.props.save(this.fabricDesign, ReactDOM.findDOMNode(this.refs.comment).value, this.upCharges, true);
		this.props.close();
	}
	toggleAccordion(activeKey) {
		this.props.toggleAccordion(activeKey);
	}
	getStylesFormFields() {
		var _that = this;
		var fabric_designs = this.fabricDesign;

		let is_collapse = false;
		if (fabric_designs && fabric_designs.length > 0) {
			const _styles = fabric_designs.map((type) => {
				var designs = type.Designs.map((design) => {
					if (design.type == 'option') {
						let upchargesList = _that && _that.upCharges;
						let scopeUpcharges = _that.getUpcharge(design, design.selected, upchargesList);

						const upcharge = _that.hasUpcharge(design, design.selected, scopeUpcharges);
						
						return (
							<div className={cx('input-group')} key={design.id + design.type}>
								<label htmlFor={"type-" + type.id + "-design-" + design.id}>{design.name}</label>
								<MultipleChoice 
									isMultiple={false} 
									options={design.values} 
									selected={design.values[design.selected]} 
									disabled={design.disabled}
									rel={"design-" + design.id} 
									save={_that.saveDesignSelection.bind(this, type.id, design.id)} 
								/>
								<span className={cx({
									'foot-note': true,
									'design': true,
									'green': upcharge
								})}>{(upcharge) ? "Upcharge:  " + upcharge.display_name : "No Upcharge"}</span>
							</div>
						);
					} else if (design.type == 'text') {
						return (
							<div className={cx('input-group')} key={design.id + design.type}>
								<label htmlFor={"type-" + type.id + "-design-" + design.id}>{design.name}</label>
								<input type="text" rel={"design-" + design.id} defaultValue={design.text} onChange={_that.saveDesignSelectionText.bind(this, type.id, design.id)} />
							</div>
						);
					}
				});
				let elements = null;
				if (type.collapse) {
					is_collapse = true;
					elements = (
						<Panel header={type.name} key={type.id}>
							<div className={cx('input-category')} key={type.id} >
								{designs}
							</div>
						</Panel>
					);
				} else {
					elements = (
						<div className={cx('input-category')} key={type.id} >
							<h3>{type.name}</h3>
							{designs}
						</div>
					)
				}

				return elements;
			});
			if (is_collapse) {
				return <Collapse
					accordion={this.accordion}
					onChange={this.toggleAccordion}
					activeKey={this.props.activeKey}
				>{_styles}</Collapse>
			} else {
				return _styles;
			}
		}
	}
	render() {
		if(!this.fabricDesign){
			this.updateFabricDesign(this.props);
		}
		return (
			<div className={cx('form-container')} >
				{this.getStylesFormFields()}

				<div className={cx('input-group')}>
					<label htmlFor="comments">Custom Upcharge</label>
					<input type="number" id="upcharge" ref="upcharge" defaultValue={this.getAdditionalUpcharge()} />
				</div>
				<div className={cx('input-group')}>
					<label htmlFor="comments">Comments</label>
					<textarea id="comment" ref="comment" defaultValue={this.props.comment} ></textarea>
				</div>
				<button className={cx('cancel', 'action', 'secondary')} onClick={this.props.close}>Cancel</button>
				<button onClick={this.saveAndClose} className={cx('action', 'primary')} >Save</button>
			</div>
		);
	}
}

StylesForm.propTypes = {
	lists: PropTypes.object,
	save: PropTypes.func,
	loadFields: PropTypes.func,
	fabricDesign: PropTypes.array,
	upcharges: PropTypes.array
};