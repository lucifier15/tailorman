import React, { Component, PropTypes } from 'react';

//Binding styles
import styles from 'css/components/filecapture';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

//@class
export default class FileCapture extends Component {
	constructor(props) {
		super(props);

		this.fileCapture = this.fileCapture.bind(this);
	}

	fileCapture(e) {
		const { fileType, uploadFileDetails, emptyFileUploaded } = this.props;

		const target = e && e.target;

		let file = target.files;

		if(file && file.length > 0) {
			file = file[0];
			// if(fileType && !file.type.match(fileType)) {
				// uploadFileDetails();
			// } else {
				let reader = new FileReader();
				reader.onload = result => {
		        	uploadFileDetails(file, result['target']['result']);
		      	};
		      	reader.readAsBinaryString(file);
			// }
		} else {
			emptyFileUploaded && emptyFileUploaded();
		}
	}

	render() {
		let { accept } = this.props;

		accept = accept || 'file/*';

		return (
			<div className={cx('upload-wrapper')}>
				<input name="filecaptureuploader" type="file" accept={accept} onChange={this.fileCapture} />
			</div>
		);
	}
}

FileCapture.propTypes = {
	accept: PropTypes.string,
	fileType: PropTypes.string,
	uploadFileDetails: PropTypes.func,
	emptyFileUploaded: PropTypes.func
};