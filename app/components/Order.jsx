import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import styles from 'css/components/order';
import back from 'images/back-arrow.png';
const cx = classNames.bind(styles);

class Order extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let sale_item = (
			<Link to="/order/sales" className={cx('option')}>
				Sale Items
			</Link>
		);
		if (!this.props.order || parseInt(this.props.order.details.order_id) <= 0)
			sale_item = null;
		return (
			<div className={cx('container')}>
				<div className={cx('header-note')}>
					<span className={cx('header-label')}>Customer:   </span>
					<span className={cx('header-content')}>{this.props.customer.name}</span>
				</div>
				<div className={cx('form-container')}>
					<Link to="/customer/actions" className={cx('back')} ><img src={back} /></Link>
					<Link to="/order/customer" className={cx('option')} >
						Customer Details
				</Link>
					<Link to="/order/details" className={cx('option')}>
						Order Details
				</Link>
					{sale_item}

				</div>
			</div>
		);
	}
}

Order.propTypes = {
	user: PropTypes.object,
	customer: PropTypes.object,
	lists: PropTypes.object,
	order: PropTypes.object
};


function mapStateToProps({lists, customer, user}) {
	return {
		customer,
		user,
		lists
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(Order);
