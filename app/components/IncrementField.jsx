import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/incrementField';
import ReactDOM from 'react-dom';
const cx = classNames.bind(styles);


export default class IncrementField extends Component {
	constructor(props) {
		super(props);
		this.save = this.save.bind(this);
		this.default = this.props.default || 0;
		this.minus = this.minus.bind(this);
		this.plus = this.plus.bind(this);
		this.value = this.default;
	}
	minus() {
		var el = ReactDOM.findDOMNode(this.refs.number);
		el.value = parseFloat(el.value) - parseFloat(this.props.step);
		this.save();
	}
	componentDidUpdate() {
		this.value = this.props.default;
	}
	plus() {
		var el = ReactDOM.findDOMNode(this.refs.number);
		el.value = parseFloat(el.value) + parseFloat(this.props.step);
		this.save();
	}
	save() {
		this.props.save(ReactDOM.findDOMNode(this.refs.number).value, this.props.rel);
	}
	render() {
		return (
			<div className={cx('incrementFieldContainer')} >
				<div className={cx('minus')} onClick={this.minus} >-</div>
				<input type="text" ref="number" step="any" className={cx('numberField')} defaultValue={parseFloat(this.value).toFixed(3)} onChange={this.save.bind(this)} />
				<div className={cx('plus')} onClick={this.plus} >+</div>
			</div>
		);
	}
}

IncrementField.propTypes = {
	step: PropTypes.oneOf(['0.25', '0.125']),
	save: PropTypes.func,
	default: PropTypes.string,
	rel: PropTypes.string
};