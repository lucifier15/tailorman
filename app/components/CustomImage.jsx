/**
 *
 * Renders an image, enforcing the usage of the alt="" tag
 *
 */

import React, { Component, PropTypes } from 'react';


class CustomImage extends Component { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);

        this.handleImageLoadError = this.handleImageLoadError.bind(this);

        this.state = {
            loadingError: null
        };
    }

    handleImageLoadError(e) {
        this.setState({
            loadingError: this.props.src
        });
    }

    render() {
        let options = {};

        let { loadingError } = this.state;
        let { src, width, height, alt, className } = this.props;

        if (src == loadingError) {
            src = 'https://s3.ap-south-1.amazonaws.com/assets.web.tm/defaultImage.jpg';
        }

        if (width) {
            options['width'] = width;
        }

        if (height) {
            options['height'] = height;
        }

        return ( 
            < img {...options} className={className} src={src} alt={alt || 'No Image'} onError={this.handleImageLoadError} />
        );
    }
}

// We require the use of src and alt, only enforced by react in dev mode
CustomImage.propTypes = {
    src: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
        ]).isRequired,
    className: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string
};

export default CustomImage;