import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/numberForm';
import ReactDOM from 'react-dom';
import _ from 'lodash';
const cx = classNames.bind(styles);
const ENTER_KEY_CODE = 13;
const NEXT_KEY_CODE = 39;
const BACK_KEY_CODE = 8;
const PREVIOUS_KEY_CODE = 37;
const VALID_KEY_CODES = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57];

export default class NumberForm extends Component {
	constructor(props) {
		super(props)
		this.getLength = this.getLength.bind(this);
		this.saveValue = this.saveValue.bind(this);
		this.renderFields = this.renderFields.bind(this);
		this.getNextField = this.getNextField.bind(this);
		this.getPreviousField = this.getPreviousField.bind(this);
		this.keyDownHandler = this.keyDownHandler.bind(this);
	}

	getLength() {
		const { type, length } = this.props;
		if (type == 'normal' && length) {
			return parseInt(length);
		} else if (type === 'phone') {
			return 10;
		}
	}

	saveValue() {
		const inputContainer = ReactDOM.findDOMNode(this.refs.container);
		const inputValue = Object.keys(inputContainer.children).reduce((prev, next, index) => {
			if (prev == '') {
				return inputContainer.children[next].value;
			} else {
				const currValue = (inputContainer.children[next].value.length > 0) ? inputContainer.children[next].value : ' ';
				return prev + currValue;
			}
		}, '');
		this.props.save(inputValue, this.props.rel);
	}

	keyDownHandler(event) {
		if (event.keyCode === NEXT_KEY_CODE) {
			const nextField = this.getNextField(parseInt(event.target.attributes.rel.value));
			if (nextField) nextField.focus();
			event.stopPropagation();
		} else if (event.keyCode === PREVIOUS_KEY_CODE) {
			const previousField = this.getPreviousField(parseInt(event.target.attributes.rel.value));
			previousField.focus();
		} else if (event.keyCode === BACK_KEY_CODE) {
			if (event.target.value.length == 0) {
				const previousField = this.getPreviousField(parseInt(event.target.attributes.rel.value));
				if (previousField) previousField.focus();
				event.stopPropagation();
			}
		}
		else if (VALID_KEY_CODES.indexOf(event.keyCode) > -1) {
			if (event.target.value.length == 1) {
				const nextField = this.getNextField(parseInt(event.target.attributes.rel.value));
				if (nextField) {
					nextField.focus();
					nextField.value = '';
				} else {
					event.preventDefault();
				}
				event.stopPropagation();
				return;
			}
		}
	}


	getNextField(fieldIndex) {
		if (this.getLength() > fieldIndex + 1) {
			return ReactDOM.findDOMNode(this.refs[fieldIndex + 1]);
		}
	}

	getPreviousField(fieldIndex) {
		if (fieldIndex > 0) {
			return ReactDOM.findDOMNode(this.refs[fieldIndex - 1]);
		}
	}

	renderFields() {
		const length = this.getLength();
		const fieldLength = [];
		for (var i = 0; i < length; i++) {
			fieldLength.push(0);
		}
		var _default = this.props.default;
		if (!_default)
			_default = '';
		return fieldLength.map((val, index) => {
			var _currVal = _.isNaN(parseInt(_default[index])) ? '' : parseInt(_default[index]);
			return <input type="number" max="9" min="0" step="1" onChange={this.saveValue} ref={index} rel={index} onKeyDown={this.keyDownHandler} defaultValue={_currVal} key={index} />
		});
	}

	render() {
		return (
			<div className={cx('numberFormContainer')} ref="container" value="" >
				{this.renderFields()}
			</div>
		);
	}
}

NumberForm.propTypes = {
	type: PropTypes.oneOf(['normal', 'phone']),
	save: PropTypes.func,
	rel: PropTypes.string
};