import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/order/sales';
import ReactDOM from 'react-dom';
import Select from 'react-select';
const cx = classNames.bind(styles);

export default class ImageGallery extends Component {
	constructor(props) {
		super(props);
		this.getThumbnails = this.getThumbnails.bind(this);
		this.preview = this.preview.bind(this);
		this.nopreview = this.nopreview.bind(this);
	}

	getThumbnails(){
		const self = this;
		return this.props.thumbnails.map((thumbnail, index) => {
			return <li key={index} onClick={self.preview.bind(self, thumbnail)}><img src={thumbnail} /></li>;
		});
	}

	preview(thumbnail){
		this.props.showThumbnail(thumbnail, true);
	}
	nopreview(thumbnail){
		this.props.showThumbnail(thumbnail, false);
	}

	render() {
		let preview = null;
		if(this.props.thumbnail.show){
			preview = <div className={cx('image-preview')} onClick={this.nopreview.bind(this, this.props.thumbnail.dataURL)}>
				<img src={this.props.thumbnail.dataURL} />
			</div>
		}
		return (
			<div className={cx('gallery-container')}>
				{preview}
				<h3>Saved Images</h3>
				<div className={cx('gallery')}><ul>{this.getThumbnails()}</ul></div>
			</div>
		);
	}
}

ImageGallery.propTypes = {
	thumbnails: PropTypes.array,
	showThumbnail: PropTypes.func,
	thumbnail: PropTypes.object
};