import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/measurementForm';
import IncrementField from './IncrementField';
import MultipleChoice from './MultipleChoice';
import ReactDOM from 'react-dom';
import SelectForm from './SelectForm';
import Select from 'react-select';
import ImageCapture from 'components/ImageCapture';
import _ from 'lodash';
const cx = classNames.bind(styles);
import { UPCHARGE_UNIT_VALUE, UPCHARGE_UNIT_PERCENTAGE } from 'types';

const mandatoryFields = ["CUSTOMER IS WEARING","MASTER SUGGESTED","CUSTOMER FINAL FIT PREFERENCE","CUSTOMER IS WEARING (BODY OVER)", "CUSTOMER IS WEARING (ARMHOLE)","MASTER SUGGESTED (BODY OVER)","MASTER SUGGESTED (ARMHOLE)","FINAL FIT PREFERENCE (ARMHOLE)","FINAL FIT PREFERENCE (BODY OVER)"];

export default class MeasurementsForm extends Component {
	constructor(props) {
		super(props);
		this.saveMeasurementType = this.saveMeasurementType.bind(this);
		this.save = this.save.bind(this);
		this.validate = this.validate.bind(this);
		this.measurement_type = 0;
		this.saveMeasurement = this.saveMeasurement.bind(this);
		this.profile = this.props.profile || {
			profile: {
				comment: '',
				name: '',
				item_type_id: 0,
				measurement_source_id: -1,
				profile_id: -1
			}
		}
	}
	componentWillMount() {
		this.props.loadFields(0, true);
		if (this.props.profile && this.props.profile.measurement_source_id) {
			this.props.loadFields(this.props.profile.measurement_source_id);
			if (this.props.profile.measurements && this.props.profile.measurements.length > 0) {
				this.measurements = [];
				this.measurementDescr = [];
				const values = _.map(this.props.profile.measurements, 'value');
				const ids = _.map(this.props.profile.measurements, 'id');
				ids.forEach((id, index) => {
					this.measurements[id] = values[index];
					if(this.props.profile.values && this.props.profile.values[index]) {
						this.measurementDescr[id] = this.props.profile.values[index].descr;
					}
				});
			} else {
				this.measurements = [];
				this.measurementDescr = [];
			}
		}
	}


	saveMeasurementType(selected, type) {
		this.profile.measurement_source_id = selected.value;
		this.props.loadFields(selected.value);
	}

	saveMeasurement(value, rel) {
		let descr = null;

		if (_.isArray(value)) {
			const field = _.find(this.props.lists.measurement_fields, { measurement_type_id: parseInt(rel) });
			const option = _.find(field.options, { descr: value[0] });
			if (option) {
				value = option.value;
				descr = option.descr;
			}
		}
		this.measurements[rel] = parseFloat(value);

		this.measurementDescr[rel] = descr;

		this.props.save(this.measurements, true, descr);
	}

	getMeasurementUpcharge(measurement) {
		if (measurement.upcharge_value > 0) {
			return {
				display: "Upcharge: " + measurement.upcharge_value + ' INR if >' + measurement.upcharge_limit,
				value: measurement.upcharge_value,
				type: UPCHARGE_UNIT_VALUE
			}
		}
		else if (measurement.upcharge_persentage > 0) {
			return {
				display: "Upcharge: " + measurement.upcharge_persentage + "% on MRP if >" + measurement.upcharge_limit,
				value: measurement.upcharge_persentage,
				type: UPCHARGE_UNIT_PERCENTAGE
			}
		} else {
			return {
				display: "No Upcharge",
				value: 0,
				type: -1
			}
		}
	}
	isMandatory(fieldName){
		fieldName = fieldName && fieldName.toUpperCase();
		const mandatory = mandatoryFields.indexOf(fieldName);
    	if(mandatory >= 0 ){
    		return <em className = {cx('mandatory')}>*</em>;
    	}
		return '';
	}
	getMeasurementFormFields() {
		const self = this;
		if (!self.measurements) {
			self.measurements = [];
		}
		if(!self.measurementDescr) {
			self.measurementDescr = [];
		}
		if (this.props.lists.measurement_fields && this.props.lists.measurement_fields.length > 0) {
			return this.props.lists.measurement_fields.map((measurement, index) => {
				var _default = self.measurements[measurement.measurement_type_id];
				const measurement_upcharge = self.getMeasurementUpcharge(measurement);
				if (measurement.category != "Enumerated" || !measurement.options) {
					_default = (_default) ? _default.toString() : '0.000';
					return (
						<div className={cx('input-group')} key={measurement.measurement_type_id}>
							<label htmlFor={"measurement-" + measurement.measurement_type_id}>{measurement.code}{this.isMandatory(measurement.code)}</label>
							<IncrementField default={_default.toString()} step="0.125" save={self.saveMeasurement} rel={measurement.measurement_type_id.toString()} />
							<span className={cx({
								'foot-note': true,
								'green': measurement_upcharge.value > 0
							})}>{measurement_upcharge.display}</span>
						</div>
					);
				} else {
					const _selectedField = _.find(measurement.options, { value: parseFloat(_default) });
					let selected;
					if (_selectedField)
						selected = [_selectedField.descr];
					return (
						<div className={cx('input-group')} key={measurement.measurement_type_id}>
							<label htmlFor={"measurement-" + measurement.measurement_type_id}>{measurement.code}{this.isMandatory(measurement.code)}</label>
							<MultipleChoice isMultiple={false} rel={measurement.measurement_type_id.toString()} save={self.saveMeasurement} selected={selected} options={_.map(measurement.options, 'descr')} />
							<span className={cx({
								'foot-note': true,
								'green': measurement_upcharge.value > 0
							})}>{measurement_upcharge.display}</span>
						</div>
					);
				}

			});
		}
	}

	getMeasurementValues() {
		var _that = this;
		return this.props.lists.measurement_fields.map((measurement) => {
			var _ref = "measurement-" + measurement.measurement_type_id;
			var upcharge = undefined;
			if (measurement.upcharge_limit > 0 && parseFloat(_that.measurements[measurement.measurement_type_id]) >= parseFloat(measurement.upcharge_limit)) {
				const _upc = _that.getMeasurementUpcharge(measurement);
				upcharge = {
					value: _upc.value,
					type: "Higher Size Upcharge",
					unit: _upc.type
				}
			}
			return {
				id: measurement.measurement_type_id,
				value: _that.measurements[measurement.measurement_type_id],
				upcharge: upcharge,
				code:measurement.code,
				descr: _that.measurementDescr[measurement.measurement_type_id]
			}
		});
	}
	validate(){
			let message =[],isValid = true;
            
			let  measurementValues = this.getMeasurementValues();
		    mandatoryFields.map((mandatoryField) => {

		    	measurementValues.map( (item) => {
		    		let {code} = item;
		    		code = code && code.toUpperCase();
		    		if(code === mandatoryField){
		    			if(!item.value){
		    				isValid = false;
		    				message.push(code);
		    			}
		    		}
		    	});
		    });
			if ((ReactDOM.findDOMNode(this.refs.profile_name).value.trim().length == 0)){
				  isValid = false;
                  message.push("Profile Name");
			}
			return {
				message:(isValid)? "": "Please provide values for "+message.toString(),
				isValid
			}
		}

	save() {
        const self =this;
		if (this.validate().isValid) {
			this.props.save({
				measurement_type: this.measurement_type,
				measurements: this.getMeasurementValues(),
				profile: {
					comment: ReactDOM.findDOMNode(self.refs.comment).value,
					name: ReactDOM.findDOMNode(self.refs.profile_name).value,
					item_type_id: 0,
					measurement_source_id: self.profile.measurement_source_id || self.profile.profile.measurement_source_id,
					profile_id: self.profile.profile_id || undefined,
					measurements: this.getMeasurementValues()
				}
			});
			this.props.close();
		} else {
			window.scrollTo(0, 0)
			
			this.props.message('MEARSUREMENT_FORM_VALIDATION',this.validate().message );
		}
	}

	render() {
		let measurement_source = (
			<div className={cx('input-group')}>
				<label htmlFor="measurement_type">Measurement Type</label>
				<SelectForm type="measurement_type" rel="measurement_type" options={this.props.lists.measurement_types} value={this.profile.measurement_source_id} save={this.saveMeasurementType} disabled={this.props.is_source_disabled} />
			</div>
		);
		let image = <ImageCapture addImage={this.props.addImage} />;
		if (!this.props.addImage)
			image = null;

		return (
			<div className={cx('form-container', 'measurement-form')} >
				{measurement_source}
				<div className={cx('input-group')}>
					<label htmlFor="profile_name">Measurement Profile Name</label>
					<input type="text" id="profile_name" ref="profile_name" defaultValue={this.profile.name} />
				</div>
				{image}
				{this.getMeasurementFormFields()}
				<div className={cx('input-group')}>
					<label htmlFor="comment">Comment</label>
					<textarea id="comment" ref="comment" defaultValue={this.profile.comment}></textarea>
				</div>
				<button className={cx('action', 'secondary')} onClick={this.props.close}>Cancel</button>
				<button className={cx('action', 'primary')} onClick={this.save}>Save</button>
			</div>
		);
	}
}

MeasurementsForm.propTypes = {
	lists: PropTypes.object,
	save: PropTypes.func,
	loadFields: PropTypes.func,
	close: PropTypes.func,
	profile: PropTypes.object,
	addImage: PropTypes.func,
	message: PropTypes.func,
	is_source_disabled: PropTypes.bool
};