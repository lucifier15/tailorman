import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/hamburger';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import hamburger_icon from 'images/hamburger.png';
import { Link } from 'react-router';


const cx = classNames.bind(styles);

export default class HamburgerMenu extends Component {
	constructor(props) {
		super(props);
		this.showContent = this.showContent.bind(this);
	}

	showContent() {
		if (this.props.isOpen) {
			return (
				<div className={cx({
					'menu': true,
					'open': this.props.isOpen
				})}>
					<span className={cx('label')}>Logged in as</span>
					<span className={cx('user')}>{this.props.data.user}</span>
					<Link className={cx('action')} to="/clear">Reset</Link>
					<Link className={cx('action')} to="/reset">Logout</Link>
				</div>
			);
		} else {
			return null;
		}
	}
	render() {
		return (
			<div className={cx('hamburger')}>
				<img src={hamburger_icon} onClick={this.props.toggle.bind(this, this.props.isOpen)} />
				{this.showContent()}
			</div>
		);
	}
}

HamburgerMenu.propTypes = {
	isOpen: PropTypes.bool,
	data: PropTypes.object,
	toggle: PropTypes.func
};