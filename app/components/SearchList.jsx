import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/searchList';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import clear from 'images/clear.png';
const cx = classNames.bind(styles);

export default class SearchList extends Component {
    constructor(props) {
        super(props);
        this.search = this.search.bind(this);
        this.renderList = this.renderList.bind(this);
        this.hasSearched = false;
        this.clear = this.clear.bind(this);
        this.selected = undefined;
    }

    search() {
        const keyword = ReactDOM.findDOMNode(this.refs.keyword).value;
        if (keyword.length === 0)
            this.hasSearched = false;
        this.props.search(keyword);
    }

    select(id) {
        this.selected = id;
        this.props.select(id);
      //  this.clear();
    }
    clear() {
        ReactDOM.findDOMNode(this.refs.keyword).value = '';
      //  this.search();
    }

    renderList(clearList) {
        const self = this;
        if ((this.props.results && this.props.results.length > 0)& !clearList.clearList) {
            return this.props.results.map((result, index) => {
                return <li onClick={self.select.bind(this, result.id)} className={cx({
                    'active': this.selected && String(this.selected).length > 0 && this.selected == result.id
                })} key={index}>
                    {this.props.renderItem(result)}
                </li>
            });
        } else {
            if (!this.hasSearched)
                return (<span className={cx('no-results-label')}>Please search above for results</span>);
            return (<span className={cx('no-results-label')}>No results match your query</span>);

        }

    }

    render() {
        let {clearList} = this.props;
        return (
            <div className={cx('select-list')}>
                <input type="text" ref="keyword" placeholder={this.props.placeholder} />
                <button onClick={this.clear} className={cx('input-group-button')}><img src={clear} /></button>
                <button onClick={this.search} className={cx('action')}>search</button>
                <ul className={cx('list-container')}>
                    {this.renderList({clearList})}
                </ul>
            </div >
        );
    }
}

SearchList.propTypes = {
    search: PropTypes.func,
    select: PropTypes.func,
    results: PropTypes.array,
    renderItem: PropTypes.func,
    placeholder: PropTypes.string
};