import {
	createStore,
	applyMiddleware,
	compose
} from 'redux';
import {
	routerMiddleware
} from 'react-router-redux';
import thunk from 'redux-thunk';
import rootReducer from 'reducers';
import promiseMiddleware from 'middlewares/promiseMiddleware';
import createLogger from 'redux-logger';
import persistState from 'redux-localstorage'

/*
 * @param {Object} initial state to bootstrap our stores with for server-side rendering
 * @param {History Object} a history object. We use `createMemoryHistory` for server-side rendering,
 *                          while using browserHistory for client-side
 *                          rendering.
 */
export default function configureStore(initialState, history) {
	// Installs hooks that always keep react-router and redux store in sync
	const middleware = [thunk, promiseMiddleware, routerMiddleware(history)];
	let store;

	if (__DEVCLIENT__) {
		//middleware.push(createLogger());
		store = createStore(rootReducer, initialState, compose(
			applyMiddleware(...middleware),
			typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f,
			persistState(['user', 'order', 'sales', 'history', 'lists', 'message', 'customer', 'routing', 'stores'], {
				key: "TailormanStore",
				slicer: (paths) => {
					return (state) => {
						let subset = {};
						paths.forEach((path) => {
							if (path == 'user') {
								if (state[path].authenticated === false && initialState[path].authenticated === true) {
									subset[path] = initialState[path];
								} else {
									subset[path] = state[path];
								}
							} else {
								subset[path] = state[path]
							}
						});
						return subset;
					}
				}
			})
		));
	} else {
		store = createStore(rootReducer, initialState, compose(applyMiddleware(...middleware), f => f));
	}

	if (module.hot) {
		// Enable Webpack hot module replacement for reducers
		module.hot.accept('reducers', () => {
			const nextReducer = require('reducers');
			store.replaceReducer(nextReducer);
		});
	}

	return store;
}