import * as types from 'types';
import {
	combineReducers
} from 'redux';
import _ from 'lodash';

const history = (
	state = {
		orders: [],
		accordionKey: ['0']
	},
	action
) => {
	switch (action.type) {
		case types.HISTORY_POPULATE_ORDERS:
			return {...state,
				orders: action.orders
			};
		case types.TOGGLE_ACCORDION:
			return {...state,
				accordionKey: [action.key]
			};
		default:
			return state;
	}
};


export default history;