import * as types from 'types';
import _ from 'lodash';

const dashboard = (
	state = {
		queryresult: [],
		querystring: '',
		queryfilters: false,
		userqueries:[]
	},
	action
) => {
	switch (action.type) {
		case types.DASHBOARD_RESULTS_SUCCESS:
			return {
				...state,
				queryresult: action.queryresult,
				querystring: action.querystring
			}
		case types.DASHBOARD_FILTERS_SUCCESS:
			return {
				...state,
				queryfilters: action.query_filters
			}
		case types.TOGGLE_DASHBOARD_ACCORDION:
			return {
				...state,
				accordionKey: action.key
			}
		case types.DASHBOARD_USER_QUERIES_SUCCESS:
			return {
				...state,
				userqueries: action.user_queries
			}
		default:
			return state;
	}
};
export default dashboard;