import {
	combineReducers
} from 'redux';
import user from 'reducers/user';
import message from 'reducers/message';
import order from 'reducers/order';
import stores from 'reducers/stores';
import lists from 'reducers/lists';
import customer from 'reducers/customer';
import {
	routerReducer as routing
} from 'react-router-redux';
import history from 'reducers/history';
import menu from 'reducers/menu';
import workorder from 'reducers/workorder';
import dashboard from 'reducers/dashboard';
import * as types from 'types';
// Combine reducers with routeReducer which keeps track of
// router state


const appReducer = combineReducers({
	user,
	message,
	order,
	routing,
	stores,
	lists,
	customer,
	history,
	menu,
	workorder,
	dashboard
})

const rootReducer = (state, action) => {
	if (action.type === 'RESET_STATES') {
		state = {};
	}
	if (action.type === 'CLEAR_DATA') {
		const user = state.user;
		state = {};
		state.user = user;
	}

	if (action.type === types.CLEAR_ORDER_DATA) {
		state.order = {};
	}
	return appReducer(state, action)
}

export default rootReducer;