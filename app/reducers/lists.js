import * as types from 'types';

const lists = (
	state = {
		priorities: [],
		payment_types: [],
		finish_types: [],
		tailors: [],
		salesmen: [],
		measurement_types: [],
		item_types: [],
		styles: [],
		customers: [],
		fabrics: [],
		measurement_fields: [],
		fabric_design_fields: [],
		customerSources: [],
		occasions: [],
		rtwsku:[],
		rtwsku_size:[],
		rtwsku_fit:[]
	},
	action
) => {
	console.log(action.type);
	switch (action.type) {
		
		case types.FETCH_LIST_PRIORITIES:
			return {
				...state,
				priorities: action.priorities
			}
		case types.FETCH_LIST_PAYMENT_TYPES:
			return {
				...state,
				payment_types: action.payment_types
			}
		case types.FETCH_LIST_FINISH_TYPES:
			return {
				...state,
				finish_types: action.finish_types
			}
		case types.FETCH_LIST_TAILORS:
			return {
				...state,
				tailors: action.tailors
			}
		case types.FETCH_LIST_SALESMEN:
			return {
				...state,
				salesmen: action.salesmen
			}
		case types.FETCH_LIST_STYLES:
			return {
				...state,
				styles: action.styles
			}
		case types.FETCH_LIST_MEASUREMENT_TYPES:
			return {
				...state,
				measurement_types: action.measurement_types
			}
		case types.FETCH_LIST_MEASUREMENT_FIELDS:
			return {
				...state,
				measurement_fields: action.measurement_fields
			}
		case types.CLEAR_LIST_MEASUREMENT_FIELDS:
			return {
				...state,
				measurement_fields: []
			}
		case types.FETCH_LIST_FABRIC_DESIGN_FIELDS:
			return {
				...state,
				fabric_design_fields: action.fabric_design_fields
			}
		case types.FETCH_LIST_ITEM_TYPES:
			return {
				...state,
				item_types: action.item_types
			}
		case types.FETCH_LIST_FABRICS:
			return {
				...state,
				fabrics: action.fabrics
			}
		case types.CLEAR_LIST_FABRICS:
			return {
				...state,
				fabrics: []
			};
		case types.FETCH_LIST_CUSTOMERS:
			return {
				...state,
				customers: action.customers
			}
		case types.CLEAR_LIST_CUSTOMERS:
			return { ...state, customers: [] };
		case types.FETCH_LIST_CAMERAS:
			return {
				...state,
				cameras: action.cameras
			}
		case types.FETCH_LIST_CUSTOMER_SOURCES:
			return {
				...state,
				customerSources: action.customerSources
			}
		case types.FETCH_CUSTOMER_PROFILE_LIST:
			return {
				...state,
				profiles: action.profiles
			}
		case types.FETCH_CUSTOMER_ORDER_LIST:
			return {
				...state,
				orders: action.orders
			}
		case types.FETCH_LIST_OCCASIONS:
			return {
				...state,
				occasions: action.occasions
			}

		case types.FETCH_RTW_SKU:
			return {
				...state,
				rtwsku: action.rtwsku
			}
		case types.FETCH_RTW_SIZE:
			return {
				...state,
				rtwsku_size:action.rtwsku_size
			}
		case types.FETCH_RTW_FIT:
			return {
				...state,
				rtwsku_fit:action.rtwsku_fit
			}
		default:
			return state;
	}
}

export default lists;