import * as types from 'types';

/*
 * Message store for global messages, i.e. Network messages / Redirect messages
 * that need to be communicated on the page itself. Ideally
 * messages/notifications should appear within the component to give the user
 * more context. - My 2 cents.
 */
export default function message(state = {
  message: '',
  type: 'SUCCESS'
}, action = {}) {
  switch (action.type) {
    case types.LOGIN_SUCCESS_USER:
    case types.SIGNUP_SUCCESS_USER:
    case types.ORDER_CUSTOMER_SAVE_SUCCESS:
    case types.ORDER_DETAILS_SAVE_SUCCESS:
    case types.ORDER_SAVE_SUCCESS:
    case types.SALE_ITEM_SAVE_SUCCESS:
    case types.ALTERATION_SAVE_SUCCESS:
    case types.SAVE_MEASUREMENT_PROFILE_SUCCESS:
    case types.SUCCESS_CONDITION:
      return { ...state, message: action.message, type: 'SUCCESS' };
    case types.DISMISS_MESSAGE:
      return { ...state, message: '', type: 'SUCCESS' };
    case types.LOGIN_ERROR_USER:
    case 'MEARSUREMENT_FORM_VALIDATION':
    case 'IMAGE_UPLOADED_FAILURE':
    case 'ALREADY_EXIST_CONDITION':
    case 'ERROR_CONDITION':
      return { ...state, message: action.message, type: 'ERROR' }
    case 'UPDATE_GENERAL_MESSAGE':
    case 'IMAGE_UPLOADED_SUCCESS':
      return { ...state, message: action.message, type: 'SUCCESS' }
    default:
      return state;
  }
}
