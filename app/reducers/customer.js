import * as types from 'types';

const customer = (state = {
	customer_id: 0,
	is_new_customer: 'yes',
	gender: 'male',
	phone: '',
	email: '',
	name: '',
	address: '',
	pincode: ''
}, action) => {
	switch (action.type) {
		case types.ORDER_CUSTOMER_UPDATE_GENDER:
			return {...state,
				gender: action.gender
			};
		case types.ORDER_CUSTOMER_UPDATE_IS_NEW_CUSTOMER:
			return {...state,
				is_new_customer: action.isNewCustomer
			}
		case types.ORDER_CUSTOMER_UPDATE_PINCODE:
			return {...state,
				pincode: action.pincode
			}
		case types.ORDER_CUSTOMER_UPDATE_PHONE:
			return {...state,
				phone: action.phone
			}
		case types.ORDER_CUSTOMER_UPDATE_SOURCE:
			return {...state,
				source_id: action.source_id
			}
		case types.ORDER_CUSTOMER_UPDATE:
			return {...state,
				phone: action.customer.phone,
				mobile: action.customer.mobile,
				pincode: action.customer.pincode || '',
				is_new_customer: action.customer.is_new_customer,
				gender: action.customer.gender || 'male',
				name: action.customer.name,
				address: action.customer.address || '',
				email: action.customer.email,
				customer_id: action.customer.customer_id || 0,
				dob: action.customer.dob || '',
				source_id: action.customer.source_id || 1,
				height: action.customer.height || 0,
				weight: action.customer.weight || 0,
				comment: action.customer.comment || ''
			}
		case types.ORDER_UPDATE_CUSTOMER_ID:
			return {...state,
				customer_id: action.id
			}
		default:
			return state;
	}
}

export default customer;