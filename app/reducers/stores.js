import * as types from 'types';
import {
	combineReducers
} from 'redux';

const list = (
	state = [],
	action
) => {
	switch (action.type) {
		case types.FETCH_STORES_SUCCESS:
			return action.stores
		default:
			return state;
	}
};

const selected = (
	state = {},
	action
) => {
	switch (action.type) {
		case types.USER_SELECT_STORE:
			return action.store;
		default:
			return state;
	}
}

export default combineReducers({
	list,
	selected
});