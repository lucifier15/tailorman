import * as types from 'types';
import {
    combineReducers
} from 'redux';

import { fabricDesignDependency } from 'util';

const selectedForm = (
    state = '',
    action
) => {
    switch (action.type) {
        case types.SELECT_ORDER_FORM:
            return action.order_form_type;
        default:
            return state;
    }
};

const thumbnails = (
    state = [],
    action
) => {
    switch (action.type) {
        case types.ORDER_ADD_IMAGE_THUMBNAIL:
            return [...state, action.imageDataURL];
        case types.ORDER_POPULATE_THUMBNAILS:
            return action.imageDataURLs;
        default:
            return state;
    }
}
const thumbnail = (
    state = {
        show: false,
        dataURL: ''
    }, action
) => {
    switch (action.type) {
        case types.SHOW_THUMBNAIL:
            return {
                show: action.show,
                dataURL: action.thumbnail
            };
        default:
            return state;
    }
}
const selectedSaleItem = (
    state = -1,
    action
) => {
    switch (action.type) {
        case types.ORDER_SALE_ITEM_SELECT:
            return action.index;
        default:
            return state;
    }
}

const details = (state = {
    tailor_id: 0,
    salesman_id: 0,
    payment_type_id: 0,
    priority_id: 0,
    benificiary_phone: '',
    thumbnails: [],
    occasion: '',
    total_amount: 0,
    is_full_payment: true,
    billing_address: '',
    delivery_address: '',
    PIN: 0
}, action) => {
    switch (action.type) {
        case types.ORDER_DETAILS_UPDATE_TAILOR:
            return {
                ...state,
                tailor_id: action.tailor_id
            }
        case types.ORDER_DETAILS_UPDATE_SALESMAN:
            return {
                ...state,
                salesman_id: action.salesman_id
            }
        case types.ORDER_DETAILS_UPDATE_PIN:
            return {
                ...state,
                PIN: action.pin
            }
        case types.ORDER_DETAILS_UPDATE_PIN_Date:
            return{
                ...state,
                pindate : action.pindate
            }
        case types.ORDER_DETAILS_UPDATE_PAYMENT_TYPE:
            return {
                ...state,
                payment_type_id: action.payment_type_id
            }
        case types.ORDER_DETAILS_UPDATE_PRIORITY_TYPE:
            return {
                ...state,
                priority_id: action.priority_id
            };
        case types.ORDER_DETAILS_UPDATE_FULL_PAYMENT_FLAG:
            return {
                ...state,
                is_full_payment: action.value
            };
        case types.ORDER_DETAILS_UPDATE_OCCASION:
            return {
                ...state,
                occasion: action.occasion
            };
        case types.ORDER_DETAILS_UPDATE_BENIFICIARY_PHONE:
            return {
                ...state,
                benificiary_phone: action.benificiary_phone
            };
        case types.ORDER_DETAILS_UPDATE_TOTAL_AMOUNT:
            return {
                ...state,
                total_amount: action.total_amount
            };
        case types.ORDER_DETAILS_UPDATE:
            return action.order_details;
        case types.SELECT_ORDER:
            return action.order;
        default:
            return state;
    }
}



const isFormOpen = (state = {
    measurements: false,
    styles: false,
    saleItem: false,
    signature: false
}, action) => {
    switch (action.type) {
        case types.ORDER_SALE_ITEM_OPEN_MEASUREMENTS:
            return {
                ...state,
                measurements: !action.close
            }
        case types.ORDER_SALE_ITEM_OPEN_STYLES:
            return {
                ...state,
                styles: !action.close
            }
        case types.ORDER_SALES_OPEN_FORM:
            return {
                ...state,
                saleItem: !action.close
            }
        case types.OPEN_SIGNATURE:
            return { ...state,
                signature: !action.close
            }
        default:
            return state;
    }
}

const sales = (state = [], action) => {
    switch (action.type) {
        case types.UPDATE_ORDER_SALES_INIT_SALE_ITEM:
            let newState = _.cloneDeep(state);
            newState[newState.length-1]['comment'] = action.comment;

            return newState;
        case types.ORDER_SALES_INIT_SALE_ITEM:
            return [...state, {
                measurement_type: 1,
                item_type_id: action.item_type,
                comment: action.comment || '',
                profile_id: -1,
                is_new_measurements: true,
                style: -1,
                finish_type_id: 0,
                discount_type: types.UPCHARGE_UNIT_VALUE,
                order_flag: 1
            }];
        case types.ORDER_ITEMS_POPULATE:
            return [...action.sales];
        case types.ORDER_SALES_UPDATE_SALE_ITEM_ENTRY:
            let new_state = _.cloneDeep(state);

            new_state[action.payload.index][action.payload.field] = action.payload.value;
            new_state[action.payload.index]['fabric_designs'] = fabricDesignDependency(new_state[action.payload.index]['fabric_designs']);

            return new_state;
        case types.ORDER_SALE_ITEM_DELETE:
            return state.filter((saleItem, index) => index !== parseInt(action.index));
        case types.ORDER_SALES_UPDATE_SALE_ITEM:
            const customizer = (obj, src) => {
                if (obj) {
                    return obj;
                }
            }
            state[action.index] = _.mergeWith(action.order_item, state[action.index], customizer);
            return [...state];
        default:
            return state;
    }
}

const invoice = (state = {}, action) => {
    switch (action.type) {
        case types.POPULATE_INVOICE_DATA:
            return action.details;
        default:
            return state;
    }
}



const orderReducer = combineReducers({
    selectedForm,
    details,
    sales,
    isFormOpen,
    selectedSaleItem,
    thumbnails,
    thumbnail,
    invoice
});

export default orderReducer;