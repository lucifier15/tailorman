import * as types from 'types';
import {
	combineReducers
} from 'redux';
import _ from 'lodash';

import { fabricDesignDependency } from 'util';

const workorder = (
	state = {
		workflowStages: [],
		accordionKey: ['0'],
		orderItems: [],
		workorders: [],
		workflows: [],
		selectedWorkflow: -1,
		stage_id: -1,
		sortedBy: 'order_item_id',
		sortOrder: 'desc',
		selected: {},
		isListItemMenuOpen: false,
		stageList: [],
		selectedWorkOrderItem: false,
		query: []
	},
	action
) => {
	switch (action.type) {
		case types.TOGGLE_WORKORDER_LIST_ITEM_MENU:
			return { ...state, isListItemMenuOpen: action.isOpen }
		case types.SELECT_WORK_ORDER_ITEM:
			return { ...state, selectedWorkOrderItem: action.order_item_id }
		case types.SELECT_WORKFLOW:
			return { ...state, selectedWorkflow: action.workflow }
		case types.TOGGLE_WORKORDER_ACCORDION:
			const _accordion = state.accordionKey;
			return {
				...state,
				accordionKey: action.key
			}
		case types.WORKORDER_SAVE_QUERY_FIELDS:
			return { ...state, query: action.query }
		case types.UPDATE_WORK_ORDER_ITEM_FABRIC_DESIGN:
			const selected = state.selected;
			selected.order_item.design.comment = action.comment;
			selected.order_item.design.fabric_design = fabricDesignDependency(action.fabric_design);

			return { ...state, selected: selected };
		case types.POPULATE_WORKFLOW_STAGE_LIST:
			return {
				...state,
				workflowStages: action.workflowStages
			}
		case types.POPULATE_APPLICABLE_STAGE_LIST:
			return {
				...state,
				stageList: action.stageList
			}
		case types.POPULATE_WORK_ORDERS:
			return {
				...state,
				workorders: action.workorders
			}
		case types.FETCH_LIST_WORKFLOW:
			return { ...state, workflows: action.workflows }
		case types.POPULATE_WORK_ORDER_ITEM_DETAILS:
			return {
				...state,
				workOrderItem :_.cloneDeep(action.details.order),
				workOrderItemSales : _.cloneDeep(action.details.sales)
			}
		case types.POPULATE_ORDER_ITEM_DETAILS:
			return {
				...state,
				selected: _.cloneDeep(action.details)
			}
		case types.WORKORDER_UPDATE_MEASUREMENTS:
			return {
				...state,
				selected: _.cloneDeep(state.selected)
			}
		case types.SELECT_WORK_ORDER_STAGE:
			return {
				...state,
				stage_id: action.stage_id
			}
		case types.SORT_WORK_ORDERS:
			if (state.sortedBy == action.sortedBy && state.sortOrder == 'asc')
				return { ...state, workorders: _.reverse(state.workorders), sortedBy: action.sortedBy, sortOrder: 'desc' }
			else
				return { ...state, workorders: _.sortBy(state.workorders, [action.sortedBy]), sortedBy: action.sortedBy, sortOrder: 'asc' }
		case types.WORK_ORDER_UPDATE_SALE_ITEM_ENTRY :
			let new_state_Selected = _.cloneDeep(state.workOrderItem);
			new_state_Selected[action.payload.field] = action.payload.value;
			return {
				...state,
				workOrderItem : new_state_Selected
			};
		default:
			return state;
	}
};

export default workorder;