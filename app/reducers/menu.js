import * as types from 'types';


export default function menu(state = {
  isOpen: false
}, action = {}) {
  switch (action.type) {
    case types.TOGGLE_HAMBURGER_MENU:
        return {...state, isOpen: action.status};
    default:
      return state;
  }
}
