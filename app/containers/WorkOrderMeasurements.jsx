import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from 'css/components/workorder';
import ImageViewer from './ImageViewer';
import {
	push
} from 'react-router-redux';
import { fetchList, clearList } from '../actions/list';
import _ from 'lodash';
import MeasurementsForm from 'components/MeasurementsForm';
import {
	getWorkOrderDetails,
	updateWorkOrderItemStage,
	getWorkOrders,
	updateProfile
} from 'actions/workorder';
import {
	updateMessage,
	saveProfile
} from 'actions/order';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import back from 'images/back-arrow.png';
import Slider from 'react-slick';
import * as types from 'types';

const cx = classNames.bind(styles);

class WorkOrderMeasurements extends Component {
	constructor(props) {
		super(props);
		this.closeForm = this.closeForm.bind(this);
		this.loadFields = this.loadFields.bind(this);
		this.saveMeasurements = this.saveMeasurements.bind(this);
		this.showMessage = this.showMessage.bind(this);
		this.renderCarousel = this.renderCarousel.bind(this);
		this.handleViewerClose = this.handleViewerClose.bind(this);
		this.onImageButtonClick = this.onImageButtonClick.bind(this);
		this.afterChange = this.afterChange.bind(this);
		this.state = {
			images: [],
			activeImageIndex: 0,
			imageViewer: false
		}
	}
	handleViewerClose() {
		this.setState({
			imageViewer: false
		});
	}
	onImageButtonClick() {
		this.setState({
			images: this.props.workorder.selected.images,
			imageViewer: true
		});
	}
	afterChange(activeIndex) {
		this.state = {
			activeImageIndex: activeIndex,
		}
	}

	componentDidMount() {
		const workorder = _.find(this.props.workorder.workorders, { order_item_id: parseInt(this.props.params.order_item_id) });
		this.props.dispatch(fetchList('measurement_type'));
		// this.props.dispatch(getWorkOrderMeasurementDetails(workorder.order_id, workorder.order_item_id, workorder.profile_id, this.props.params.type));
		this.props.dispatch(getWorkOrderDetails(workorder.order_id, workorder.order_item_id, workorder.profile_id, this.props.params.type,false,workorder.cusomer_id));
	}

	saveMeasurements(value, updateFields) {
		if (value.profile) {
			const workorder = _.find(this.props.workorder.workorders, { order_item_id: parseInt(this.props.params.order_item_id) });
			value.profile.customer_id = workorder.cusomer_id;
			value.profile.item_type_id = workorder.item_type_id;
			if (this.props.params.type == 'default') {
				if (parseInt(this.props.params.order_item_id) === parseInt(this.props.workorder.selected.order_item.order_item_id)) {
					if (parseInt(this.props.workorder.selected.order_item.profile_id) === parseInt(workorder.profile_id)) {
						value.profile.profile_id = 0;
					}
				}
			}
			saveProfile(value.profile, value.measurements).then(function (profile_id) {
				workorder.profile_id = profile_id;
				const stage = _.find(this.props.workorder.stageList, { stage_id: workorder.current_stage_id });
				workorder.stage_id = workorder.current_stage_id;
				if (this.props.params.type == 'default') {
					updateWorkOrderItemStage(workorder, workorder, value.profile.comment, this.props.user.user.user_id, workorder.current_stage_id).then((status) => {
						this.props.dispatch(getWorkOrders.apply(this, this.props.workorder.query));
						this.props.dispatch(push('/workorder'));
						this.props.dispatch(updateMessage('UPDATE_GENERAL_MESSAGE', 'Measurements Updated'));
					});
				} else {
					this.props.dispatch(getWorkOrders.apply(this, this.props.workorder.query));
					this.props.dispatch(push('/workorder'));
					this.props.dispatch(updateMessage('UPDATE_GENERAL_MESSAGE', 'Measurements Updated'));
				}
			}.bind(this));
		}
		if (updateFields) {
			this.props.dispatch(updateProfile());
		}
	}

	showMessage(type, value) {
		this.props.dispatch(updateMessage(type, value));
	}

	loadFields(measurement_type_id, clear) {
		if (this.props.params.type == 'default')
			this.props.dispatch(fetchList('measurement_field', { item_type_id: this.props.workorder.selected.order_item.item_type_id, measurement_type_id: measurement_type_id, clear: clear }));
		else {
			if (!clear) {
				this.props.dispatch({
					type: types.FETCH_LIST_MEASUREMENT_FIELDS,
					measurement_fields: this.props.workorder.selected.order_item.profile.measurements.map((field) => {
						return {
							code: field.code,
							category: null,
							measurement_type_id: field.id
						}
					})
				});
			} else {
				this.props.dispatch({
					type: types.FETCH_LIST_MEASUREMENT_FIELDS,
					measurement_fields: []
				})
			}
		}

	}

	closeForm() {
		this.props.dispatch(push('/workorder'));
	}
	renderCarousel() {


		if (this.props.workorder.selected.images && this.props.workorder.selected.images.length > 0) {
			var settings = {
				dots: true,
				infinite: false,
				speed: 500,
				slidesToShow: 1,
				slidesToScroll: 1,
				adaptiveHeight: true
			};
			return (
				<div className={'viewer-wrap'} >
					<div className={cx('react-viewer-icon', 'viewer-button', 'viewer-fullscreen')}
						onClick={this.onImageButtonClick}>SHOW</div>
					<Slider afterChange={this.afterChange} {...settings }>
						{
							this.props.workorder.selected.images.map((image, index) => {
								return (
									<div className={cx('image-container')} key={index}>
										<img src={image} />
									</div>)
							})
						}
					</Slider>
				</div>
			)
		} else {
			return (
				<h3> No Images Found</h3>
			)
		}
	}
	render() {
		if (this.props.workorder.selected.order_item) {
			return (
				<div>
					<div className={cx('half')}>
						<div className={cx('container')}>
							<h1>Update Measurements </h1>
							<Link to="/workorder" className={cx('back')} ><img src={back} /></Link>
							<MeasurementsForm
								lists={this.props.lists}
								loadFields={this.loadFields}
								close={this.closeForm}
								save={this.saveMeasurements}
								profile={this.props.workorder.selected.order_item.profile}
								message={this.showMessage}
								is_source_disabled={(this.props.params.type == 'default') ? false : true} />
						</div>
					</div>
					<div className={cx('half', 'stick-right', 'slick-wrapper')}>
						<h1>Images </h1>
						<ImageViewer 
							visible={this.state.imageViewer} 
							onClose={this.handleViewerClose} 
							images={this.state.images} 
							activeIndex={this.state.activeImageIndex} />
						{this.renderCarousel()}
					</div>
				</div>
			);
		} else {
			return null;
		}

	}
}

WorkOrderMeasurements.propTypes = {
	workorder: PropTypes.object,
	user: PropTypes.object,
	lists: PropTypes.object
};


function mapStateToProps({ workorder, user, customer, lists }) {
	return {
		workorder,
		user,
		customer,
		lists
	};
}

export default connect(mapStateToProps)(WorkOrderMeasurements);