import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import {
	push
} from 'react-router-redux';

class Reset extends Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		this.props.dispatch({
			type: 'CLEAR_DATA'
		});
		this.props.dispatch(push('/stores'));
	}

	render() {
		return null;
	}
}

Reset.propTypes = {
	user: PropTypes.object
};

// Function passed in to `connect` to subscribe to Redux store updates.
// Any time it updates, mapStateToProps is called.
function mapStateToProps({ user }) {
	return {
		user
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(Reset);