import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import styles from 'css/components/stores';
import { fetchStoresList, selectStore } from '../actions/users';
import _ from 'lodash';
import back from 'images/back-arrow.png';

const cx = classNames.bind(styles);

class Stores extends Component {
	constructor(props) {
		super(props);
		this.showStores = this.showStores.bind(this);
		this.selectStore = this.selectStore.bind(this);
	}
	componentDidMount() {
		this.props.dispatch(fetchStoresList(this.props.user.user.stores));
	}
	showStores() {
		var _that = this;
		return this.props.stores.list.map(function (store) {
			return <div className={cx('option')} key={store.store_id} onClick={_that.selectStore} rel={store.code}>{store.address}</div>;
		});
	}
	selectStore(event) {
		const _store = _.find(this.props.stores.list, { code: event.target.attributes.rel.value });
		this.props.dispatch(selectStore(_store));
	}
	render() {
		let backLink = null;
		if(this.props.user.user && this.props.user.user.roles && this.props.user.user.roles.workflow && this.props.user.user.roles.workflow.length > 0){
			backLink = 	<Link to='/landing' className={cx('back')} ><img src={back} /></Link>

		}
		return (
			<div className={cx('container')}>
				{backLink}
				<h1>Select Store</h1>
				{this.showStores()}
			</div>
		);
	}
}

Stores.propTypes = {
	user: PropTypes.object,
	stores: PropTypes.object
};

// Function passed in to `connect` to subscribe to Redux store updates.
// Any time it updates, mapStateToProps is called.
function mapStateToProps({stores, user}) {
	return {
		stores,
		user
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(Stores);