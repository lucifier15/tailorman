import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import Helmet from 'react-helmet';
import {
    push
} from 'react-router-redux';
import styles from 'css/components/workorder';
import _ from 'lodash';
import { connect } from 'react-redux';
import back from 'images/back-arrow.png';
import $ from 'jquery';
import QueryBuilder from 'components/QueryBuilder';
import QueryResult from 'components/QueryResult';
import Collapse, { Panel } from 'rc-collapse';
import moment from 'moment';
const cx = classNames.bind(styles);
import {
    getQueryResults,
    getDashboardQueryFilters,
    setDashboardActiveKey,
    getQuerySave,
    getUserDashboardQuerys
} from '../actions/dashboard';

const QueryResultGridColumns = [{
    displayName: 'Item',
    dataIndex: 'order_item_id',
    column_width: 'small'
}, {
    displayName: 'Customer',
    dataIndex: 'm_customer_name',
    column_width: 'medium'
}, {
    displayName: 'Order',
    dataIndex: 'order_id',
    column_width: 'small'
}, {
    displayName: 'SKU',
    dataIndex: 'supplier_product_code',
    column_width: 'medium'
}, {
    displayName: 'Mrp',
    dataIndex: 'mrp',
    column_width: 'small'
}, {
    displayName: 'QTY',
    dataIndex: 'qty',
    column_width: 'small'
}, {
    displayName: 'Priority',
    dataIndex: 'm_priority_type_code',
    column_width: 'medium'
}, {
    displayName: 'Customer ID',
    dataIndex: 'customer_id',
    column_width: 'medium'
}, {
    displayName: 'Status',
    column_width: 'medium',
    dataIndex: 'b_workflow_stage_code'
}, {
    displayName: 'Store',
    column_width: 'medium',
    dataIndex: 'address'
}];
class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.queryStringSearch = this.queryStringSearch.bind(this);
        this.toggleAccordion = this.toggleAccordion.bind(this);
        this.queryStringSave = this.queryStringSave.bind(this);
    }
    toggleAccordion(activeKey){
        this.props.dispatch(setDashboardActiveKey(activeKey));
    }
    queryStringSearch(query_string) {
        this.props.dispatch(getQueryResults(query_string));
        this.props.dispatch(setDashboardActiveKey(['1', '0']));
    }
    queryStringSave(query_string,query_name){
        let user_id =this.props.user.user.user_id;

       this.props.dispatch(getQuerySave(query_string,user_id,query_name));
    }
    componentDidMount() {
        this.props.dispatch(getDashboardQueryFilters());
        this.props.dispatch(getUserDashboardQuerys());
    }
    getQueryBulderView() {
        if (this.props.dashboard.queryfilters) {
            return <QueryBuilder
                completeQueryString={this.queryStringSearch}
                completeQueryStringSave={this.queryStringSave}
                queryfilters={this.props.dashboard.queryfilters}
                userqueries={this.props.dashboard.userqueries}
            />
        } else {
            return;
        }
    }
    render() {
        return (

            <div className={cx('container', 'big')}>

                <Helmet
                    script={[{
                        src: "https://unpkg.com/jQuery-QueryBuilder@2.4.4/dist/js/query-builder.standalone.js",
                        type: "text/javascript"
                    }]}
                    link={[{
                        href: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
                        rel: "stylesheet",
                        type: "text/css"
                    }, {
                        href: "https://unpkg.com/jQuery-QueryBuilder@2.4.4/dist/css/query-builder.default.min.css",
                        rel: "stylesheet",
                        type: "text/css"
                    }]}
                />
                <h1>Dashboard</h1>
                <Link to="/landing" className={cx('back')} ><img src={back} /></Link>
                <div className={cx('form-container', 'big')}>
                    <Collapse
                        accordion={false}  
                        onChange={this.toggleAccordion}
                        activeKey={this.props.dashboard.accordionKey}
                    >
                        <Panel header="Query Builder">
                            {this.getQueryBulderView()}
                        </Panel>
                        <Panel header="Query Results">
                            <QueryResult
                                queryresult={this.props.dashboard.queryresult}
                                columns={QueryResultGridColumns}
                            />
                        </Panel>
                    </Collapse>
                </div>
            </div>
        );
    }
}
function mapStateToProps({ dashboard,user }) {

    return {
        dashboard,
        user
    };
}
export default connect(mapStateToProps)(Dashboard);