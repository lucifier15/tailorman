import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import styles from 'css/components/customeractions';
import { fetchList } from 'actions/list';
import { selectOrder } from 'actions/order';
import SelectForm from 'components/SelectForm';
const cx = classNames.bind(styles);
import back from 'images/back-arrow.png';

class CustomerActions extends Component {
    constructor(props) {
        super(props);
        this.selectOrder = this.selectOrder.bind(this);
    }
    componentDidMount() {
        this.props.dispatch(fetchList('orders', { customer_id: this.props.customer.customer_id }))
    }
    selectOrder(selectedOrder, type) {
        if (type == 'order') {
            var _order = _.find(this.props.lists.orders, { order_id: selectedOrder.value });
            this.props.dispatch(selectOrder(_order, true));
        }
    }
    render() {
        return (
            <div className={cx('container', 'customer-actions')}>
                <div className={cx('header-note')}>
                    <span className={cx('header-label')}>Customer:   </span>
                    <span className={cx('header-content')}>{this.props.customer.name}</span>
                </div>

                <h1>Orders</h1>
                <Link to="/customer" className={cx('back')} ><img src={back} /></Link>

                <div className={cx('form-container')}>
                    <Link to="/order" className={cx('option')} >
                        New Order
				    </Link>
                    <Link to="/order/alteration" className={cx('option')} >
                        Order Alteration
				    </Link>
                    <div className={cx('input-group')}>
                        <label htmlFor="select_order">Edit Order</label>
                        <SelectForm type="order" rel="order" options={this.props.lists.orders} save={this.selectOrder} />
                    </div>
                    <Link to="/order/history" className={cx('option')}>
                        Order History
				</Link>
                </div>
            </div>
        );
    }
}

CustomerActions.propTypes = {
    user: PropTypes.object,
    customer: PropTypes.object,
    lists: PropTypes.object
};


function mapStateToProps({lists, customer, user}) {
    return {
        customer,
        user,
        lists
    };
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(CustomerActions);
