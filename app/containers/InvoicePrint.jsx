import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/invoice';
import _ from 'lodash';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import moment from 'moment';
import numeral from 'numeral';
import { getInvoiceDetails, getUpchargedTotal } from 'actions/order';

const tailormanLogoDataURL = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAYAAABS3GwHAAAgAElEQVR4nO19d1wU1/r+Q5e2UgWEgIUmimI31iRqNGKJhliiiYqJXstN8cZoEnM10ZuiiTFqvErUJGLFaOyFYozYb+yiBBAFRUWa9Lq83z/47fz2zOxSd3YWmOfzeT+7Z+bMmfeU55wzp7zHyMPDg/Ly8iBDRnODQqGAEQCSWhEZMqSCsUKhkFoHGTIkgUKhgLHUSsiQISVkAsho1pAJIKNZQyaAjGYNmQAymjVkAsho1pAJIKNZQyaAjGYNmQAiYN68eTh//jxiY2MxZsyYBoX11ltv4fz58zh//jyCg4N1pKEMDgqFglC1HEIWHUhISAjxsWnTJrKysqpTODY2NhQeHi4Iy8/PT/I4NhVRKBQkE0DHUl5eLii0RET379+noUOH1iqMESNG0IMHDzSGc/r0acnj2FREJoCOJTQ0lAoLC6l79+40ePBgOnTokKAAL1++vNowVq9eLXjml19+oZ49e9Lo0aOJiKht27aSx7UpiEwAHcvu3btp/fr1zLUpU6ZQRkYGU6Cjo6OpdevWjL9u3brRtWvXGH+JiYk0fPhwxt+pU6fo3XfflTyuTUEUCgXJH8E6Rm5uLuPetm0bOnTogO3bt3PXBg8ejLi4OAwfPhwAMGfOHFy+fBldunTh/GzYsAEBAQE4fvw4E15iYiKcnZ1FjEEzg9wC6E42bdpEe/bs0Xr/zTffpOLiYqaWv3r1KuPOzMyksWPHag1j3759NHfuXMnj2hRE7gLpWF588UUiInJzcyMANGnSJNq1axdToH19fenChQsaP3CPHDlCLi4unN8FCxbQ4cOHqW/fvgSA+vXrR5WVleTu7i55XJuCyAQQQe7cuUNEVUOf6mjZsiXjLyUlhbm/du1a5n6fPn24e2VlZbRu3ToiIoqIiJA8jk1FZAKIIK1ataLMzExBt8bExIQAkJubG4WHh1NRURHj5+LFizRy5EgunM6dOwtaiFu3bkkev6YkMgFEEmNjYwoNDaUdO3bQ5s2byc7OjgDQhAkTKCcnR2P3R4Vt27aRvb09AaCXXnqJwsPDKSIigmbPni15vJqayATQk7Ro0YJ27dpVbcFXR3p6Ok2ZMkVyvZu6yAQQWczNzWnp0qVUVlamtbCXlJRovXfixAnq1auX5PFoqiITQESZPXu24ENXHVFRURQUFEStW7emtWvXVtsibN68mXx9fSWPU1MTmQAiyLRp0+j27dvVdm9mzJgheG7QoEH0v//9T+tzSqWS1q1bJy+G06HIBNCRWFhY0KxZsyg+Pr7amvzbb7/lPoi1ybx58wRLJzS1CH369JE83o1dZAI0UNzd3Wnp0qWUlpZWbYH99ddf61Rz29nZ0YoVK6iioqLacE+cOEETJkwgU1NTydOiMYpMgHrK8OHDaceOHVRaWlptAd2zZw/17NlTazghISE0f/58srGx0Xi/ffv2ggk1TUhJSaFly5bJ3aM6ikyAOoivry8tXLiQrl+/XmOBjIiIoOeff77ahN++fTvn/+HDhzRu3Dit/gMCAmpFBCKiyMhIevvtt8nJyUnyNDN0kQlQg3h7e9O7775LMTExNRa8yspK2rRpE/Xo0aPaMCdNmqS1yxQeHk6urq5an/X396fVq1dTfn5+jfrk5eVRREQETZw4kRwcHCRPS0MUmQAapEePHrRo0SL6888/a1XjpqWl0Zdffkk+Pj7VhhsUFESHDx+uMbxnz57VuN7fzc2NPvvssxo/utXD3Lt3L82YMYPatGkjeRobisgEAKht27Y0adIkCgsLq3WBIiKKjY2lWbNmkbW1dbXhu7u71zjOrwnXrl2jUaNG1aj/+PHj6dixY7UOV6lU0unTp2nZsmU0ZMgQas753+wIYG1tTV26dKHQ0FAKCwujy5cv16lQPnz4kFatWkXdu3ev8V0ODg60bNmyWnVXqsOJEydo0KBBNb7P39+fFi9eTFeuXKlT+NnZ2RQZGUlLliyhUaNGCXaqNWVRKBRkpFAomuQJMY6OjvD09ERQUBD8/f3RvXt3BAYGolWrVnUKJycnB8ePH8f+/ftx4MABlJaWVuvfyckJ7777LubOnQsHB4eGRIHBwYMHsWbNGsTExNTot1+/fhg7diyCg4Ph7+9fp/colUrcuHED8fHxuHr1KuLj43Hnzh2kpqairKysvuobJBQKBRo1AZydneHi4gIXFxf4+fmhdevWCAgIgJeXF3x9fWFjY1OvcB8+fIiYmBgcPXoUJ06cEGxz1ARfX1/MmTMH06dPh5iHjkRFRWHjxo3Yu3dvrfz37dsXw4YNw+DBg9GvX796vzctLQ0pKSlITk5GUlISHj16hHv37iE9PR2ZmZnIzMxEeXl5vcOXAgZLAG9vbwQGBsLT0xO2trawtLSEra0t7Ozs4OTkBBcXF9jZ2cHFxQXW1tYNfl9JSQkuXLiAM2fO4OTJk4iNjUVFRUWtng0ODsbUqVPx+uuvN1iPuuD27dsICwtDREQEHj9+XKtn2rRpgxdeeAEDBgxAv3794OfnpxNdSktLkZmZiZycHDx79gzZ2dnIyclBbm4uCgoKUFpailu3biEmJqZWlYm+oFAoDGtPcOfOnenAgQMN6jPXBhkZGRQdHU1ffPEFjRo1qtqhR03Srl07+vDDD2s1JyA2iouLadeuXTRq1CgyMjKqUzwCAgJo6tSptHbtWjp79myDv1dqQlZWFn3yySeSlzOVGNQ3wLRp0/Dzzz/rNMzy8nKkpqYiMTERd+7cwc2bN3HlyhUkJycjPz+/TmF5eHjglVdewbhx4zBs2DAYGRnpVFdd4N69e9i3bx8OHDiA2NjYOj/v6OgIb29vdOnSBR06dICfnx98fHzg5eUFMzMznekZHR2NYcOGobKyUmdh1gcG0wV69dVX8fvvvwMA8vPzYWtrW63/yspKFBUVobCwEFlZWVw/NCMjA8nJyUhLS8P9+/eRkpKCJ0+egIjqpZe/vz+GDBmCESNGYPDgwTA3N69XOFIgISEBkZGRiIqKwtmzZ5GVlVXvsFxdXdGmTRu0bdsWrq6uaN++PVxdXbnuqJOTE6ytrWFpaVltOLm5uVWFzsgI0dHRGDp0aL110gUkJYCbmxtatWoFJycnHD9+HKamppg5cyZ69uyJd955h/O3a9cu/Pe//8WzZ88AVI1SFBUVITc3F8XFxSguLtaZTq1atULv3r0xaNAgDBo0CD169NBZ2FIiPz8fZ8+exenTp3Hu3DlcuXKlzi1gdbC2toa1tTUcHBxgaWkJS0tLODk54bXXXsNbb73F+UtLS8OECROwY8cOeHp64rvvvsOFCxeQlZWFa9euIScnR2c61QaSfAPMmDGDzp07J+gfHj16lADQ0aNHmetBQUGi6GFubk7+/v40efJk+v777yk2NlawUb2pIjMzk6Kioug///kPjR8/nnx8fERZURoQEMC8t7y8nABQz549BToplUoKDw+nbt26Nc1vAC8vL+zatQt9+vThrhUVFcHKygoAEBgYiFu3biE+Pp4ZnVBdry+srKzg6uoKDw8PdOzYEd7e3ujcuTP8/f3h4eFR/wg1MaSmpiIhIQFxcXFISkpCfHw814Wsb2vRoUMH3L59m3MXFRXByckJxcXFXD4XFBQgJSUFHTt25PwtWLAA3377bYPjVBMUCgVMRX8LAHd3d1y9ehX29vbIzMzEkiVL8Ntvv2Hp0qWYPXs2CgsLuUJuYWHBPadUKrmuDx8WFhZwcXGBvb09nJyc4OrqCjc3N9jb28PDwwPu7u6wt7eHu7s7XFxc9BHNRg1PT094enpiyJAhzPXMzEykpqYiIyMDqampSEtLQ0ZGBp48eYKMjAxuqDM7O1uQV48ePWIqOXNzc9jZ2aG4uBh//fUX/Pz8kJ2djU6dOsHX1xeffPIJpk6dipUrV8LS0hLLli0TP+L66AKptgheuHCBM/kBVJn5I6paYqC6pr4bKjs7m1lrY2xsTNu2baMbN27Qo0ePSKlUithRkFEXZGdn0+XLlwVdmKSkJMaft7c3AaANGzYQEVFpaSmzS27mzJmc3+qWlOtC9GIcd8aMGejQoQOePn2K/v37Mx86KiOvqppDoVCgZcuW3P2cnBwUFhZy7pYtW2Ly5MkIDAyEm5sbjI1l276GAnt7e3Tr1g0DBw5krvMn6VTLQ7KzswFUtQqtW7fm7oeFheH7778HAGzatElMlQHo4YikBQsWAAA+/PBDweyqKjFUQ3QODg7MeLMqkVSwtbWt95CmDP2APxT65MkTxu3k5ASgqmulgr29PeNn/vz5KCgoQEBAAIKCgkTStAqiEsDd3R1+fn5QKpXYvXs3c8/U1FRQG/AXj/GHxWxsbAxyAkrG/wffdHt6ejrjdnV1BQBmXsLOzk4QzoEDBwAAL730kq5VZCAqAby9vQEA8fHxgpWE9vb2XHdHRQD+Sk1+C6ApoWQYFvh5+OjRI8bt7u4OgCWAqlVQh2pQ5LnnntO1igxEJYCJiQkAaFwlaG9vzzWXqsTg1x782UuZAIYPPgH4XSBVf189bx0dHQXhqMqO2F1eUQlw7949AFVLCvgfrOr9PlVi8BNCvZ8IyARoDODnIb8L5ObmBqBmAqhm4ZOSknStIgPRCZCSkoIWLVpg5MiRzD11AmRkZAAQNoWq6yrIBDB8ODg4MN9pT58+Ze6rCJCens51i/kEsLGx4cpLZGSkmOqKPwqkmtFbtWoVc1090qqCzu8CyS1A44OTkxMzlM1vAVR5nJuby+0N4BNgy5YtMDU1RUxMTONuAQDgxx9/RGpqKtq3b8992QNgNrLcv38fAAQ7qWQCND7Y2NgIWnf1wQz1Sk7VDVInzMqVK7nNRW+//bbY6opPACJC//79UVZWhtGjR+PGjRvo378/1xSWl5ejsLAQffv2xQsvvMA8K38EN06od2WLi4uZD2EbGxt899136NChA3fd3t4ewcHBOHnyJD788EMAVTvtVBWjqBBzKYSvry916tSJAJCrqyvFxcVx09wqu/ilpaUazYhXVlbSc889x4T322+/6XwKX4buERwczOTbqVOnNPpT7UCrrKzkrqWnp9OAAQMIAPXv359CQkJEOxRQNLMogYGBFBsby0Xq3LlzZG5uTgAoLCysVonIXwcEgP7444/654oMvWHatGlMvkVERNTquejoaG5d0JEjR5h7y5YtaxwEcHBw4Jj9+PFjevr0KRGxB7ypFsFVh4SEBEHY/JPUZRgmFixYwOTbDz/8UOMziYmJAv+FhYXM/pA333xT5wTQ+XLoyZMnw8bGBgcOHEB4eDhWrlwJZ2dndOzYEY6OjsjKysJ3332HsWPHVhsOfxbYyMiI+VjSNYqKivDzzz+joqKCm4RRvdfU1BQmJiY1/qr+8918P8bGxjA1NYWxsTFMTExgZGTE/BIRlEoljIyMoFQqUVlZyfxXKpVQKpWoqKhAeXk5KioquP/arqv+q9yFhYUYO3YsvLy8dJ6WNU2GaUJUVBT3v2fPntz/ixcv4vfff0dYWBjef/99hIeH605RQPffACr2Dh06lDEJeOfOHc7P119/zV3Pzc2lsWPH0pIlS5ga4fDhw0y4LVu2pNzc3AbUS9VDiiNIjYyMyNTUlMzMzKhFixZkaWlJtra2ZGNjQ+bm5qLb/d+6dasoabl161bmPdOnT2fuHzlyhAICAmjLli3ctfv373P+//nPf3LXS0pKuCNjHz9+bPgtwIMHDwAA48aNw5o1a/Dqq6+iqKiIY3VwcDAWLlwIoMoeT8+ePZGQkAD+rjT+JJi9vb2oBqeksFdDRNwK2cZmVKo68FsA/lxARkYGbt++jdDQUJiYmOCtt96Cl5cXDhw4gDFjxmDt2rWwtbXF6NGj8fHHH6N79+4AgMTERJ3rqnMCbN++HStXrsQ//vEPpKSkYNKkSYiLi4ONjQ2WLFnCDXMBVSv9EhISAICLpAr8RNO0YEqX4He5agM3Nzd4eXnBzs4OrVq1gr29PWxtbWFtbY0WLVrAwsICFhYWMDMzg4mJCebPny+IlzZMnz4dISEhMDc358JS/ZqamkKpVCI7Oxv37t1Damoqrl69ips3byIuLq7W+qvvtdAlaloO4evry/2fOnUqHBwcMHLkSG6YfMWKFQgLC0NUVBQ6dOiALVu2AAC++eYb3SsrxihQSEgI14Tl5eUJDo17/Pgx9evXj3lm1apVjJ/58+cz90eMGKGrFlojfv3111rFbezYsbRjxw5KSkqi4uLiOr2jLic97tmzp17xuHHjBn322WfUtm3bGt/xzTff1OsdNSE5OZkx0uXh4cEMdWoa4Ni4cWO1YS5fvlzn5VRU69B9+/alqKgoJhLZ2dm0evVqjQc27Ny5k/HL/+IPDQ3VXQ5pwKpVq6qNT58+fTRas6gLalMoVbJu3boGvau8vLzGQrNw4cIGvUMbcnNzGbPrFhYWlJ6ezt1/9uyZxmOhRo4cSTExMczZaDExMYJ5BV0SQLRN8efOncPQoUPh4+ODTp06obCwEJcuXWI2ThsZGXHLXVUbJVTgL4Pg39c1qrNJExoais2bN4v6fl3D1NQUn376KQYOHIhhw4ZptJ8klh0ehUIBe3t77ruutLQUT5484b4NWrZsCUdHRxQUFDDPHT58GIcPH4a7uztcXV3x+PFjwX4CXUN0qxCJiYlaP15Iba03fyEcfxWh+r5RMaDNctqUKVMaXeFXx4ABA3D69GlmaFEFMQ1ROTk5ISUlhXNrWhSnfl8daWlpSEtLE003dRjErvIWLVoIRg74LYDYNnw0EcDHx0f3484SoEePHoLVuIC4I1/8/OQX6Lqe0yAWDIIATk5OzChPbm6uYFRG7C6QJvtDERERor5Tn/jggw/Qrl075lp9Rr5qC36Lzu/KyARQg7OzM7OJIiMjg7FGZmxsLLpxKz4BRo8eLbpFAn3jq6++YtxZWVmibTnkE4BvHkXTLjApYBAEqGkzvIODA7d8WizwJ+L+/e9/i/o+KTB+/HjBenz+h6iuwM9TPgHEntepLQySAPxZYGdnZ8Zkoq5RVlbG9Id9fX0FE3NNBRMmTOD+5+fnN8hsenXg5yl/UEMmgBr43Rt+Yond/SkoKGC6QK+99pqo75MSo0eP5v4TkWgEqGlUTyaAGmqqLcQmQF5eHoqKijj34MGDRX2flHj++ec5Y7WA9uHfhoJfwHNycpgTNvnW4KSCQRCgJmsQYo8AqRcCCwuLJtv9Aaq2JHbq1Ilz13ZtUl3Bt/KnOjxPhZpOAdIXDIIA/MTiE0DsITP19/n7+zf5vcfqewDEJIC64YOysjKmmykTQA38jS58AojdX1RvAVTmHJsy1LuUYhHAzs5OsHxdfWjb2tpa1IGN2sIgCMCvDfizwGITQH0IVGxblIYA9SFlsQhgYmIiqNjUR9pqc6iePmAQBOAfds1foyLmVkgAzEKx5kAAlYFaQLyPYEBo50l9zsHCwsIgTt2UnAAmJiZMTVBaWio4k8rGxkZUHdTD79u3r6jvMgSon9MWHx8PpVIpynv4FZt6RWNmZiYTAAC300mF/Px8wU4lsQkwcuRIODk5oVevXkzhaKrw8/Pj4pmcnCzagdX8Lo46AUxMTHR6+HZ9oZdD8qqDymKCCiUlJcx4sZGRkeh9xdatWyM5OdlgRib0gb179+Kzzz7D4MGDRSuI6hUbINz3rG59QypITgCVaRAVVCY8VFDtiRUbzanwA1WkF3ufA7+A87tahnDaj+RdIGNjY+bsgIqKCmaFosqWjozGB34B558RIVbXqy6QnAAqQ04q8BNJJkDTgXpXFxC2CFLA4Ahgbm7OJJSRkZF8HGojBf9UUPV8LS8vZ771pILkJau0tBQlJSWc28rKilmspTIDKKPxgb8RX/1brrS0VCYAUFVLqA97KhQKhgCGUlPIqDv4m23Uh7MLCws1WqrQNyQnAMAmlJmZGTMiIxOg8YJPAPWZ4YKCApkAKvBnftXX/hCRQSSUjLqDv81UnQD8PJcKBkEA/uI3/m4ifkLKMHyUlJQIzK6or+kS0yJFXWAQBKhp/b9MgMaHnJwcJt8UCgVjCULMRXh1gUEQgL8kl78i01ASS0btkZ2dzZ0DDFRtkKnu+FSpYBAE4FsN4xOAb1JDhuGD363l7+t++PChPtXRCoMgAD8x+Mf2GEpiyag9+DU837Sl2EZvawuDIADfSGrbtm2rvS/D8KE6KUgFQ63UDIIAqampzIZpLy8vxhJEamqqFGrJaAD4BODbJU1OTtanOlphEAQoLi7G3bt3ObexsTH8/f05d1JSkjwX0MjAP+Xdx8eH+5+enq438+c1wSAIAFRtzVNHx44duf95eXmiHJAmQzyoV2gAey5YUlKSYKGcVDAYAvAPd+vatSvj5hNEhuEiPz+f6eJ4eHgw3wB///23FGpphMEQ4MaNG4y7R48ejPvatWv6VEdGA3Dnzh3G1GRgYCCzOaYuJ1mKDYMhwOXLl5mdYJ06dWLWBP31119SqCWjHrh16xbj7ty5M+M2pMrMYAjw5MkT7sxgoGon2PPPP8+5L1261KQOk27KuHz5MuPu1asX91+pVOL69ev6VkkrDIYAAHDhwgXG3b9/f+5/bm4urly5om+VZNQDFy9eZNzqpmZu3bplUEtbDIoAZ8+eZdx8M+WnTp3SozYy6oPs7Gzme65jx47MCZ+XLl2SQi2tMCgC/Pnnn4y7e/fuzIRYdHS0vlWSUUecOXOG6aoOGjSIuX/u3Dl9q1QtDIoACQkJghnCYcOGcf9jY2NFO9NKhm4QExPDuF966SXGza/kpIZBEQAAoqKiGPeYMWO4/6WlpThx4oS+VZJRBxw9epT7b2ZmxnRjExMTce/ePSnU0gqDI8CRI0cY97BhwxjTiPv27dO3SjJqiTt37iApKYlzDxw4kDlsJDIyUgq1qoXBESA6OppZ92NlZYVRo0Zx7oMHD8rDoQaKvXv3Mm7+YYPHjh3Tpzq1gsERoLi4WJBQU6ZM4f4XFBTg0KFD+lZLRi2we/duxj127Fjuf2FhIU6ePKlvlWqEwREAECbkiBEjmFMFN23apG+VZNSA+Ph4Zgb4xRdfZEbwIiMjDXJFr0ES4PDhwwJb8uqtwLFjxwxmT6mMKvz888+Me+rUqYx7z549+lSn1jBIAhQVFeH3339nrs2ZM4dxy62AYSE8PJz7b2lpiddff51zl5SU4MCBA1KoVSMMkgAA8NNPPzFuf39/Zk3J+vXr9a2SDC04fvw4Y7hg4sSJjHnLQ4cOMatDDQkGS4BTp04J9gJ/9NFH3P9Hjx5h//79+lZLhgasWrWKcb/33nuMm1+ZGRQUCgUBMEhZuHAh8eHi4sLd79atm+C+IaNt27a1jvu6deukVrdWuHv3LqN3jx49mPuPHj2SvBxpE4VCQQbbAgBAWFiY4BSRRYsWcf+vXLlicGtLmhu++uorxr148WLGvXHjRn2qU3cYcgsAgLZu3crUKGVlZWRlZcXd79+/vz4rvAahqbUA2dnZZGJiwuns6ekp8KPeYhuaGHwLAABffvkl4zYzM8PChQs595kzZwxuiW1zwfLly5nDSz7//HPm/m+//Wb4w9WG3gIAoOjoaKZWKSoqohYtWnD3e/fura9Kr0FoSi1Abm4umZmZcfq6uLgI/HTp0kXyslOdNIoWAAA+/vhjxm1paclcu3jxokFOszdlLF68mFmTxf8WOHv2rEFtfdSKxtACAKCzZ88ytUtZWRnZ2tpy9319ffVV+dUbTaUFSE9PZ3T18PAQ+Bk4cKDkZaYmaTQtAAC8//77jNvMzAxff/01505ISMCWLVv0rVazxNy5cxn32rVrGffly5dx+vRpfapUfzSWFgAAnTx5UlDTeHp6cvetrKyotLRUH5VgvdAUWoDLly8zemqai+nfv7/kZaU20qhaAACYPXu24Jr6mqCioiLMmzdPnyo1O7zxxhuMm9/qxsbG4syZM/pUqWFoTC0AANq+fbugxhkyZAjj5+rVq/qoDOuMxt4C/PDDD4yO06ZNE/gJCAiQvIzUVhQKBTU6Ajg4OJBSqWQS/eHDh4wfb29vfZWJOqExE+DJkyeMftbW1lRYWMj42bZtm+Tlo64EaFRdIKDK7oz6cggAcHd3ZybMkpKSsHTpUj1r1rQxfvx4xr1582ZmxadSqWyc3c/G1gKo5P79+4Jayt/fn/Fz8+ZNfVWQtUJjbQHWrVvH6Pbyyy8L/Lz//vsEgIyMjCQvG7WVRtkFUommNUC3bt1i/LRr104f5aPWqAsBfvzxR6nVJSLhak9TU1PKzs5m/MTHx0teHupLgEbXBVLhzJkzzC4koMoM3xdffMG5k5OTMWvWLH2r1qQQHBzMuHfu3MnszwaE3aNGhcbaAgAgc3NzysvLE9RaXbt2Zfzt3btXXxWmViiVSvLy8qp13L7//nupVaa3336b0WnKlCkCPytWrJC8HNRXGnUXSCXBwcGCTHn06BHTFzU2NqbU1FR9lBmtKCoqImdn51rHa9GiRZLqu3nzZkYfLy8vgZ/ExETJ87/ZEwAAbdu2TZA5u3fvZvxIvVYoIyODjI2Nax2nfv36SabrX3/9JdAnPj5e4K9Dhw6S571MAFTV8Onp6YIMmjVrFuPv9ddf10f50Yhjx47VOV7Xr1/Xu56ZmZnUsmVLRo/w8HCBvw8++EDyfJcJoCa9evXSmKFBQUGMv88//1zsMiRAbGxsnfr/KmndujWdOXNGr7p26tSJ0eG9994T+KkPmQ1RFAoFGSkUCsrLy0NTwKefforly5cz154+fYq2bdsyZjl27tyJiRMn6uy9hYWFOH/+PO7fv4/MzEzk5+ejpKQEmZmZiIuLExwZVFd07NgRfn5+cHR0hLOzM6ytrWFpaYnWrVtj9OjRjPHghiA4OJix7jx06FCBQduMjAx4enqipKREJ++UEgqFonGPAmmSyMhIQY0VGxsr8KermrWiokLS9S8RERE6iUdoaCgTrmHY0EoAAAjfSURBVI+PD1VUVAj88UfYGrM0uRYAACwsLHD//n3GLiVQZWFCfU7AzMwM165dQ0BAQIPeR0Q4duwYLl26hMLCQpibmwMATE1NYWxsDGNjY1hYWMDCwgJWVlawtraGtbU1rKysYG5uDmNjY5iYmMDExAQAUFlZiYqKCiiVSlRUVKCkpARlZWUoLS1FSUkJSktLkZmZiaysLLRp0waTJ09mTtOsD/71r38xtn0UCgX+/vtvQRqGhoYKTCA2ZjTJFgAAderUSWMtN3/+fMafvb09paWl6aQGbaxYvHixIP00fXyvWrVK8nzVtTSpj2C+TJo0SWOGv/rqq4w/d3d3yszMFLucGSQ+//xzQbrFxMQI/B05ckTy/JQJUA/54osvNGZ87969GX/t27cXrG9p6liyZIkgvfbt2yfwd/36dcnzUSZAA2TXrl2CTC0uLiZvb2/Gn4+PT7NpCRYtWiRIpx07dgj8PXjwgJpy+WgWBAA0j/g8ffqUWrduzfhr164dPX78WB9lUDLMmzdPkD6//PKLwF9ubi61adNG8ryTCaADMTc31ziVf+/ePcGsp4eHByUlJemjLOodkyZNqlXhLy8vp8DAQMnzTSaADsXZ2ZmePHkiyOy4uDiysLBg/NrZ2dHly5f1USb1guLiYho8eLAgTTR1e4iIevXqJXl+yQQQQdq0aUO5ubmCDL969aqABCYmJnTixAmxy6boSE1N1ThRd+DAAY3+BwwYIHk+yQQQUfz8/KigoECQ8VeuXCFzc3OB/82bN4tdRkVDbGws2dvbM/ExNzenU6dOafTfGKy5yQTQgQQGBgosGhBVLQPWRILFixeLXVZ1Dv56fgDUqlUrjfuky8rKqG/fvpLni0wAPUpgYKDG3WTXrl3TOPQ3YcIEfZRbnUDTUuWOHTtqnPXOy8ujbt26SZ4fUkizJgAA8vf3p4yMDEGhSEhIIDc3N4H/rl270r179/RQhOuH7OxsGjp0qEDvl19+mUpKSgT+09LSyM/PT/J8kEqaPQGAqg9jTYU6LS1N444nhUJBhw4d0kNxrhtOnjwpmNcAQLNnz9boPy4ujlxdXSVPfylFJsD/E0dHR43mFIuKiujFF1/U+MzSpUvFLtO1xtdff61RR20b6//44w+ytLSUPN2lFpkAamJmZiY4iUaFKVOmaHxmxIgRGucW9IXs7GwaPXq0QK+WLVtqHcLdsWOH5GltKCITQINomhkl0rxyEqiyVbp//34xy7lGREZGauzyVPedsnz5csnT15BEJoAW0da92b17t1bLDh988IGIxZ2FpjX8ACg0NFTrM/wdX7LIBKhWNBmBIqoyv6jNHEinTp0oNjZWrHJPN27c0LpMQZspxadPnzaaAyv0LTIBapCePXvSgwcPBIWqrKxM48IylXz00Uca99M2BNp2ZHl7e9PFixc1PnP27FmNw7myVIlMgFqIg4MDHTt2TGMB41tNVhc/Pz+tz9UFycnJNHz4cI3veOONN6ioqEjjcxs2bJA87QxdZALUQb766iuNBe3atWsC20PqMmPGjHpvtNm4cSNZWVkJwjQyMqINGzZofW7mzJmSp1djEJkAdZRx48ZRTk6OxkI3d+5crc85OztrHV3ShKSkJBoxYoTGsLp166bVYlxycnKzWcqsC5EJUA9xd3fXuHGciOjw4cPMqZV8CQ4OpoSEhGoL/5o1awRLs1WiyUqbCjt37pQnt+ooMgEaIJ999pnGglhQUCAwK64upqamtHbtWsFzDx48oJdfflnjMx4eHnTkyBGthV/TNkdZahaZAA2Uvn37UlxcnNbWoH379lqfDQ4Optu3b1NxcTFt3ryZOfVeXaZMmaJx1SoR0c2bN5vtSk5diEwAHcnq1as1FtCysjKBMS6+aBumtLW1pV9//VVrrb9+/XrJ493YRSaADmX48OGUkpKisbCeO3euThtOxowZo9Vi3bNnzygkJETy+DYFkQmgY7G2tqYtW7ZorbXXrl1LNjY2Wp+3tLSkn376SevzBw8elCe2dCgyAUSSkJAQrfaFUlNTBeYZAdCgQYPo7t27Gp8pLy+nOXPmSB6vpiYyAUQUe3v7avvwW7duJR8fH3JxcaGVK1dq9Xfy5Eny8fGRPD5NUWQC6EF69+5NycnJWgt4cXGx1ntyX19cadTnBBsyLCwsMHPmTISHh6Nfv3545ZVXsG3bNo1+W7RoIbh25swZeHt74++//8amTZuwc+dOTJ8+XWy1myfkFkC34u3tTaWlpUxNfvz4cQJA8+bN01rbExGVlpZya/09PT0F9zWddCNL/UXuAokg6enpVFBQQJs2bWIKr+p+q1atNBqmevToETk6OnL+1M8/zs7OpvXr1xNR1VIJqePYVEQmgI5l4sSJRETcmpxZs2bRrl27GFMlzs7OGq1VFxQU0KpVq8ja2przu2zZMjp48CB3cuPYsWOppKREYNBXlvqJTAAdy969e2nTpk1a78+ZM0dQ8K9du8a4Hzx4QMOGDdMaxokTJ+SPYx2J/BGsY5iYmOD27duC6x4eHti3bx9+/PFH7tqTJ0/Qr18/BAUF4dNPP2X8Hj9+HCtWrND4jqysLLi5uele+eYKuQXQnRw5ckRgeeGdd96h/Px8ppbfv38/OTg4MP4GDhxIiYmJjL+rV68K9vNevHiRJkyYIHlcm4LIXSAdy8cff0zPnj2joKAgGj58uEY7QwsXLtT6vLm5OYWFhQmeWb9+PXXt2pWmTp1KlZWVAvLIUj+RCaBjadGihaDwqnDnzp1aW2cICQmhrKwsjeHs27dP8ng2FZEJIILMmjVLUGjXrFmj1Z6QNnFxcdFocEteDKc7kQkgknzyySdUXl5Od+/e1Xg0UV1k7ty5lJeXR48fP5bt++hYFAoFGSkUCsrLy4MM3cLd3R1Pnz5FeXl5g8NydHREcXExioqKdKCZDBUUCgVkAshotlAoFJDnAWQ0a8gEkNGsIRNARrOGTAAZzRoyAWQ0a8gEkNGsIRNARrOGEapmxWTIaJYw9fDwgDwRJqM5QqFQ4P8A4/bp0jFemEcAAAAASUVORK5CYII=';
import { UPCHARGE_UNIT_VALUE, UPCHARGE_UNIT_PERCENTAGE, DELIVERY_UPCHARGE } from 'types';

const cx = classNames.bind(styles);

const getProductRate = (sale, add_discount) => {
	/**
	 * Getting the complete Tax Details
	 */
	let completeTax = 0;
	if (sale.taxes && sale.taxes.length > 0) {
		sale.taxes.map((_tax_item) => {
			completeTax += _tax_item.percent;
		});
	}
	let _mrp = sale.mrp;
	if (add_discount)
		_mrp = discountedMRP(sale);

	let productRateBeforeMrp = _mrp / (100 + completeTax) * 100;
	return parseFloat(productRateBeforeMrp.toFixed(2));//sale.mrp - totalTax(sale)));
};
const discountedMRP = (sale) => {
	let mrp = sale.mrp;
	if (sale.discount_value > 0 && sale.discount_type > 0) {
		if (sale.discount_type == UPCHARGE_UNIT_VALUE)
			mrp = (sale.mrp - sale.discount_value).toFixed(2);
		else if (sale.discount_type == UPCHARGE_UNIT_PERCENTAGE) {
			mrp = (sale.mrp - (parseFloat(sale.mrp) * parseFloat(sale.discount_value) / 100)).toFixed(2);
		}
	}
	return mrp;
}
const getDiscount = (sale, value) => {
	if (sale.discount_value > 0 && sale.discount_type > 0) {
		if (sale.discount_type == UPCHARGE_UNIT_PERCENTAGE)
			value = (value - (parseFloat(value) * parseFloat(sale.discount_value) / 100)).toFixed(2);
	}
	return value;
}
const getTaxDeductedUpcharge = (upcharge, taxes) => {
	const _taxes = _.orderBy(taxes, ['order'], ['asc']);
	let _val = upcharge;
	let _taxpercent = 0;
	for (var i = 0; i < _taxes.length; i++) {
		_taxpercent += _taxes[i].percent;
	}
	_val = _val / (100 + _taxpercent) * 100;
	return _val;
};
const getRoundOffValue = (details) => {
	let sales = details.sales;
	var completeAmount = 0;
	sales && sales.map((sale_item) => {
		/**
		 * Calculating the Upcharge Total Amount
		 */
		let upcharges = sale_item.upcharge || [];
		/*
					Representing the Upcharges in the Array Instead of Object
			*/
		upcharges = (upcharges && _.isArray(upcharges)) ? upcharges : [];

		upcharges.map((upcharge, index) => {
			if (upcharge.type != DELIVERY_UPCHARGE) {
				let _upcharge = parseFloat(upcharge.value).toFixed(2);
				if (upcharge.unit === UPCHARGE_UNIT_PERCENTAGE) {
					_upcharge = ((parseFloat(sale_item.mrp) * parseFloat(_upcharge)) / 100).toFixed(2);
				}
				_upcharge = getDiscount(sale_item, _upcharge);
				_upcharge = (_upcharge * sale_item.qty).toFixed(2);
				completeAmount += parseFloat(getTaxDeductedUpcharge(_upcharge, sale_item.taxes).toFixed(2));
			} else if (upcharge.value) {
				completeAmount += upcharge.value;
			}
		});
		/**
		 *  Product rate
		 */
		completeAmount += ((getProductRate(sale_item, true)) * sale_item.qty);
	});

	/**
	 * Calculating the Taxes amount
	 */
	var tax_amount = 0;

	if (sales && sales.length > 0
		&& sales[0].taxes
		&& sales[0].taxes.length > 0) {
		let taxes = sales[0].taxes;

		const totalTaxValues = (index) => {
			let total = 0;
			sales.map((value, i) => {
				total += (value.taxes[index].value * value.qty);
			});
			return parseFloat(total.toFixed(2));
		};
		taxes.map((tax_item, index) => {
			tax_amount += totalTaxValues(index);
		});
	}

	completeAmount = completeAmount + (parseFloat(tax_amount));

	let roundOffValue = (parseFloat(details.order.total_amount.toFixed(2)) - parseFloat(completeAmount.toFixed(2)));
	return (roundOffValue.toFixed(2));
}
class InvoicePrint extends Component {
	constructor(props) {
		super(props);
		this.renderForm = this.renderForm.bind(this);
	}
	componentDidMount() {
		this.props.dispatch(getInvoiceDetails(this.props.params.customer_id, this.props.params.order_id));
	}

	renderForm(details) {
		const invoice_json = {
			title: "TAX INVOICE",
			company: {
				name: "Camden Apparel Solutions Pvt. Ltd",
				address: "Camden Apparel Solutions Pvt. Ltd., SURVEY NO.48/2 KUDLU VILLAGE, ANEKAL TALUK SARJAPURA HOBLI, BANGALORE DISTRICT, BBMP, BANGALORE-560068",
				contact: "Phone : 1800 3000 1575 / E-mail : care@tailorman.com",
				cin: "U18204KA2012PTC066352",
				range: "SINGASANDRA / A WING , KENDRIYASADAN",
				division: "KORAMANGALA / A WING , KENDRIYASADAN",
				excise: "AAFCC0250LEM001",
				commissionerate: "BANGALORE-I/PO Box - 540 QUEEN’S ROAD, BANGALORE",
				pan: "AAFCC0250L",
				gst: "29AAFCC0250L1ZO"
			},
			declaration: "We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct."
		};
		var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
		var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

		function inWords(num) {
			if ((num = num.toString()).length > 9) return 'overflow';
			const n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
			if (!n) return; var str = '';
			str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
			str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
			str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
			str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
			str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
			return str;
		}

		const emptyRowCells = () => {
			return <div className={cx('full', 'empty-table-height', 'noborder')}>
				<span className={cx('col-1')}></span>
				<span className={cx('col-4')}></span>
				<span className={cx('col-1')}></span>
				<span className={cx('col-1')}></span>
				<span className={cx('col-2')}></span>
				<span className={cx('col-2')}></span>
				<span className={cx('col-1')}></span>
				<span className={cx('col-1')}></span>
				<span className={cx('col-1')}></span>
				<span className={cx('col-2', 'noborder')}></span>
			</div>;
		};

		const taxesElements = (sales) => {
			if (sales && sales.length > 0
				&& sales[0].taxes
				&& sales[0].taxes.length > 0) {
				let taxes = sales[0].taxes;

				const totalTaxValues = (index) => {
					let total = 0;
					sales.map((value, i) => {
						total += (value.taxes[index].value * value.qty);
					});
					return total.toFixed(2);
				};

				return taxes.map((item, index) => {
					return <div key={index} className={cx('full', 'align-left', 'noborder')}>
						<span className={cx('col-1')}></span>
						<span className={cx('col-4', 'pull-right', 'text-small')}>{item.name}</span>
						<span className={cx('col-1')}></span>
						<span className={cx('col-1')}></span>
						<span className={cx('col-2')}></span>
						<span className={cx('col-2')}></span>
						<span className={cx('col-1')}></span>
						<span className={cx('col-1')}></span>
						<span className={cx('col-1')}></span>
						<span className={cx('col-2', 'align-right', 'noborder')}>{totalTaxValues(index)}</span>
					</div>;
				});
			}
		};
		const totaldiff = (details) => {

			let roundOffValue = getRoundOffValue(details);
			// if (sales && sales.length > 0
			// 	&& sales[0].taxes
			// 	&& sales[0].taxes.length > 0) {
			// 	let taxes = sales[0].taxes;
			// 	const totalTaxValues = (index) => {
			// 		let total = 0;
			// 		sales.map((value, i) => {
			// 			total += ( value.taxes[index].value * value.qty) ;
			// 		});
			// 		return total.toFixed(2);
			// 	};
			//      let total =0;
			// 	taxes.map((item, index) => {
			// 		   total= total+ totalTaxValues(index);
			// 	});

			return (<div className={cx('full', 'align-left', 'noborder')}>
				<span className={cx('col-1')}></span>
				<span className={cx('col-4', 'pull-right', 'text-small')}>round-off</span>
				<span className={cx('col-1')}></span>
				<span className={cx('col-1')}></span>
				<span className={cx('col-2')}></span>
				<span className={cx('col-2')}></span>
				<span className={cx('col-1')}></span>
				<span className={cx('col-1')}></span>
				<span className={cx('col-1')}></span>
				<span className={cx('col-2', 'align-right', 'noborder')}>{roundOffValue}</span>
			</div>)

			// }
		};
		if (details.customer && details.sales) {
			let invoiceDate = (moment().utcOffset() === 0) ? moment().utcOffset("+05:30").format('lll') : moment().format('lll');

			return (
				<div className={cx('table', 'invoice-table')}>
					<div className={cx('invoice-logo')}><img src={tailormanLogoDataURL} alt="tailorman Logo" /></div>
					<div className={cx('full')}><span className={cx('col-16', 'hightlight', 'noborder')}>{invoice_json.title}</span></div>
					<div className={cx('full')}><span className={cx('col-16', 'hightlight', 'noborder')}>{invoice_json.company.name.toLowerCase()}</span></div>
					<div className={cx('full')}><span className={cx('col-16', 'noborder')}>{invoice_json.company.address.toLowerCase()}</span></div>
					<div className={cx('full')}><span className={cx('col-16', 'noborder')}>{"CIN: " + invoice_json.company.cin + " / " + invoice_json.company.contact}</span></div>

					<div className={cx('full')}>
						<span className={cx('col-8', 'nopad')}>
							<span className={cx('col-16', 'label', 'border-bottom')}>GST No.</span>
							<span className={cx('col-16', 'border-bottom')}>{invoice_json.company.gst}</span>
							<span className={cx('col-16', 'label', 'border-bottom')}>PAN No.</span>
							<span className={cx('col-16', 'border-bottom')}>{invoice_json.company.pan}</span>
						</span>
						<span className={cx('col-8', 'nopad', 'noborder')}>
							<span className={cx('col-8', 'label')}>Invoice No.</span>
							<span className={cx('col-8', 'label', 'noborder')}>Buyer's Order no.</span>
							<span className={cx('col-8', )}>{details.order.order_id}</span>
							<span className={cx('col-8', 'noborder')}>{details.order.order_id}</span>

							<span className={cx('col-8', 'label')}>Date & Time of Issue of Invoice</span>
							<span className={cx('col-8', 'label', 'noborder')}>Payment Mode</span>
							<span className={cx('col-8', )}>{invoiceDate}</span>
							<span className={cx('col-8', 'noborder')}>{details.order.payment_type}</span>

							<span className={cx('col-8', 'label')}>PI Date & Time</span>
							<span className={cx('col-8', 'label', 'noborder')}>PI No.</span>
							<span className={cx('col-8', )}>{details.order.pin_date ? moment(details.order.pin_date).format('lll') : moment().format('lll')}</span>
							<span className={cx('col-8', 'noborder')}>{details.order.pin_number || '-'}</span>
						</span>
					</div>

					<div className={cx('full')}>
						<span className={cx('col-8', 'nopad', 'align-left')}>
							<span className={cx('col-16', 'label', 'noborder', 'hightlight')}>Buyer Name & Billing Address</span>
							<span className={cx('col-16', 'noborder')}>{details.customer.name}</span>
							<span className={cx('col-16', 'noborder')}>{details.customer.phone || details.customer.mobile}</span>
							<span className={cx('col-16', 'noborder', 'notransform')}>{details.customer.email}</span>

							<span className={cx('col-16', 'noborder', 'notransform', 'top-bottom-spacing')}>{details.order.billing_address}</span>
						</span>
						<span className={cx('col-8', 'nopad', 'align-left')}>
							<span className={cx('col-16', 'label', 'noborder', 'hightlight')}>Buyer Name & Shipping Address</span>
							<span className={cx('col-16', 'noborder')}>{details.customer.name}</span>
							<span className={cx('col-16', 'noborder')}>{details.customer.phone || details.customer.mobile}</span>
							<span className={cx('col-16', 'noborder', 'notransform')}>{details.customer.email}</span>

							<span className={cx('col-16', 'noborder', 'notransform', 'top-bottom-spacing')}>{details.order.delivery_address}</span>
						</span>
					</div>

					<div className={cx('full')}>
						<span className={cx('col-1', 'label')}>SI No.</span>
						<span className={cx('col-4', 'label')}>Description of Goods</span>
						<span className={cx('col-1', 'label')}>HSN No</span>
						<span className={cx('col-1', 'label')}>GST</span>
						<span className={cx('col-2', 'label', 'align-right')}>MRP (INR)</span>
						<span className={cx('col-2', 'label', 'align-right')}>Rate (INR)</span>
						<span className={cx('col-1', 'label')}>Qty</span>
						<span className={cx('col-1', 'label')}>per</span>
						<span className={cx('col-1', 'label')}>Disc.</span>
						<span className={cx('col-2', 'label', 'align-right', 'noborder')}>Amount (INR)</span>
					</div>

					{details.sales.map((sale, _index) => {
						const upCharges = (sale) => {
							if ((sale.upcharge && sale.upcharge.length > 0)) {
								let upcharges = sale.upcharge || [];
								/*
									Representing the Upcharges in the Array Instead of Object
								*/
								upcharges = (upcharges && _.isArray(upcharges)) ? upcharges : [];
								return upcharges.map((upcharge, index) => {
									if (upcharge.type != DELIVERY_UPCHARGE) {
										let _upcharge = upcharge.value.toFixed(2);
										if (upcharge.unit === UPCHARGE_UNIT_PERCENTAGE) {

											_upcharge = ((parseFloat(sale.mrp) * parseFloat(_upcharge)) / 100).toFixed(2);
										}
										_upcharge = getDiscount(sale, _upcharge);
										_upcharge = (_upcharge * sale.qty).toFixed(2);

										let upchargeBeforeDiscount = (upcharge.value).toFixed(2);
										if (upcharge.unit === UPCHARGE_UNIT_PERCENTAGE)
											upchargeBeforeDiscount = ((parseFloat(sale.mrp) * parseFloat(upcharge.value)) / 100).toFixed(2);
										return (
											<div className={cx('full', 'noborder', 'align-left')} key={"tax_" + sale.order_item_id + "_" + index}>
												<span className={cx('col-1')}></span>
												<span className={cx('col-4')}><span className={cx('col-16', 'noborder')}> {upcharge.type.toLowerCase()} </span></span>
												<span className={cx('col-1')}></span>
												<span className={cx('col-1')}></span>
												<span className={cx('col-2', 'align-right')}></span>
												<span className={cx('col-2', 'align-right')}>{numeral(getTaxDeductedUpcharge(upchargeBeforeDiscount, sale.taxes)).format('0,0.00')}</span>
												<span className={cx('col-1')}></span>
												<span className={cx('col-1')}></span>
												<span className={cx('col-1')}>{(sale.discount_value > 0 && sale.discount_type == UPCHARGE_UNIT_PERCENTAGE ? sale.discount_value : '')} {(sale.discount_value > 0 && sale.discount_type == UPCHARGE_UNIT_PERCENTAGE) ? '%' : ''}</span>
												<span className={cx('col-2', 'align-right', 'noborder')}>{numeral(getTaxDeductedUpcharge(_upcharge, sale.taxes)).format('0,0.00')}</span>
											</div>
										);
									}
								});
							}
						}

						const totalTaxPercent = (sale) => {
							if (sale.taxes && sale.taxes.length > 0) {
								let totalPercent = 0;

								sale.taxes.map((tax, index) => {
									totalPercent += tax.percent;
								});

								return totalPercent + "%";
							}
						};

						const totalTax = (sale, isDiscounted) => {
							if (sale.taxes && sale.taxes.length > 0) {
								let completeTax = 0;
								sale.taxes.map((_tax_item) => {
									completeTax += _tax_item.percent;
								});
								let totalTax = 0;
								for (var i = 0; i < sale.taxes.length; i++) {
									let _mrp = sale.mrp;
									if (isDiscounted)
										_mrp = discountedMRP(sale);
									totalTax += ((parseFloat(sale.taxes[i].percent) / 100) * _mrp);

									//((_mrp) / (parseFloat(sale.taxes[i].percent) + 100) * parseFloat(sale.taxes[i].percent))	
								}
								return totalTax;
							}
						}

						const delivery = (sale) => {
							const _deliv = _.find(sale.upcharge, { type: DELIVERY_UPCHARGE });
							if (_deliv) {
								let upchargeBeforeDiscount = (_deliv.value).toFixed(2);
								if (_deliv.unit === UPCHARGE_UNIT_PERCENTAGE)
									upchargeBeforeDiscount = ((parseFloat(sale.mrp) * parseFloat(_deliv.value)) / 100).toFixed(2);

								return (
									<div className={cx('full', 'noborder', 'align-left')}>
										<span className={cx('col-1')}></span>
										<span className={cx('col-4')}><span className={cx('col-16', 'noborder')}> {_deliv.type.toLowerCase()} </span></span>
										<span className={cx('col-1')}></span>
										<span className={cx('col-1')}></span>
										<span className={cx('col-2')}></span>
										<span className={cx('col-2')}>{numeral(getTaxDeductedUpcharge(upchargeBeforeDiscount, sale.taxes)).format('0,0.00')}</span>
										<span className={cx('col-1')}></span>
										<span className={cx('col-1')}></span>
										<span className={cx('col-1')}></span>
										<span className={cx('col-2', 'align-right', 'noborder')}>{numeral(_deliv.value).format('0,0.00')}</span>
									</div>
								)
							} else {
								return null;
							}
						}
						return (
							<div key={_index}>
								<div className={cx('full', 'noborder', 'align-left')}>
									<span className={cx('col-1')}>{_index + 1}</span>
									<span className={cx('col-4')}>{sale.display_name}</span>
									<span className={cx('col-1')}>{sale.hsn_code || '-'}</span>
									<span className={cx('col-1', 'align-right', 'text-small')}>{totalTaxPercent(sale)}</span>
									<span className={cx('col-2', 'align-right')}>{numeral(parseFloat(sale.mrp).toFixed(2)).format('0,0.00')}</span>
									<span className={cx('col-2', 'align-right')}>{numeral(getProductRate(sale)).format('0,0.00')}</span>
									<span className={cx('col-1')}>{sale.qty}</span>
									<span className={cx('col-1')}>pcs</span>
									<span className={cx('col-1')}>{(sale.discount_value > 0 ? sale.discount_value : '')} {(sale.discount_value > 0 && sale.discount_type == UPCHARGE_UNIT_PERCENTAGE) ? '%' : ''}</span>
									<span className={cx('col-2', 'align-right', 'noborder')}>{numeral(parseFloat(getProductRate(sale, true)) * sale.qty).format('0,0.00')}</span>
								</div>
								{upCharges(sale)}
								{delivery(sale)}

								{emptyRowCells()}
							</div>
						);
					})}

					{taxesElements(details.sales)}
					{totaldiff(details)}

					{emptyRowCells()}

					<div className={cx('full', 'align-left')}>
						<span className={cx('col-1', 'label')}></span>
						<span className={cx('col-4', 'label', 'hightlight')}>Total</span>
						<span className={cx('col-1', 'label')}></span>
						<span className={cx('col-1', 'label')}></span>
						<span className={cx('col-2', 'label')}></span>
						<span className={cx('col-2', 'label')}></span>
						<span className={cx('col-1', 'label')}></span>
						<span className={cx('col-1', 'label')}></span>
						<span className={cx('col-1', 'label')}></span>
						<span className={cx('col-2', 'label', 'highlight', 'align-right', 'noborder')}>{numeral(details.order.total_amount.toFixed(2)).format('0,0.00')}</span>
					</div>

					<div className={cx('full', 'border-top', 'border-bottom')}>
						<span className={cx('col-4', 'label', 'hightlight')}>
							Amount in words
							</span>
						<span className={cx('col-12', 'noborder', 'col-text-align-left')}>
							<span className={cx('noborder', 'hightlight')}>INR:</span> {inWords(details.order.total_amount)}
						</span>
					</div>


					<div className={cx('full')}>
						<span className={cx('col-10')}>
							<span className={cx('col-16', 'noborder')}>Customer Signature</span>
							<img src={details.signature} className={cx('signature')} />
						</span>
						<span className={cx('col-6', 'noborder', 'nopad')}>
							<span className={cx('col-16', 'noborder')}>{"for " + invoice_json.company.name}</span>
							<span className={cx('col-16', 'noborder')}></span>
							<span className={cx('col-16', 'noborder')}></span>
							<span className={cx('col-16', 'noborder')}></span>
							<span className={cx('col-16', 'noborder')}>Authorised Signatory</span>

						</span>
					</div>
					<div className={cx('full', 'border-bottom', 'text-small')}><span className={cx('col-16', 'noborder', 'text-small', 'notransform')}>{"Declaration: " + invoice_json.declaration}</span></div>

					<div className={cx('full')}>
						<span className={cx('col-10', 'noborder', 'notransform', 'text-small')}>You have allowed Tailorman to send you regular updates about Tailorman via SMS and email</span>
						<span className={cx('col-4', 'noborder', 'notransform', 'text-small')}>This is a computer generated invoice</span>
						<span className={cx('col-2', 'noborder', 'text-small')}>E & O.E</span>
					</div>
				</div>
			);
		}

	}
	render() {
		//return null;
		return (
			<div className={cx('container', 'big', 'print')}>
				{this.renderForm(this.props.order.invoice)}
			</div>
		);
	}
}

InvoicePrint.propTypes = {
	order: PropTypes.object
};


function mapStateToProps({ order }) {
	return {
		order
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(InvoicePrint);