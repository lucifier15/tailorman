import React, { Component, PropTypes } from 'react';
import Viewer from 'react-viewer';
import 'css/ImageViewer.css';

class ImageViewer extends Component {
  constructor() {
    super();
  }
  render() {
    let images = [];
    if(this.props.images){
        this.props.images.map((image,index)=>{
          images.push({
            src : image,
            alt : index
          });
        });
    }
    return (
      <div>
        <Viewer
        visible={this.props.visible}
        onClose={this.props.onClose}
        images={images}
        activeIndex = {this.props.activeIndex}
        />
      </div>
    );
  }
}
export default ImageViewer;
