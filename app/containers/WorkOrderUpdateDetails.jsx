import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from 'css/components/workorder';
import {
	push
} from 'react-router-redux';
import { fetchList } from '../actions/list';
import _ from 'lodash';
import SelectForm from 'components/SelectForm';
import {
	getWorkOrderItemDetails,
	updateWorkOrderItemEntry,
	saveWorkOrderItem
} from 'actions/workorder';
import {
	updateMessage,
	getDeliveryUpcharge
} from 'actions/order';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import back from 'images/back-arrow.png';
import { DELIVERY_UPCHARGE, UPCHARGE_UNIT_VALUE } from 'types';

const cx = classNames.bind(styles);

class WorkOrderDetails extends Component {
	constructor(props) {
		super(props);
		this.closeForm = this.closeForm.bind(this);
		this.saveSelectFormValues = this.saveSelectFormValues.bind(this);
		this.saveAndClose = this.saveAndClose.bind(this);
	}
	saveAndClose() {
		let order_item = this.props.workorder.workOrderItem;
		let updated_Order_Item = Object.assign({

		}, order_item, {
				delivery_date: ReactDOM.findDOMNode(this.refs.delivery_date).value,
				fiton_date: ReactDOM.findDOMNode(this.refs.fiton_date).value,
				style: order_item.style_id || order_item.sku_id,
				comment: ReactDOM.findDOMNode(this.refs.comment).value
			});
		if (updated_Order_Item.finish_type_id > 0 && updated_Order_Item.priority_id > 0) {

			this.props.dispatch(saveWorkOrderItem(updated_Order_Item, this.props.workorder.workOrderItemSales, this.props.workorder));
		}
	}
	saveSelectFormValues(selected, type) {
		if (type == "priority_id") {
			let order_item = this.props.workorder.workOrderItem;
			this.props.dispatch(updateWorkOrderItemEntry(parseInt(selected.value), type));
			//getDeliveryUpcharge(order_item.item_type_id, selected.value).then((delivery_upcharge) => {
				// let upcharges = order_item.upcharge || order_item.upcharges || [];
				// if (upcharges.length > 0) {
				//     upcharges = _.compact(upcharges.filter((upcharge) => {
				//         return upcharge.type != DELIVERY_UPCHARGE;
				//     }));
				// }
				// if (delivery_upcharge) {
				//     upcharges.push({
				//         type: DELIVERY_UPCHARGE,
				//         value: delivery_upcharge,
				//         unit: UPCHARGE_UNIT_VALUE
				//     });
				// }
				// this.props.dispatch(updateWorkOrderItemEntry(upcharges, 'upcharges'));
			//})
		} else {
			this.props.dispatch(updateWorkOrderItemEntry(parseInt(selected.value), type));
		}
	}
	componentDidMount() {
		//const workorder = _.find(this.props.workorder.workorders, { order_item_id: parseInt(this.props.params.order_item_id) });
		this.props.dispatch(getWorkOrderItemDetails(parseInt(this.props.params.order_item_id)));
		if (this.props.lists.priorities && this.props.lists.priorities.length === 0) {
			this.props.dispatch(fetchList('priority'));
		}
		if (this.props.lists.finish_types && this.props.lists.finish_types.length === 0) {
			this.props.dispatch(fetchList('finish_type'));
		}
	}
	/*saveFabricDesign(fabric_design, comment, upCharges, isFinal) {
		this.props.dispatch(saveFabricDesign(this.props.workorder.selected.order_item.order_item_id, fabric_design, comment, isFinal));
		this.props.dispatch(updateMessage('UPDATE_GENERAL_MESSAGE', 'Fabric Style Updated'));
	}*/
	updateFieldOrderItemDetails(e, type) {
		var selectedValue = e.target.value;
		this.props.dispatch(updateWorkOrderItemEntry(selectedValue, type));
	}
	closeForm() {
		this.props.dispatch(push('/workorder'));
	}

	render() {
		if (this.props.workorder.workOrderItem) {
			const orderItem = this.props.workorder.workOrderItem;
			console.log(this.props.workorder.workOrderItem);
			return (
				<div className={cx('container', 'big')}>
					<h1>Update Order Item Details</h1>
					<Link to="/workorder" className={cx('back')} ><img src={back} /></Link>
					<div className={cx('form-container')}>
						<div className={cx('input-group')}>
							<label htmlFor="item_type">Item Type</label>
							<input type="text" value={orderItem.item_type} readOnly />
						</div>
						<div className={cx('input-group')}>
							<label htmlFor="sku">SKU</label>
							<input type="text" value={orderItem.product_sku} readOnly />
						</div>
						<div className={cx('input-group')}>
							<label htmlFor="mrp">MRP</label>
							<input type="number" value={orderItem.mrp} readOnly />
						</div>

						<div className={cx('input-group')}>
							<label htmlFor="qty">quantity</label>
							<input type="number" value={orderItem.qty} readOnly />
						</div>
						<div className={cx('input-group')}>
							<label htmlFor="delivery_date">Delivery Date</label>
							<input type="date" ref="delivery_date" value={orderItem.delivery_date} onChange={(e) => this.updateFieldOrderItemDetails(e, 'delivery_date')} />
						</div>
						<div className={cx('input-group')}>
							<label htmlFor="qty">Finish</label>
							<SelectForm type="finish_type_id" rel="finish_type_id" options={this.props.lists.finish_types} value={orderItem.finish_type_id || orderItem.finish_type} save={this.saveSelectFormValues} />
						</div>
						<div className={cx('input-group')}>
							<label htmlFor="qty">Priority</label>
							<SelectForm type="priority_id" rel="priority_id" options={this.props.lists.priorities} value={orderItem.priority_id} save={this.saveSelectFormValues} />
						</div>
						<div className={cx('input-group')}>
							<label htmlFor="fiton_date">Fiton Date</label>
							<input type="date" ref="fiton_date" value={orderItem.fiton_date} onChange={(e) => this.updateFieldOrderItemDetails(e, 'fiton_date')} />
						</div>
						<div className={cx('input-group')}>
							<label htmlFor="comments">Comments</label>
							<textarea ref="comment" value={orderItem.comment} onChange={(e) => this.updateFieldOrderItemDetails(e, 'comment')}></textarea>
						</div>
						<button className={cx('cancel', 'action', 'secondary')} onClick={this.closeForm}>Cancel</button>
						<button onClick={this.saveAndClose} className={cx('action', 'primary')} >Save</button>
					</div>
				</div>
			);
		} else {
			return null;
		}
	}
}

WorkOrderDetails.propTypes = {
	workorder: PropTypes.object,
	user: PropTypes.object,
	lists: PropTypes.object,
	history: PropTypes.object
};

function mapStateToProps({ workorder, user, customer, lists, history }) {
	return {
		workorder,
		user,
		customer,
		lists,
		history
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(WorkOrderDetails);