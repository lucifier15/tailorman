import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from 'css/components/order/history';
import {
	push
} from 'react-router-redux';
import { fetchList } from '../../actions/list';
import _ from 'lodash';
import {
	populateOrders,
	setActiveKey
} from '../../actions/history';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import Collapse, { Panel } from 'rc-collapse';
import back from 'images/back-arrow.png';

const cx = classNames.bind(styles);

class OrderHistory extends Component {
	constructor(props) {
		super(props);
		this.accordion = true;
		this.renderAccordion = this.renderAccordion.bind(this);
		this.toggleAccordion = this.toggleAccordion.bind(this);
		this.getOrderItemDiv = this.getOrderItemDiv.bind(this);
	}

	componentDidMount() {
		this.props.dispatch(populateOrders(this.props.customer.customer_id));
	}

	shouldComponentUpdate(props, state) {
		return !_.isEqual(props.history, this.props.history);
	}

	renderAccordion() {
		const self = this;
		if (this.props.history.orders && this.props.history.orders.length > 0) {
			return this.props.history.orders.map((order, index) => {
				return (
					<Panel header={(order.order_date && order.order_date != 'Invalid date') ? order.order_id + " " + order.order_date + " " + order.name : order.name} key={index}>
						{self.getOrderItemDiv(order.sales)}
					</Panel>
				)
			});
		} else {
			return null;
		}

	}

	getOrderItemDiv(sales) {
		if (sales.length === 0) {
			return <div>No Sale Items</div>
		} else {
			return sales.map((sale, index) => {
				return (
					<div key={index}>
						<span>{sale.display_name}</span>
					</div>
				);
			})
		}
	}

	toggleAccordion(activeKey) {
		this.props.dispatch(setActiveKey(activeKey));
	}

	render() {
		return (
			<div className={cx('container')}>
				<div className={cx('header-note')}>
					<span className={cx('header-label')}>Customer:   </span>
					<span className={cx('header-content')}>{this.props.customer.name}</span>
				</div>
				<h1>History</h1>
				<Link to="/reset" className={cx('review')}>
					Logout
				</Link>
				<Link to="/customer/actions" className={cx('back')} ><img src={back} /></Link>
				<div className={cx('form-container')}>
					<Collapse
						accordion={this.accordion}
						onChange={this.toggleAccordion}
						activeKey={this.props.history.accordionKey}
						>
						{this.renderAccordion()}
					</Collapse>
				</div>
			</div>
		);
	}
}

OrderHistory.propTypes = {
	history: PropTypes.object,
	user: PropTypes.object,
	customer: PropTypes.object,
};


function mapStateToProps({history, user, customer}) {
	return {
		history,
		user,
		customer
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(OrderHistory);