import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

import { Link } from 'react-router';
import { push } from 'react-router-redux';

import { connect } from 'react-redux';

import classNames from 'classnames/bind';

import styles from 'css/components/order/sales';

import SelectForm from 'components/SelectForm';
import SearchList from 'components/SearchList';
import CustomImage from 'components/CustomImage';
import MultipleChoice from 'components/MultipleChoice';

import { fetchList, clearList } from '../../actions/list';
import {
    selectSaleItem,
    initNewSaleItem,
    updateSaleItemEntry,
    deleteSaleItem,
    saveOrderItem,
    updateSaleItemProfile,
    updateMessage,
    getDeliveryUpcharge,
    saveCommentOrderItems
} from '../../actions/order';

import back from 'images/back-arrow.png';

import { DELIVERY_UPCHARGE, UPCHARGE_UNIT_VALUE } from 'types';

const cx = classNames.bind(styles);

class OrderSaleItem extends Component {
    constructor(props) {
        super(props);

        this.isMTM = false;

        this.saleItem = {};
        this.itemType = {};

        this.save = this.save.bind(this);
        this.delete = this.delete.bind(this);
        this.selectStyle = this.selectStyle.bind(this);
        this.searchFabrics = this.searchFabrics.bind(this);
        this.toggleProfile = this.toggleProfile.bind(this);
        this.renderStyleList = this.renderStyleList.bind(this);
        this.getFabricOptions = this.getFabricOptions.bind(this);
        this.saveSelectFormValues = this.saveSelectFormValues.bind(this);
    }

    componentDidMount() {
        let { 
            order: { sales }, 
            params: { item_type_id, id }, 
            customer: { customer_id },
            lists: { item_types, styles, measurement_types, priorities },
            dispatch
        } = this.props;

        if (!sales[id])
            dispatch(initNewSaleItem(item_type_id, customer_id, sales));
        else {
            this.saleItem = sales[id];
        }

        const item_type_details = _.find(item_types, { item_type_id: parseInt(item_type_id) });

        this.isMTM = item_type_details && item_type_details.isMTM;

        dispatch(updateSaleItemEntry(this.isMTM, 'isMTM', id));

        if (item_types && item_types.length === 0)
            dispatch(fetchList('item_type'));

        if (styles && styles.length === 0)
            dispatch(fetchList('style'));

        if (measurement_types && measurement_types.length === 0)
            dispatch(fetchList('measurement_type'));

        if (priorities && priorities.length === 0)
            dispatch(fetchList('priority'));
            
        this.itemType = _.find(item_types, { item_type_id: parseInt(item_type_id) });

        dispatch(fetchList('profile', { item_type_id: parseInt(item_type_id), customer_id: customer_id }));

        dispatch(fetchList('finish_type'));

        window.scrollTo(0, 0);
    }

    saveSelectFormValues(selected, type) {
        switch (type) {
            case 'style':
                const fabric = _.find(this.props.lists.fabrics, { fabric_id: parseInt(selected.value) });
                ReactDOM.findDOMNode(this.refs.mrp).value = fabric.shirt_mrp;
                ReactDOM.findDOMNode(this.refs.sku).value = fabric.sku_code;
                this.props.dispatch(updateSaleItemEntry(fabric.shirt_mrp, 'mrp', this.props.params.id));
                this.props.dispatch(updateSaleItemEntry(fabric.sku_code, 'sku', this.props.params.id));
                this.props.dispatch(updateSaleItemEntry(selected.value, 'style', this.props.params.id));
                break;
            case 'profile_id':
                const profile = _.find(this.props.lists.profiles, { id: parseInt(selected.value) });
                this.props.dispatch(updateSaleItemProfile(profile, this.props.order.sales[this.props.params.id].item_type_id, this.props.params.id));
                this.props.dispatch(updateSaleItemEntry(parseInt(selected.value), type, this.props.params.id));
                break;
            case 'priority_id':
                getDeliveryUpcharge(this.saleItem.item_type_id, selected.value).then((delivery_upcharge) => {
                    let upcharges = this.saleItem.upcharge || this.saleItem.upcharges || [];
                    upcharges = _.isArray(upcharges) ? upcharges : [];
                    if (upcharges.length > 0) {
                        upcharges = _.compact(upcharges.filter((upcharge) => {
                            return upcharge.type != DELIVERY_UPCHARGE;
                        }));
                    }
                    if (delivery_upcharge) {
                        upcharges.push({
                            type: DELIVERY_UPCHARGE,
                            value: delivery_upcharge,
                            unit: UPCHARGE_UNIT_VALUE
                        });
                    }
                    this.props.dispatch(updateSaleItemEntry(parseInt(selected.value), type, this.props.params.id));
                    this.props.dispatch(updateSaleItemEntry(upcharges, 'upcharges', this.props.params.id));
                })
                break;
            case 'comment':
                this.props.dispatch(updateSaleItemEntry(selected, type, this.props.params.id));
                break;
            default:
                this.props.dispatch(updateSaleItemEntry(parseInt(selected.value), type, this.props.params.id));
        }
    }

    getFabricOptions() {
        return this.props.lists.fabrics.map((fabric) => {
            return fabric.sku;
        });
    }

    toggleProfile(value) {
        if (value[0] == 'yes') {
            value = true;
        } else {
            value = false;
        }
        this.props.dispatch(updateSaleItemEntry(value, 'is_new_measurements', this.props.params.id));
    }
    save() {
        var item = _.find(this.props.lists.item_types, { item_type_id: parseInt(this.props.params.item_type_id) });
        var style = _.find(this.props.lists.fabrics, { fabric_id: this.props.order.sales[this.props.params.id].style });
        const getDisplayName = (item, style, sale) => {
            if (style && item) {
                return item.descr + " - " + style.name + " - " + style.product_sku_code;
            } else if (sale.display_name)
                return sale.display_name;
            else
                return '';
        }
        let saleItem = {};
        if (!this.isMTM) {
            saleItem = {
                style: this.props.order.sales[this.props.params.id].style,
                mrp: parseFloat(ReactDOM.findDOMNode(this.refs.mrp).value),
                qty: parseInt(ReactDOM.findDOMNode(this.refs.qty).value),
                delivery_date: ReactDOM.findDOMNode(this.refs.delivery_date).value,
                priority_id: 1,
                order_id: this.props.order.details.order_id,
                display_name: getDisplayName(item, style, this.props.order.sales[this.props.params.id]),
                order_item_id: this.props.order.sales[this.props.params.id].order_item_id,
                workflow_id: 1,
                finish_type_id: 1,
                isMTM: this.isMTM,
                item_type_id: parseInt(this.props.params.item_type_id),
                profile_id: -1,
                comment: ReactDOM.findDOMNode(this.refs.comment).value,
            }
        } else {
            saleItem = {
                style: this.props.order.sales[this.props.params.id].style,
                mrp: parseFloat(ReactDOM.findDOMNode(this.refs.mrp).value),
                qty: parseInt(ReactDOM.findDOMNode(this.refs.qty).value),
                fiton_date: ReactDOM.findDOMNode(this.refs.fiton_date).value,
                delivery_date: ReactDOM.findDOMNode(this.refs.delivery_date).value,
                comment: ReactDOM.findDOMNode(this.refs.comment).value,
                item_type_id: parseInt(this.props.params.item_type_id),
                priority_id: this.props.order.sales[this.props.params.id].priority_id,
                workflow_id: 1,
                profile_id: this.props.order.sales[this.props.params.id].profile_id,
                order_id: this.props.order.details.order_id,
                is_new_measurements: this.props.order.sales[this.props.params.id].is_new_measurements,
                display_name: getDisplayName(item, style, this.props.order.sales[this.props.params.id]),
                order_item_id: this.props.order.sales[this.props.params.id].order_item_id,
                finish_type_id: this.props.order.sales[this.props.params.id].finish_type_id,
                isMTM: this.isMTM
            }
        }

        const isUpdate = (saleItem.order_item_id) ? true : false;
        if (this.is_new_measurements) {
            this.props.dispatch(updateSaleItemEntry(-1, 'profile_id', this.props.params.id));
            this.props.dispatch(updateSaleItemEntry(saleItem.profile_id, 'backup_profile_id', this.props.params.id));
        }
        if (saleItem.style > 0 && saleItem.qty > 0 && saleItem.finish_type_id > 0 && saleItem.priority_id > 0) {
            this.props.dispatch(saveOrderItem(saleItem, isUpdate, this.props.params.id));
            /*
                Updating the comments section for all the Sales Items
                New Feature 
                -----------
                Comments entered at the sku level should be retained for the category and should show up for all other skus also
            */
            this.props.dispatch(saveCommentOrderItems(saleItem,this.props.order.sales));
            if (this.isMTM)
                this.props.dispatch(push('/order/sales/' + this.props.params.id + '/details'));
            else
                this.props.dispatch(push('/order/sales'));
        } else {
            if (this.isMTM)
                this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', 'Please select SKU, Quantity, Finish type & Priority'));
            else
                this.props.dispatch(updateMessage('MEASUREMENT_FORM_VALIDATION', 'Please select SKU and update Quantity'));
            window.scrollTo(0, 0)
        }
    }

    delete() {
        let { order: { sales }, params: { id }, dispatch } = this.props;

        dispatch(deleteSaleItem(sales[id].order_item_id, id));
        dispatch(push('/order/sales'));
    }

    renderStyleList(style) {
        return (
            <div>
                <span className={cx('select-main-label')}>{style.name}</span>
                <span className={cx('select-email')}>{style.mrp + " INR"}</span>
                <span className={cx('select-phone')}>{style.product_sku_code}</span>
            </div>
        )
    }

    searchFabrics(keyword) {
        let { params: { item_type_id }, dispatch } = this.props;

        if (keyword.length === 0) {
            dispatch(clearList('fabric'));
        } else {
            dispatch(fetchList('fabric', { keyword: keyword, item_type_id: item_type_id }));
        }
    }

    selectStyle(id) {
        let refs = this.refs;
        let { params, lists: { fabrics }, dispatch } = this.props;
        
        const fabric = _.find(fabrics, { fabric_id: parseInt(id) });

        ReactDOM.findDOMNode(refs.mrp).value = fabric.mrp;
        ReactDOM.findDOMNode(refs.description).value = fabric.name;
        ReactDOM.findDOMNode(refs.sku).value = fabric.product_sku_code;

        dispatch(updateSaleItemEntry(fabric.mrp, 'mrp', params.id));
        dispatch(updateSaleItemEntry(fabric.name, 'fabric_description', params.id));
        dispatch(updateSaleItemEntry(fabric.fabric_sku_code, 'sku', params.id));
        dispatch(updateSaleItemEntry(fabric.product_sku_code, 'display_sku', params.id));
        dispatch(updateSaleItemEntry(id, 'style', params.id));
    }

    render() {
        let { 
            order: { sales }, 
            params: { id, item_type_id },
            lists: { fabrics, profiles, item_types, finish_types, priorities },
            customer: { name }
        } = this.props;

        let fabricImage = '';
        let profileField = null;
        let fabricImageElement = '';

        this.saleItem = sales[id];

         if(this.saleItem && this.saleItem.sku) {
            fabricImage = `https://s3.ap-south-1.amazonaws.com/assets.web.tm/product/${this.saleItem.sku}.jpg`;
            fabricImageElement = <div className={cx('input-group')}>
                                    <label htmlFor="fabric_image_text">Fabric Image</label>
                                    <CustomImage src={fabricImage} width='210px' height='210px' />
                                </div>;
        }

        if (this.saleItem && fabrics.length > 0 && this.saleItem.style > 0) {
            const fabric = _.find(fabrics, { fabric_id: parseInt(this.saleItem.style) });
            this.saleItem.sku = (fabric) ? fabric.fabric_sku_code : undefined;
        }
        
        if (sales[id] && !sales[id].is_new_measurements) {
            profileField = (<div className={cx('input-group')}>
                <label htmlFor="profiles">Profiles</label>
                <SelectForm type="profiles" rel="profile_id" options={profiles} value={this.saleItem.profile_id} save={this.saveSelectFormValues} />
            </div>);
        }

        const item_type = _.find(item_types, { item_type_id: parseInt(item_type_id) }) || {};

        const is_new_measurements = sales[id] ? sales[id].is_new_measurements : true;

        let mtm_fields = (
            <div className={cx('benign-container')}>
                <div className={cx('input-group')}>
                    <label htmlFor="qty">Finish</label>
                    <SelectForm type="finish_type_id" rel="finish_type_id" options={finish_types} value={this.saleItem && (this.saleItem.finish_type_id || this.saleItem.finish_type)} save={this.saveSelectFormValues} />
                </div>
                <div className={cx('input-group')}>
                    <label htmlFor="qty">Priority</label>
                    <SelectForm type="priority_id" rel="priority_id" options={priorities} value={this.saleItem && this.saleItem.priority_id} save={this.saveSelectFormValues} />
                </div>
                <div className={cx('input-group')}>
                    <label htmlFor="fiton_date">Fiton Date</label>
                    <input type="date" ref="fiton_date" defaultValue={this.saleItem && this.saleItem.fiton_date} />
                </div>
                <div className={cx('input-group')}>
                    <label htmlFor="is_new_profile">Is New Measurement</label>
                    <MultipleChoice isMultiple={false} options={['yes', 'no']} selected={(is_new_measurements === false) ? 'no' : 'yes'} rel="is_new_measurements" save={this.toggleProfile} />
                </div>
                {profileField}
            </div>
        );
        if (!this.isMTM)
            mtm_fields = null;

        return (
            <div className={cx('container')}>
                <div className={cx('header-note')}>
                    <span className={cx('header-label')}>Customer:   </span>
                    <span className={cx('header-content')}>{name}</span>
                </div>
                <Link to="/order" className={cx('review')}>
                    Review
				</Link>
                <Link to="/order/sales" className={cx('back')} ><img src={back} /></Link>
                <div className={cx('form-container')}>
                    <h1>Sale Item</h1>
                    <div className={cx('input-group')}>
                        <label htmlFor="item_type">Item Type</label>
                        <input type="text" ref="item_type" defaultValue={item_type.descr} readOnly />
                    </div>
                    <div className={cx('input-group')}>
                        <label htmlFor="select_customer">Search Fabric</label>
                        <SearchList
                            search={this.searchFabrics}
                            select={this.selectStyle}
                            results={fabrics}
                            renderItem={this.renderStyleList}
                            placeholder="Search for SKU" />
                    </div>
                    <div className={cx('input-group')}>
                        <label htmlFor="desc">Description</label>
                        <input type="text" ref="description" defaultValue={this.saleItem && this.saleItem.fabric_description} readOnly />
                    </div>
                    {fabricImageElement}
                    <div className={cx('input-group')}>
                        <label htmlFor="sku">SKU</label>
                        <input type="text" ref="sku" defaultValue={this.saleItem && (this.saleItem.display_sku || this.saleItem.product_sku)} readOnly />
                    </div>
                    <div className={cx('input-group')}>
                        <label htmlFor="mrp">MRP</label>
                        <input type="number" min="0.00" step="0.01" ref="mrp" defaultValue={this.saleItem && this.saleItem.mrp} />
                    </div>

                    <div className={cx('input-group')}>
                        <label htmlFor="qty">quantity</label>
                        <input type="number" min="0" step="1" ref="qty" defaultValue={this.saleItem && this.saleItem.qty} />
                    </div>
                    <div className={cx('input-group')}>
                        <label htmlFor="delivery_date">Delivery Date</label>
                        <input type="date" ref="delivery_date" defaultValue={this.saleItem && this.saleItem.delivery_date} />
                    </div>
                    {mtm_fields}
                    <div className={cx('input-group')}>
                        <label htmlFor="comments">Comments</label>
                        <textarea id="comment" ref="comment" value={this.saleItem && this.saleItem.comment} onChange = {(e) => this.saveSelectFormValues(e.target.value,'comment')}></textarea>
                    </div>
                    <button onClick={this.delete} className={cx('action', 'warning')}>Delete</button>
                    <button onClick={this.save} className={cx('action', 'primary')}>Save Sale</button>

                </div>
            </div>
        );
    }
}

OrderSaleItem.propTypes = {
    user: PropTypes.object,
    customer: PropTypes.object,
    order: PropTypes.object,
    lists: PropTypes.object
};


function mapStateToProps({ order, lists, user, customer }) {
    return {
        order,
        lists,
        user,
        customer
    };
}

export default connect(mapStateToProps, null, null, { withRef: true })(OrderSaleItem);