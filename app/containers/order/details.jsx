import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

import classNames from 'classnames/bind';
import styles from 'css/components/order/details';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { updatePriorityType, updatePaymentType, updateTailor, updateOrderDetails, updateBenificiaryPhone, updateOccasion, updateSalesman, updateMessage } from '../../actions/order';
import { fetchList } from '../../actions/list';
import NumberForm from '../../components/NumberForm';
import SelectForm from '../../components/SelectForm';
import back from 'images/back-arrow.png';
import moment from 'moment';

const cx = classNames.bind(styles);

class OrderDetails extends Component {
	constructor(props) {
		super(props);
		this.saveValues = this.saveValues.bind(this);
		this.saveSelectFormValue = this.saveSelectFormValue.bind(this);
		this.saveNumberFieldValue = this.saveNumberFieldValue.bind(this);
		this.getSelected = this.getSelected.bind(this);
		this.getOccasionID = this.getOccasionID.bind(this);
	}

	componentDidMount() {
		if (this.props.lists) {
			if (this.props.lists.payment_types && this.props.lists.payment_types.length === 0)
				this.props.dispatch(fetchList('payment_type'));
			if (this.props.lists.tailors && this.props.lists.tailors.length === 0)
				this.props.dispatch(fetchList('tailor'));
			if (this.props.lists.occasions && this.props.lists.occasions.length === 0)
				this.props.dispatch(fetchList('occasion'));
			if (this.props.lists.salesmen && this.props.lists.salesmen.length === 0)
				this.props.dispatch(fetchList('salesman'));
		}
		window.scrollTo(0, 0)
	}

	saveValues() {

		const _that = this;
		const getDisplayName = () => {
			const _input_value = ReactDOM.findDOMNode(this.refs.order_name).value;
			if (_input_value.trim().length == 0) {
				return ReactDOM.findDOMNode(_that.refs.date).value;
			} else {
				return _input_value;
			}
		};
		const formValues = {
			//From fields in this component
			order_date: ReactDOM.findDOMNode(_that.refs.date).value,
			occasion_date: ReactDOM.findDOMNode(_that.refs.occasion_date).value,
			benificiary_name: ReactDOM.findDOMNode(_that.refs.benificiary_name).value,
			benificiary_email: ReactDOM.findDOMNode(_that.refs.benificiary_email).value,
			payment_details: ReactDOM.findDOMNode(_that.refs.payment_details).value,
			comment: ReactDOM.findDOMNode(_that.refs.comment).value,
			billing_address: ReactDOM.findDOMNode(_that.refs.billing_address).value,
			delivery_address: ReactDOM.findDOMNode(_that.refs.delivery_address).value,
			display_name: getDisplayName(),

			//From props
			benificiary_phone: _that.props.order.details.benificiary_phone,
			occasion: _that.props.order.details.occasion,
			payment_type_id: _that.props.order.details.payment_type_id,
			tailor_id: _that.props.order.details.tailor_id,
			salesman_id: _that.props.order.details.salesman_id,
			customer_id: _that.props.customer.customer_id,
			user_id: _that.props.user.user.id || _that.props.user.user.user_id,
			store_id: _that.props.stores.selected.store_id,
			order_id: _that.props.order.details.order_id
		}
		if (formValues.payment_type_id > 0 && formValues.salesman_id >= 0 && formValues.tailor_id >= 0 && formValues.display_name.length > 0 && formValues.billing_address.length>0 && formValues.delivery_address.length>0) {
			this.props.dispatch(updateOrderDetails(formValues));
		} else {
			this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', 'Payment Type, Salesman, Tailor, Shipping Address and Billing Address are mandatory'));
			window.scrollTo(0, 0)
		}

	}
	saveSelectFormValue(selected, type) {
		switch (type) {
			case 'payment_type':
				this.props.dispatch(updatePaymentType(selected.value));
				break;
			case 'tailor':
				this.props.dispatch(updateTailor(selected.value));
				break;
			case 'occasion':
				const occasion = _.find(this.props.lists.occasions, { id: parseInt(selected.value) }).name;
				this.props.dispatch(updateOccasion(occasion));
				break;
			case 'salesman_id':
				this.props.dispatch(updateSalesman(selected.value));
				break;

		}
	}
	getSelected(type) {
		switch (type) {
			case 'payment_type':
				if (this.props.lists.payment_types && this.props.lists.payment_types.length > 0 && this.props.order.details.payment_type)
					return _.find(this.props.lists.payment_types, { id: this.props.order.details.payment_type }).name;
				else
					return '';
			case 'tailor':
				if (this.props.lists.tailors && this.props.lists.tailors.length > 0 && this.props.order.details.tailor)
					return _.find(this.props.lists.tailors, { id: this.props.order.details.tailor_id }).id;
				else
					return '';
			case 'salesman_id':
				if (this.props.lists.salesmen && this.props.lists.salesmen.length > 0 && this.props.order.details.salesman_id)
					return _.find(this.props.lists.salesmen, { id: this.props.order.details.salesman_id }).id;
				else
					return '';
		}
	}

	saveNumberFieldValue(value, type) {
		switch (type) {
			case 'benificiary_phone':
				this.props.dispatch(updateBenificiaryPhone(value));
		}
	}

	getOccasionID() {
		if (this.props.order.details.occasion.length > 0) {
			const occasion = _.find(this.props.lists.occasions, { name: this.props.order.details.occasion });
			if (occasion && occasion.id)
				return occasion.id;
			else
				return -1;
		}
		else {
			return -1;
		}
	}
	render() {
		return (
			<div className={cx('container')}>
				<div className={cx('header-note')}>
					<span className={cx('header-label')}>Customer:   </span>
					<span className={cx('header-content')}>{this.props.customer.name}</span>
				</div>
				<Link to="/order" className={cx('review')}>
					Review
				</Link>
				<Link to="/order/customer" className={cx('back')} ><img src={back} /></Link>
				<h1>Order Details</h1>
				<div className={cx('form-container')}>
					<div className={cx('input-group')}>
						<label htmlFor="date">Order Date</label>
						<input type="date" id="date" ref="date" defaultValue={this.props.order.details.order_date || moment().format('YYYY-MM-DD')} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="date">Order Name</label>
						<input type="text" id="order_name" ref="order_name" defaultValue={this.props.order.details.display_name} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="occasion">Occasion</label>
						<SelectForm type="occasion" rel="occasion" options={this.props.lists.occasions} value={this.getOccasionID()} save={this.saveSelectFormValue} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="occasion_date">Occasion Date</label>
						<input type="date" id="occasion_date" ref="occasion_date" defaultValue={this.props.order.details.occasion_date} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="benificiery_name">Benificiary Name</label>
						<input type="text" id="benificiary_name" ref="benificiary_name" defaultValue={this.props.order.details.benificiary_name} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="benificiary_email">Benificiary Email</label>
						<input type="email" id="benificiary_email" ref="benificiary_email" defaultValue={this.props.order.details.benificiary_email} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="benificiary_phone">Benificiary Phone</label>
						<NumberForm type="phone" rel="benificiary_phone" save={this.saveNumberFieldValue} default={this.props.order.details.benificiary_phone} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="address">Billing Address</label>
						<textarea id="billing_address" ref="billing_address" defaultValue={this.props.order.details.billing_address} ></textarea>
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="address">Delivery Address</label>
						<textarea id="delivery_address" ref="delivery_address" defaultValue={this.props.order.details.delivery_address} ></textarea>
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="payment_type">Payment Type</label>
						<SelectForm type="payment_type" rel="payment_type" options={this.props.lists.payment_types} value={this.props.order.details.payment_type_id} save={this.saveSelectFormValue} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="tailor">Tailor</label>
						<SelectForm type="tailor" rel="tailor" options={this.props.lists.tailors} value={this.props.order.details.tailor_id} save={this.saveSelectFormValue} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="tailor">Salesman</label>
						<SelectForm type="salesman_id" rel="salesman_id" options={this.props.lists.salesmen} value={this.props.order.details.salesman_id} save={this.saveSelectFormValue} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="payment_details">Payment Details</label>
						<input type="text" id="payment_details" ref="payment_details" defaultValue={this.props.order.details.payment_details} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="comments">Comments</label>
						<textarea id="comment" ref="comment" defaultValue={this.props.order.details.comment} ></textarea>
					</div>
					<div className={cx('divider')}></div>
					<button onClick={this.saveValues} className={cx('action', 'primary')}>Save</button>
				</div>
			</div>
		);
	}
}


OrderDetails.propTypes = {
	order: PropTypes.object,
	lists: PropTypes.object,
	user: PropTypes.object,
	stores: PropTypes.object,
	customer: PropTypes.object
};


function mapStateToProps({ order, lists, user, stores, customer }) {
	return {
		order,
		lists,
		user,
		stores,
		customer
	};
}

export default connect(mapStateToProps)(OrderDetails);