import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import * as types from 'types';
import classNames from 'classnames/bind';
import styles from 'css/components/order/sales';
import SelectForm from 'components/SelectForm';
import MultipleChoice from 'components/MultipleChoice';
import MeasurementsForm from 'components/MeasurementsForm';
import ImageGallery from 'components/ImageGallery';
import StylesForm from 'components/StylesForm';
import {
	push
} from 'react-router-redux';
import { fetchList } from '../../actions/list';
import {
	openForm,
	updateSaleItemEntry,
	saveProfile,
	saveOrderItem,
	saveImageAWS,
	updateOrderTotal,
	updateMessage,
	getThumbnails,
	showThumbnail
} from '../../actions/order';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import back from 'images/back-arrow.png';
import { setActiveKey } from '../../actions/history';
import _ from 'lodash';
const cx = classNames.bind(styles);

class MeasurementsAndStyles extends Component {
	constructor(props) {
		super(props);
		this.saleItem = this.props.order.sales[this.props.params.id];
		this.openForm = this.openForm.bind(this);
		this.closeForm = this.closeForm.bind(this);
		this.saveMeasurements = this.saveMeasurements.bind(this);
		this.saveFabricDesign = this.saveFabricDesign.bind(this);
		this.saveImageAWS = this.saveImageAWS.bind(this);
		this.showMessage = this.showMessage.bind(this);
		this.showPreviewImage = this.showPreviewImage.bind(this);
		this.toggleAccordion = this.toggleAccordion.bind(this);
	}
	componentDidMount() {
		if (!this.saleItem)
			this.saleItem = this.props.order.sales[this.props.params.id];
		if (this.props.order.thumbnails.length == 0) {
			this.props.dispatch(getThumbnails(this.props.order.details.order_id));
		}
	}
	componentDidUpdate() {
		if (!this.saleItem)
			this.saleItem = this.props.order.sales[this.props.params.id];
	}

	showMessage(type, value) {
		this.props.dispatch(updateMessage(type, value));
	}

	openForm(type) {
		this.props.dispatch(openForm(type));
	}
	closeForm(type) {
		this.props.dispatch(openForm(type, true));
	}

	loadFields(type, item_type_id, measurement_type_id, clear) {
		switch (type) {
			case 'styles':
				this.props.dispatch(fetchList('fabric_design_field', { item_type_id: item_type_id, clear: clear }));
				break;
			case 'measurements':
				this.props.dispatch(fetchList('measurement_field', { item_type_id: item_type_id, measurement_type_id: measurement_type_id, clear }));
				break;
		}
	}

	saveMeasurements(value, isTemp) {
		if (isTemp) {
			this.props.dispatch(updateSaleItemEntry(value, 'measurements', this.props.params.id));
			return;
		}
		value.profile.customer_id = this.props.customer.customer_id;
		value.profile.item_type_id = this.props.order.sales[this.props.params.id].item_type_id;
		saveProfile(value.profile, value.measurements).then(function (profile_id) {
			value.profile.profile_id = profile_id;
			this.props.dispatch(updateSaleItemEntry(value.profile, 'profile', this.props.params.id));
			this.props.dispatch(updateSaleItemEntry(profile_id, 'profile_id', this.props.params.id));
			this.props.dispatch(updateSaleItemEntry(value.profile.measurement_source_id, 'measurement_source_id', this.props.params.id));
			this.props.dispatch(updateSaleItemEntry(false, 'is_new_measurements', this.props.params.id));
			this.props.dispatch(saveOrderItem(this.props.order.sales[this.props.params.id], true, this.props.params.id));
			this.props.dispatch(updateMessage(types.SAVE_MEASUREMENT_PROFILE_SUCCESS, 'Profile & Measurements have been saved'));
		}.bind(this));
	}

	saveFabricDesign(fabric_design, comment, upCharges) {
		this.props.dispatch(updateSaleItemEntry(fabric_design, 'fabric_designs', this.props.params.id));
		this.props.dispatch(updateSaleItemEntry(comment, 'fabric_design_comment', this.props.params.id));
		this.props.dispatch(updateSaleItemEntry(upCharges, 'upcharges', this.props.params.id));
	}

	saveImageAWS(file) {
		this.props.dispatch(saveImageAWS(file, this.props.order.details.order_id, types.IMAGE_TYPE_MEASUREMENT));
	}
	showPreviewImage(dataURL, show) {
		this.props.dispatch(showThumbnail(dataURL, show));
	}
	toggleAccordion(activeKey) {
		this.props.dispatch(setActiveKey(activeKey));
	}
	render() {
		this.saleItem = this.props.order.sales[this.props.params.id];
		if (!this.props.order.isFormOpen.measurements && !this.props.order.isFormOpen.styles) {
			let measurement = null;
			if (this.saleItem.is_new_measurements) {
				measurement = <div className={cx('action-item')} onClick={this.openForm.bind(this, 'measurements')}>Add Measurements</div>;
			}
			return (
				<div className={cx('container')}>

					<Link to="/order/sales" className={cx('back')} ><img src={back} /></Link>
					<div className={cx('form-container')}>
						{measurement}
						<div className={cx('action-item')} onClick={this.openForm.bind(this, 'styles')}>Add Style Details</div>
						<ImageGallery thumbnails={this.props.order.thumbnails} thumbnail={this.props.order.thumbnail} showThumbnail={this.showPreviewImage} />
						<Link className={cx('action', 'primary')} to={'/order/sales/'}>Back</Link>
					</div>
				</div>
			);
		} else if (this.props.order.isFormOpen.measurements) {
			return (
				<div className={cx('container')}>
					<MeasurementsForm
						lists={this.props.lists}
						loadFields={this.loadFields.bind(this, 'measurements', this.saleItem.item_type_id)}
						close={this.closeForm.bind(this, 'measurements')}
						save={this.saveMeasurements}
						profile={this.saleItem.profile}
						addImage={this.saveImageAWS}
						message={this.showMessage} />
				</div>);
		} else if (this.props.order.isFormOpen.styles) {
			let saleItem = this.saleItem;
			let upcharges = saleItem.upcharges || saleItem.upcharge || [];

			if(!_.isArray(upcharges)) {
				upcharges = [];
			}
			return (
				<div className={cx('container')}>
					<StylesForm
						lists={this.props.lists}
						save={this.saveFabricDesign}
						close={this.closeForm.bind(this, 'styles')}
						loadFields={this.loadFields.bind(this, 'styles', this.saleItem.item_type_id)}
						fabricDesign={this.saleItem.fabric_designs}
						comment={this.saleItem.fabric_design_comment}
						upcharges={upcharges}
						toggleAccordion={this.toggleAccordion}
						activeKey={this.props.history.accordionKey}
					/>
				</div>
			);
		}
	}
}

MeasurementsAndStyles.propTypes = {
	user: PropTypes.object,
	customer: PropTypes.object,
	order: PropTypes.object,
	lists: PropTypes.object,
	history: PropTypes.object
};


function mapStateToProps({ order, lists, user, customer, history }) {
	return {
		order,
		lists,
		user,
		customer,
		history
	};
}

export default connect(mapStateToProps, null, null, { withRef: true })(MeasurementsAndStyles);
