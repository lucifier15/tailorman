import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from 'css/components/order/sales';
import { fetchList } from '../../actions/list';
import { getOrderItems, selectOrder, saveImageAWS, openForm, saveProfile, saveOrderItem, updateMessage, updateSaleItemEntry } from '../../actions/order';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import SelectForm from 'components/SelectForm';
import MeasurementsForm from 'components/MeasurementsForm';
const cx = classNames.bind(styles);

class OrderAlteration extends Component {
	constructor(props) {
		super(props);
		this.select = this.select.bind(this);
		this.renderOrderItems = this.renderOrderItems.bind(this);
		this.saveMeasurements = this.saveMeasurements.bind(this);
		this.closeForm = this.closeForm.bind(this);
		this.saveImageAWS = this.saveImageAWS.bind(this);
		this.order = {};
		this.order_item = {};
	}

	componentDidMount() {
		this.props.dispatch(fetchList('orders', { customer_id: this.props.customer.customer_id }));
		this.props.dispatch(fetchList('measurement_type'));
	}

	select(selected, type) {
		switch (type) {
			case 'order':
				const order = _.find(this.props.lists.orders, { id: selected.value });
				this.props.dispatch(selectOrder(order), false);
				this.props.dispatch(getOrderItems(selected.value, this.props.customer.customer_id));
				this.order = order;
				break;
			case 'order_item':
				this.order_item = _.find(this.props.order.sales, { order_item_id: selected.value });
				this.order_item.profile.measurement_source_id = _.find(this.props.lists.measurement_types, { name: 'Alteration' }).id;
				console.log(this.order_item);
				this.props.dispatch(openForm('measurements'));
				break;
		}
	}
	renderOrderItems() {
		if (this.props.order && this.props.order.sales && this.props.order.sales.length > 0) {
			return (
				<div className={cx('input-group')}>
					<label htmlFor="select_order">Select Item</label>
					<SelectForm type="order_item" rel="order_item" options={this.props.order.sales} save={this.select} />
				</div>
			);
		} else {
			return <h4>No Order Items</h4>;
		}
	}

	loadFields(type, item_type_id, measurement_type_id, clear) {
		this.props.dispatch(fetchList('measurement_field', { item_type_id: item_type_id, measurement_type_id: measurement_type_id, clear }));
	}

	closeForm(type) {
		this.props.dispatch(openForm('measurements', true));
	}
	saveMeasurements(value) {
		if (value.profile) {
			value.profile.customer_id = this.props.customer.customer_id;
			value.profile.item_type_id = this.order_item.item_type_id;
			value.profile.profile_id = undefined;
			saveProfile(value.profile, value.measurements).then(function (profile_id) {
				this.order_item.workflow_id = 2;
				this.order_item.profile_id = profile_id;
				value.profile.profile_id = profile_id;
				const saleIndex = _.findIndex(this.props.order.sales, { order_item_id: this.order_item.order_item_id });
				this.props.dispatch(updateSaleItemEntry(value.profile, 'profile', saleIndex));
				this.props.dispatch(saveOrderItem(this.order_item, false, saleIndex));
				this.props.dispatch(updateMessage('ALTERATION_SAVE_SUCCESS', 'The measurements have been altered'));
			}.bind(this));
		}

	}

	saveImageAWS(dataURL) {
		this.props.dispatch(saveImageAWS(dataURL, this.props.order.details.order_id));
	}

	render() {
		if (this.props.order.isFormOpen.measurements) {
			return (
				<div className={cx('container')}>
					<MeasurementsForm
						lists={this.props.lists}
						loadFields={this.loadFields.bind(this, 'measurements', this.order_item.item_type_id)}
						close={this.closeForm}
						save={this.saveMeasurements}
						profile={this.order_item.profile}
						addImage={this.saveImageAWS} />
				</div>);
		} else {
			return (
				<div className={cx('container')}>
					<h1>Alterations</h1>
					<div className={cx('form-container')}>
						<div className={cx('input-group')}>
							<label htmlFor="select_order">Select Order</label>
							<SelectForm type="order" rel="order" value={this.props.order.details.order_id} options={this.props.lists.orders} save={this.select} />
						</div>
						{this.renderOrderItems()}
						<Link className={cx('action', 'primary')} to='/stores'>Done</Link>
					</div>
				</div>
			);
		}

	}
}

OrderAlteration.propTypes = {
	user: PropTypes.object,
	customer: PropTypes.object,
	order: PropTypes.object,
	lists: PropTypes.object
};


function mapStateToProps({order, lists, user, customer}) {
	return {
		order,
		lists,
		user,
		customer
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps, null, null, { withRef: true })(OrderAlteration);
