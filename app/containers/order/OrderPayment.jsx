import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from 'css/components/order/sales';
import SelectForm from 'components/SelectForm';
import MultipleChoice from 'components/MultipleChoice';
import ImageCapture from 'components/ImageCapture';
import Signature from 'components/Signature';
import * as types from 'types';
import {
	push
} from 'react-router-redux';
import { fetchList } from '../../actions/list';
import _ from 'lodash';
import {
	updateOrderTotal,
	saveCompleteOrder,
	updateFullPaymentFlag,
	saveImage,
	saveImageAWS,
	calculateTaxes,
	updateOrderPIN,
	updateOrderPINDate,
	openForm,
	updateSaleItemEntry,
	updateMessage
} from '../../actions/order';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import back from 'images/back-arrow.png';
const cx = classNames.bind(styles);


class OrderPayment extends Component {
	constructor(props) {
		super(props);
		this.renderSaleItems = this.renderSaleItems.bind(this);
		this.saveOrder = this.saveOrder.bind(this);
		this.updateFullPayment = this.updateFullPayment.bind(this);
		this.saveImageAWS = this.saveImageAWS.bind(this);		
		this.updateDetails = this.updateDetails.bind(this);
		this.isEdited = false;
		this.toggleSignature = this.toggleSignature.bind(this);
		this.saveSignature = this.saveSignature.bind(this);
		this.updateDiscount = this.updateDiscount.bind(this);
		this.updateSubTotals = this.updateSubTotals.bind(this);
		this.updateDiscountComment = this.updateDiscountComment.bind(this);

	}

	componentDidMount() {
		this.props.dispatch(calculateTaxes(this.props.order.sales, this.props.order.details.store_id));
		window.scrollTo(0, 0)
	}

	shouldComponentUpdate(props, state) {
		return this.props.order !== props.order;
	}

	updateDiscount(saleIndex, value, rel) {
		if (_.isArray(value)) {
			switch (value[0]) {
				case '%':
					this.props.dispatch(updateSaleItemEntry(types.UPCHARGE_UNIT_PERCENTAGE, 'discount_type', saleIndex));
					break;
				case 'INR':
					this.props.dispatch(updateSaleItemEntry(types.UPCHARGE_UNIT_VALUE, 'discount_type', saleIndex));
					break;
			}
		} else {
			this.props.dispatch(updateSaleItemEntry(value.target.value, 'discount_value', saleIndex));
		}
	}

	updateDiscountComment(saleIndex, value, rel) {
	
	 this.props.dispatch(updateSaleItemEntry(value.target.value, 'discount_comment', saleIndex));
	}
	updateSubTotals() {
		this.props.dispatch(calculateTaxes(this.props.order.sales, this.props.order.details.store_id));
	}

	renderSaleItems() {
		const self = this;
		return this.props.order.sales.map(function (sale, _index) {
			const upCharges = (sale) => {
				if (sale.upcharges || sale.profile) {
					let upcharges = (sale.upcharges && _.isArray(sale.upcharges)) ? sale.upcharges : [];
					if (sale.profile && sale.profile.measurements && sale.profile.measurements.length > 0) {
						upcharges = upcharges.concat(_.compact(_.map(sale.profile.measurements, 'upcharge')));
					}
					const calcUpcharge = (sale, upcharge) => {
						if (upcharge.unit === types.UPCHARGE_UNIT_PERCENTAGE)
							return Math.floor(parseFloat(sale.mrp) * parseFloat(upcharge.value) / 100);
						else if (upcharge.unit == types.UPCHARGE_UNIT_VALUE)
							return upcharge.value;
						else if (upcharge.value)
							return upcharge.value;
					}
					const additionals = upcharges.map((upcharge, index) => {
						if (upcharge.type != types.DELIVERY_UPCHARGE)
							return (<div key={index}><span className={cx('label')}>{upcharge.type.toLowerCase()}</span><span className={cx('value')}>{calcUpcharge(sale, upcharge)}</span></div>);
					});
					return (<div><span className={cx('label')}>Additional Charges <em>(per item)</em></span>{additionals}</div>);
				}
			}

			const taxes = (sale) => {
				if (sale.taxes && sale.taxes.length > 0) {
					const _taxes = sale.taxes.map((tax, index) => {
						return (
							<div key={index}><span className={cx('label')}>{tax.name}</span><span className={cx('value')}>{tax.percent + "%"}</span></div>
						);
					});
					return (<div><span className={cx('label')}>Taxes <em>(per item)</em></span>{_taxes}</div>);
				}
			};

			const totalTax = (sale) => {
				if (sale.taxes && sale.taxes.length > 0) {
					return ( sale.taxes.reduce((prev, next) => { return prev + next.value }, 0).toFixed(2));
				}
			}


			let upcharges = 0;

			if (sale.upcharges && sale.upcharges.length > 0) {
				upcharges = sale.upcharges.reduce((prev, next) => { return prev + next.value }, 0);
			}

			const saleDiscountType = (sale.discount_type == types.UPCHARGE_UNIT_PERCENTAGE) ? '%' : 'INR';

			let delivery_upcharge_item = null;
			const delivery_upcharge = _.find(sale.upcharges, { type: types.DELIVERY_UPCHARGE })
			if (delivery_upcharge) {
				delivery_upcharge_item = <div><span className={cx('label')}>Delivery</span><span className={cx('value')}>{delivery_upcharge.value}</span></div>
			}

			const getGrossValue = (sale, totalTax) => {
				let mrp = sale.discountedMRP;
				if (!_.isNaN(totalTax)) {
					return mrp - totalTax;
				} else {
					return '';
				}
			}

			return (
				<li key={_index}>
					<div><span className={cx('label')}>{sale.display_name}</span></div>
					{upCharges(sale)}
					<div className={cx('discount')}>
						<span className={cx('label')}>Discount</span>
						<div className={cx('value-group')}>
							<input type="number" rel="discount" onChange={self.updateDiscount.bind(this, _index)} defaultValue={sale.discount_value ? sale.discount_value : 0} />
							<MultipleChoice isMultiple={false} rel="discount_type" options={['%', 'INR']} selected={saleDiscountType} save={self.updateDiscount.bind(this, _index)} />
							<button className={cx('action')} onClick={self.updateSubTotals}>add</button>
						</div>
					</div>
					<div className={cx('input-group')} ><span className={cx('label')}>Discount Comment</span><input type="text" className={cx('large')} rel="discount_comment"  onChange={self.updateDiscountComment.bind(this, _index)} defaultValue={sale.discount_comment ? sale.discount_comment : ""} ></input></div>

					<div><span className={cx('label')}>Gross Value <em>(per item)</em></span><span className={cx('value')}>{getGrossValue(sale, totalTax(sale)).toFixed(2)}</span></div>
					{taxes(sale)}
					<div><span className={cx('label')}>Total Tax <em>(per item)</em></span><span className={cx('value')}>{!_.isNaN(totalTax(sale)) ? totalTax(sale) : ''}</span></div>
					{delivery_upcharge_item}
					<div><span className={cx('label')}>Quantity</span><span className={cx('value')}>{sale.qty}</span></div>
					<div><span className={cx('label')}>Sub Total</span><span className={cx('value')}>{!_.isNaN(parseFloat(sale.subTotal).toFixed(2)) ? sale.subTotal : ''}</span></div>
				</li>
			);
		})
	}

	saveImageAWS(file) {
		this.props.dispatch(saveImageAWS(file, this.props.order.details.order_id, types.IMAGE_TYPE_INVOICE));
	}
	updateFullPayment(value, type) {

		if (value[0] == 'yes') {
			value = true;
		} else {
			value = false;
		}

		switch (type) {
			case 'order_flag':
				const _val = value ? 2 : 1;
				this.props.order.sales.map((sale, index) => {
					this.props.dispatch(updateSaleItemEntry(_val, 'order_flag', index));
				});
				break;
			case 'is_full_payment':
				this.props.dispatch(updateFullPaymentFlag(value));
				break;
		}

	}
	updateDetails(type) {
		switch (type) {
			case 'pin':
				this.props.dispatch(updateOrderPIN(ReactDOM.findDOMNode(this.refs.PIN).value));
				break;
			case 'pindate':
				this.props.dispatch(updateOrderPINDate(ReactDOM.findDOMNode(this.refs.PINDate).value));
				break;
			case 'total':
				this.props.dispatch(updateOrderTotal(ReactDOM.findDOMNode(this.refs.total_amount).value));
				this.isEdited = true;
				break;
		}
	}

	saveOrder() {
		function validate(order) {
			let measurent_missing = false;
			if(!order.details.PIN){
				return 'Please Enter the Proforma Invoice Number';
			}
			if(!order.details.pindate){
				return 'Please Enter the Proforma Invoice Date';
			}
			let discount_comment_missing = false;
			for (var i = 0; i < order.sales.length; i++) {
				if (order.sales[i].isMTM) {
					if (order.sales[i].profile_id <= 0 && order.sales[i].order_flag == 1){
						measurent_missing = true;
					}
				}
				if(order.sales[i].discount_value && order.sales[i].discount_value != 0){
					if(!order.sales[i].discount_comment){
						discount_comment_missing = true;
					}
				}
			}
			if(measurent_missing && discount_comment_missing){
                  return 'Please put the order on hold, as one or more of the sale items donot have measurements and discount comment'
			}else if(measurent_missing){
				return 'Please put the order on hold, as one or more of the sale items donot have measurement'
			}else if(discount_comment_missing){
				return 'Please put the order on hold, as one or more of the sale items donot have discount comment'
			}else{
				return null;
			}
		}
		let validateMessage = validate(this.props.order);
		if (!validateMessage) {
			const order = this.props.order;
			order.sales = order.sales.map((sale) => {
				if (sale.isMTM) {
					let upcharges = sale.upcharges || [];
					if (sale.profile.measurements) {
						upcharges = upcharges.concat(_.compact(_.map(sale.profile.measurements, 'upcharge')))
					}
					sale.upcharges = upcharges;
				}
				return sale;
			});
			this.props.dispatch(saveCompleteOrder(order, this.props.user.user));
		} else {
			this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', validateMessage));
			window.scrollTo(0, 0);
		}

	}
	toggleSignature() {
		this.props.dispatch(openForm('signature', this.props.order.isFormOpen.signature));
	}
	saveSignature(dataURL) {
		this.props.dispatch(saveImage(dataURL, this.props.order.details.order_id, types.IMAGE_TYPE_SIGNATURE));
	}
	render() {
		let signature = null;
		if (this.props.order.isFormOpen.signature) {
			signature = <Signature save={this.saveSignature} rel="customer_signature" close={this.toggleSignature} />
		}
		return (
			<div className={cx('container')}>
				<h1>Payment</h1>
				<div className={cx('header-note')}>
					<span className={cx('header-label')}>Customer:   </span>
					<span className={cx('header-content')}>{this.props.customer.name}</span>
				</div>
				<Link to="/order" className={cx('review')}>
					Review
				</Link>
				<Link to="/order/sales" className={cx('back')} ><img src={back} /></Link>
				<div className={cx('form-container')}>
					<div className={cx('input-group')}>
						<label htmlFor="total_amount">Proforma Invoice Number</label>
						<input className={cx('large')} type="text" ref="PIN" value={this.props.order.details.PIN} onChange={this.updateDetails.bind(this, 'pin')} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="total_amount">Proforma Invoice Date</label>
						<input className={cx('large')} type="datetime-local" ref="PINDate" value={this.props.order.details.pindate} onChange={this.updateDetails.bind(this, 'pindate')} />
					</div>
					<ul className={cx('cart')}>
						{this.renderSaleItems()}
					</ul>
					<ImageCapture addImage={this.saveImageAWS} />
					<div className={cx('input-group')}>
						<label htmlFor="is_full_payment">Full Payment</label>
						<MultipleChoice isMultiple={false} options={['yes', 'no']} selected={(this.props.order.details.is_full_payment === false) ? 'no' : 'yes'} rel="is_full_payment" save={this.updateFullPayment} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="is_on_hold">On Hold</label>
						<MultipleChoice isMultiple={false} options={['yes', 'no']} selected={(this.props.order.sales && this.props.order.sales[0] && this.props.order.sales[0].order_flag == 1) ? 'no' : 'yes'} rel="order_flag" save={this.updateFullPayment} />
					</div>
					{signature}
					<div className={cx('input-group')}>
						<label htmlFor="total_amount">Total Amount</label>
						<input className={cx('large')} type="text" ref="total_amount" value={!this.props.order.details.total_amount ? '' : this.props.order.details.total_amount} />
					</div>
					<button className={cx('action', 'primary')} onClick={this.toggleSignature}>Add Customer Signature</button>
					{
						//<a className={cx('action', 'secondary')} target="_blank" to={"/order/invoice?customer_id="+ this.props.customer.customer_id+"&order_id="+this.props.order.details.order_id}>Email Invoice</a>
					}
					<button className={cx('submit', 'action', 'primary')} onClick={this.saveOrder}>Save Order</button>
				</div>
			</div>
		);
	}
}

OrderPayment.propTypes = {
	user: PropTypes.object,
	customer: PropTypes.object,
	order: PropTypes.object,
	lists: PropTypes.object
};


function mapStateToProps({ order, lists, user, customer }) {
	return {
		order,
		lists,
		user,
		customer
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps, null, null, { withRef: true })(OrderPayment);
