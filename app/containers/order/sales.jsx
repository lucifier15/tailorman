import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import {
    push
} from 'react-router-redux';
import classNames from 'classnames/bind';
import styles from 'css/components/order/sales';
import { fetchList } from '../../actions/list';
import { getOrderItems, getUpCharge, updateSaleItemEntry, updateMessage } from '../../actions/order';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import back from 'images/back-arrow.png';
const cx = classNames.bind(styles);

class OrderSales extends Component {
    constructor(props) {
        super(props);
        this.saleItemsList = this.saleItemsList.bind(this);
        this.getSaleItemIndex = this.getSaleItemIndex.bind(this);
        this.validateOrderItems = this.validateOrderItems.bind(this);
    }
    componentDidMount() {
        if (this.props.order.sales.length === 0)
            this.props.dispatch(getOrderItems(this.props.order.details.order_id, this.props.order.details.customer_id));

        if (this.props.lists) {
            if (this.props.lists.item_types && this.props.lists.item_types.length === 0)
                this.props.dispatch(fetchList('item_type'));
            if (this.props.lists.styles && this.props.lists.styles.length === 0)
                this.props.dispatch(fetchList('style'));
            if (this.props.lists.measurement_types && this.props.lists.measurement_types.length === 0)
                this.props.dispatch(fetchList('measurement_type'));
            if (this.props.lists.priorities && this.props.lists.priorities.length === 0)
                this.props.dispatch(fetchList('priority'));
        }
        window.scrollTo(0, 0);
    }


    saleItemsList() {
        var self = this;
        if (this.props.order.sales.length > 0) {
            return this.props.order.sales.map((saleItem, index) => {
                if (saleItem) {
                    return (
                        <div key={index} className={cx('sale-item')}>
                            <div className={cx('display')}><span>{saleItem.display_name}</span></div>
                            <Link className={cx('action', 'small')} to={"/order/sales/" + index + "/type/" + saleItem.item_type_id}>Edit</Link>

                        </div>
                    );
                } else {
                    return null;
                }
            });
        } else {
            return <div className={cx('sale-item')}>No Sale Items Added</div>
        }
    }

    getSaleItemIndex() {
        return this.props.order.sales.length
    }
    validateOrderItems() {
        function validate(sales) {
            if (sales.length > 0) {
                for (var i = 0; i < sales.length; i++) {
                    if (!(sales[i].style > 0 && sales[i].qty > 0)) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        }
        if (validate(this.props.order.sales))
            this.props.dispatch(push('/order/payment'));
        else {
            this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', 'Please fill in the details of all sale items'));
            window.scrollTo(0, 0);
        }
    }
    renderEntries() {
        var self = this;
        var saleItemTypes = this.props.lists.item_types.map(function (item_type, index) {
            return <Link className={cx('add-entry')} key={index} to={"/order/sales/" + self.getSaleItemIndex() + "/type/" + item_type.item_type_id}>{"Add new " + item_type.descr}</Link>
        });
        return (
            <div className={cx('container')}>
                <div className={cx('header-note')}>
                    <span className={cx('header-label')}>Customer:   </span>
                    <span className={cx('header-content')}>{this.props.customer.name}</span>
                </div>
                <h1>Sale Items</h1>
                <div className={cx('form-container')}>
                    {this.saleItemsList()}
                    <div className={cx('input-group')}>
                        {saleItemTypes}
                    </div>
                    <button className={cx('submit', 'action', 'primary')} onClick={this.validateOrderItems} >Checkout</button>
                </div>
            </div>
        )

    }
    render() {
        return (
            <div className={cx('container')}>
                <Link to="/order" className={cx('review')}>
                    Review
				</Link>
                <Link to="/order/details" className={cx('back')} ><img src={back} /></Link>
                {this.renderEntries()}
            </div>
        );
    }
}

OrderSales.propTypes = {
    user: PropTypes.object,
    customer: PropTypes.object,
    order: PropTypes.object,
    lists: PropTypes.object
};


function mapStateToProps({ order, lists, user, customer }) {
    return {
        order,
        lists,
        user,
        customer
    };
}

export default connect(mapStateToProps, null, null, { withRef: true })(OrderSales);
