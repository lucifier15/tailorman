import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

import classNames from 'classnames/bind';
import styles from 'css/components/order/customer';
import ReactDOM from 'react-dom';
import NumberForm from '../../components/NumberForm';
import { connect } from 'react-redux';
import MultipleChoice from '../../components/MultipleChoice';
import SelectForm from '../../components/SelectForm';
import { updateCustomerGender, updateCustomerIsNew, updateCustomerPincode, updateCustomerPhone, updateCustomer, updateCustomerSource } from '../../actions/order';
import { fetchList } from '../../actions/list';
import moment from 'moment';
import back from 'images/back-arrow.png';
const cx = classNames.bind(styles);

class OrderCustomer extends Component {
	constructor(props) {
		super(props);
		this.saveNumberFieldValue = this.saveNumberFieldValue.bind(this);
		this.saveMultiChoiceFieldValue = this.saveMultiChoiceFieldValue.bind(this);
		this.saveValues = this.saveValues.bind(this);
		this.formatDate = this.formatDate.bind(this);
		this.saveSelectFormValue = this.saveSelectFormValue.bind(this);
		this.extractPincode = this.extractPincode.bind(this);
	}
	extractPincode(address) {
		if (address) {
			var lastToken = address.trim().split(" ").slice(-1)[0].trim();
			var _address = address.trim().split(" ").slice(0, -1).join(" ");
			if (parseInt(lastToken) > 99999) {
				return {
					pincode: lastToken,
					address: _address
				}
			} else {
				return {
					address: address,
					pincode: ''
				}
			}
		} else {
			return {
				address: address,
				pincode: ''
			};
		}

	}

	componentDidMount() {
		this.props.dispatch(fetchList('customer_source'));
	}
	formatDate(date) {
		if (date) {
			return moment(date).format('YYYY-MM-DD');
		} else
			return '';

	}
	saveNumberFieldValue(value, field) {
		if (field == 'pincode') {
			this.props.dispatch(updateCustomerPincode(value));
		} else if (field == 'phone') {
			this.props.dispatch(updateCustomerPhone(value))
		}
	}
	saveMultiChoiceFieldValue(value, field, isMultiple) {
		switch (field) {
			case 'gender':
				this.props.dispatch(updateCustomerGender(value));
			case 'new_customer':
				this.props.dispatch(updateCustomerIsNew(value));
		}
	}
	saveSelectFormValue(selected, type) {
		if (type == 'customer_sources') {
			this.props.dispatch(updateCustomerSource(selected.value));
		}
	}

	saveValues() {
		var _that = this;
		const formValues = {
			email: ReactDOM.findDOMNode(_that.refs.email).value,
			name: ReactDOM.findDOMNode(_that.refs.name).value,
			address: ReactDOM.findDOMNode(_that.refs.address).value,
			phone: _that.props.customer.phone,
			gender: _that.props.customer.gender,
			dob: ReactDOM.findDOMNode(_that.refs.dob).value,
			height: ReactDOM.findDOMNode(_that.refs.height).value,
			weight: ReactDOM.findDOMNode(_that.refs.weight).value,
			source_id: _that.props.customer.source_id,
			comment: ReactDOM.findDOMNode(_that.refs.comment).value,
			customer_id: _that.props.customer.customer_id
		}
		this.props.dispatch(updateCustomer(formValues));
	}
	render() {
		return (
			<div className={cx('container')}>
				<Link to="/order" className={cx('review')}>
					Review
				</Link>
				<Link to="/order" className={cx('back')} ><img src={back} /></Link>
				<h1>Customer Details</h1>
				<div className={cx('form-container')}>
					<div className={cx('input-group')}>
						<label htmlFor="name">Name</label>
						<input type="text" id="name" ref="name" defaultValue={this.props.customer.name} />
					</div>

					<div className={cx('input-group')}>
						<label htmlFor="email">Email</label>
						<input type="email" id="email" ref="email" defaultValue={this.props.customer.email} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="dob">Date of Birth</label>
						<input type="date" id="dob" ref="dob" defaultValue={this.formatDate(this.props.customer.dob)} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="phone">Phone</label>
						<NumberForm type="phone" rel="phone" save={this.saveNumberFieldValue} default={this.props.customer.phone} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="address">Address</label>
						<textarea id="address" ref="address" defaultValue={this.props.customer.address} ></textarea>
					</div>

					<div className={cx('input-group')}>
						<label htmlFor="gender">Gender</label>
						<MultipleChoice isMultiple={false} options={['male', 'female']} selected={this.props.customer.gender} rel="gender" save={this.saveMultiChoiceFieldValue} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="height">Height (in Inches)</label>
						<input type="text" ref="height" defaultValue={this.props.customer.height} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="height">Weight (in kg)</label>
						<input type="number" ref="weight" defaultValue={this.props.customer.weight} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="customer_sources">Source</label>
						<SelectForm type="customer_sources" rel="customer_sources" options={this.props.lists.customerSources} value={this.props.customer.source_id} save={this.saveSelectFormValue} />
					</div>
					<div className={cx('input-group')}>
						<label htmlFor="comments">Comments</label>
						<textarea id="comment" ref="comment" defaultValue={this.props.customer.comment} ></textarea>
					</div>
					<button onClick={this.saveValues} className={cx('action', 'primary')}>Save</button>
				</div>
			</div>
		);
	}
}

OrderCustomer.propTypes = {
	order: PropTypes.object,
	users: PropTypes.object,
	customer: PropTypes.object,
	lists: PropTypes.object
};


function mapStateToProps({ order, customer, user, lists }) {
	return {
		order,
		customer,
		user,
		lists
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(OrderCustomer);
