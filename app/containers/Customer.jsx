import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import styles from 'css/components/customer';
import NumberForm from '../components/NumberForm';
import SelectForm from '../components/SelectForm';
import SearchList from '../components/SearchList';
import { selectCustomer, saveCustomerPhone } from '../actions/customer';
import { fetchList, clearList } from '../actions/list';
import { setActiveKey } from '../actions/history';
import { Link } from 'react-router';
import back from 'images/back-arrow.png';
import Collapse, { Panel } from 'rc-collapse';


import _ from 'lodash';
const cx = classNames.bind(styles);

class Customer extends Component {
	constructor(props) {
		super(props);
		this.selectCustomer = this.selectCustomer.bind(this);
		this.saveCustomerPhone = this.saveCustomerPhone.bind(this);
		this.searchCustomers = this.searchCustomers.bind(this);
		this.selectNewCustomer = this.selectNewCustomer.bind(this);
		this.accordion = true;
		this.toggleAccordion = this.toggleAccordion.bind(this);
		this.renderCustomerListItem = this.renderCustomerListItem.bind(this);
	}
	searchCustomers(keyword) {
		if (keyword.length === 0) {
			this.props.dispatch(clearList('customer'));
		} else {
			this.props.dispatch(fetchList('customer', { keyword: keyword }));
		}
	}
	saveCustomerPhone(value) {
		this.props.dispatch(saveCustomerPhone(value));
	}
	selectCustomer(value) {
		var self = this;
		var _customer = _.find(this.props.lists.customers, { customer_id: parseInt(value) });
		this.props.dispatch(selectCustomer(_customer));

	}
	selectNewCustomer() {
		var self = this;
		var _customer = {
			name: ReactDOM.findDOMNode(self.refs.name).value,
			email: ReactDOM.findDOMNode(self.refs.email).value,
			phone: self.props.customer.phone
		}
		this.props.dispatch(selectCustomer(_customer, true));
	}
	toggleAccordion(activeKey) {
		this.props.dispatch(setActiveKey(activeKey));
	}
	renderCustomerListItem(customer) {
		return (
			<div>
				<span className={cx('select-main-label')}>{customer.name}</span>
				<span className={cx('select-email')}>Customer ID: {customer.id}</span>
				<span className={cx('select-email')}>{customer.email}</span>
				<span className={cx('select-phone')}>{customer.phone}</span>
			</div>
		)
	}
	render() {
		return (
			<div className={cx('container')}>
				<h1>Customer</h1>
				<Link to="/stores" className={cx('back')} ><img src={back} /></Link>
				<Collapse
					accordion={this.accordion}
					onChange={this.toggleAccordion}
					activeKey={this.props.history.accordionKey}
					>
					<Panel header="Existing Customers" >
						<div className={cx('form-container')}>
							<h3>Select Customer</h3>
							<div className={cx('input-group')}>
								<label htmlFor="select_customer">Select Existing Customer</label>
								<SearchList
									search={this.searchCustomers}
									select={this.selectCustomer}
									results={this.props.lists.customers}
									renderItem={this.renderCustomerListItem}
									placeholder="search for email, phone or name" />
							</div>
						</div>
					</Panel>
					<Panel header="New Customer">
						<div className={cx('form-container')}>
							<h3>New Customer</h3>
							<div className={cx('input-group')}>
								<label htmlFor="name">Name</label>
								<input type="text" ref="name" />
							</div>
							<div className={cx('input-group')}>
								<label htmlFor="email">Email</label>
								<input type="email" ref="email" />
							</div>
							<div className={cx('input-group')}>
								<label htmlFor="phone">Phone</label>
								<NumberForm type="phone" ref="phone" save={this.saveCustomerPhone} />
							</div>
							<button onClick={this.selectNewCustomer} className={cx('action', 'primary')}>Save</button>
						</div>
					</Panel>
				</Collapse>
			</div>
		);
	}
}

Customer.propTypes = {
	user: PropTypes.object,
	customer: PropTypes.object,
	lists: PropTypes.object,
	history: PropTypes.object
};
function mapStateToProps({customer, user, lists, history}) {
	return {
		customer,
		lists,
		user,
		history
	};
}

export default connect(mapStateToProps)(Customer);