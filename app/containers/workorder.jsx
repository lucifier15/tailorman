import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from 'css/components/workorder';
import {
    push
} from 'react-router-redux';
import SelectForm from 'components/SelectForm';
import MultipleChoice from 'components/MultipleChoice';
import _ from 'lodash';
import { fetchList } from 'actions/list';
import {
    getWorkflowStages,
    setActiveKey,
    getWorkOrders,
    selectWorkOrder,
    sortWorkOrders,
    toggleWorkOrderListItemMenu,
    updateWorkOrderItemStage,
    selectWorkflow as _selectWorkflow,
    saveQueryFields
} from '../actions/workorder';
import {
    updateMessage
} from '../actions/order';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import back from 'images/back-arrow.png';
import Collapse, { Panel } from 'rc-collapse';
import print from 'images/print.png';
import ants from 'images/ants.png';
import moment from 'moment';

const cx = classNames.bind(styles);

class WorkOrder extends Component {
    constructor(props) {
        super(props);
        this.accordion = false;
        this.toggleAccordion = this.toggleAccordion.bind(this);
        this.selectWorkflowStage = this.selectWorkflowStage.bind(this);
        this.renderWorkOrders = this.renderWorkOrders.bind(this);
        this.sortBy = this.sortBy.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.search = this.search.bind(this);
        this.selectWorkflow = this.selectWorkflow.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(fetchList('workflow'));
    }

    toggleAccordion(activeKey) {
        this.props.dispatch(setActiveKey(activeKey));
    }

    selectWorkflowStage(selected) {
        
        let selectedStages = [];
        this.props.workorder.workflowStages.map((stage) => {
            if (selected.indexOf(stage.name) > -1) {
                selectedStages.push(stage.id);
            }
        });
        this.props.dispatch(selectWorkOrder(selectedStages));
    }

    search() {
        const from_date = ReactDOM.findDOMNode(this.refs.from_date).value || moment().subtract(7, 'days').format('YYYY-MM-DD');
        const to_date = ReactDOM.findDOMNode(this.refs.to_date).value || moment().format('YYYY-MM-DD');
        const customer = ReactDOM.findDOMNode(this.refs.customer).value || '';
        const customerId = ReactDOM.findDOMNode(this.refs.customerId).value || null;
        const orderId = ReactDOM.findDOMNode(this.refs.orderId).value || null;
        this.props.dispatch(saveQueryFields([this.props.workorder.stage_id, from_date, to_date, customer,customerId,orderId]));
        this.props.dispatch(getWorkOrders(this.props.workorder.stage_id, from_date, to_date, customer,customerId,orderId));
        this.props.dispatch(setActiveKey(['1', '0']));
    }
    sortBy(type) {
        this.props.dispatch(sortWorkOrders(type));
    }

    toggleMenu(order_item_id, e) {
        this.props.dispatch(toggleWorkOrderListItemMenu(!this.props.workorder.isListItemMenuOpen, order_item_id, this.props.user.user, this.props.workorder.selectedWorkflow));
    }
    updateStage(workorder, stage, e) {
        this.props.dispatch(updateWorkOrderItemStage(workorder, stage, '', this.props.user.user.user_id, this.props.workorder.stage_id));
        this.props.dispatch(getWorkOrders.apply(this, this.props.workorder.query));
        this.props.dispatch(updateMessage('UPDATE_GENERAL_MESSAGE', 'Order Item\'s Stage updated'));
        e.preventDefault();
        e.stopPropagation();
    }
    selectWorkflow(selected) {
        if (selected.length == 1) {
            const workflow = _.find(this.props.workorder.workflows, { code: selected[0] });
            this.props.dispatch(_selectWorkflow(workflow.workflow_id));
            this.props.dispatch(getWorkflowStages(this.props.user.user, workflow.workflow_id));
        } else {
            return;
        }
    }
    renderMenu(workorder) {
        const self = this;
        if (this.props.workorder.selectedWorkOrderItem == workorder.order_item_id && this.props.workorder.isListItemMenuOpen) {
            const stageChanges = this.props.workorder.stageList.map((stage, index) => {
                return (
                    <a key={index} onClick={self.updateStage.bind(this, workorder, stage)}>{"Set as " + stage.code}</a>
                )
            });
            let printWrap =(<Link to={"/workorder/order/" + workorder.order_id + "/item/" + workorder.order_item_id + "/profile/" + workorder.profile_id + "/print"}>View & Print</Link>);
            if(workorder.store == "Online Store" && workorder.modified_by == 0 ){
                printWrap =<Link to={"/workorder/ordernew/" + workorder.order_id + "/item/" + workorder.order_item_id + "/profile/" + workorder.profile_id + "/print"}>View & Print</Link>  
             }
            return (
                <div className={cx('ants-menu')}>
                    {printWrap}
                    {stageChanges}
                    <Link to={"/workorder/update/" + workorder.order_item_id + "/measurements/default"}>Update Measurements</Link>
                    <Link to={"/workorder/update/" + workorder.order_item_id + "/measurements/pattern"}>Update Pattern Measurements</Link>
                    <Link to={"/workorder/update/" + workorder.order_item_id + "/measurements/FGQC"}>Update FGQC Measurements</Link>
                    <Link to={"/workorder/update/" + workorder.order_item_id + "/style"}>Update Styles</Link>
                    <Link to={"/workorder/update/" + workorder.order_item_id + "/details"}>Update Details</Link>
                </div>
            )
        } else {
            return null;
        }
    }

    renderWorkOrders() {
        const self = this;
        if (this.props.workorder.workorders && this.props.workorder.workorders.length > 0) {
            const workordersElements = this.props.workorder.workorders.map((workorder, index) => {
                return <div className={cx('workorder')} key={index}>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'order_id',
                        'small': true
                    })}>{workorder.order_id}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'order_item_id',
                        'small': true
                    })}>{workorder.order_item_id}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'customer_name',
                        'medium': true
                    })}>{workorder.customer_name}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'order_date',
                        'medium': true
                    })}>{moment(workorder.order_date).format('L')}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'product_sku',
                        'medium': true
                    })}>{workorder.product_sku}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'fit_on_date',
                        'medium': true
                    })}>{workorder.fit_on_date}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'delivery_date',
                        'medium': true
                    })}>{workorder.delivery_date}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'mrp',
                        'small': true
                    })}>{workorder.mrp}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'qty',
                        'small': true
                    })}>{workorder.qty}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'priority',
                        'medium': true
                    })}>{workorder.priority}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'cusomer_id',
                        'medium': true
                    })}>{workorder.cusomer_id}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'current_status',
                        'medium': true
                    })}>{workorder.current_status}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'store',
                        'medium': true
                    })}>{workorder.store}</div>
                    <div className={cx({
                        'sorted': this.props.workorder.sortedBy == 'store',
                        'medium': true
                    })}>{workorder.email}</div>
                    <div className={cx({
                        'medium': true
                    })}>{workorder.customer_mobile}</div>


                    <div onClick={this.toggleMenu.bind(this, workorder.order_item_id)} className={cx({ 'toggleMenu': true, 'up': (index > this.props.workorder.workorders.length / 2) })}><img src={ants} />{this.renderMenu(workorder)}</div>
                </div>
            });
            return (
                <div className={cx('workorders')}>
                    <div className="scroll_info">
                        Scroll right to view more data
                    </div>
                    <div className={cx('workorder')}>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'order_id',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'small': true
                        })} onClick={this.sortBy.bind(this, 'order_id')}>Order</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'order_item_id',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'small': true
                        })} onClick={this.sortBy.bind(this, 'order_item_id')}>Item</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'customer_name',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'medium': true
                        })} onClick={this.sortBy.bind(this, 'customer_name')}>Customer</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'order_date',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'medium': true
                        })} onClick={this.sortBy.bind(this, 'order_date')}>Order Date</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'product_sku',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'medium': true
                        })} onClick={this.sortBy.bind(this, 'product_sku')}>SKU</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'product_sku',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'medium': true
                        })} onClick={this.sortBy.bind(this, 'fit_on_date')}>Fit On Date</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'fit_on_date',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'medium': true
                        })} onClick={this.sortBy.bind(this, 'delivery_date')}>Delivery Date</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'delivery_date',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'small': true
                        })} onClick={this.sortBy.bind(this, 'mrp')}>MRP</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'qty',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'small': true
                        })} onClick={this.sortBy.bind(this, 'qty')}>QTY</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'priority',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'medium': true
                        })} onClick={this.sortBy.bind(this, 'priority')}>Priority</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'cusomer_id',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'medium': true
                        })} onClick={this.sortBy.bind(this, 'cusomer_id')}>Customer ID</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'current_status',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'medium': true
                        })} onClick={this.sortBy.bind(this, 'current_status')}>Status</div>
                        <div className={cx({
                            'sorted': this.props.workorder.sortedBy == 'store',
                            'desc': this.props.workorder.sortOrder == 'desc',
                            'asc': this.props.workorder.sortOrder == 'asc',
                            'head': true,
                            'medium': true
                        })} onClick={this.sortBy.bind(this, 'store')}>Store</div>

                        <div className={cx({
                            'head': true,
                            'medium': true
                        })} >Email</div>

                        <div className={cx({
                            'head': true,
                            'medium': true
                        })}>Contact</div>
                       
                    </div>
                    {workordersElements}
                </div>
            )
        } else {
            return (
                <h3> No Work Order items in the date range </h3>
            )
        }
    }

    render() {
        let workordersearch = (
            <div>
                <div className={cx('input-group')}>
                    <label htmlFor="workflowStage">Order Status</label>
                    <MultipleChoice
                        isMultiple={true}
                        options={_.map(this.props.workorder.workflowStages, 'name')} selected={['']} rel='stage' save={this.selectWorkflowStage} />

                    {
                        //<SelectForm type="workflowStage" rel="workflowStage" options={this.props.workorder.workflowStages} value={this.props.workorder.stage_id} save={this.selectWorkflowStage} />
                    }
                </div>
                <div className={cx('input-group')}>
                    <label htmlFor="customer">Customer</label>
                    <input type="text" ref="customer" />
                </div>
                <div className={cx('input-group')}>
                    <label htmlFor="customer">Customer Id</label>
                    <input type="number" ref="customerId" />
                </div>
                <div className={cx('input-group')}>
                    <label htmlFor="customer">Order Id</label>
                    <input type="number" ref="orderId" />
                </div>
                <div className={cx('input-group')}>
                    <label htmlFor="from_date">From Date</label>
                    <input type="date" ref="from_date" />
                </div>
                <div className={cx('input-group')}>
                    <label htmlFor="to_date">To Date</label>
                    <input type="date" ref="to_date" />
                </div>

                <button className={cx('action')} onClick={this.search}>Search</button>
            </div>
        );
        if (this.props.workorder.selectedWorkflow === -1) {
            workordersearch = null;
        }
        return (
            <div className={cx('container', 'big')}>
                <h1>Order Workflow</h1>
                <Link to="/landing" className={cx('back')} ><img src={back} /></Link>
                <div className={cx('form-container', 'big')}>
                    <Collapse
                        accordion={false}
                        onChange={this.toggleAccordion}
                        activeKey={this.props.workorder.accordionKey}
                    >
                        <Panel header="Search Orders">
                            <div className={cx('input-group')}>
                                <label htmlFor="workflowStage">Select Workflow</label>
                                <MultipleChoice
                                    isMultiple={false}
                                    options={_.map(this.props.workorder.workflows, 'code')} selected={['']} rel='workflow' save={this.selectWorkflow} />
                            </div>
                            {workordersearch}
                        </Panel>
                        <Panel header="Orders">
                            {this.renderWorkOrders()}
                        </Panel>
                    </Collapse>
                </div>
            </div>
        );
    }
}

WorkOrder.propTypes = {
    workorder: PropTypes.object,
    user: PropTypes.object,
    lists: PropTypes.object
};


function mapStateToProps({ workorder, user, customer, lists }) {
    return {
        workorder,
        user,
        customer,
        lists
    };
}

export default connect(mapStateToProps)(WorkOrder);