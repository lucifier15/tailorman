import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/workorder';
import _ from 'lodash';
import {
	getWorkOrderDetails,
	getEigthFraction
} from '../actions/workorder';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import back from 'images/back-arrow.png';
import moment from 'moment';
import { Link } from 'react-router';

import { dismissMessage } from '../actions/messages';

const cx = classNames.bind(styles);
const pad = (n, width, z) => {
	z = z || '0';
	n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
class WorkOrderPrint extends Component {
	constructor(props) {
		super(props);
		this.renderForm = this.renderForm.bind(this);
	}

	componentDidMount() {
		const profile_id = this.props.params.profile_id == -1 ? undefined : this.props.params.profile_id;
		this.props.dispatch(getWorkOrderDetails(this.props.params.order_id, this.props.params.order_item_id, profile_id, undefined, true));
		this.props.dispatch(dismissMessage());
	}

	getProfiles(default_profile, other_profiles) {
		let labels;
		if (default_profile) {
			labels = _.map(default_profile.measurements, 'type');
		}
		if (other_profiles) {
			other_profiles = _.orderBy(other_profiles, [(profile) => { return profile.type; }], ['desc']);

			other_profiles.forEach((profile) => {
				labels = labels.concat(_.map(profile.profile.measurements, 'type'));
			});
		}


		labels = _.uniq(labels);

		let values = [];
		if (default_profile) {
			values.push({
				heading: 'Bespoke Form',
				values: labels.map((label) => {
					if((label && label.toUpperCase()) === 'LEG ALIGNMENT') {
						const vals = _.find(default_profile.values, { measurement_type: label });
						return (!vals) ? '' : vals.descr;
					} else {
						const vals = _.find(default_profile.measurements, { type: label });
						return (!vals) ? '' : vals.value;
					}
				})
			});
		}

		if (other_profiles) {
			other_profiles.forEach((profile) => {
				values.push({
					heading: profile.type,
					values: labels.map((label) => {
						const vals = _.find(profile.profile.measurements, { type: label });
						if (!vals)
							return '';
						else if (profile.type == 'FGQC')
							return getEigthFraction(vals.value);
						else
							return vals.value;
					})
				});
			});
		}


		return {
			labels: labels,
			values: values
		}
	}

	renderForm(details) {
		const profiles = this.getProfiles(details.order_item.profile, details.order_item._profiles);
		let button_tonal_sku = false;
		let lining_tonal_sku = [];
		let size_32 = false;
		let size_24 = false;
		const fabric_info = [{
			key: 'product_sku',
			title: 'SKU'
		}, {
			key: 'supplier_product_code',
			title: 'Supplier Product Code'
		}, {
			key: 'material_composition',
			title: 'Material Composition'
		}, {
			key: 'care_info',
			title: 'Care Information'
		}, {
			key: 'marker_type',
			title: 'Marker Type'
		}, {
			key: 'marker_width',
			title: 'Marker Width'
		}, {
			key: 'fabric_design',
			title: 'Fabric Design'
		}, {
			key: 'fabric_width',
			title: 'Fabric Width'
		}, {
			key: 'pattern_repeat_size',
			title: 'Pattern Repeat Size'
		}, {
			key: 'nap_test',
			title: 'Nap Test'
		}, {
			key: 'pocket_color',
			title: 'Pocket Color'
		}, {
			key: 'tonal_button_sku',
			title: 'Buttons'
		}, {
			key: 'buttons_supplier_product_code',
			title: 'Buttons Supplier Product Code'
		}, {
			key: 'thread_shade',
			title: 'Thread'
		}, {
			key: 'seam_tape',
			title: 'Seam Tape'
		}, {
			key: 'tonal_lining_sku',
			title: 'Lining'
		}, {
			key: 'lining_supplier_product_code',
			title: 'Lining Supplier Product Code'
		}, {
			key: 'lining_width',
			title: 'Lining Width'
		}, {
			key: 'tonal_ucf',
			title: 'Tonal UCF'
		}, {
			key: 'canvas',
			title: 'Canvas'
		}, {
			key: 'fusing',
			title: 'Fusing'
		}, {
			key: 'size_32',
			title: 'Size 32L'
		}, {
			key: 'size_24',
			title: 'Size 24L'
		}];

		return (
			<div className={cx('table')}>
				<div className={cx('full')}>
					<span className={cx('col-11', 'nopad')}>
						<div className={cx('full')}>
							<span className={cx('col-4', 'label', 'no-color')}>Type of Sample</span>
							<span className={cx('col-6')}>{details.order_item.finish_type}</span>
							<span className={cx('col-6', 'label', 'noborder')}>Work Order No.</span>
						</div>
						<div className={cx('full')}>
							<span className={cx('col-4', 'label', 'no-color')}>Priority</span>
							<span className={cx('col-6')}>{details.order_item.priority}</span>
							<span className={cx('col-6', 'noborder')}>{details.order.order_id}</span>
						</div>
						<div className={cx('full')}>
							<span className={cx('col-4', 'label', 'no-color')}>IMP No.</span>
							<span className={cx('col-6')}>{[details.order_item.product_sku.slice(0, 2), details.order_item.order_item_id].join('_')}</span>
							<span className={cx('col-3', 'label')}>Order Date</span>
							<span className={cx('col-3', 'noborder')}>{moment(details.order.order_date).format('ll')}</span>
						</div>
						<div className={cx('full')}>
							<span className={cx('col-4', 'label', 'no-color')}>CAD File Name</span>
							<span className={cx('col-6')}>{(details.order_item.profile && details.order_item.profile.name && details.order_item.profile.profile_id > -1) ? ['T' + pad(details.customer.customer_id, 6), details.order_item.product_sku.slice(0, 2) + '1', details.order_item.profile.profile_name].join('_').slice(0, 20) : ''}</span>
							<span className={cx('col-3', 'label')}>Ex Factory Fiton</span>
							<span className={cx('col-3', 'noborder')}></span>
						</div>

					</span>
				</div>
				<div className={cx('full', 'sub-header')}>Customer</div>

				<div className={cx('full')}>
					<span className={cx('col-4', 'label')}>Name</span>
					<span className={cx('col-6')}>{details.customer.name}</span>
					<span className={cx('col-3', 'label')}>Order Source</span>
					<span className={cx('col-3', 'noborder')}>{details.order.store}</span>
				</div>
				<div className={cx('full')}>
					<span className={cx('col-4', 'label')}>Address</span>
					<span className={cx('col-6')}>{details.customer.address}</span>
					<span className={cx('col-3', 'label')}>Tailor Name</span>
					<span className={cx('col-3', 'noborder')}>{details.order.tailor_name}</span>
				</div>

				<div className={cx('full')}>
					<span className={cx('col-4', 'label')}>Gender</span>
					<span className={cx('col-6')}>{details.customer.gender}</span>
					<span className={cx('col-3', 'label')}>SalesMan Name</span>
					<span className={cx('col-3', 'noborder')}>{details.order.sales_man}</span>
				</div>

				<div className={cx('full')}>
					<span className={cx('col-4', 'label')}>Email</span>
					<span className={cx('col-6', 'notransform')}>{details.customer.email}</span>
					<span className={cx('col-3', 'label')}>Measurement Profile</span>
					<span className={cx('col-3', 'noborder')}>{(details.order_item.profile && details.order_item.profile.name ? details.order_item.profile.name : '')}</span>
				</div>

				<div className={cx('full')}>
					<span className={cx('col-4', 'label')}>Mobile</span>
					<span className={cx('col-6')}>{details.customer.mobile}</span>
					<span className={cx('col-3', 'label')}>Order Punched By</span>
					<span className={cx('col-3', 'noborder')}>{details.order.user_name}</span>
				</div>



				<div className={cx('full')}>
					<span className={cx('col-8', 'sub-header')}>{details.order_item.item_type + " Fit"}</span>
					<span className={cx('col-8', 'sub-header')}>{details.order_item.item_type + " Fabric & Design"}</span>
				</div>
				<div className={cx('full')}>
					<span className={cx('col-8', 'nopad', 'noborder')}>
						<div className={cx('full')}>
							<span className={cx('col-12')}>Selected Method</span>
							<span className={cx('col-4')}>{(details.order_item.profile && details.order_item.profile.measurement_source) ? details.order_item.profile.measurement_source : ''}</span>
						</div>


						<div className={cx('full')}>
							<span className={cx('col-12')}>Block</span>
							<span className={cx('col-4')}>{details.order_item.block || 'NA'}</span>
						</div>
						<div className={cx('full')}>
							<span className={cx('col-12')}>Size</span>
							<span className={cx('col-4')}>{(details.order_item.item_size || 'NA')}</span>
						</div>

						<div className={cx('full')}>
							<span className={cx('col-8', 'label')}>Measurements (inches)</span>
							{
								profiles.values.map((value, index) => {
									return <span className={cx('col-2', 'label')} key={index}>{value.heading} </span>
								})
							}
							<span className={cx('col-2', 'label', 'noborder')}>Actual FGQC</span>
						</div>

						{profiles.labels.map((label, index) => {
							let bool = ((label && label.toUpperCase()) === 'LEG ALIGNMENT');

							if (!(
								parseFloat(profiles.values[0].values[index]) != 0 && !_.isNaN(parseFloat(profiles.values[0].values[index])) ||
								parseFloat(profiles.values[1].values[index]) != 0 && !_.isNaN(parseFloat(profiles.values[1].values[index])) ||
								profiles.values[2].values[index].length > 0
							) && (!bool)) return null;
							else if(bool && !profiles.values[0].values[index]) {
								return null;
							}
							else {
								return (
									<div className={cx('full')} key={"profile_" + index}>
										<span className={cx('col-8', 'label', 'no-color')}>{label.toLowerCase()}</span>
										<span className={cx('col-2')}>{profiles.values[0].values[index]}</span>
										<span className={cx('col-2')}>{profiles.values[1].values[index]}</span>
										<span className={cx('col-2')}>{profiles.values[2].values[index]}</span>
										<span className={cx('col-2', 'noborder')}></span>
									</div>
								)
							}

						})}

						<div className={cx('full')}>
							<span className={cx('col-8', 'label', 'no-color')}>{details.order_item.item_type + " Fit Comments"}</span>
							<span className={cx('col-8')}>{(details.order_item._profiles.length > 0) ? details.order_item._profiles.reduce((prev, next) => { return (prev || '') + " " + (next.profile.comment || ''); }, details.order_item.profile.comment) : details.order_item.profile.comment}</span>
						</div>
					</span>
					<span className={cx('nopad')}></span>
					<span className={cx('col-8', 'nopad', 'noborder')}>
						<div className={cx('full')}>
							<span className={cx('col-8', 'label', 'no-color')}>Style</span>
							<span className={cx('col-8', 'noborder')}>{details.order_item.product_sku}</span>
						</div>

						<div className={cx('full')}>
							<span className={cx('col-8', 'label', 'no-color')}>Qty</span>
							<span className={cx('col-8', 'noborder')}>{details.order_item.qty}</span>
						</div>



						<div className={cx('full')}>
							<span className={cx('col-8', 'label', 'no-color')}>SKU</span>
							<span className={cx('col-8', 'noborder')}>{details.order_item.product_sku}</span>
						</div>



						{(
							details.order_item.design &&
							details.order_item.design.fabric_design &&
							details.order_item.design.fabric_design.length > 0) ?
							details.order_item.design.fabric_design.map((type) => {
								let is_button = false;
								let is_lining = 0;
								//toggle collapse
								if (type.collapse) {
									let disable = true;
									for (var i = 0; i < type.Designs.length; i++) {
										if (type.Designs[i].selected > -1)
											disable = false;
									}
									if (disable)
										return null;
								}
								return type.Designs.map((design, index) => {
									//For Tonal SKU
									if (design.is_button && design.selected) {
										if (design.values[design.selected].toLowerCase() != 'tonal' && design.selected > -1)
											is_button = true;
									}
									if (design.is_lining) {
										is_lining++;
										if (!design.selected || design.values[design.selected].toLowerCase() == 'Tonal' || design.selected == -1)
											lining_tonal_sku[is_lining] = details.order_item.tonal_lining_sku;
									}
									if (is_button && design.name.toLowerCase() == 'button sku') {
										button_tonal_sku = design.text;
									}
									if ((design.name.toLowerCase() == 'lining sku' || (design.name.toLowerCase().indexOf('lining') > -1 && design.name.toLowerCase().indexOf('sku') > -1))) {
										if (!lining_tonal_sku[is_lining] && design.text && design.text.length > 0)
											lining_tonal_sku[is_lining] = design.text;
									}

									//For Sizes
									if (design['Size 32L'] && design.selected > -1) {
										size_32 = design['Size 32L'][design.selected];
									}
									if (design['Size 24L'] && design.selected > -1) {
										size_24 = design['Size 24L'][design.selected];
									}

									if (!design['disabled']) {
										return (
											<div className={cx('full')} key={"fabric_design_" + design.name + "_" + index}>
												<span className={cx('col-8', 'label', 'no-color')}>{design.name.toLowerCase()}</span>
												<span className={cx('col-8', 'noborder')}>{(design.selected > -1) ? design.values[design.selected].toLowerCase() : design.text || ''}</span>
											</div>
										)
									}
								})
							}) : null}
						<div className={cx('full', 'border')}>
							<span className={cx('col-6', 'label', 'no-color', 'noborder')}>{details.order_item.item_type + " Comments"}</span>
							<span className={cx('col-10', 'noborder')}>{[details.order_item.design.comment, details.order_item.comment].join('  ')}</span>
						</div>
					</span>
				</div>

				<div className={cx('full', 'sub-header')}>{details.order_item.item_type + " Comments"}</div>
				{console.log(lining_tonal_sku)}
				{
					fabric_info.map((info, __index) => {
						if (button_tonal_sku)
							details.order_item.tonal_button_sku = button_tonal_sku;

						if (lining_tonal_sku.length > 0)
							details.order_item.tonal_lining_sku = _.compact(lining_tonal_sku).join(" ");
						if (!details.order_item[info.key] && typeof details.order_item[info.key] != 'string')
							return null;
						if (size_32)
							details.order_item.size_32 = size_32;
						if (size_24)
							details.order_item.size_24 = size_24;
						return (
							<div className={cx('full')} key={__index}>
								<span className={cx('col-4', 'label', 'no-color')}>{info.title}</span>
								<span className={cx('col-12', 'noborder')}>{details.order_item[info.key]}</span>
							</div>
						)
					})
				}


			</div>
		);
	}
	render() {
		let { workorder: { selected } } = this.props;

		if (selected && selected.order) {
			return (
				<div className={cx('container')}>
					<div className={cx('container', 'big', 'print', 'flexify')}>
						<h1 className={cx('no-bottom-margin')}>Workorder {this.props.workorder.selected.order_item.item_type}</h1>

						<Link to="/workorder" className={cx('back')} ><img src={back} /></Link>
						<a className={cx('print-button', 'review')} onClick={window.print}>Print</a>
						{this.renderForm(this.props.workorder.selected)}
					</div>
				</div>
			);
		} else {
			return null;
		}

	}
}

WorkOrderPrint.propTypes = {
	workorder: PropTypes.object,
	user: PropTypes.object
};


function mapStateToProps({ workorder, user, customer }) {
	return {
		workorder,
		user,
		customer
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(WorkOrderPrint);