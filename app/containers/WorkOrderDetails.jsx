import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/workorder';
import _ from 'lodash';
import {
    getWorkOrderDetails
} from '../actions/workorder';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import back from 'images/back-arrow.png';
import moment from 'moment';
import { Link } from 'react-router';


const cx = classNames.bind(styles);

class WorkOrderPrint extends Component {
    constructor(props) {
        super(props);
        this.renderForm = this.renderForm.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(getWorkOrderDetails(this.props.params.order_id, this.props.params.order_item_id));
    }

    renderForm(details) {
        return (
            <div></div>
        );
    }
    render() {
        if (this.props.workorder.selected && this.props.workorder.selected.order) {
            return (
                <div className={cx('container', 'big', 'print', 'flexify')}>
                    <Link to="/workorder" className={cx('back')} ><img src={back} /></Link>
                    {this.renderForm(this.props.workorder.selected)}
                </div>
            );
        } else {
            return null;
        }

    }
}

WorkOrderDetails.propTypes = {
    workorder: PropTypes.object,
    user: PropTypes.object
};


function mapStateToProps({workorder, user, customer}) {
    return {
        workorder,
        user,
        customer
    };
}

export default connect(mapStateToProps)(WorkOrderDetails);