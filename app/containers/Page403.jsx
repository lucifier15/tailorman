import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import styles from 'css/components/stores';
import _ from 'lodash';

const cx = classNames.bind(styles);

class Page403 extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={cx('container')}>
                <h1>Forbidden</h1>
                <p>
                    Sorry, looks like you donot have enough access credentials to view the page you requested.
                    Please contact the administrator to gain access.
                </p>
            </div>
        );
    }
}

Page403.propTypes = {
    user: PropTypes.object
};

// Function passed in to `connect` to subscribe to Redux store updates.
// Any time it updates, mapStateToProps is called.
function mapStateToProps({user}) {
    return {
        user
    };
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(Page403);