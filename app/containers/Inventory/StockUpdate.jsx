import React, { Component, PropTypes } from 'react';

import Popup from 'react-popup';

//Custom components
import FileCapture from 'components/FileCapture';

const ERROR_MESSAGE = 'PLEASE FILL THE MANDATORY FIELDS!';
const CSV_FORMAT_ERROR_MESSAGE = 'PLEASE PROVIDE THE CORRECT FORMAT!';
const STOCK_UPDATE_TEMPLATE_DOWNLOAD_FILE_NAME = 'stockupdate_template.csv';

//Hard coded values
const StockUpdateRecords = [{
	value: 1,
	name: 'In Stock'
}, {
	value: 0,
	name: 'Out Stock'
}];

const DuplicateTemplateData = [
	["supplier_product_code"], ["SPTTEST01"], ["SPTTEST02"], ["SPTTEST03"], ["SPTTEST04"], ["SPTTEST05"]
];

//@class
class StockUpdate extends Component {
	constructor(props) {
		super(props);

		this.state = {
			fileUploaded: null,
			selectedValue: null,
			isWebApplicablility: true
		};

		this.fileCapture = this.fileCapture.bind(this);
		this.confirmPopup = this.confirmPopup.bind(this);
		this.selectStockUpdate = this.selectStockUpdate.bind(this);
		this.onStockUpdateSubmit = this.onStockUpdateSubmit.bind(this);
	}

	confirmPopup(obj) {
		const self = this;
		const { selectedValue, isWebApplicablility } = this.state;

		let selectedElement = '';

		if(selectedValue === 1) {
			selectedElement = <span><b>In Stock</b> and <b>IsAvailableForWeb as {isWebApplicablility.toString().toUpperCase()}</b></span>;
		} else {
			selectedElement = <b>Out Stock</b>;
		}

		const contentElement = <div>You have selected {selectedElement}. Do you want to Procced?</div>;

		Popup.create({
		    title: 'Confirmation',
		    content: contentElement,
		    buttons: {
		        left: [{
		            text: 'CANCEL',
		            action: function () {
		                Popup.close();
		            }
		        }],
		        right: [{
		            text: 'OK',
		            action: function () {
		                self.props.updateStockAction(obj);
		                Popup.close();
		            }
		        }]
		    }
		});
	}

	onStockUpdateSubmit() {
		let { errorNotification } = this.props;
		let { selectedValue, fileUploaded, isWebApplicablility } = this.state;

		if(selectedValue != null && fileUploaded) {
			let obj = fileUploaded;

			obj.value = selectedValue;
			obj.isWebApplicablility = (selectedValue === 1) ? isWebApplicablility : null;

			this.confirmPopup(obj);
		} else {
			this.props.errorNotification(ERROR_MESSAGE, 'error');
		}
	}

	selectStockUpdate(selected) {
		const { selectedValue } = this.state;

		let obj = {
			selectedValue: selected,
			isWebApplicablility: true
		};

		this.setState(obj);

		if(selectedValue === null) {
			//To dismiss the message popup at top
			this.props.errorNotification();
		}
	}

	fileCapture(file, data) {
		
		if(file && data) {
			let obj = {};

			let headers = [];
			let records = [];

			const splitData = data.split("\n");

			const { updateStockAction } = this.props;
			const { selectedValue, isWebApplicablility } = this.state;

			//To remove the double quote
			const doubleQuoteRemoval = (extra, value) => {
				if(extra && value[3] === '"') {
					value = value.substr(4);
				} else if(value[0] === '"') {
					value = value.substr(1);
				} else {
					//do nothing
				}

				if(value[value.length-1] === '"') {
					value = value.substr(0, value.length-1);
				}

				return value;
			}

			splitData.map((item, index) => {
				let values = item.split(",");

				values.map((value, type) => {
					if(value) {
						let bool = (index === 0 && type === 0) ? true : false;

						value = doubleQuoteRemoval(bool, value);

						if(index === 0) {
							obj[value] = [];
							headers.push(value);
						} else {
							obj[headers[type]] && obj[headers[type]].push(value);
						}
					}
				});
			});

			if(obj['supplier_product_code']) {
				this.setState({
					fileUploaded: obj
				});
			} else {
				this.setState({
					fileUploaded: null
				});
			}
		} else {
			this.props.errorNotification(CSV_FORMAT_ERROR_MESSAGE, 'error');
		}
	}

	render() {
		let missingProductsElement = '';
		let csvDataHref = 'data:text/csv;charset=utf-8,';

		const { notExistProducts, errorNotification } = this.props;
		const { selectedValue, fileUploaded, isWebApplicablility } = this.state;

		const errMessage = (selectedValue != null) ? null : ERROR_MESSAGE;

		const multipleList = StockUpdateRecords.map((item, index) => {
			const className = (selectedValue === item.value) ? 'selected' : '';

			return <li className={className} onClick={() => this.selectStockUpdate(item.value)} key={index}>
					{item.name}
				</li>;
		});

		if(DuplicateTemplateData && DuplicateTemplateData.length > 0) {
			DuplicateTemplateData.map((item, index) => {
				item = item.map((value) => {
					return `%22${value}%22`;
				});

				csvDataHref += `${item}%0A`;
			});
		}

		if(notExistProducts && notExistProducts.length > 0) {
			missingProductsElement = <div className={'input-group'}>
										<label htmlFor="missingProducts">Below products do not exist,</label>
										<ul>
										{ notExistProducts.map((item, index) => {
											return <li key={index}>{item}</li>
										}) }
										</ul>
									</div>;
		}

		return (
			<span>
            	<div className={'download-wrapper'}>
            		<a className={'download-anchor'} download={STOCK_UPDATE_TEMPLATE_DOWNLOAD_FILE_NAME} target="_blank" href={csvDataHref}>DOWNLOAD SAMPLE TEMPLATE</a>
            	</div>

                <div className={'input-group'}>
                    <label htmlFor="stockUpdate">
                    	Stock type
                    	<em className={'mandatory'}>*</em>
                    </label>
                    <ul className={'optionsContainer'}>{multipleList}</ul>
                </div>

                {(selectedValue === 1) ? <label>
                	<input type="checkbox" checked={isWebApplicablility} onChange={() => this.setState({isWebApplicablility: !isWebApplicablility})} />
        			Also make available for web
       			</label> : ''}

                <div className={'input-group'}>
                    <label htmlFor="stockUpdate">
                    	Upload CSV file
                    	<em className={'mandatory'}>*</em>
                    </label>
                    <FileCapture 
                    	fileType={'csv'} 
                    	accept={'.csv'} 
                    	uploadFileDetails={this.fileCapture} 
                    	value={fileUploaded}
                    	emptyFileUploaded={() => this.setState({ fileUploaded: null })}
                    />
                </div>

                <input className={'submit-button'} type="button" value="SUBMIT" onClick={this.onStockUpdateSubmit} />

                {missingProductsElement} 
            </span>
		);
	}
}

StockUpdate.propTypes = {
	errorNotification: PropTypes.func,
	updateStockAction: PropTypes.func
};

export default StockUpdate;