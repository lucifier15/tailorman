import React, { Component } from 'react';

import { Link } from 'react-router';
import { connect } from 'react-redux';

import Collapse, { Panel } from 'rc-collapse';

//images
import back from 'images/back-arrow.png';

//Custom styles
import styles from 'css/components/inventory';

//@actions
import { updateMessage } from 'actions/order';
import { dismissMessage } from 'actions/messages';
import { updateStockUpdate } from 'actions/inventory';

//Custom components
import StockUpdate from './StockUpdate';

//constants
import { ERROR_CONDITION, SUCCESS_CONDITION } from 'types';
import RTWStockTransfer from './RTWStockTransfer';

//@class
class Inventory extends Component {
	constructor(props) {
		super(props);

		this.state = {
			notExistProducts: null
		};

		this.errorNotification = this.errorNotification.bind(this);
		this.updateStockAction = this.updateStockAction.bind(this);
	}

	componentWillMount() {
		this.errorNotification();
	}

	errorNotification(message, isError) {
		let { dispatch } = this.props;

		if (isError) {
			let condition = (isError === 'error') ? ERROR_CONDITION : SUCCESS_CONDITION;

			//To display the error message popup at top
			dispatch(updateMessage(condition, message));
		} else {
			//To dismiss the message popup at top
			dispatch(dismissMessage());
		}
	}

	updateStockAction(obj) {
		const { dispatch } = this.props;

		dispatch(updateStockUpdate(obj))
			.then((type) => {
				let notExist = type && type.data && type.data.not_exist;

				if (!notExist || notExist.length === 0) {
					notExist = null;
				}

				this.setState({
					notExistProducts: notExist
				});

				this.errorNotification('Updated the stock successfully!', 'success');
			})
			.catch((err) => {
				this.setState({
					notExistProducts: null
				});

				this.errorNotification('Something went wrong!', 'error');
			});
	}

	render() {
		const { notExistProducts } = this.state;

		return (
			<div className={'container big'}>
				<h1>Inventory</h1>
				<Link to="/landing" className={'back'} ><img src={back} /></Link>

				<div className={'form-container big'}>
					<Collapse
						defaultActiveKey={'0'}
					>
						{/* <Panel header='Stock Update'>
							<StockUpdate
								notExistProducts={notExistProducts}
								updateStockAction={this.updateStockAction}
								errorNotification={this.errorNotification}
							/>
						</Panel> */}
						<Panel header='RTW Stock Transfer'>
							<RTWStockTransfer />
						</Panel>
					</Collapse>
				</div>
			</div>
		);
	}
}

Inventory.propTypes = {

};

function mapStateToProps({ }) {
	return {

	};
}

export default connect(mapStateToProps)(Inventory);