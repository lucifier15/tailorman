import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

import classNames from 'classnames/bind';
import styles from 'css/components/order/details';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { updatePriorityType, updatePaymentType, updateTailor, updateOrderDetails, updateBenificiaryPhone, updateOccasion, updateSalesman, updateMessage } from '../../actions/order';
import { fetchList,searchRTW,fetchSizeList,fetchFitList,transferInventory } from '../../actions/list';
import { fetchStoresList } from '../../actions/users';
import NumberForm from '../../components/NumberForm';
import SelectForm from '../../components/SelectForm';
import back from 'images/back-arrow.png';
import moment from 'moment';
import SearchList from 'components/SearchList';

const cx = classNames.bind(styles);

class RTWStockTransfer extends Component {
	constructor(props) {
        super(props);
        this.searchRTW = this.searchRTW.bind(this);
        this.renderStyleList = this.renderStyleList.bind(this);
        this.onFromStoreSelect=this.onFromStoreSelect.bind(this);
        this.onToStoreSelect=this.onToStoreSelect.bind(this);
        this.onSelectSize=this.onSelectSize.bind(this);
        this.onSelectFit=this.onSelectFit.bind(this);
        this.onInventoryTransfer=this.onInventoryTransfer.bind(this);
        this.selectStyle=this.selectStyle.bind(this);
        this.state={
            fromStoreValue:null,
            toStoreValue:null,
            sizeValue:null,
            fitValue:null,
            selectedFabricId:null,
            fabricInventoryCount:null,
            productSelected:false,
            product_sku_code:null,
            item_type_id:null,
            clearList:false
        }
    }
    
    componentDidMount() {
        this.props.dispatch(fetchStoresList(this.props.user.user.stores));
        this.props.dispatch(fetchSizeList());
        this.props.dispatch(fetchFitList());
    }

    searchRTW(keyword){
        
        const {fromStoreValue,toStoreValue,sizeValue,fitValue,productSelected} = this.state;
        //if(!productSelected){
        if(!_.isNull(fromStoreValue) && keyword !="" &&  !_.isNull(sizeValue) &&  !_.isNull(fitValue)){
        this.props.dispatch(searchRTW(fromStoreValue,keyword,sizeValue.name,fitValue.name));
        this.setState({
            clearList:false
        });
        }else{
            this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', 'Please select From Store,Size and Fit values and enter keyword'));
            window.scrollTo(0, 0);
            this.setState({
                clearList:true
            });
        }
   // }
    }

    onInventoryTransfer(){
        const _that = this;
        const formValues = {
			//From fields in this component
			from_store_id: _that.state.fromStoreValue,
			to_store_id: _that.state.toStoreValue,
			size: !_.isNull(_that.state.sizeValue)?_that.state.sizeValue.name:null,
			fit: !_.isNull(_that.state.fitValue)?_that.state.fitValue.name:null,
			qty: ReactDOM.findDOMNode(_that.refs.quantity).value,
            description: ReactDOM.findDOMNode(_that.refs.description).value,
            created_by: _that.props.user.user.user_id,
            fabric_id:_that.state.selectedFabricId,
            product_sku_code:_that.state.product_sku_code,
            item_type_id:_that.state.item_type_id
        }
        
        const selectedValues = _.findKey(formValues, (value) => { return !value;});
        
        if(selectedValues == undefined){
            if(Number(formValues.qty) > Number(_that.state.fabricInventoryCount)){
            this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', 'Entered quantity should not be greater than Product inventory count'));
			window.scrollTo(0, 0)
        }else{
            this.props.dispatch(transferInventory(formValues));
            this.setState({
                fromStoreValue:null,
                toStoreValue:null,
                sizeValue:null,
                fitValue:null,
                clearList:true
            });
    
            _that.refs.quantity.value='';
            _that.refs.description.value='';
            _that.refs.searchList.clear();
            this.props.dispatch(updateMessage('ORDER_DETAILS_SAVE_SUCCESS', 'Successfully Transferred RTW Inventory Details'));
            window.scrollTo(0, 0);
        }
            
        }else if(_.isNull(_that.state.fabricInventoryCount)){
            this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', 'Please select any product'));
			window.scrollTo(0, 0)
        }
        else if(formValues.qty > _that.state.fabricInventoryCount){
            this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', 'Entered quantity should not be greater than Product inventory count'));
			window.scrollTo(0, 0)
        }
    else {
			this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', 'All fields are mandatory'));
			window.scrollTo(0, 0)
        }
        
        
 
    

    }

    selectStyle(style){
        const selectedFabricId = _.find(this.props.lists.rtwsku, { product_sku_code: style});
        const product_selected = true;
        this.setState({
            selectedFabricId : selectedFabricId.fabric_id,
            fabricInventoryCount:selectedFabricId.inventory_count,
            product_sku_code:selectedFabricId.product_sku_code,
            productSelected:true,
            item_type_id:selectedFabricId.item_type_id
        });
        
    }
    renderStyleList(style) {
        return (
            <div>
                <span className={cx('select-main-label')}>{style.name}</span>
                <span className={cx('select-email')}>{style.mrp + " INR"}</span>
                <span className={cx('select-phone')}>{style.product_sku_code}</span>
                <span>{style.inventory_count}</span>
            </div>
        )
    }
    
    getFromStore(){
		if (this.props.stores.list.length > 0) {
			return this.props.stores.list.map((listItem) => {
				return (
					listItem.address
                );
			});
		}
		else {
			return -1;
		}
	
    }

    onFromStoreSelect(fromStoreValue){
        const toStoreValue = this.state.toStoreValue;
        if(!_.isNull(toStoreValue)){
            if(fromStoreValue.value == toStoreValue){
                this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', 'From Store and To Store should not be the samesss'));
            window.scrollTo(0, 0);
            this.setState({
                fromStoreValue:null
            })
            }else{
                this.setState({
        fromStoreValue : fromStoreValue.value
       })
            }
        }else{
       this.setState({
        fromStoreValue : fromStoreValue.value
       })
    }
    }

    onToStoreSelect(toStoreValue){
        const fromStoreValue = this.state.fromStoreValue;
        if(!_.isNull(fromStoreValue)){
            if(toStoreValue.value == fromStoreValue){
                this.props.dispatch(updateMessage('MEARSUREMENT_FORM_VALIDATION', 'From Store and To Store should not be the same'));
            window.scrollTo(0, 0);
            this.setState({
                toStoreValue:null
            })
            }else{
                this.setState({
        toStoreValue : toStoreValue.value
       })
            }
        }else{
       this.setState({
        toStoreValue : toStoreValue.value
       })
    }
    }

    onSelectSize(sizeValue){
        this.setState({
            sizeValue
        })
    }
    onSelectFit(fitValue){
        this.setState({
            fitValue
        })
    }

	render() {
        let fromStoreArray = [];
        let toStoreArray = [];

        this.props.stores.list.map((listItem)=>{
            fromStoreArray.push({
                id:listItem.store_id,
                name:listItem.address
            });
            toStoreArray.push({
                id:listItem.store_id,
                name:listItem.address
            })
        });
        let clearList = this.state.clearList;
		return (
            
			<div className={cx('container')}>
				
					<div className={cx('input-group')}>
						<label htmlFor="fromstore" className="required">From Store</label>
						<SelectForm type="fromstore" rel="fromstore" options={fromStoreArray} value={this.state.fromStoreValue} save={this.onFromStoreSelect}/>
					</div>
                    <div className={cx('input-group')}>
						<label htmlFor="tostore" className="required">To Store</label>
						<SelectForm type="tostore" rel="tostore" options={toStoreArray} value={this.state.toStoreValue} save={this.onToStoreSelect}/>
					</div>
                    <div className={cx('input-group')}>
						<label htmlFor="size" className="required">Size</label>
						<SelectForm type="size" rel="size" options={this.props.lists.rtwsku_size} value={this.state.sizeValue} save={this.onSelectSize}/>
					</div>
                    <div className={cx('input-group')}>
						<label htmlFor="fit" className="required">Fit</label>
						<SelectForm type="fit" rel="fit" options={this.props.lists.rtwsku_fit} value={this.state.fitValue} save={this.onSelectFit}/>
					</div>
                    <div className={cx('input-group')}>
                        <label htmlFor="select_customer" className="required">Search & Select</label>
                        <SearchList 
                        ref="searchList"
            clearList={clearList}
            search={this.searchRTW}
            results={this.props.lists.rtwsku}
            renderItem={this.renderStyleList}
            select={this.selectStyle}
            placeholder="Search for SKU" />
                    </div>
                    <div className={cx('input-group')}>
                    <label htmlFor="quantity" className="required">Quantity</label>
                    <input type="number" ref="quantity" />
                </div>
                <div className={cx('input-group')}>
						<label htmlFor="description" className="required">Description</label>
						<textarea id="description" ref="description" ></textarea>
					</div>
                <button  className={cx('action', 'primary')} onClick={this.onInventoryTransfer}>TRANSFER</button>
					
			</div>
		);
	}
}


RTWStockTransfer.propTypes = {
	user: PropTypes.object,
	stores: PropTypes.object
};


function mapStateToProps({ user, stores,lists }) {
	return {
		stores,
        user,
        lists
	};
}

export default connect(mapStateToProps)(RTWStockTransfer);