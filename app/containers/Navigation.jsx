import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import tailormanLogo from 'images/tailorman-logo.png';
import classNames from 'classnames/bind';
import styles from 'css/components/navigation';
import { toggleMenu } from 'actions/menu';
import HamburgerMenu from 'components/HamburgerMenu';

const cx = classNames.bind(styles);


class Navigation extends Component {
	constructor(props) {
		super(props);
		this.toggle = this.toggle.bind(this);
	}
	toggle(status){
		this.props.toggleMenu(status);
	}
	render() {
		let header = (
			<nav className={cx('navigation')} role="navigation">
				<HamburgerMenu isOpen={this.props.menu.isOpen} data={{ user: this.props.user.user.email }} toggle={this.toggle} />
				<div>
					<Link to="/" className={cx('logo')} activeClassName={cx('active')}>
						<img src={tailormanLogo} />
					</Link>
				</div>
			</nav>
			
		);
		if (this.props.pathname === '/login' || this.props.pathname.indexOf('/invoice/customer') > -1) {
			header = null;
		}
		return header;
	}
}


Navigation.propTypes = {
	user: PropTypes.object,
	pathname: PropTypes.string,
	menu: PropTypes.object,
	toggleMenu: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		user: state.user,
		pathname: (state.routing.locationBeforeTransitions !== null) ? state.routing.locationBeforeTransitions.pathname : '',
		menu: state.menu
	};
}

export default connect(mapStateToProps, {toggleMenu})(Navigation);
