import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from 'css/components/workorder';
import {
	push
} from 'react-router-redux';
import { fetchList, clearList } from '../actions/list';
import _ from 'lodash';
import StylesForm from 'components/StylesForm';
import {
	getWorkOrderDetails,
	saveFabricDesign,
	getWorkOrderStyles
} from 'actions/workorder';
import {
	updateMessage
} from 'actions/order';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import back from 'images/back-arrow.png';
import { setActiveKey } from '../actions/history';

const cx = classNames.bind(styles);

class WorkOrderStyles extends Component {
	constructor(props) {
		super(props);
		this.closeForm = this.closeForm.bind(this);
		this.loadFields = this.loadFields.bind(this);
		this.saveFabricDesign = this.saveFabricDesign.bind(this);
		this.toggleAccordion = this.toggleAccordion.bind(this);
	}
	toggleAccordion(activeKey) {
		this.props.dispatch(setActiveKey(activeKey));
	}

	componentDidMount() {
		const workorder = _.find(this.props.workorder.workorders, { order_item_id: parseInt(this.props.params.order_item_id) });
		this.props.dispatch(getWorkOrderStyles(workorder.order_item_id,workorder.item_type_id));
	}

	saveFabricDesign(fabric_design, comment, upCharges, isFinal) {
		let { workorder: { selected: { order_item: { order_item_id } } }, dispatch } = this.props;

		dispatch(saveFabricDesign(order_item_id, fabric_design, comment, isFinal));
		dispatch(updateMessage('UPDATE_GENERAL_MESSAGE', 'Fabric Style Updated'));
	}

	loadFields() {
		this.props.dispatch(fetchList('fabric_design_field', { item_type_id: this.props.workorder.selected.order_item.item_type_id }));
	}

	closeForm() {
		this.props.dispatch(push('/workorder'));
	}

	render() {
		if (this.props.workorder.selected.order_item) {
			return (
				<div className={cx('container', 'big')}>
					<h1>Update Styles </h1>
					<Link to="/workorder" className={cx('back')} ><img src={back} /></Link>
					<StylesForm
						lists={this.props.lists}
						save={this.saveFabricDesign}
						close={this.closeForm}
						loadFields={this.loadFields}
						fabricDesign={this.props.workorder.selected.order_item.design.fabric_design}
						comment={this.props.workorder.selected.order_item.design.comment}
						toggleAccordion={this.toggleAccordion}
						activeKey={this.props.history.accordionKey} />
				</div>
			);
		} else {
			return null;
		}
	}
}

WorkOrderStyles.propTypes = {
	workorder: PropTypes.object,
	user: PropTypes.object,
	lists: PropTypes.object,
	history: PropTypes.object
};


function mapStateToProps({ workorder, user, customer, lists, history }) {
	return {
		workorder,
		user,
		customer,
		lists,
		history
	};
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps)(WorkOrderStyles);