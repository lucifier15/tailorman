-- Added fabric name and description

DROP FUNCTION IF EXISTS "GetOrderItemListV2"(in_order_id integer);

CREATE OR REPLACE FUNCTION "GetOrderItemListV2"(in_order_id integer) RETURNS TABLE(order_item_id integer, order_id integer, workflow_id integer, sku_id integer, item_type_id integer, mrp double precision, qty integer, finish_type_id integer, fit_on_date date, delivery_date date, comment character varying, profile_id integer, priority_id integer, display_name character varying, product_sku character varying, sku character varying, fabric_description character varying, item_type character varying, finish_type character varying, priority character varying, block character varying, item_size character varying, upcharge json, taxes json, discount_value double precision, discount_type integer, order_flag integer, bill_amount double precision, pickup_location character varying, discount_comment character varying, hsn_code integer)
    LANGUAGE plpgsql
    AS $$ 
declare
var_block character varying ;
var_item_size character varying ;

BEGIN 
return query 
SELECT i.order_item_id, i.order_id, i.workflow_id, i.sku_id, i.item_type_id, 
i.mrp, i.qty, i.finish_type,i.fit_on_date, i.delivery_date, 
i.comment, (select s.profile_id from b_item_wf_stage s ,b_workflow_stage ws where s.workflow_stage_id=ws.stage_id and s.order_item_id=i.order_item_id and s.current_stage_flag='Y' limit 1), i.priority_id,i.display_name , f.supplier_product_code , f.sku_code, f.name, it.descr ,ft.code ,p.code ,var_block, var_item_size,
i.upcharge , i.taxes ,i.discount,i.discount_type ,i.order_flag , i.bill_amount,(CASE WHEN i.pickup_id IS NOT NULL THEN (select sl.address from m_store sl where sl.store_id = i.pickup_id) ELSE (CASE WHEN i.delivery_id IS NOT NULL THEN (select address from m_customer_addresses where m_customer_addresses_id =i.delivery_id) ELSE '' END) END) as pickup_location,
i.discount_comment, f.hsn_code
FROM public.b_order_item i 
inner join m_fabric f on i.sku_id=f.fabric_id 
inner join m_item_type it on i.item_type_id = it.item_type_id
left join m_finish_type ft on i.finish_type = ft.finish_type_id
left join m_priority_type p on i.priority_id = p.priority_type_id
where i.order_Id=in_order_id 
;

END;
$$;

ALTER FUNCTION public."GetOrderItemListV2"(in_order_id integer) OWNER TO tailorman_db;
------------------------------------------------------------------------------------------------------------------------------------------------