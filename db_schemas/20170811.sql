-- Adding a description column in b_profile_measurement table

ALTER TABLE b_profile_measurement ADD COLUMN descr character varying;
-------------------------------------------------------------------------------------------------------------------
-- To get the description value

-- FUNCTION: public."GetMeasurementProfileValuesV2"(integer)

-- DROP FUNCTION public."GetMeasurementProfileValuesV2"(integer);

CREATE OR REPLACE FUNCTION public."GetMeasurementProfileValuesV2"(
  in_profile_id integer)
RETURNS TABLE(measurement_type_id integer, value double precision, descr character varying, measurement_type character varying)
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE 
    ROWS 1000.0
AS $function$
 BEGIN 
  return query SELECT m.measurement_type_id, m.value , m.descr, mt.code 
  FROM b_profile_measurement m  , m_measurement_type mt

  where  m.profile_id=in_profile_id 
and m.measurement_type_id = mt.measurement_type_id
order by mt.show_order_id
  ;

END;

$function$;

ALTER FUNCTION public."GetMeasurementProfileValuesV2"(integer)
    OWNER TO tailorman_db;
---------------------------------------------------------------------------------------------------------------------
-- To save or update the description value

CREATE OR REPLACE FUNCTION public."SaveMeasurementProfileValues"
(in_profile_id integer,in_measurement_type_id integer,in_value double precision, in_descr character varying)
    RETURNS boolean
    LANGUAGE 'plpgsql'
    VOLATILE
    COST 100.0
AS $function$ 
DECLARE
 
BEGIN 

UPDATE b_profile_measurement
   SET  
       value=in_value,
       descr=in_descr
 WHERE profile_id=in_profile_id  and measurement_type_id=in_measurement_type_id;

 if not found then
    INSERT INTO b_profile_measurement(
            profile_id, measurement_type_id, value, descr)
    VALUES (in_profile_id, in_measurement_type_id, in_value, in_descr);

   end if;
return true; 
END;
$function$;
---------------------------------------------------------------------------------------------------------------------
-- To get the description value

-- FUNCTION: public."GetMeasurementProfileValuesV2"(integer, integer)

-- DROP FUNCTION public."GetMeasurementProfileValuesV2"(integer, integer);

CREATE OR REPLACE FUNCTION public."GetMeasurementProfileValuesV2"(
  in_profile_id integer,
  in_wo_order integer)
RETURNS TABLE(measurement_type_id integer, value double precision, descr character varying, measurement_type character varying)
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE 
    ROWS 1000.0
AS $function$
 BEGIN 

if in_wo_order= 1 then 
  return query SELECT m.measurement_type_id, m.value, m.descr, mt.code 
  FROM b_profile_measurement m  , m_measurement_type mt

  where  m.profile_id=in_profile_id 
and m.measurement_type_id = mt.measurement_type_id and mt.wo_show_order is not null
order by mt.wo_show_order
  ;
else

 return query SELECT m.measurement_type_id, m.value , m.descr, mt.code 
  FROM b_profile_measurement m  , m_measurement_type mt

  where  m.profile_id=in_profile_id 
and m.measurement_type_id = mt.measurement_type_id
order by mt.show_order_id ;

end if;

END;

$function$;

ALTER FUNCTION public."GetMeasurementProfileValuesV2"(integer, integer)
    OWNER TO tailorman_db;
-----------------------------------------------------------------------------------------------------------------