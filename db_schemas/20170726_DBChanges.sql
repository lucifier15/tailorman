-- TAXES PROC CHANGES
CREATE OR REPLACE FUNCTION public."GetTaxes"(
	local boolean
)
RETURNS SETOF json 
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE 
    ROWS 1000.0
AS $function$

BEGIN

if local then
RETURN QUERY SELECT cast('[{ "name": "CGST", "percent" : 6,  "order" : 1 }, {"name" : "SGST" , "percent" : 6, "order" : 2}]' as json);

else
RETURN QUERY SELECT cast('[{ "name": "IGST", "percent" : 12,  "order" : 1 }]' as json);

end if;
  
END;

$function$;

ALTER FUNCTION public."GetTaxes"(boolean)
    OWNER TO tailorman_db;
-------------------------------------------------------------------