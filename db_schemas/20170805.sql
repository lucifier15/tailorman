ALTER TABLE m_fabric ADD hsn_code integer;

--  DROP FUNCTION IF EXISTS "GetOrderItemListV2"(in_order_id integer);

-- CREATE OR REPLACE FUNCTION "GetOrderItemListV2"(in_order_id integer) RETURNS TABLE(order_item_id integer, order_id integer, workflow_id integer, sku_id integer, item_type_id integer, mrp double precision, qty integer, finish_type_id integer, fit_on_date date, delivery_date date, comment character varying, profile_id integer, priority_id integer, display_name character varying, product_sku character varying, item_type character varying, finish_type character varying, priority character varying, block character varying, item_size character varying, upcharge json, taxes json, discount_value double precision, discount_type integer, order_flag integer, bill_amount double precision, pickup_location character varying, discount_comment character varying, hsn_code integer)
--     LANGUAGE plpgsql
--     AS $$ 
-- declare
-- var_block character varying ;
-- var_item_size character varying ;

-- BEGIN 
-- return query 
-- SELECT i.order_item_id, i.order_id, i.workflow_id, i.sku_id, i.item_type_id, 
-- i.mrp, i.qty, i.finish_type,i.fit_on_date, i.delivery_date, 
-- i.comment, (select s.profile_id from b_item_wf_stage s ,b_workflow_stage ws where s.workflow_stage_id=ws.stage_id and s.order_item_id=i.order_item_id and s.current_stage_flag='Y' limit 1), i.priority_id,i.display_name , f.supplier_product_code , it.descr ,ft.code ,p.code ,var_block, var_item_size,
-- i.upcharge , i.taxes ,i.discount,i.discount_type ,i.order_flag , i.bill_amount,(CASE WHEN i.pickup_id IS NOT NULL THEN (select sl.address from m_store sl where sl.store_id = i.pickup_id) ELSE (CASE WHEN i.delivery_id IS NOT NULL THEN (select address from m_customer_addresses where m_customer_addresses_id =i.delivery_id) ELSE '' END) END) as pickup_location,
-- i.discount_comment, f.hsn_code
-- FROM public.b_order_item i 
-- inner join m_fabric f on i.sku_id=f.fabric_id 
-- inner join m_item_type it on i.item_type_id = it.item_type_id
-- left join m_finish_type ft on i.finish_type = ft.finish_type_id
-- left join m_priority_type p on i.priority_id = p.priority_type_id
-- where i.order_Id=in_order_id 
-- ;

-- END;
-- $$;

-- ALTER FUNCTION public."GetOrderItemListV2"(in_order_id integer) OWNER TO tailorman_db;



-------


ALTER TABLE public.b_order
    ADD COLUMN pin_date date;

-----------------
-- Updated in 20170821_58888.sql

-- CREATE OR REPLACE FUNCTION public."UpdateOrderV2"(
-- 	in_order_id integer,
-- 	in_tailor_id integer,
-- 	in_customer_id integer,
-- 	in_order_type_id integer,
-- 	in_store_id integer,
-- 	in_user_id integer,
-- 	in_order_date date,
-- 	in_occasion character varying,
-- 	in_occation_date date,
-- 	in_benficiary_name character varying,
-- 	in_benficiary_mobile character varying,
-- 	in_benificiary_email character varying,
-- 	in_priority_id integer,
-- 	in_payment_type_id integer,
-- 	in_payment_details character varying,
-- 	in_total_amount double precision,
-- 	in_comment character varying,
-- 	in_display_name character varying,
-- 	in_full_payment_flag character,
-- 	in_billing_address character varying,
-- 	in_delivery_address character varying,
-- 	in_pin_number character varying,
-- 	in_sales_man_id integer,
--     in_pin_date date)
--     RETURNS boolean
--     LANGUAGE 'plpgsql'
--     COST 100.0
--     VOLATILE 
-- AS $function$

 
-- DECLARE

-- BEGIN 

-- update b_order set (
--              tailor_id, customer_id, order_type_id, store_id, user_id, 
--             order_date, occasion, occation_date, benficiary_name, benficiary_mobile, 
--             benificiary_email, created_by, created_time, modified_by, modified_time, 
--             priority_id, payment_type_id, payment_details,total_amount,comment,display_name,full_payment_flag,billing_address,delivery_address, pin_number,sales_man_id,pin_date)
--     = ( in_tailor_id, in_customer_id, in_order_type_id, in_store_id, in_user_id, 
--             in_order_date, in_occasion, in_occation_date,in_benficiary_name, in_benficiary_mobile, 
--             in_benificiary_email, in_user_id, now(), in_user_id, now(), 
--             in_priority_id, in_payment_type_id, in_payment_details,in_total_amount,in_comment,in_display_name,in_full_payment_flag, in_billing_address,in_delivery_address, in_pin_number,in_sales_man_id,in_pin_date) where order_id=in_order_id ;
-- return true ;
-- END;

-- $function$;

-- ALTER FUNCTION public."UpdateOrderV2"(integer, integer, integer, integer, integer, integer, date, character varying, date, character varying, character varying, character varying, integer, integer, character varying, double precision, character varying, character varying, character, character varying, character varying, character varying, integer,date)
--     OWNER TO tailorman_db;

------

-- Updated in 20170821_58888.sql

-- DROP FUNCTION public."GetOrderDetails"(integer);

-- CREATE OR REPLACE FUNCTION public."GetOrderDetails"(
-- 	in_order_id integer)
--     RETURNS SETOF "TABLE(order_id integer, tailor_id integer, customer_id integer, order_type_id integer, store_id integer, user_id integer, order_date date, occasion character varying, occation_date date, benficiary_name character varying, benficiary_mobile character varying, benificiary_email character varying, created_by integer, created_time timestamp with time zone, modified_by integer, modified_time timestamp with time zone, payment_type_id integer, payment_details character varying, total_amount double precision, comment character varying, display_name character varying, full_payment_flag character, billing_address character varying, delivery_address character varying, pin_number character varying, sales_man_id integer, tailor_name character varying, order_type text, store character varying, user_name character varying, payment_type character varying, sales_man character varying, shipping_charges integer,pin_date date)"
--     LANGUAGE 'plpgsql'
--     COST 100.0
--     VOLATILE 
--     ROWS 1000.0
-- AS $function$

--  BEGIN 
--   return query SELECT o.order_id, o.tailor_id, o.customer_id, o.order_type_id, o.store_id, o.user_id, 
--        o.order_date, o.occasion, o.occation_date, o.benficiary_name, o.benficiary_mobile, 
--        o.benificiary_email, o.created_by, o.created_time, o.modified_by, o.modified_time, 
--         o.payment_type_id, o.payment_details, o.total_amount ,o.comment, o.display_name ,o.full_payment_flag ,o.billing_address,
--        o.delivery_address, o.pin_number,o.sales_man_id  , t.name ,(case when o.order_type_id =1 then 'Offline' else 'Online' end) as order_type ,s.address ,u.fname,p.code ,sm.name, o.international_shipping_charges,o.pin_date
--   FROM b_order o , m_tailor t,  m_store s, m_user u,m_payment_type p , m_sales_man sm
-- where o.order_id=in_order_id 
-- 	and o.tailor_id = t.tailor_id
-- 	and o.store_id =s.store_id
-- 	and o.payment_type_id =p.payment_type_id
-- 	and o.sales_man_id = sm.sales_man_id
-- 	and o.user_id=u.user_id
-- ;
-- END;

-- $function$;

-- ALTER FUNCTION public."GetOrderDetails"(integer)
--     OWNER TO tailorman_db;