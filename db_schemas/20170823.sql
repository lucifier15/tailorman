CREATE OR REPLACE VIEW public.dashboard_view_query AS
 SELECT i.order_item_id, i.order_id, i.workflow_id, i.sku_id, i.item_type_id, 
       i.mrp, i.qty, i.finish_type,i.fit_on_date, i.delivery_date, (select s.profile_id from b_item_wf_stage s ,b_workflow_stage ws where s.workflow_stage_id=ws.stage_id and s.order_item_id=i.order_item_id and s.current_stage_flag='Y' limit 1), i.priority_id,i.display_name , f.supplier_product_code , it.descr ,ft.code AS m_finish_type_code ,p.code As m_priority_type_code ,
       o.order_date, o.occasion, o.occation_date, o.benficiary_name, o.benficiary_mobile, 
       o.benificiary_email, o.created_by, o.created_time, o.modified_by, o.modified_time, 
       o.payment_details, o.total_amount ,o.comment ,o.full_payment_flag ,o.billing_address,
       o.delivery_address, o.pin_number,t.name AS m_tailor_name ,(case when o.order_type_id =1 then 'Offline' else 'Online' end) as order_type ,s.address ,u.fname,pm.code ,sm.name,
       c.name as m_customer_name, c.mobile, c.address AS m_customer_name_address, c.customer_id ,(select ws.code from b_item_wf_stage s ,b_workflow_stage ws where s.workflow_stage_id=ws.stage_id and s.order_item_id=i.order_item_id and s.current_stage_flag='Y' limit 1) AS b_workflow_stage_code,
       (select ws.stage_id from b_item_wf_stage s ,b_workflow_stage ws where s.workflow_stage_id=ws.stage_id and s.order_item_id=i.order_item_id and s.current_stage_flag='Y' limit 1),o.store_id
  FROM public.b_order_item i , m_fabric f, m_item_type it , m_finish_type ft , m_priority_type p ,
  b_order o , m_tailor t,  m_store s, m_user u,m_payment_type pm , m_sales_man sm ,m_customer c 

  where i.order_id=o.order_id
	and i.sku_id=f.fabric_id 
	and i.item_type_id = it.item_type_id
	and i.finish_type = ft.finish_type_id
	and i.priority_id = p.priority_type_id
	and o.tailor_id = t.tailor_id
	and o.store_id =s.store_id
	and o.payment_type_id =pm.payment_type_id
	and o.sales_man_id = sm.sales_man_id
	and o.user_id=u.user_id
	and c.customer_id =o.customer_id;
ALTER TABLE public.dashboard_view_query
    OWNER TO tailorman_db;
