-- Updating the pin_date column data type with timestamp without time zone

ALTER TABLE b_order ALTER COLUMN pin_date TYPE timestamp without time zone;
---------------------------------------------------------------------------------------
-- Modifying in_pin_date datatype from date to timestamp without time zone
-- Dropping the "UpdateOrderV2" proc if exists

DROP FUNCTION IF EXISTS public."UpdateOrderV2"(
	in_order_id integer,
	in_tailor_id integer,
	in_customer_id integer,
	in_order_type_id integer,
	in_store_id integer,
	in_user_id integer,
	in_order_date date,
	in_occasion character varying,
	in_occation_date date,
	in_benficiary_name character varying,
	in_benficiary_mobile character varying,
	in_benificiary_email character varying,
	in_priority_id integer,
	in_payment_type_id integer,
	in_payment_details character varying,
	in_total_amount double precision,
	in_comment character varying,
	in_display_name character varying,
	in_full_payment_flag character,
	in_billing_address character varying,
	in_delivery_address character varying,
	in_pin_number character varying,
	in_sales_man_id integer,
    in_pin_date date);

-- Create or replace "UpdateOrderV2" proc

CREATE OR REPLACE FUNCTION public."UpdateOrderV2"(
	in_order_id integer,
	in_tailor_id integer,
	in_customer_id integer,
	in_order_type_id integer,
	in_store_id integer,
	in_user_id integer,
	in_order_date date,
	in_occasion character varying,
	in_occation_date date,
	in_benficiary_name character varying,
	in_benficiary_mobile character varying,
	in_benificiary_email character varying,
	in_priority_id integer,
	in_payment_type_id integer,
	in_payment_details character varying,
	in_total_amount double precision,
	in_comment character varying,
	in_display_name character varying,
	in_full_payment_flag character,
	in_billing_address character varying,
	in_delivery_address character varying,
	in_pin_number character varying,
	in_sales_man_id integer,
    in_pin_date timestamp without time zone)
    RETURNS boolean
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE 
AS $function$

 
DECLARE

BEGIN 

update b_order set (
             tailor_id, customer_id, order_type_id, store_id, user_id, 
            order_date, occasion, occation_date, benficiary_name, benficiary_mobile, 
            benificiary_email, created_by, created_time, modified_by, modified_time, 
            priority_id, payment_type_id, payment_details,total_amount,comment,display_name,full_payment_flag,billing_address,delivery_address, pin_number,sales_man_id,pin_date)
    = ( in_tailor_id, in_customer_id, in_order_type_id, in_store_id, in_user_id, 
            in_order_date, in_occasion, in_occation_date,in_benficiary_name, in_benficiary_mobile, 
            in_benificiary_email, in_user_id, now(), in_user_id, now(), 
            in_priority_id, in_payment_type_id, in_payment_details,in_total_amount,in_comment,in_display_name,in_full_payment_flag, in_billing_address,in_delivery_address, in_pin_number,in_sales_man_id,in_pin_date) where order_id=in_order_id ;
return true ;
END;

$function$;

ALTER FUNCTION public."UpdateOrderV2"(integer, integer, integer, integer, integer, integer, date, character varying, date, character varying, character varying, character varying, integer, integer, character varying, double precision, character varying, character varying, character, character varying, character varying, character varying, integer,timestamp without time zone)
    OWNER TO tailorman_db;
------------------------------------------------------------------------------------------------------------------------------------------------
-- Modifying pin_date datatype from date to timestamp without time zone to return in that format
-- Dropping the "GetOrderDetails" proc if exists

DROP FUNCTION IF EXISTS public."GetOrderDetails"(
  in_order_id integer);

-- Create or replace "GetOrderDetails" proc

CREATE OR REPLACE FUNCTION public."GetOrderDetails"(
	in_order_id integer)
    RETURNS TABLE(order_id integer, tailor_id integer, customer_id integer, order_type_id integer, store_id integer, user_id integer, order_date date, occasion character varying, occation_date date, benficiary_name character varying, benficiary_mobile character varying, benificiary_email character varying, created_by integer, created_time timestamp with time zone, modified_by integer, modified_time timestamp with time zone, payment_type_id integer, payment_details character varying, total_amount double precision, comment character varying, display_name character varying, full_payment_flag character, billing_address character varying, delivery_address character varying, pin_number character varying, sales_man_id integer, tailor_name character varying, order_type text, store character varying, user_name character varying, payment_type character varying, sales_man character varying, shipping_charges integer,pin_date timestamp without time zone)
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE 
    ROWS 1000.0
AS $function$

 BEGIN 
  return query SELECT o.order_id, o.tailor_id, o.customer_id, o.order_type_id, o.store_id, o.user_id, 
       o.order_date, o.occasion, o.occation_date, o.benficiary_name, o.benficiary_mobile, 
       o.benificiary_email, o.created_by, o.created_time, o.modified_by, o.modified_time, 
        o.payment_type_id, o.payment_details, o.total_amount ,o.comment, o.display_name ,o.full_payment_flag ,o.billing_address,
       o.delivery_address, o.pin_number,o.sales_man_id  , t.name ,(case when o.order_type_id =1 then 'Offline' else 'Online' end) as order_type ,s.address ,u.fname,p.code ,sm.name, o.international_shipping_charges,o.pin_date
  FROM b_order o , m_tailor t,  m_store s, m_user u,m_payment_type p , m_sales_man sm
where o.order_id=in_order_id 
	and o.tailor_id = t.tailor_id
	and o.store_id =s.store_id
	and o.payment_type_id =p.payment_type_id
	and o.sales_man_id = sm.sales_man_id
	and o.user_id=u.user_id
;
END;

$function$;

ALTER FUNCTION public."GetOrderDetails"(integer)
    OWNER TO tailorman_db;
------------------------------------------------------------------------------------------------------------------------------------------------