-- Function: "GetFabricDesignParamsV3"(integer)

-- DROP FUNCTION "GetFabricDesignParamsV3"(integer);

CREATE OR REPLACE FUNCTION "GetFabricDesignParamsV3"(in_item_type_id integer)
  RETURNS SETOF json AS
$BODY$

BEGIN

if in_item_type_id=1 then 
RETURN QUERY SELECT cast('[{
		"id": 1,
		"name": "ONLY TUXEDO SHIRTS",
		"Designs": [{
			"id": 1,
			"name": "COLLAR",
			"type" : "option",
			"values" : ["REGULAR WING","REGULAR CUTAWAY","NARROW CUTAWAY"],
			"upcharge" : [],
			"upcharge percentage" :  []
		}, {
			"id": 2,
			"name": "PINTUCK/FRILLS",
			"type" : "option",
			"values" : ["YES","NO"],
			"upcharge" : [700,0],
			"upcharge percentage" : []
			
		},
		{
			"id": 3,
			"name": "BLACK PEARL BUTTONS",
			"type" : "option",
			"values" : ["YES (ALL BUTTONS)","YES (4 BUTTONS)","NO"],
			"upcharge" : [600,300,0 ] ,
			"upcharge percentage" : []
			
		}
		]
	},

	{
		"id": 2,
		"name": "ONLY CLASSIC SHIRTS",
		"Designs": [{
			"id": 1,
			"name": "COLLAR",
			"type" : "option",
			"values" : ["CLASSIC","NARROW CLASSIC","CUTAWAY","NARROW CUTAWAY","MAO","BUTTON DOWN","KURTA","OTHER (see comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"disable_dependency": {
				"MAO": [2]
			}
		},{
			"id": 2,
			"name": "COLLAR BONE",
			"type" : "option",
			"values" : ["FIXED","REMOVABLE"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		},{
			"id": 3,
			"name": "PLACKET",
			"type" : "option",
			"values" : ["PLACKET","WITHOUT PLACKET","OTHER"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		},
		{
			"id": 4,
			"name": "CHEST POCKET",
			"type" : "option",
			"values" : ["1 ON WEARERS LEFT","2","NONE"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		},
		{
			"id": 5,
			"name": "INNET POCKET",
			"type" : "option",
			"values" : ["YES","NONE"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}
		]
	},
	{
		"id": 3	,
		"name": "ALL SHIRTS",
		"Designs": [{
			"id": 1,
			"name": "SHIRT HEM",
			"type" : "option",
			"values" : ["CHINESE CUT BOTTOM","STRAIGHT BOTTOM WITH SIDE SLIT"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 2,
			"name": "BOX PLEAT AT NECK",
			"type" : "option",
			"values" : ["NONE","YES"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 3,
			"name": "MONOGRAM",
			"type" : "option",
			"values" : ["NONE","YES"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},  {
			"id": 4,
			"name": "MONOGRAM TEXT",
			"type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 5,
			"name": "MONOGRAM COLOUR",
			"type" : "option",
			"values" : ["TONAL","WHITE","BLACK","NAVY","RED","DARK GREEN","MAROON"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		},

		{
			"id": 6,
			"name": "MONOGRAM POSITION",
			"type" : "option",
			"values" : ["LEFT CUFF","POCKET"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 7,
			"name": "CUFF",
			"type" : "option",
			"values" : ["REGULAR","DOOUBLE/FRENCH (Rs 300)","SHORT SLEEVE","OTHER (see comments)"],
			"upcharge" : [0,300,0,0],
			"upcharge percentage" : []
			
		},
		{
			"id": 8,
			"name": "CONTRAST IN COLLAR",
			"type" : "option",
			"values" : ["NONE","TOTAL(OUTSIDE,INSIDE BAND AND UNDER-SIDE) (Rs 300)","ONLY INSIDE BAND (Rs 300)","ONLY UNDER-SIDE (Rs 300)"],
			"upcharge" : [0,300 ,300,300],
			"upcharge percentage" : []
			
		},{
			"id": 9,
			"name": "COLLAR CONTRAST FABRIC OPTIONS",
			"type" : "option",
			"values" : ["CF4","CF5","CF328","CF329","CF623","OTHER (see comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},{
			"id": 10,
			"name": "CUFF",
			"type" : "option",
			"values" : ["NONE","TOTAL(OUTSIDE AND INSIDE) (Rs 300)","ONLY INSIDE (Rs 300)"],
			"upcharge" : [0,300 ,300],
			"upcharge percentage" : []
			
		},{
			"id": 11,
			"name": "CUFF CONTRAST FABRIC OPTIONS",
			"type" : "option",
			"values" : ["CF4","CF5","CF328","CF329","CF623","OTHER (see comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},{
			"id": 12,
			"name": "BUTTON PLACKET",
			"type" : "option",
			"values" : ["NONE","ONLY BUTTON PLACKET(GOES UNDER MAIN PLACKET WHEN FASTENED) (Rs 300)"],
			"upcharge" : [0,300] ,
			"upcharge percentage" : []
			
		},{
			"id": 13,
			"name": "BUTTON PLACKET CONTRAST FABRIC OPTIONS",
			"type" : "option",
			"values" : ["CF4","CF5","CF328","CF329","CF623","OTHER (see comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		}
		]
	}
]' as json)
  ;

elsif in_item_type_id=3 then 
RETURN QUERY SELECT cast('[{
		"id": 1,
		"name": "TROUSER",
		"Designs": [{
			"id": 1,
			"name": "PLEAT STYLE",
			"type" : "option",
			"values" : ["Flatfront","2-Pleats","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 2,
			"name": "SIDE POCKETS",
			"type" : "option",
			"values" : ["Slant","Straight",	"Other (See comments)" ],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 3,
			"name": "WAISTBAND V NOTCH OPENING",
			"type" : "option",
			"values" : ["No", "Yes" ],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 4,
			"name": "HEM",
			"type" : "option",
			"values" : ["Plain","Turn Up" ],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 5,
			"name": "STYLE",
			"type" : "option",
			"values" : ["Standard","Tuxedo - satin on side ST 1-Black  (Rs 500)","Tuxedo - satin on side ST 2 -Navy (Rs 500)","Tuxedo - satin on side ST 3-Wine (Rs 500)","Tuxedo - satin on side ST 4 White (Rs 500)"],
			"upcharge" : [0,500,500,500,500] ,
			"upcharge percentage" : []
		}, {
			"id": 6,
			"name": "LINING",
			"type" : "option",
			"values" : ["None","Knee Lining (Rs 100)","Other (See comments)"],
			"upcharge" : [0,100,0] ,
			"upcharge percentage" : []
		}, {
			"id": 7,
			"name": "WAISTBAND",
			"type" : "option",
			"values" : ["Plain","Buckle -(Rs 100)","Side Elastic (Rs 500)"],
			"upcharge" : [0,100,500] ,
			"upcharge percentage" : []
		}, {
			"id": 8,
			"name": "INSIDE WAISTBAND POCKET",
			"type" : "option",
			"values" : ["No","Yes (Rs 200)"],
			"upcharge" : [0,200] ,
			"upcharge percentage" : []
		}
		]
	}
]' as json)
  ;
  elsif in_item_type_id=4 then

  
RETURN QUERY SELECT cast('[{
		"id": 1,
		"name": "BUNDY",
		"Designs": [{
			"id": 1,
			"name": "STYLE",
			"type" : "option",
			"values" : ["CLASSIC","MULTIBUTTON","CUT WAY","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"Size 32L" : [7,3,7,0],
			"Size 24L" : [0,21,0,0]
		}, {
			"id": 2,
			"name": "COLLAR FABRIC",
			"type" : "option",
			"values" : ["SELF FABRIC","SATIN - BLACK (Rs 500)", "SATIN - NAVY (Rs 500)","SATIN - WINE (Rs 500)","SATIN - WHITE (Rs 500)", "VELVET - BLACK (Rs 500)","VELVET - NAVY (Rs 500)","VELVET - WINE (Rs 500)","Other (See comments)" ],
			"upcharge" : [0,500,500,500,500,500,500,500,0] ,
			"upcharge percentage" : []
		}, {
			"id": 3,
			"name": "PIPING",
			"type" : "option",
			"values" : ["FLAT PIPING (Rs 500)", "CORD PIPING (Rs 500)" ],
			"upcharge" : [500,500] ,
			"upcharge percentage" : []
		}, {
			"id": 4,
			"name": "PIPING FABRIC AND COLOUR",
			"type" : "option",
			"values" : ["SELF FABRIC","SATIN - BLACK", "SATIN - NAVY","SATIN - WINE","SATIN - WHITE", "VELVET - BLACK","VELVET - NAVY","VELVET - WINE","Other (See comments)" ],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 5,
			"name": "BUTTONS",
			"is_button" : "true",
			"type" : "option",
			"values" : ["MATCHING","SELF FABRIC","SATIN - BLACK", "SATIN - NAVY","SATIN - WINE","SATIN - WHITE", "VELVET - BLACK","VELVET - NAVY","VELVET - WINE","METAL (Rs 500)","Other (See comments)"],
			"upcharge" : [0,0,0,0,0,0,0,0,0,500,0] ,
			"upcharge percentage" : []
		},
		{
			"id": 6,
			"name": "BUTTON SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		}, {
			"id": 7,
			"name": "STRAIGHT POCKET (DOUBLE WELT)",
			"type" : "option",
			"values" : ["DOUBLEBONE BESOM IN SATIN","SATIN - BLACK", "SATIN - NAVY","SATIN - WINE","SATIN - WHITE", "VELVET - BLACK","VELVET - NAVY","VELVET - WINE","Other (See comments)"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 8,
			"name": "STRAIGHT POCKET (SINGLE WELT)",
			"type" : "option",
			"values" : ["SELF FABRIC","SATIN - BLACK", "SATIN - NAVY","SATIN - WINE","SATIN - WHITE", "VELVET - BLACK","VELVET - NAVY","VELVET - WINE","Other (See comments)"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 9,
			"name": "POCKET PIPING",
			"type" : "option",
			"values" : [ "FLAT PIPING","AT CHEST WELT","AT FLAP","CORD PIPING","Other (See comments)"],
			"upcharge" : [],
			"upcharge percentage" : [15,15,15,15,0]
		}, {
			"id": 10,
			"name": "LINING",
			"is_lining" : "true" ,
			"type" : "option",
			"values" : [ "FUN","SILK ( +2500 )","Other (See comments)"],
			"upcharge" : [0,2500,0],
			"upcharge percentage" : []
		}, {
			"id": 11,
			"name": "LINING SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		},

		{
			"id": 12,
			"name": "POCKET SQUARE",
			"type" : "option",
			"values" : [ "SAME AS LINING FABRIC","PLAIN RED","GOLD","Other (See comments)"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 13,
			"name": "PLACKET TYPE",
			"type" : "option",
			"values" : [ "NORMAL PLACKET","CONCEALED PLACKET"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 14,
			"name": "DECORATIVE STITCH",
			"type" : "option",
			"values" : [ "MATCHING PICK STITCH (Rs 300)","MATCHING TOP STITCH (Rs 300)"],
			"upcharge" : [300,300],
			"upcharge percentage" : []
		}, {
			"id": 15,
			"name": "BOTTOM HEM",
			"type" : "option",
			"values" : [ "ROUND","SQUARE"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 16,
			"name": "FRONT PLACKET",
			"type" : "option",
			"selected": 0,
			"values" : [ "YES","NO"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 17,
			"name": "FLAT PIPING",
			"type" : "option",
			"values" : ["YES","NO"],
			"upcharge" : [500,0] ,
			"upcharge percentage" : []
		}, {
			"id": 18,
			"name": "FLAT PIPING FABRIC AND COLOR",
			"type" : "option",
			"values" : ["SATIN BLACK", "NAVY", "WINE", "WHITE", "VELVET - BLACK", "OTHER SEE COMMENTS"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 19,
			"name": "FLAT PIPING LOCATION",
			"type" : "option",
			"values" : ["COLLAR","LAPEL", "OTHER SEE COMMENTS"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}
		]
	}
]' as json)
  ;

    elsif in_item_type_id=5 then

  
RETURN QUERY SELECT cast('[{
		"id": 1,
		"name": "SHERWANI",
		"Designs": [{
			"id": 1,
			"name": "STYLE",
			"type" : "option",
			"values" : ["CLASSIC","MULTIBUTTON","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 2,
			"name": "COLLAR FABRIC",
			"type" : "option",
			"values" : ["SELF FABRIC","SATIN - BLACK (Rs.500)", "SATIN - NAVY (Rs.500)","SATIN - WINE (Rs.500)","SATIN - WHITE (Rs.500)", "VELVET - BLACK (Rs.500)","VELVET - NAVY (Rs.500)","VELVET - WINE (Rs.500)","Other (See comments)" ],
			"upcharge" : [0,500,500,500,500,500,500,500,0] ,
			"upcharge percentage" : []
		}, {
			"id": 3,
			"name": "PIPING",
			"type" : "option",
			"values" : ["NONE","FLAT PIPING (Rs.500)", "CORD PIPING (Rs.500)" ],
			"upcharge" : [0,500,500] ,
			"upcharge percentage" : []
		}, {
			"id": 4,
			"name": "PIPING FABRIC AND COLOUR",
			"type" : "option",
			"values" : ["SELF FABRIC","SATIN - BLACK", "SATIN - NAVY","SATIN - WINE","SATIN - WHITE", "VELVET - BLACK","VELVET - NAVY","VELVET - WINE","Other (See comments)" ],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 5,
			"name": "BUTTONS",
			"is_button" : "true" ,
			"type" : "option",
			"values" : ["MATCHING","SELF FABRIC","SATIN - BLACK", "SATIN - NAVY","SATIN - WINE","SATIN - WHITE", "VELVET - BLACK","VELVET - NAVY","VELVET - WINE","METAL (Rs 500)","Other (See comments)"],
			"upcharge" : [0,0,0,0,0,0,0,0,0,500,0] ,
			"upcharge percentage" : []
		},
		{
			"id": 6,
			"name": "BUTTON SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		}

		, {
			"id": 7,
			"name": "LINING",
			"is_lining" : "true",
			"type" : "option",
			"values" : [ "FUN","SILK ( +2500 )","Other (See comments)"],
			"upcharge" : [0,2500,0],
			"upcharge percentage" : []
		},{
			"id": 8,
			"name": "LINING SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		},

		 {
			"id": 9,
			"name": "MONOGRAM LOCATION",
			"type" : "option",
			"values" : [ "ON LINING","AT UNDERCOLLAR FELT"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 10,
			"name": "MONOGRAM TEXT",
			"type" : "text",
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 11,
			"name": "MONOGRAM COLOR",
			"type" : "option",
			"values" : ["Tonal", "White", "Black", "Navy", "Red", "Dark Green","Orange","Pink", "Other (See comments)"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 12,
			"name": "PLACKET TYPE",
			"type" : "option",
			"values" : [ "NORMAL PLACKET","CONCEALED PLACKET"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 13,
			"name": "DECORATIVE STITCH",
			"type" : "option",
			"values" : [ "MATCHING PICK STITCH (Rs 300)","MATCHING TOP STITCH (Rs 300)"],
			"upcharge" : [300,300],
			"upcharge percentage" : []
		},
		{
			"id": 14,
			"name": "POCKET SQUARE",
			 "type" : "option",
			"values" : ["GOLD","SAME AS LINING FABRIC"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		}
		]
	}
]' as json)
  ;

  elsif in_item_type_id=6 then
  
RETURN QUERY SELECT cast('[{
		"id": 1,
		"name": "WAISTCOAT",
		"Designs": [{
			"id": 1,
			"name": "CLOSURE AND LAPEL",
			"type" : "option",
			"values" : ["REGULAR 5 BUTTONS","HIGH 6 BUTTONS","LOW 4 BUTTONS","LOW 3 BUTTONS","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"Size 32L" : [0,0,0,0,0],
			"Size 24L" : [6,7,5,4,0]
		},  {
			"id": 2,
			"name": "PIPING",
			"type" : "option",
			"values" : ["FLAT PIPING (Rs.500)", "CORD PIPING (Rs.500)" ],
			"upcharge" : [500,500] ,
			"upcharge percentage" : []
		}, {
			"id": 3,
			"name": "PIPING FABRIC AND COLOUR",
			"type" : "option",
			"values" : ["SELF FABRIC","SATIN - BLACK", "SATIN - NAVY","SATIN - WINE","SATIN - WHITE", "VELVET - BLACK","VELVET - NAVY","VELVET - WINE","Other (See comments)" ],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 4,
			"name": "BUTTONS",
			"is_button" : "true",
			"type" : "option",
			"values" : ["MATCHING","SELF FABRIC","SATIN - BLACK", "SATIN - NAVY","SATIN - WINE","SATIN - WHITE", "VELVET - BLACK","VELVET - NAVY","VELVET - WINE","METAL (Rs 500)","Other (See comments)"],
			"upcharge" : [0,0,0,0,0,0,0,0,0,500,0] ,
			"upcharge percentage" : []
		},
		{
			"id": 5,
			"name": "BUTTON SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		}

		, {
			"id": 6,
			"name": "POCKET PIPING (for STRAIGHT/SLANT POCKETS only)",
			"type" : "option",
			"values" : [ "FLAT PIPING","AT CHEST WELT","AT FLAP","CORD PIPING","Other (See comments)"],
			"upcharge" : [],
			"upcharge percentage" : []
		},  {
			"id": 7,
			"name": "PIPING FABRIC",
			"type" : "option",
			"values" : ["SATIN - BLACK", "SATIN - NAVY","SATIN - WINE","SATIN - WHITE", "VELVET - BLACK","VELVET - NAVY","VELVET - WINE","Other (See comments)"],
			"upcharge" : [],
			"upcharge percentage" : []
		},{
			"id": 8,
			"name": "LINING - INSIDE FABRIC",
			"is_lining" : "true",
			"type" : "option",
			"values" : [ "FUN","SILK (Rs. 1250)","Other (See comments)"],
			"upcharge" : [0,2500,0],
			"upcharge percentage" : []
		}, {
			"id": 9,
			"name": "LINING - INSIDE FABRIC SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		},{
			"id": 10,
			"name": "LINING - OUTSIDE FABRIC",
			"is_lining" : "true",
			"type" : "option",
			"values" : [ "FUN","SILK (Rs. 1250)","Other (See comments)"],
			"upcharge" : [0,2500,0],
			"upcharge percentage" : []
		}, {
			"id": 11,
			"name": "LINING - OUTSIDE FABRIC SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		},

		{
			"id": 12,
			"name": "CHEST POCKET",
			"type" : "option",
			"values" : [ "NO","YES"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 13,
			"name": "ALL POCKETS IN SATIN",
			"type" : "option",
			"values" : [ "NO","YES","SATIN - BLACK", "SATIN - NAVY","SATIN - WINE","SATIN - WHITE"],
			"upcharge" : [],
			"upcharge percentage" : []
		}, {
			"id": 14,
			"name": "PICK STITCH",
			"type" : "option",
			"values" : [ "NO","YES (STANDARD)"],
			"upcharge" : [0, 300],
			"upcharge percentage" : []
		}, {
			"id": 15,
			"name": "FLAT PIPING",
			"type" : "option",
			"values" : ["YES","NO"],
			"upcharge" : [500,0] ,
			"upcharge percentage" : []
		}, {
			"id": 16,
			"name": "FLAT PIPING FABRIC AND COLOR",
			"type" : "option",
			"values" : ["SATIN BLACK", "NAVY", "WINE", "WHITE", "VELVET - BLACK", "OTHER SEE COMMENTS"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 17,
			"name": "FLAT PIPING LOCATION",
			"type" : "option",
			"values" : ["COLLAR","LAPEL", "OTHER SEE COMMENTS"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}
		]
	}
]' as json)
  ;

  elsif in_item_type_id=2  then
RETURN QUERY SELECT cast('[{
		"id": 1,
		"name": "REGULAR JACKETS",
		"collapse" : "true",
		"Designs": [{
			"id": 1,
			"name": "CLOSURE AND LAPEL",
			"type" : "option",
			"values" : ["SB2 NORMAL NOTCH","SB2 NARROW NOTCH","SB2 BROAD NOTCH","SB2 NORMAL PEAK","SB2 NARROW PEAK","SB1 NARROW NOTCH","SB1 NARROW PEAK","SB1 NORMAL PEAK","SB1 SUPER NARROW","SB3 NORMAL NOTCH","SB1 SHAWL","SB1 NORMAL PEAK TUXEDO","SB1 NARROW PEAK TUXEDO","SB1 SHAWL TUXEDO","SB2 SHAWL TUXEDO","FORD TUXEDO","Other"],
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"Size 32L" : [3,3,3,3,3,2,2,2,2,4,2,2,2,2,3,2,0],
			"Size 24L" : [9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,0]
		}, {
			"id": 2,
			"name": "MELTON",
			"type" : "option",
			"values" : ["NA","Tonal","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 3,
			"name": "VENTS",
			"type" : "option",
			"values" : ["Middle [1]","Side [2]","No Vent"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 4,
			"name": "LINING STYLE",
			"type" : "option",
			"values" : ["Full","Half"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 6,
			"name": "INTERNAL FACING",
			"type" : "option",
			"values" : ["Straight 4 pockets","Framed 4 pockets","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 7,
			"name": "MONOGRAM POSITION",
			 "type" : "option",
			"values" : ["Yes (lining )","None ","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 8,
			"name": "MONOGRAM TEXT (Max 16 Char)",
			"type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 9,
			"name": "MONOGRAM COLOUR",
			 "type" : "option",
			"values" : ["Tonal", "White", "Black", "Navy", "Red", "Dark Green","Orange", "Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 10,
			"name": "JACKET FINISH",
			 "type" : "option",
			"values" : ["Firm"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 11,
			"name": "TUXEDO SATIN TRIM",
			 "type" : "option",
			"values" : ["N/A","ST 1-black", "ST 2 -Navy" , "ST 3-Wine" ,"ST 4 White", "Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"disable_dependency": {
				"N/A": [26,27,28]
			}
			
		},
		{
			"id": 26,
			"name": "TUXEDO SATIN TRIM LOCATION - LAPEL",
			 "type" : "option",
			"values" : ["YES","NO"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 27,
			"name": "TUXEDO SATIN TRIM LOCATION - WELT POCKET",
			 "type" : "option",
			"values" : ["YES","NO"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 28,
			"name": "TUXEDO SATIN TRIM LOCATION - FLAP BONE",
			 "type" : "option",
			"values" : ["YES","NO"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 12,
			"name": "JACKET POCKETS",
			 "type" : "option",
			"values" : ["Flap straight", "Flap slant", "Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 13,
			"name": "TICKET POCKETS",
			 "type" : "option",
			"values" : ["None", "Straight with Flap", "Slant with Flap", "Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 14,
			"name": "ELBOW PATCHES",
			 "type" : "option",
			"values" : ["None", "Mock Suede", "Self"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 15,
			"name": "SLEEVE BUTTONS",
			 "type" : "option",
			"values" : ["Kissing", "Overlapping", "Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 16,
			"name": "SLEEVE BUTTON HOLE",
			 "type" : "option",
			"values" : ["All tonal", "Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 17,
			"name": "JACKET CONSTRUCTION",
			 "type" : "option",
			"values" : ["Floating Chest Piece", "Half Canvas (Rs 2000)", "Full Canvas (Rs 4000 )"],
			"upcharge" : [0,2000,4000] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 18,
			"is_button" : "true",
			"name": "BUTTONS (Body and Sleeve)",
			 "type" : "option",
			"values" : ["Tonal","Satin ST 1-Black", "ST 2 -Navy","ST 3-Wine","ST 4 White", "Metal - Gold (Rs 500)", "Metal - Silver (Rs 500)","Other (See comments)"],
			"upcharge" :[0,0,0,0,0, 500,500,0] ,
			"upcharge percentage" : []
			
			
		},
		{
			"id": 19,
			"name": "BUTTON SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		},
		{
			"id": 20,
			"name": "CUFF FINISHING",
			 "type" : "option",
			"values" : ["4 closed", "4 open (Rs 500)", "Other (See comments)"],
			"upcharge" : [0,500,0] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 21,
			"is_lining" : "true",
			"name": "LINING FABRIC",
			 "type" : "option",
			"values" : ["Fun (specify article)-", "Silk Lining (Rs 2500) - Specify Article","Other (See comments)"],
			"upcharge" : [0,2500,0] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 22,
			"name": "LINING SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		},
		{
			"id": 23,
			"name": "PICK STITCH",
			 "type" : "option",
			"values" : ["None","Yes (Standard) - (Rs 300)"],
			"upcharge" : [0,300] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 24,
			"name": "TRAVELFLEX",
			 "type" : "option",
			"values" : ["NO","YES (Rs 3000)"],
			"upcharge" : [0,3000] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 25,
			"name": "POCKET SQUARE",
			 "type" : "option",
			"values" : ["GOLD","SAME AS LINING FABRIC"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		}, {
			"id": 29,
			"name": "FLAT PIPING",
			"type" : "option",
			"values" : ["YES","NO"],
			"upcharge" : [500,0] ,
			"upcharge percentage" : []
		}, {
			"id": 30,
			"name": "FLAT PIPING FABRIC AND COLOR",
			"type" : "option",
			"values" : ["SATIN BLACK", "NAVY", "WINE", "WHITE", "VELVET - BLACK", "OTHER SEE COMMENTS"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 31,
			"name": "FLAT PIPING LOCATION",
			"type" : "option",
			"values" : ["COLLAR","LAPEL", "OTHER SEE COMMENTS"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}
		]
	},
	{
		"id": 2,
		"name": "BANDHGALA JACKETS",
		"collapse" : "true",
		"Designs": [{
			"id": 1,
			"name": "CLOSURE",
			"type" : "option",
			"values" : ["Classic Bundhgala without cord piping","Classic Bundhgala with cord piping","Multi Button Bandhgala","Cut away Bandhgala","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"Size 32L" : [6,6,3,6,0],
			"Size 24L" : [9,9,29,9,0]
		},
		{
			"id": 2,
			"name": "VENTS",
			"type" : "option",
			"values" : ["Middle [1]","Side [2]","No vent"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},{
			"id": 3,
			"name": "LINING STYLE",
			"type" : "option",
			"values" : ["*Full","Half"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},{
			"id": 4,
			"name": "INTERNAL FACING",
			"type" : "option",
			"values" : ["*Straight 4 pockets","Framed 4 pockets","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},{
			"id": 5,
			"name": "MONOGRAM POSITION",
			 "type" : "option",
			"values" : ["*Yes lining","None","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 6,
			"name": "MONOGRAM TEXT (Max 18 Char)",
			"type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 7,
			"name": "MONOGRAM COLOUR",
			 "type" : "option",
			"values" : ["Tonal", "White", "Black", "Navy", "Red", "Dark Green","Orange","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 8,
			"name": "JACKET CONSTRUCTION",
			 "type" : "option",
			"values" : ["*Floating Chest Piece"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 9,
			"name": "JACKET FINISH",
			 "type" : "option",
			"values" : ["*Firm"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 10,
			"name": "JACKET POCKETS",
			 "type" : "option",
			"values" : ["*Flap straight","Flap slant","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 11,
			"name": "TICKET POCKETS",
			 "type" : "option",
			"values" : ["*None","Straight with Flap","Slant with Flap","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 12,
			"name": "SLEEVE BUTTONS",
			 "type" : "option",
			"values" : ["*Kissing","Overlapping","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 13,
			"name": "SLEEVE BUTTON HOLE",
			 "type" : "option",
			"values" : ["*All tonal","Other (See comments)"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 14,
			"name": "CUFF FINISHING",
			 "type" : "option",
			"values" : ["*4 closed","4 open - (Rs 500)","Other (See comments)"],
			"upcharge" : [0,500,0] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 15,
			"is_button" : "true",
			"name": "BUTTON (Body and Sleeve)",
			 "type" : "option",
			"values" : ["Tonal","ST 1-Black","ST 2 -Navy","ST 3-Wine","ST 4 White","Metal  - Gold (Rs 500)","Metal  - Silver (Rs 500)","Self","Other (See comments)"],
			"upcharge" : [0,0,0,0,0,500,500,0,0] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 16,
			"name": "BUTTON SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		},
		
		{
			"id": 17,
			"is_lining" : "true",
			"name": "LINING FABRIC",
			"type" : "option",
			"values" : ["Fun (specify article)","Silk Lining (Upcharge Rs 2500) - Specify Article","Other (See comments)" 	],
			"upcharge" : [0,2500,0] ,
			"upcharge percentage" : []
			
		},{
			"id": 18,
			"name": "LINING SKU",
			 "type" : "text",
			"upcharge" : [] ,
			"upcharge percentage" : [],
			"in_factory" : "yes"
			
		},
		
		
		{
			"id": 19,
			"name": "PICK STITCH",
			 "type" : "option",
			"values" : ["*None","Yes (Standard) - (Rs 300)"],
			"upcharge" : [0,300] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 20,
			"name": "CORD PIPING(Satin Trim)",
			 "type" : "option",
			"values" : [ "N/A","ST 1-Black - (Rs 500)","ST 2 -Navy - (Rs 500)","ST 3-Wine - (Rs 500)","ST 4 White - (Rs 500)","Other (See comments)"],
			"upcharge" : [0,500,500,500,500,0] ,
			"upcharge percentage" : [],
			"enable_dependency": {
				"N/A" : [21]
			}
			
		},
		{
			"id": 21,
			"name": "FRONT PLACKET",
			 "type" : "option",
			"values" : ["YES","NO"],
			"disabled": true,
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 22,
			"name": "TRAVELFLEX",
			 "type" : "option",
			"values" : ["NO","YES (Rs 3000)"],
			"upcharge" : [0,3000] ,
			"upcharge percentage" : []
			
		},
		{
			"id": 23,
			"name": "POCKET SQUARE",
			 "type" : "option",
			"values" : ["GOLD","SAME AS LINING FABRIC"],
			"upcharge" : [] ,
			"upcharge percentage" : []
			
		}, {
			"id": 24,
			"name": "FLAT PIPING",
			"type" : "option",
			"values" : ["YES","NO"],
			"upcharge" : [500,0] ,
			"upcharge percentage" : []
		}, {
			"id": 25,
			"name": "FLAT PIPING FABRIC AND COLOR",
			"type" : "option",
			"values" : ["SATIN BLACK", "NAVY", "WINE", "WHITE", "VELVET - BLACK", "OTHER SEE COMMENTS"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}, {
			"id": 26,
			"name": "FLAT PIPING LOCATION",
			"type" : "option",
			"values" : ["COLLAR","LAPEL", "OTHER SEE COMMENTS"],
			"upcharge" : [] ,
			"upcharge percentage" : []
		}
		]
	}
]' as json);
  end if;
  
  
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION "GetFabricDesignParamsV3"(integer)
  OWNER TO tailorman_db;