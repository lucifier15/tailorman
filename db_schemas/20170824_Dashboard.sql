CREATE TABLE dashboard_queries(
    dashboard_queries_id serial,
    query  character varying(500) NOT NULL,
    query_name  character varying(500) NOT NULL,
    user_id integer
);