-- updating bundy 'NECK' item_type pattern default value from 1.5 50 1.25

update b_item_type_pattern_default_values set default_value=1.25 where item_type_id=4 and measurement_type_id=29;
-------------------------------------------------------------------------------------------------------------------