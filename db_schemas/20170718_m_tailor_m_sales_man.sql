--M_TAILOR TABLE
select currval('m_tailor_tailor_id_seq');
select setval('m_tailor_tailor_id_seq',((select max(tailor_id) from m_tailor));

INSERT INTO m_tailor(name) VALUES ('Naveen Alokam'), ('Eliyas');
update m_tailor set name='Dasthagir' where name='Dasthgr';
update m_tailor set name='Ayath Basha' where name='Ayath';

--M_SALES_MAN TABLE
select currval('m_sales_man_sales_man_id_seq');
select setval('m_sales_man_sales_man_id_seq',(select max(sales_man_id) from m_sales_man));

INSERT INTO m_sales_man(name) VALUES ('Naveen Alokam'), ('Eliyas');
update m_sales_man set name='Dasthagir' where name='Dasthgr';
update m_sales_man set name='Ayath Basha' where name='Ayath';