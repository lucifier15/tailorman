//Fetching RTWSKU Details query (Search & Select)

CREATE OR REPLACE FUNCTION "GetRTWSKUDetails"(
    IN in_store_id integer,
    IN in_product_sku_code character varying
    IN in_size character varying
    IN in_fit character varying)
  RETURNS TABLE(fabric_id integer, fabric_sku_code character varying, name character varying, mrp double precision, product_sku_code character varying, item_type_id integer, store_id integer, inventory_count bigint, size character varying, fit character varying) AS
$BODY$ 
BEGIN  
  return query SELECT f.fabric_id,f.sku_code,f.name,f.shirt_mrp,f.supplier_product_code,m.item_type_id,mc.store_id,sum(mc.inventory_count),mc.size,mc.fit
 from m_fabric f 
 join m_item_type m on m.item_type_id = f.item_type_id and m.mtm_flag='N'
 join m_category_rtw_inventory_measurment mc on mc.item_type_id=m.item_type_id and mc.sku_code=f.supplier_product_code and mc.inventory_count > 0 and mc.store_id=in_store_id
 where f.in_stock_qty>0 and  
 	f.supplier_product_code ilike  '%' || in_product_sku_code || '%'
GROUP BY f.fabric_id, f.sku_code,  f.name, f.shirt_mrp, f.supplier_product_code,m.item_type_id,mc.size,
mc.store_id,mc.fit order by 1
;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;


//Fetching Size query

SELECT DISTINCT size from m_category_rtw_inventory_measurment;


//Fetching Fit query

SELECT DISTINCT fit from m_category_rtw_inventory_measurment;


//Creating rtw_inventory_transfer TABLE

create table rtw_inventory_transfer(
  rtw_inventory_transfer_id serial not null,
  from_store_id integer not null,
  to_store_id integer not null,
  fabric_id integer not null,
  size character varying not null,
  fit character varying not null,
  qty integer not null,
  description text,
  created_date timestamp without time zone DEFAULT now() NOT NULL,
  isactive character(1) DEFAULT 'Y'::bpchar NOT NULL,
  created_by integer not null,
  PRIMARY KEY(rtw_inventory_transfer_id)
);


//On Clicking Transfer btn,inserting details into the table

(`INSERT INTO rtw_inventory_transfer(from_store_id,to_store_id,size,fit,qty,description,fabric_id,created_by)values(${from_store_id},${to_store_id},'${size}','${fit}',${qty},'${description}',${created_by},${fabric_id})`)