--DROP FUNCTION IF EXISTS "GetOrderItemDetails"(in_order_item_id integer);
CREATE OR REPLACE FUNCTION public."GetOrderItemDetails"(
	in_order_item_id integer)
RETURNS TABLE(order_item_id integer, order_id integer, workflow_id integer, sku_id integer, style_id integer, item_type_id integer, mrp double precision, qty integer, finish_type_id integer, fit_on_date date, delivery_date date, comment character varying, profile_id integer, priority_id integer, display_name character varying, product_sku character varying, item_type character varying, finish_type character varying, priority character varying, block character varying, item_size character varying, upcharge json, taxes json, discount_value double precision, discount_type integer, care_info character varying, fabric_design character varying, construction character varying, color_info character varying, fabric_width double precision, fusing character varying, thread_shade character varying, material_info_for_wo character varying, order_status character varying, supplier_product_code character varying, material_composition character varying, marker_type character varying, marker_width double precision, pattern_repeat_size character varying, nap_test character varying, pocket_color character varying, buttons character varying, buttons_supplier_product_code character varying, seam_tape character varying, lining character varying, lining_supplier_product_code character varying, lining_width double precision, tonal_ucf character varying, canvas character varying, tonal_button_sku character varying, tonal_lining_sku character varying, discount_comment character varying)
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE 
    ROWS 1000.0
AS $function$

 
declare
var_block character varying ;
var_item_size character varying ;
var_measurement_profile_id integer;
BEGIN 
select ws.profile_id into var_measurement_profile_id from public.b_item_wf_stage ws where ws.order_item_id = in_order_item_id and ws.workflow_stage_id =1 limit 1;
 
  return query 
SELECT i.order_item_id, i.order_id, i.workflow_id, i.sku_id, i.style_id, i.item_type_id, 
       i.mrp, i.qty, i.finish_type,i.fit_on_date, i.delivery_date,
       i.comment, 
       var_measurement_profile_id, 
       i.priority_id,i.display_name , f.supplier_product_code , it.descr ,ft.code ,p.code ,(select b.block_name from b_profile_measurement pm , b_customer_measurement_profile pf , m_item_type_block b
      where pm.profile_id =var_measurement_profile_id and 
      pf.profile_id =pm.profile_id and
      pm.value =b.block_id
      and (pm.measurement_type_id in(
      select m.measurement_type_id from m_measurement_type m where m.item_type_id= i.item_type_id and
      m.measurement_source_id = pf.measurement_source_id  and m.code='FINAL TRYON DROP' ) or pm.measurement_type_id =-1000) limit 1
      ) as i_block, (select cast( pm.value as character varying) from b_profile_measurement pm , b_customer_measurement_profile pf 
      where pm.profile_id =var_measurement_profile_id and 
      pf.profile_id =pm.profile_id 
      and (pm.measurement_type_id in(
      select m.measurement_type_id from m_measurement_type m where m.item_type_id= i.item_type_id and
      m.measurement_source_id = pf.measurement_source_id  and m.code='FINAL TRYON SIZE' ) or pm.measurement_type_id =-1001 )limit 1
      ) as i_size,
       i.upcharge , i.taxes  ,i.discount,i.discount_type ,f.care_info, f.fabric_design ,f.construction,f.color_info,
 (case when i.item_type_id =1 then f.fabric_width else null  end),
       f.fusing,f.thread_shade,f.material_info_for_wo,
       (select ws.code from b_item_wf_stage s ,b_workflow_stage ws where s.workflow_stage_id=ws.stage_id and s.order_item_id=i.order_item_id and s.current_stage_flag='Y' limit 1) as status
      ,f.supplier_product_code1,
f.material_composition,
f.marker_type,
f.marker_width ,
f.pattern_repeat_size,
f.nap_test,
f.pocket_color,
f.buttons,
f.buttons_supplier_product_code,
f.seam_tape,
(case when i.item_type_id =3 then null else f.lining end),
f.lining_supplier_product_code,
(case when i.item_type_id =3 then null else f.lining_width  end),
f.tonal_ucf,
f.canvas ,f.tonal_button_sku , 
(case when i.item_type_id =3 then null else f.tonal_lining_sku  end),
i.discount_comment
  FROM public.b_order_item i , m_fabric f, m_item_type it , m_finish_type ft , m_priority_type p
  where i.order_item_id=in_order_item_id 
 and i.sku_id=f.fabric_id 
 and i.item_type_id = it.item_type_id
 and i.finish_type = ft.finish_type_id
 and i.priority_id = p.priority_type_id
  
  ;
END;

$function$;

ALTER FUNCTION public."GetOrderItemDetails"(integer)
    OWNER TO tailorman_db;