CREATE TABLE public.b_rest_access
(
    b_rest_access_id serial NOT NULL,
    key character varying NOT NULL,
    email character varying NOT NULL,
    PRIMARY KEY (b_rest_access_id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.b_rest_access
    OWNER to tailorman_db;