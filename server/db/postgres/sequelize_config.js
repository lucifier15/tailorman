module.exports = {
	development: {
		username: process.env.PGUSER || 'tailorman_db',
		password: '64Y09b7G',
		database: 'tmPreProd',
		host: 'tailormandb.cup9fuxknyze.ap-south-1.rds.amazonaws.com',
		port: 5432,
		dialect: 'postgres'
	},
	test: {
		username: process.env.PGUSER || 'root',
		password: null,
		database: 'react_webpack_node_test',
		host: '127.0.0.1',
		dialect: 'postgres'
	},
	production: {
		username: process.env.PGUSER || 'tailorman_db',
		password: '64Y09b7G',
		database: 'tmProdNew',
		host: 'tailormandb.cup9fuxknyze.ap-south-1.rds.amazonaws.com',
		port: 5432,
		dialect: 'postgres'
	}
};