import Models from '../models';
const db = Models.sequelize;

export function getPriorities(req, res) {
	db.query('select * from "GetPriorityList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getPaymentTypes(req, res) {
	db.query('select * from "GetPaymenttypeList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getTailors(req, res) {
	db.query('select * from "GetTailorList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	})
}

export function getSalesmen(req, res) {
	db.query('select * from "GetSalesManList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getFinishTypes(req, res) {
	db.query('select * from "GetProductFinishTypeList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getStyles(req, res) {
	db.query('select * from "GetStyleList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getItemTypes(req, res) {
	db.query('select * from "GetItemTypeList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getMeasurementFields(req, res) {
	db.query('select * from "GetMeasurementTypeList"(:in_item_type_id, :in_measurement_source_id)', {
		raw: true,
		replacements: {
			in_item_type_id: req.query.item_type_id,
			in_measurement_source_id: req.query.measurement_type_id
		}
	}).then((result) => {
		return db.query('select * from "GetMeasurementMasterData"()').then((masterResult) => {
			return {
				fields: result[0],
				masterData: _.groupBy(masterResult[0], 'measurement_type_id')
			}
		});
	}).then((result) => {
		const fields = result.fields.map((field) => {
			if (field.category == 'Enumerated' && result.masterData[field.measurement_type_id]) {
				field.options = result.masterData[field.measurement_type_id];
			}
			return field;
		})
		res.send(fields);
	});
}

export function getFabricDesignFields(req, res) {
	db.query('select * from "GetFabricDesignParamsV3"(:in_item_type_id)', {
		raw: true,
		replacements: {
			in_item_type_id: req.query.item_type
		}
	}).then((result) => {
		if (result[0][0].GetFabricDesignParamsV3)
			res.send(result[0][0].GetFabricDesignParamsV3);
		else
			res.send([]);
	});
}

export function getMeasurementTypes(req, res) {
	db.query('select * from "GetMeasurementSourceList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getCustomerSourceList(req, res) {
	db.query('select * from "GetSourceList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getWorkflows(req, res) {
	db.query('select * from "GetWorkflowList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getOccasions(req, res) {
	db.query('select * from "GetOccationList"()', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getFabrics(req, res) {
	const item_type_id = req.query.item_type_id;
	db.query('select * from "GetFabricList"(:item_type_id)', {
		raw: true,
		replacements: {
			item_type_id: item_type_id
		}
	}).then((result) => {
		res.send(result[0]);
	});
}

export function searchFabrics(req, res) {
	const item_type_id = req.query.item_type_id;
	const product_sku = req.query.keyword;
	db.query('select * from "GetSKUDetails"(:in_item_type_id,:in_product_sku_code)', {
		raw: true,
		replacements: {
			in_item_type_id: item_type_id,
			in_product_sku_code: product_sku
		}
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getCustomerList(req, res) {
	const keyword = req.query.keyword || '';
	db.query('select * from "GetCustomerList"(:in_param)', {
		raw: true,
		replacements: {
			in_param: keyword
		}
	}).then((result) => {
		res.send(result[0]);
	})
}

export function getWorkFlowStageList(req, res) {
	const user_id = req.body.user_id;
	const roles = req.body.roles;
	const workflow_id = req.body.workflow_id;
	db.query('select * from "GetWorkFlowStageList"(' + user_id + ', ' + 'ARRAY[\'' + roles.join('\',\'') + '\'], ' + workflow_id + ')').then((result) => {
		res.send(result[0]);
	})
}
export function getWorkFlowList(req, res) {
	const keyword = req.query.keyword || '';
	db.query('select * from "GetWorkflowList"()').then((result) => {
		res.send(result[0]);
	});
}

export function getRTWList(req,res){
	const store_id = req.query.store_id;
	const product_sku = req.query.keyword;
	const size = req.query.size;
	const fit = req.query.fit;
	db.query('select * from "GetRTWSKUDetails"(:in_store_id,:in_product_sku_code,:in_size,:in_fit)', {
		raw: true,
		replacements: {
			in_store_id: store_id,
			in_product_sku_code: product_sku,
			in_size:size,
			in_fit:fit
		}
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getSizeList(req,res){
	db.query('select distinct size as name from m_category_rtw_inventory_measurment where size is not null').then((result) => {
		res.send(result[0]);
	});
}

export function getFitList(req,res){
	db.query('select distinct fit as name from m_category_rtw_inventory_measurment where fit is not null').then((result) => {
		res.send(result[0]);
	});
}



export function transferInventory(req,res){
	const formValues = req.body;
	const from_store_id = formValues.from_store_id;
	const to_store_id = formValues.to_store_id;
	const size = formValues.size;
	const fit = formValues.fit;
	const qty = formValues.qty;
	const description = formValues.description;
	const created_by = formValues.created_by;
	const fabric_id = formValues.fabric_id;
   const product_sku_code = formValues.product_sku_code;
   const item_type_id = formValues.item_type_id;


	db.query(`INSERT INTO rtw_inventory_transfer(from_store_id,to_store_id,size,fit,qty,description,fabric_id,created_by)values(${from_store_id},${to_store_id},'${size}','${fit}',${qty},'${description}',${created_by},${fabric_id})`).then((result) => {
	
		db.query(`Select distinct * from m_category_rtw_inventory_measurment where sku_code='${product_sku_code}' AND fit='${fit}' AND size='${size}' AND store_id=${to_store_id}`).then((results)=>{
			var UpdateOrInsertQuery;
			if(results[0].length>0){
				UpdateOrInsertQuery=`Update m_category_rtw_inventory_measurment set inventory_count=inventory_count+${qty} where sku_code='${product_sku_code}' AND fit='${fit}' AND size='${size}' AND  item_type_id='${item_type_id}' AND store_id=${to_store_id}`;
			}else{
				UpdateOrInsertQuery=`INSERT INTO m_category_rtw_inventory_measurment(item_type_id,sku_code,fit,size,store_id,inventory_count) values('${item_type_id}','${product_sku_code}','${fit}','${size}',${to_store_id},${qty})`;
			}
					 db.query(UpdateOrInsertQuery).then((incdata)=>{
					  db.query(`Update m_category_rtw_inventory_measurment set inventory_count=inventory_count-${qty} where sku_code='${product_sku_code}' AND fit='${fit}' AND size='${size}' AND store_id=${from_store_id} AND inventory_count>0 `).then((decdata)=>{  
								res.send({
					               status: true
							   });
					  });

				   });
		})
		
	}).catch((error) => {
		res.send({
			err: error
		});
	})
}


export default {
	getPriorities,
	getPaymentTypes,
	getTailors,
	getStyles,
	getItemTypes,
	getMeasurementFields,
	getMeasurementTypes,
	getFabricDesignFields,
	getWorkflows,
	getFabrics,
	getCustomerList,
	getCustomerSourceList,
	getOccasions,
	getFinishTypes,
	getSalesmen,
	searchFabrics,
	getWorkFlowStageList,
	getWorkFlowList,
	getRTWList,
	getSizeList,
	getFitList,
	transferInventory
}