import users from './users';
import lists from './lists';
import orders from './orders';
import dashboard from './dashboard';
import google_admin from './google-admin-actions';
export {
	users,
	lists,
	orders,
	dashboard,
	google_admin
};

export default {
	users,
	lists,
	orders,
	dashboard,
	google_admin
};