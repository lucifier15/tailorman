import passport from 'passport';
import Models from '../models';
//const User = Models.User;
const db = Models.sequelize;

/**
 * POST /login
 */
export function login(req, res, next) {
	// Do email and password validation for the server
	passport.authenticate('local', (authErr, user, info) => {
		if (authErr) return next(authErr);
		if (!user) {
			return res.status(401).json({
				message: info.message
			});
		}
		// Passport exposes a login() function on req (also aliased as
		// logIn()) that can be used to establish a login session
		return req.logIn(user, (loginErr) => {
			if (loginErr) return res.status(401).json({
				message: loginErr
			});
			return res.status(200).json({
				message: 'You have been successfully logged in.',
				user: {
					id: user.user_id
				}
			});
		});
	})(req, res, next);
}

/**
 * POST /logout
 */
export function logout(req, res) {
	// Do email and password validation for the server
	req.logout();
	res.send({});
	//res.redirect('/');
}


export function getStores(req, res) {
	const stores = req.body;
	db.query('select * from "GetStoreList"(' + 'ARRAY[\'' + stores.join('\',\'') + '\']' + ')', {
		raw: true
	}).then(function (result) {
		res.send(result[0]);
	});
}



export default {
	login,
	logout,
	getStores
};