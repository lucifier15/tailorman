import googleapis from 'googleapis';
import googleAuth from 'google-auth-library';
import {
	google_admin_prod,
	google_admin_local,
	google_admin_uat
} from '../../../config/secrets';
import Promise from 'bluebird';
import _ from 'lodash';
const DOMAIN = 'tailorman.com';
const MAX_RESULTS = 1;
const ORIGINAL_ADMIN_EMAIL = 'admin@tailorman.com';


const getUserList = (client, email, cb) => {
	const service = googleapis.admin('directory_v1');
	const _getUsers = Promise.promisify(service.users.list);
	return _getUsers({
		auth: client,
		domain: DOMAIN,
		maxResults: 10,
		query: 'email=' + email,
		projection: 'full',
		orderBy: 'email'
	}).then((response) => {
		var users = response.users;
		if (users && users.length == 0) {
			console.log('No users in the domain.');
		} else {
			return users;
		}
	}).catch((err) => {
		console.log('The API returned an error: ' + err);
	});
};

const authorize = (credentials, cb) => {
	const auth = new googleAuth();
	const oauth2Client = new auth.OAuth2();
	const jwt = new googleapis.auth.JWT(
		credentials.client_email,
		null,
		credentials.private_key, [
			'https://www.googleapis.com/auth/admin.directory.user',
			'https://www.googleapis.com/auth/admin.directory.group'
		],
		ORIGINAL_ADMIN_EMAIL
	);
	return Promise.promisify(jwt.authorize, {
		context: jwt
	})().then((result) => {
		oauth2Client.setCredentials({
			access_token: result.access_token
		});
		return oauth2Client;
	});
};

const getEnvAccess = (env, currEnv) => {
	if (!env && currEnv == 'UAT')
		return true;
	if (!currEnv)
		return true;
	else if (currEnv == 'PRODUCTION')
		currEnv = 'PROD';

	if (env.Code && env.Code.length > 0) {
		const accessEnvs = env.Code.map((access) => {
			return access.value.toUpperCase();
		});
		if (accessEnvs.indexOf(currEnv) > -1 || accessEnvs.indexOf('ALL') > -1)
			return true;
	}
	return false;
}

export function getRole(email) {
	let credentials;
	if (process.env.TAILORMAN_ENV == 'PRODUCTION')
		credentials = google_admin_prod;
	else if (process.env.TAILORMAN_ENV == 'UAT')
		credentials = google_admin_uat;
	else if (process.env.TAILORMAN_ENV == 'DEV')
		return;
	else
		credentials = google_admin_local;

	return authorize(credentials).then((client) => {
		return getUserList(client, email);
	}).then((users) => {
		if (users && users.length == 1) {
			if (users[0].customSchemas && users[0].customSchemas.Module_Role && (users[0].customSchemas.Module_Role.Workflow || users[0].customSchemas.Module_Role.Store_Ops)) {
				const hasAccess = getEnvAccess(users[0].customSchemas.Env, process.env.TAILORMAN_ENV);
				//console.log("Envs", JSON.stringify(users[0].customSchemas.Env), "Has access? ", hasAccess);
				if (!hasAccess)
					return;
				return {
					roles: {
						store: _.map(users[0].customSchemas.Module_Role.Store_Ops, 'value'),
						workflow: _.map(users[0].customSchemas.Module_Role.Workflow, 'value')
					},
					stores: (users[0].customSchemas.Store) ? _.map(users[0].customSchemas.Store.Code, 'value') : []
				}
			} else
				return;
		} else {
			console.log("No user found!");
			return;
		}
	})
};

export default {
	getRole
}