import Models from '../models';
const db = Models.sequelize;
const Sequelize = Models.Sequelize;
import fs from 'fs';
import Promise from 'bluebird';
const readFile = Promise.promisify(fs.readFile);
import phantom from 'phantom';
import nodemailer from 'nodemailer';
import moment from 'moment';

import request from 'request';
import AWS from 'aws-sdk';
import uuid from 'uuid/v4';
import xml from 'xml';

import _ from 'lodash';

import { AWSS3SecretKey } from '../../../config/secrets';

import {
	Mandrill
} from 'mandrill-api';
export function checkCustomer(req, res) {
	const _customer = req.body;
	db.query('select * from m_customer where email=:in_email OR mobile =:in_mobile', {
		raw: true,
		replacements: {
			in_mobile: _customer.mobile,
			in_email: _customer.email
		}
	}).then((result) => {
		if (result.length > 0 && result[0].length > 0) {
			res.status(500).json({
				success: false,
				message: "Email or mobile number already exists"
			});
		} else {
			saveCustomer(req, res);
		}
	});
}
export function saveCustomer(req, res) {
	const _customer = req.body;

	db.query('select * from "SaveCustomer"(:in_name, :in_gender, :in_dob, :in_mobile, :in_address, :in_email, :in_height, :in_weight, :in_source_id, :in_comment)', {
		raw: true,
		replacements: {
			in_name: _customer.name,
			in_gender: (_customer.gender) ? _customer.gender[0].toUpperCase() : 'M',
			in_dob: _customer.dob || null,
			in_mobile: _customer.mobile,
			in_email: _customer.email,
			in_height: _customer.height ? parseFloat(_customer.height) : 0,
			in_weight: _customer.weight ? parseFloat(_customer.weight) : 0,
			in_source_id: _customer.source_id || 1,
			in_address: _customer.address || '',
			in_comment: _customer.comment || ''
		}
	}).then((result) => {
		res.json({
			success: true,
			customer_id: result[0][0].SaveCustomer
		});

	})
		.catch(err => {
			res.status(500).json({
				success: false,
				err: err
			});
		});
}
export function updateCustomer(req, res) {
	const customer = req.body;
	db.query('select * from "UpdateCustomer"(:in_customer_id, :in_name, :in_gender, :in_dob, :in_mobile, :in_address, :in_email, :in_height, :in_weight, :in_source_id, :in_comment)', {
		raw: true,
		replacements: {
			in_customer_id: customer.customer_id || customer.id,
			in_name: customer.name,
			in_gender: (customer.gender) ? customer.gender[0].toUpperCase() : 'M',
			in_dob: customer.dob || null,
			in_mobile: customer.mobile || customer.phone,
			in_address: customer.address || '',
			in_email: customer.email,
			in_height: customer.height ? parseFloat(customer.height) : 0,
			in_weight: customer.weight ? parseFloat(customer.weight) : 0,
			in_source_id: customer.source_id || 1,
			in_comment: customer.comment || ''
		}
	}).then((result) => {
		res.json({
			status: 'OK'
		});
	});
}

export function saveDetails(req, res) {
	const _details = req.body;
	db.query('select * from "SaveOrder"(:in_tailor_id, :in_customer_id, :in_order_type_id, :in_store_id, :in_user_id, :in_order_date, :in_occasion, :in_occation_date, :in_benificiary_name, :in_benificiary_mobile, :in_benificiary_email, :in_priority_id, :in_payment_type_id, :in_Payment_details,:in_total_amount, :in_comment, :in_display_name, :in_billing_address, :in_delivery_address, :in_pin_number, :in_sales_man_id)', {
		raw: true,
		replacements: {
			in_tailor_id: _details.tailor_id,
			in_customer_id: _details.customer_id,
			in_order_type_id: _details.order_type_id || 1,
			in_store_id: _details.store_id,
			in_occasion: _details.occasion,
			in_occation_date: _details.occasion_date || null,
			in_benificiary_email: _details.benificiary_email,
			in_benificiary_mobile: _details.benificiary_mobile || _details.benificiary_phone,
			in_benificiary_name: _details.benificiary_name,
			in_priority_id: null,
			in_payment_type_id: _details.payment_type_id,
			in_Payment_details: _details.payment_details,
			in_user_id: _details.user_id,
			in_order_date: _details.order_date || null,
			in_total_amount: _details.total_amount,
			in_comment: _details.comment,
			in_display_name: _details.display_name,
			in_billing_address: _details.billing_address,
			in_delivery_address: _details.delivery_address,
			in_pin_number: _details.PIN || '',
			in_sales_man_id: _details.salesman_id
		}
	}).then((result) => {
		res.json({
			order_id: result[0][0].SaveOrder
		});
	});
}

export function updateDetails(req, res) {
	const _details = req.body;
	db.query('select * from "UpdateOrderV2"(:in_order_id, :in_tailor_id, :in_customer_id, :in_order_type_id, :in_store_id, :in_user_id, :in_order_date, :in_occasion, :in_occation_date, :in_benificiary_name, :in_benificiary_mobile, :in_benificiary_email, :in_priority_id, :in_payment_type_id, :in_Payment_details,:in_total_amount, :in_comment, :in_display_name, :in_full_payment_flag, :in_billing_address, :in_delivery_address, :in_pin_number, :in_sales_man_id,:in_pindate)', {
		raw: true,
		replacements: {
			in_order_id: _details.order_id,
			in_tailor_id: _details.tailor_id,
			in_customer_id: _details.customer_id,
			in_order_type_id: _details.order_type_id || 1,
			in_store_id: _details.store_id,
			in_occasion: _details.occasion,
			in_occation_date: _details.occasion_date || null,
			in_benificiary_email: _details.benificiary_email,
			in_benificiary_mobile: _details.benificiary_mobile || _details.benificiary_phone || null,
			in_benificiary_name: _details.benificiary_name,
			in_priority_id: null,
			in_payment_type_id: _details.payment_type_id,
			in_Payment_details: _details.payment_details,
			in_user_id: _details.user_id,
			in_order_date: _details.order_date || null,
			in_total_amount: _details.total_amount,
			in_comment: _details.comment || '',
			in_display_name: _details.display_name,
			in_full_payment_flag: _details.is_full_payment ? 'Y' : 'N',
			in_billing_address: _details.billing_address,
			in_delivery_address: _details.delivery_address,
			in_pin_number: _details.PIN || '0',
			in_sales_man_id: _details.salesman_id,
			in_pindate: _details.pindate || null
		}
	}).then((result) => {
		res.json({
			order_id: result[0][0].SaveOrder
		});
	});
}
export function saveProfile(req, res) {
	const profile = req.body;
	db.query('select * from "SaveMeasurementProfile"(:in_customer_id, :in_item_type_id, :in_measurement_source_id, :in_profile_name, :in_comment)', {
		raw: true,
		replacements: {
			in_customer_id: profile.customer_id,
			in_item_type_id: profile.item_type_id,
			in_measurement_source_id: profile.measurement_source_id,
			in_profile_name: profile.name,
			in_comment: profile.comment
		}
	}).then((result) => {
		res.json({
			profile_id: result[0][0].SaveMeasurementProfile
		})
	});
}
export function updateProfile(req, res) {
	const profile = req.body;
	db.query('select * from "UpdateMeasurementProfile"(:in_profile_id, :in_profile_name, :in_comment)', {
		raw: true,
		replacements: {
			in_profile_id: profile.profile_id,
			in_profile_name: profile.profile_name || profile.name,
			in_comment: profile.comment
		}
	}).then((result) => {
		res.json({
			status: 'OK'
		});
	})
}

export function saveBulkMeasurement(req, res) {
	let done;

	const reqBody = req.body;

	const profileId = reqBody.profile_id;
	const measurements = reqBody.measurements;

	//After done with all save measurement
	//calls, it will be sending the response
	done = _.after(measurements.length, () => {
		return res.json({
			status: true
		});
	});

	measurements.map((item, index) => {
		db.query('select * from "SaveMeasurementProfileValues"(:in_profile_id, :in_measurement_type_id, :in_value,:in_descr)', {
			raw: true,
			replacements: {
				in_profile_id: profileId,
				in_measurement_type_id: item.id,
				in_value: item.value || '0.00',
				in_descr: item.descr || null
			}
		}).then((result) => {
			done();
		}).catch(err => {
			console.log('Error in update measurement profile: ', err);
			done();
		});
	});
}

export function saveMeasurement(req, res) {
	const measurement = req.body;

	db.query('select * from "SaveMeasurementProfileValues"(:in_profile_id, :in_measurement_type_id, :in_value)', {
		raw: true,
		replacements: {
			in_profile_id: measurement.profile_id,
			in_measurement_type_id: measurement.type_id,
			in_value: measurement.value || '0.00',
			in_descr: measurement.descr || null
		}
	}).then((result) => {
		res.json({
			status: result[0][0].SaveMeasurementProfileValues
		})
	});
}

export function saveOrderItem(req, res) {
	const _sale = req.body;
	db.query('select * From "SaveOrderItem"(:in_order_id, :in_style_id, :in_workflow_id, :in_sku_id, :in_item_type_id, :in_mrp, :in_qty, :in_finish_type, :in_fit_on_date, :in_delivery_date, :in_comment, :in_profile_id, :in_priority_id, :in_display_name)', {
		raw: true,
		replacements: {
			in_order_id: _sale.order_id,
			in_workflow_id: _sale.workflow_id || 1,
			in_sku_id: _sale.style,
			in_item_type_id: _sale.item_type_id,
			in_mrp: _sale.mrp,
			in_qty: parseInt(_sale.qty),
			in_finish_type: _sale.finish_type_id || 0,
			in_fit_on_date: _sale.fiton_date || null,
			in_delivery_date: _sale.delivery_date || null,
			in_style_id: _sale.style || 0,
			in_comment: _sale.comment || '',
			in_profile_id: _sale.profile_id,
			in_priority_id: _sale.priority_id || 0,
			in_display_name: _sale.display_name
		}
	}).then((result) => {
		res.json({
			order_item_id: result[0][0].SaveOrderItem
		});
	});
}

export function saveAlteredOrderItem(req, res) {
	const _sale = req.body;
	db.query('select * from "SaveOrderItemAlteration"( :in_order_id, :in_order_item_id , :in_workflow_id , :in_sku_id, :in_item_type_id, :in_finish_type, :in_fit_on_date, :in_delivery_date, :in_comment, :in_profile_id, :in_display_name)', {
		raw: true,
		replacements: {
			in_order_id: _sale.order_id,
			in_order_item_id: _sale.order_item_id,
			in_workflow_id: _sale.workflow_id || 2,
			in_sku_id: _sale.style || 0,
			in_item_type_id: _sale.item_type_id,
			in_finish_type: _sale.finish_type_id || 0,
			in_fit_on_date: _sale.fiton_date || null,
			in_delivery_date: _sale.delivery_date || null,
			in_comment: _sale.comment || '',
			in_profile_id: _sale.profile_id,
			in_display_name: _sale.display_name || ''
		}
	}).then((result) => {
		res.send(result[0]);
	})
}

export function updateOrderItem(req, res) {
	const sale = req.body;
	/* ======== calculate bill amount =======*/

	//assume there is no delivery upcharge
	let has_delivery_upcharge = false;

	//Keep adding upcharge to total_upcharge, exclude delivery upcharge if available.
	const total_upcharge = sale.upcharges ? sale.upcharges.reduce((prev, next) => {
		//Check for delivery upcharge
		if (next.type == 'High Priority Delivery') {
			//if available assign the upcharge to has_delivery_upcharge
			has_delivery_upcharge = next;
			return prev;
		}

		//if upcharge.unit = 2, then this is percentage type upcharge, use upcharge.value as percentage value.
		if (next.unit === 2) {
			//The total upcharge needs to be rounded down so if upcharge is 614.6, it becomes 614.
			return prev + Math.floor((sale.mrp * next.value / 100));
		} else {
			return prev + next.value;
		}
	}, 0) : 0;


	//assume bill amount is the MRP, we will add upcharge and discount in the next step
	let bill_amount = sale.mrp + total_upcharge;

	//if the order item has a discount_value column
	if (sale.discount_value) {
		//if discount_type = 2, then this is percentage type discount, use the discount_value column as a percentage value
		if (sale.discount_type === 2) {
			//The total discount has to be floored (rounded down), so if the discount is 550.9 INR it becomes 550 INR.
			bill_amount = sale.mrp + total_upcharge - Math.floor((sale.mrp + total_upcharge) * sale.discount_value / 100);
			//else discount_value is a normal amount and simply add to the upcharged mrp.
		} else {
			bill_amount = sale.mrp + total_upcharge - sale.discount_value;
		}
	}

	const discount_amount = ((sale.mrp + total_upcharge) - bill_amount) * sale.qty;

	//total bill amount will be the upcharged and discounted mrp times the order item quantity
	bill_amount = bill_amount * sale.qty;

	let delivery_upcharge = 0;
	//Finally add delivery upcharge if available.
	if (has_delivery_upcharge) {
		bill_amount += has_delivery_upcharge.value;
		delivery_upcharge = has_delivery_upcharge.value;
	}
	/* EO Calculate bill amount */
	db.query('select * from "UpdateOrderItem"(:in_order_id, :in_order_item_id, :in_style_id, :in_workflow_id, :in_sku_id, :in_item_type_id, :in_mrp, :in_qty, :in_finish_type, :in_fit_on_date, :in_delivery_date, :in_comment, :in_profile_id, :in_priority_id, :in_display_name, :in_upcharge, :in_taxes, :in_discount_type, :in_discount_value, :in_order_flag, :in_bill_amount, :in_upcharge_amount, :in_delivery_upcharge_amount, :in_discount_amount,:in_discount_comment)', {
		raw: true,
		replacements: {
			in_order_id: sale.order_id,
			in_order_item_id: sale.order_item_id,
			in_style_id: sale.style,
			in_workflow_id: 1,
			in_sku_id: sale.style,
			in_item_type_id: sale.item_type_id,
			in_mrp: sale.mrp,
			in_qty: parseInt(sale.qty),
			in_finish_type: sale.finish_type_id || 0,
			in_fit_on_date: sale.fiton_date || null,
			in_delivery_date: sale.delivery_date || null,
			in_comment: sale.comment,
			in_profile_id: sale.profile_id,
			in_priority_id: sale.priority_id,
			in_display_name: sale.display_name,
			in_upcharge: JSON.stringify(sale.upcharges) || '[]',
			in_taxes: JSON.stringify(sale.taxes) || '[]',
			in_discount_type: sale.discount_type || 0,
			in_discount_value: sale.discount_value ? parseFloat(sale.discount_value) : 0.00,
			in_order_flag: sale.order_flag || 1,
			in_bill_amount: bill_amount,
			in_upcharge_amount: total_upcharge,
			in_delivery_upcharge_amount: delivery_upcharge,
			in_discount_amount: discount_amount,
			in_discount_comment: sale.discount_comment || ""
		}
	}).then((result) => {
		res.json({
			status: result[0]
		})
	})
}
//To get the signed AWS URL
export function getSignedUrl(req, res) {
	if (!req.body.file) {
		res.json({
			success: false,
			message: 'No file found!'
		});
		return;
	}
	//Input parameters
	var reqBody = req.body;

	var file = reqBody.file;

	var fileType = file.type;
	var fileSize = file.size;
	var fileName = file.name;
	var orderId = file.order_id;

	var imagePath;

	if (fileType.match('image.*')) {
		//correct format
	} else {
		res.json({
			success: false,
			message: 'Uploaded format is incorrect!'
		});
		return;
	}

	//image path
	imagePath = 'upload/' + orderId + '/' + uuid() + fileName;

	AWS.config.region = AWSS3SecretKey.region;
	AWS.config.accessKeyId = AWSS3SecretKey.accessKeyID;
	AWS.config.secretAccessKey = AWSS3SecretKey.secretAccessKey;

	var bucketName = AWSS3SecretKey.bucketName;
	var s3bucket = new AWS.S3();

	var s3_params = {
		Bucket: bucketName,
		Key: imagePath,
		Expires: 60, // expire after 60 mins
		ContentType: fileType,
		ACL: 'public-read',
	};

	s3bucket.getSignedUrl('putObject', s3_params, function (err, data) {
		if (err) {
			console.log(err);
			res.json({
				success: false,
				error: err,
				message: 'Failed to get the signed url!'
			});
			return;
		} else {
			console.log('data: data: ', data);
			console.log('bucketName: bucketName: ', bucketName);
			console.log('fileName: fileName: ', imagePath);

			var return_data = {
				requestUrl: data,
				imageUrl: 'https://s3.ap-south-1.amazonaws.com/' + bucketName + '/' + imagePath
			};
			res.json({
				success: true,
				data: return_data,
				message: 'Uploaded completed!'
			});
		}
	});
}

export function saveImage(req, res) {
	const order_id = parseInt(req.body.order_id);
	const dataURL = req.body.dataURL;
	const type = parseInt(req.body.type);
	db.query('select * from "SaveOrderImage"(:in_order_id, :in_image_id, :in_image)', {
		raw: true,
		replacements: {
			in_order_id: order_id,
			in_image_id: type || parseInt(Math.random() * (10000 - 1) + 1),
			in_image: dataURL
		}
	}).then((result) => {
		res.json({
			status: 'OK'
		});
	});
}

export function getImages(req, res) {
	const order_id = parseInt(req.query.order_id);
	db.query('select * from "GetImageList"(:in_order_id)', {
		raw: true,
		replacements: {
			in_order_id: order_id
		}
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getImagesOfType(req, res) {
	const order_id = parseInt(req.query.order_id);
	const image_id = parseInt(req.query.image_id);
	db.query('select * from "GetImageList"(:in_order_id) where "image_id" = :in_image_id', {
		raw: true,
		replacements: {
			in_order_id: order_id,
			in_image_id: image_id
		}
	}).then((result) => {
		res.send(result[0]);
	})
}



export function saveFabricDesign(req, res) {
	const item_type_id = req.query.order_item_id;
	db.query('select * from "SaveFabricDesign"(:in_order_item_id, :in_fabric_design, :in_comment)', {
		raw: true,
		replacements: {
			in_order_item_id: item_type_id,
			in_fabric_design: JSON.stringify(req.body.fabric_design),
			in_comment: req.body.comment
		}
	}).then((result) => {
		res.json({
			status: result[0][0].SaveFabricDesign
		});
	})
}

export function getCustomerOrders(req, res) {
	const customer_id = req.query.customer_id;
	db.query('select * from "GetCustomerOrderList"(:in_customer_id)', {
		raw: true,
		replacements: {
			in_customer_id: customer_id
		}
	}).then((result) => {
		res.send(result[0]);
	});
}

export function getOrderItems(req, res) {
	const order_id = req.query.order_id;
	db.query('select * from "GetOrderItemListV2"(:in_order_id)', {
		raw: true,
		replacements: {
			in_order_id: order_id
		}
	}).then((result) => {
		res.send(result[0]);
	})
}

export function getOrderItemFabricDesign(req, res) {
	const order_item_id = req.query.order_item_id;
	db.query('select * from "GetOrderItemFabricDesign"(:in_order_item_id)', {
		raw: true,
		replacements: {
			in_order_item_id: order_item_id
		}
	}).then((result) => {
		res.send(result[0]);
	})
}

export function getProfiles(req, res) {
	const customer_id = req.query.customer_id;
	const item_type_id = req.query.item_type_id;
	db.query('select * from "GetMeasurementProfileList"(:in_customer_id, :in_item_type_id)', {
		raw: true,
		replacements: {
			in_customer_id: customer_id,
			in_item_type_id: item_type_id
		}
	}).then((result) => {
		res.send(result[0]);
	})
}


export function getMeasurementProfileValues(req, res) {
	const profile_id = req.query.profile_id;
	const is_print = req.query.is_print;
	let query = 'select * from "GetMeasurementProfileValuesV2"(:in_profile_id)';
	if (is_print)
		query = 'select * from "GetMeasurementProfileValuesV2"(:in_profile_id, :in_wo_order)';
	db.query(query, {
		raw: true,
		replacements: {
			in_profile_id: profile_id,
			in_wo_order: 1
		}
	}).then((result) => {
		res.send(result[0]);
	});
}

export function deleteOrderItem(req, res) {
	const order_item_id = req.query.order_item_id;
	db.query('select * from "DeleteOrderItem"(:in_order_item_id)', {
		raw: true,
		replacements: {
			in_order_item_id: order_item_id
		}
	}).then((result) => {
		res.send(result[0]);
	})
}

export function getTaxes(req, res) {
	const total = req.query.total;
	// const has_discount = req.query.has_discount == 'true' ? true : false;
	// const store_id = req.query.store_id;
	db.query('select * from "GetTaxes"(true)').then((result) => {
		let taxesArray = result[0][0].GetTaxes;
		if (parseFloat(total) <= 1000) {
			taxesArray = taxesArray.map((item) => {
				item.percent = 2.5;
				return item;
			});
		}
		res.send(taxesArray);
	});
}

export function getWorkOrders(req, res) {
	const stage_ids = req.body.stage_ids;
	const from_date = req.body.from_date;
	const to_date = req.body.to_date;
	const customer = req.body.customer;
	const customer_id = req.body.customer_id;
	const order_id = req.body.order_id;

	db.query('select * from "GetWorkOrderList"(' + 'ARRAY[' + stage_ids.join(',') + ']' + ', :in_start_date, :in_end_date, :in_customer_param,:in_order_id,:in_customer_id)', {
		raw: true,
		replacements: {
			in_customer_param: customer || '',
			in_start_date: from_date,
			in_end_date: to_date,
			in_order_id: order_id || null,
			in_customer_id: customer_id || null
		}
	}).then((result) => {
		res.send(result[0]);
	})
}

export function getWorkOrderStageList(req, res) {
	const order_item_id = req.body.order_item_id;
	const user_id = req.body.user_id;
	const roles = req.body.roles;
	const workflow_id = req.body.workflow_id;
	db.query('select * from "GetWorkFlowStageList"(' + user_id + ', ' + order_item_id + ' , ' + 'ARRAY[\'' + roles.join('\',\'') + '\'], ' + workflow_id + ')', {
		raw: true
	}).then((result) => {
		res.send(result[0]);
	});
}

export function saveWorkFlowStage(req, res) {
	const workflow_id = req.body.workflow_id;
	const workflow_stage_id = req.body.workflow_stage_id;
	const order_item_id = req.body.order_item_id;
	const profile_id = req.body.profile_id;
	const comment = req.body.comment;
	const user_id = req.body.user_id;
	db.query('select * from "SaveWorkFlowStage"(:in_workflow_id, :in_workflow_stage_id, :in_order_item_id, :in_profile_id, :in_comment, :in_user_id)', {
		raw: true,
		replacements: {
			in_workflow_id: workflow_id,
			in_workflow_stage_id: workflow_stage_id,
			in_order_item_id: order_item_id,
			in_profile_id: profile_id,
			in_comment: comment,
			in_user_id: user_id
		}
	}).then((result) => {
		res.send({
			status: true
		});
	}).catch((error) => {
		console.log(error);
		res.send({
			err: error
		});
	})
}

export function getOrderDetails(req, res) {
	const order_id = req.query.order_id;
	db.query('select * from "GetOrderDetails"(:in_order_id)', {
		raw: true,
		replacements: {
			in_order_id: order_id
		}
	}).then((result) => {
		res.send(result[0][0]);
	})
}

export function getCustomerDetails(req, res) {
	const customer_id = req.query.customer_id;
	db.query('select * from "GetCustomer"(:in_customer_id)', {
		raw: true,
		replacements: {
			in_customer_id: customer_id
		}
	}).then((result) => {
		res.send(result[0][0]);
	})
}

export function getOrderItemDetails(req, res) {
	const order_item_id = req.query.order_item_id;
	db.query('select * from "GetOrderItemDetails"(:in_order_item_id)', {
		raw: true,
		replacements: {
			in_order_item_id: order_item_id
		}
	}).then((result) => {
		res.send(result[0][0]);
	})
}

export function getOrderItemList(req, res) {
	const order_id = req.query.order_id;
	db.query('select * from "GetOrderItemListV2"(:in_order_id)', {
		raw: true,
		replacements: {
			in_order_id: order_id
		}
	}).then((result) => {
		res.send(result[0]);
	});
}

function sendEmail(req, content, customer, attachment, order_id) {
	var mandrill_client = new Mandrill('P8PB1qLj9O75t1TMPZ3VpA');
	const ENV = process.env.TAILORMAN_ENV;
	let to_email = [{
		email: (req.user) ? req.user.email : 'data@tailorman.com',
		name: customer.name,
		type: 'to'
	}];

	// COMMENTING FOR THE FUTURE, WHEN WE CAN ACTUALLY SEND THE EMAILS TO CUSTOMERS
	// if (ENV == 'PRODUCTION' && customer && customer.email) {
	// 	to_email[0].type = 'bcc';
	// 	to_email.push({
	// 		email: customer.email,
	// 		name: customer.name,
	// 		type: 'to'
	// 	})
	// }

	if (ENV == 'PRODUCTION') {
		to_email[0].type = 'bcc';
		to_email.push({
			email: 'data@tailorman.com',
			name: 'PPD',
			type: 'to'
		})
	}

	console.log(to_email);
	const message = {
		html: content,
		text: '',
		subject: 'Tailorman Invoice Order No: ' + order_id,
		from_email: 'care@tailorman.com',
		from_name: 'Tailorman Invoice',
		to: to_email,
		attachments: attachment
	}
	mandrill_client.messages.send({
		"message": message,
		"async": false,
		"ip_pool": "Main Pool",
		"send_at": moment().subtract(2, 'days').format('YYYY-MM-DD HH:mm:ss')
	}, function (result) {
		console.log("Message sent: " + JSON.stringify(result));
	}, function (error) {
		console.log("Error:", error);
	});

}

export function testPhantom(req, res) {
	const customer_id = req.query.customer_id;
	const order_id = req.query.order_id;
	var _ph, _page, _outObj;
	const FILETYPE = '.pdf';
	db.query('select * from "GetCustomer"(:in_customer_id)', {
		raw: true,
		replacements: {
			in_customer_id: customer_id
		}
	}).then((result) => {
		const customer = result[0][0];
		return db.query('select * from "GetCustomerOrderList"(:in_customer_id)', {
			raw: true,
			replacements: {
				in_customer_id: customer_id
			}
		}).then((result) => {
			const order = _.find(result[0], {
				order_id: parseInt(order_id)
			});
			if (order)
				return customer;
			else
				return false;
		});
	}).then((result) => {
		const customer = result;
		if (result) {
			phantom.create().then(ph => {
				_ph = ph;
				return _ph.createPage();
			}).then(page => {
				_page = page;
				let url = '';
				switch (process.env.TAILORMAN_ENV) {
					case 'PRODUCTION':
						url = 'https://store.tailorman.com';
						break;
					case 'UAT':
						url = 'https://uat-store.tailorman.com';
						break;
					default:
						url = 'http://localhost:4000';
				}
				url += '/invoice/customer/' + customer_id + '/order/' + order_id;
				return _page.open(url);
			}).then(status => {
				setTimeout(() => {
					const filename = "./invoices/" + customer.name.split(" ").join("") + "_" + order_id + "_Invoice" + FILETYPE;
					_page.render(filename, {
						quality: 100
					}).then(() => {
						let content = "<p>Dear " + customer.name + ",</p>";
						content += "<p>Greetings from Tailorman!</p>";
						content += "<p>Your invoice copy is attached with this mail.</p>";
						content += "<p>Please feel free to contact us on 1800 3000 1575 or write us on <a href='mailto:care@tailorman.com'>care@tailorman.com</a> if you have any other queries. </p>";
						content += "<p>Kind regards, <br/> Team Tailorman</p>";
						const file = fs.readFileSync(filename).toString('base64');
						sendEmail(req, content, customer, [{
							name: customer.name.split(" ").join("") + " Invoice" + FILETYPE,
							content: file,
							type: 'application/pdf'
						}], order_id);
						_page.close();
						_ph.exit();
						res.send('ok');
					})
				}, 15000);
			}).catch(e => console.log(e));
		}
	});
}


export function getPriorityUpcharge(req, res) {
	const item_type_id = req.query.item_type;
	const priority_id = req.query.priority_id;
	db.query('select * from "GetDeliveryUpcharge"(:in_item_type_id)', {
		raw: true,
		replacements: {
			in_item_type_id: item_type_id
		}
	}).then((result) => {
		res.send(result[0]);
	});
}


export function getWorkOrderMeasurementProfile(req, res) {
	const order_item_id = req.query.order_item_id;
	const type = req.query.type;
	let query;
	if (type == 'pattern') {
		query = 'select * from "GetPatternProfile"(:in_order_item_id)';
	} else if (type == 'FGQC') {
		query = 'select * from "GetFGQCProfile"(:in_order_item_id)';
	} else {
		res.send([]);
	}
	db.query(query, {
		raw: true,
		replacements: {
			in_order_item_id: order_item_id
		}
	}).then((result) => {
		res.send(result[0][0])
	});
}

export function getOrderHistory(req, res) {
	db.query('select * from "GetOrderItemHistory"()', {
		raw: true
	}).then((result) => {
		//res.send(result[0]);
		result[0].forEach(updateOrderHistory);
	})
}

export function updateOrderHistory(sale) {
	/* ======== calculate bill amount =======*/
	if (sale.upcharge && _.isArray(sale.upcharge))
		sale.upcharges = sale.upcharge;
	//assume there is no delivery upcharge
	let has_delivery_upcharge = false;
	let total_upcharge = 0;
	//Keep adding upcharge to total_upcharge, exclude delivery upcharge if available.
	total_upcharge = sale.upcharges ? sale.upcharges.reduce((prev, next) => {
		//Check for delivery upcharge
		if (next.type == 'High Priority Delivery') {
			//if available assign the upcharge to has_delivery_upcharge
			has_delivery_upcharge = next;
			return prev;
		}

		//if upcharge.unit = 2, then this is percentage type upcharge, use upcharge.value as percentage value.
		if (next.unit === 2) {
			//The total upcharge needs to be rounded down so if upcharge is 614.6, it becomes 614.
			return prev + Math.floor((sale.mrp * next.value / 100));
		} else {
			return prev + next.value;
		}
	}, 0) : 0;


	//assume bill amount is the MRP, we will add upcharge and discount in the next step
	let bill_amount = sale.mrp + total_upcharge;

	//if the order item has a discount_value column
	if (sale.discount && sale.discount_type) {
		//if discount_type = 2, then this is percentage type discount, use the discount_value column as a percentage value
		if (sale.discount_type === 2) {
			//The total discount has to be floored (rounded down), so if the discount is 550.9 INR it becomes 550 INR.
			bill_amount = sale.mrp + total_upcharge - Math.floor((sale.mrp + total_upcharge) * sale.discount / 100);
			//else discount_value is a normal amount and simply add to the upcharged mrp.
		} else {
			bill_amount = sale.mrp + total_upcharge - sale.discount;
		}
	}


	const discount_amount = ((sale.mrp + total_upcharge) - bill_amount) * sale.qty;

	//total bill amount will be the upcharged and discounted mrp times the order item quantity
	bill_amount = bill_amount * sale.qty;

	let delivery_upcharge = 0;
	//Finally add delivery upcharge if available.
	if (has_delivery_upcharge) {
		bill_amount += has_delivery_upcharge.value;
		delivery_upcharge = has_delivery_upcharge.value;
	}

	/* EO Calculate bill amount */
	db.query('select * from "UpdateOrderItemHistory"(:in_order_item_id, :in_upcharge_amount, :in_delivery_upcharge_amount, :in_discount_amount, :in_bill_amount)', {
		raw: true,
		replacements: {
			in_order_item_id: sale.order_item_id,
			in_upcharge_amount: total_upcharge,
			in_delivery_upcharge_amount: delivery_upcharge,
			in_discount_amount: discount_amount,
			in_bill_amount: bill_amount
		}
	}).then((result) => {
		console.log(result[0]);
		console.log("updated", sale.order_item_id);
	});
}

export function getLatestItemTypeComment(req, res) {
	let params = req.query;

	let itemTypeId = params.item_type_id;
	let customerId = params.customer_id;

	let query = `select boi.comment from b_order_item boi LEFT JOIN b_order b ON boi.order_id=b.order_id where boi.item_type_id=${itemTypeId} and b.customer_id=${customerId} order by order_item_id DESC LIMIT 1`;

	db.query(query)
		.then((result) => {
			res.send({
				success: true,
				data: result && result[0] && result[0][0]
			});
		})
		.catch((err) => {
			res.status(500).json({
				success: false,
				error: err
			});
		});
}
function getJsonParsedResponse(result) {
	var formatedResponse = [];
	result.map((item, index) => {
		formatedResponse.push({
			"Receipt_number": item.rcpt_num,
			"Receipt_Date": item.rcpt_dt,
			"Bussiness_Date": item.business_dt,
			"Transaction_Time": item.rcpt_tm,
			"Invoice_amount": item.inv_amt,
			"Discount_amount": item.discount_amount,
			"VAT_Amount": item.tax_amt,
			"Service_Tax_Amount": item.service_tax,
			"ServiceChargeAmount": item.service_charge,
			"Net_sale": item.mrp,
			"Payment_Mode": item.payment_mode,
			"Transaction_status": item.transaction_status
		});
	})
	return formatedResponse;
}
function getXMLParsedResponse(result) {
	var formatedResponse = [];
	var records = [];
	result.map((item, index) => {
		var record = [];

		record = [{ "Receipt_number": item.rcpt_num },
		{ "Receipt_Date": item.rcpt_dt },
		{ "Bussiness_Date": item.business_dt },
		{ "Transaction_Time": item.rcpt_tm },
		{ "Invoice_amount": item.inv_amt },
		{ "Discount_amount": item.discount_amount },
		{ "VAT_Amount": item.tax_amt },
		{ "Service_Tax_Amount": item.service_tax },
		{ "ServiceChargeAmount": item.service_charge },
		{ "Net_sale": item.mrp },
		{ "Payment_Mode": item.payment_mode },
		{ "Transaction_status": item.transaction_status }];
		records.push({
			record
		});
	})
	formatedResponse.push({
		records
	})
	return formatedResponse;
}
export function storetransactions(req, res) {
	const contype = req.headers['content-type'];
	const params = req.query;
	let dateString = `./transactionlog/${moment().format('YYYYMMDDHHmmss')}`;
	let content = `Params data : ${JSON.stringify(params)} \n Header:${JSON.stringify(req.headers)} `;
	fs.writeFile(dateString, content, function (err) {
		if (err) {
			return console.log(err);
		} else {
			let query = 'select * from v_orders_store_7 ';
			if (params.to && params.from) {
				query = `${query} where rcpt_dt between '${params.from}' and '${params.to}'`
			}
			db.query(query).then((result) => {
				if (contype != 'application/xml') {
					var parsedJson = getJsonParsedResponse(result[0]);
					res.send(parsedJson);
				} else {
					var parsedXml = getXMLParsedResponse(result[0]);
					res.set('Content-Type', 'application/xml');
					res.send(xml(parsedXml, { declaration: true }));
				}
			});
		}

	})
}

export function getOrderItemOnlineFabricDetails(req, res) {
	const order_item_id = req.query.order_item_id;
	// const has_discount = req.query.has_discount == 'true' ? true : false;
	// const store_id = req.query.store_id;
	db.query(`select * from b_order_item_fabric_design where order_item_id=${order_item_id}`).then((result) => {

		res.send(result[0]);
	});
}
export function getOrderItemOnlineMeasurements(req, res) {
	const order_item_id = req.query.order_item_id;
	// const has_discount = req.query.has_discount == 'true' ? true : false;
	// const store_id = req.query.store_id;
	db.query(`select * from b_order_item_fabric_measurement where order_item_id=${order_item_id}`).then((result) => {

		res.send(result[0]);
	});
}
export function getOrderItemOnlineStyleBucketDetails(req, res) {
	const order_item_id = req.query.order_item_id;
	db.query(`select its.style from item_type_style its LEFT JOIN b_order_item boi ON boi.style_code = its.code where boi.order_item_id=${order_item_id}`).then((result) => {

		res.send(result[0]);
	});
}

export function stockUpdate(req, res) {
	let values = '(';
	let notExistProducts = [];

	const reqBody = req.body;

	const quantity = reqBody.value;
	const productCode = reqBody.supplier_product_code;
	const isWebApplicablility = reqBody.isWebApplicablility;

	let updateSetter = `in_stock_qty=${quantity}`;

	let verifyQuery = 'select supplier_product_code from m_fabric where supplier_product_code IN ';

	productCode.map((item) => {
		values += `'${item}',`;
	});

	values = values.slice(0, -1).concat(")");

	verifyQuery += values;

	db.query(verifyQuery)
		.then((result) => {
			if(productCode.length != result[0].length) {
				productCode.map((item) => {
					if(!_.find(result[0], { supplier_product_code: item})) {
						notExistProducts.push(item);
					}
				});
			}

			updateSetter = (quantity != 1) ? updateSetter : updateSetter.concat(`,isavailableforweb=${isWebApplicablility}`);

			const updateQuery = `update m_fabric set ${updateSetter} where supplier_product_code IN ${values}`;

			return db.query(updateQuery)
				.then((value) => {
					return res.send({
						success: true,
						not_exist: notExistProducts
					});
				})
				.catch((err) => {
					return res.status(500).json({success: false, error: err});
				});			
		})
		.catch((err) => {
			return res.status(500).json({success: false, error: err});
		});
}

export default {
	saveCustomer,
	updateCustomer,
	saveDetails,
	saveOrderItem,
	saveMeasurement,
	saveFabricDesign,
	saveImage,
	saveProfile,
	updateProfile,
	getCustomerOrders,
	getOrderItemFabricDesign,
	getOrderItems,
	getProfiles,
	updateDetails,
	updateOrderItem,
	deleteOrderItem,
	getImages,
	getTaxes,
	getWorkOrders,
	getMeasurementProfileValues,
	getOrderDetails,
	getCustomerDetails,
	getOrderItemDetails,
	getWorkOrderStageList,
	saveWorkFlowStage,
	getOrderItemList,
	testPhantom,
	getImagesOfType,
	saveAlteredOrderItem,
	getPriorityUpcharge,
	getWorkOrderMeasurementProfile,
	getOrderHistory,
	getSignedUrl,
	checkCustomer,
	getLatestItemTypeComment,
	storetransactions,
	saveBulkMeasurement,
	getOrderItemOnlineMeasurements,
	getOrderItemOnlineFabricDetails,
	getOrderItemOnlineStyleBucketDetails,
	stockUpdate

}