import Models from '../models';
const db = Models.sequelize;
const Sequelize = Models.Sequelize;
import moment from 'moment';
import _ from 'lodash';
import Promise from 'bluebird';

const queryResultQuery = 'select * from dashboard_view_query';
const AllFilterQuries = [{
	query: 'select * from m_store',
	id: 'store_id',
	name: 'address',
	category_id: 'store_id',
	category_label: 'Store'
}, {
	query: 'select * from b_workflow_stage',
	id: 'stage_id',
	name: 'code',
	category_id: 'stage_id',
	category_label: 'Status'
}, {
	query: 'select * from m_item_type',
	id: 'item_type_id',
	name: 'code',
	category_id: 'item_type_id',
	category_label: 'Item Type Id'
}, {
	query: 'select * from m_finish_type',
	id: 'finish_type_id',
	name: 'code',
	category_id: 'finish_type',
	category_label: 'Finish Type'
}, {
	query: 'select * from m_priority_type',
	id: 'priority_type_id',
	name: 'code',
	category_id: 'priority_id',
	category_label: 'Priority'
}];


export function getqueryresults(req, res) {
	const _req_body = req.body;
	db.query(`${queryResultQuery} where ${_req_body.query_string.sql}`).then((result) => {
		if (result.length > 0 && result[0].length > 0) {
			res.json({
				success: false,
				data: result[0],
				message: "Success"
			});
		} else {
			res.json({
				success: false,
				data: [],
				message: "No Data Available for the returned query"
			});
		}
	});
}
function getParsedResonse(original_data, id, name, category_id, category_label) {
	let values = {};
	original_data.map((loop_record) => {
		values[loop_record[id]] = loop_record[name];
	})
	return {
		id: category_id,
		label: category_label,
		type: 'integer',
		input: 'select',
		values
	}
}
function filterQuery(_record) {
	return db.query(_record.query).then((result) => {
		return getParsedResonse(result[0], _record.id, _record.name, _record.category_id, _record.category_label);
	});
}
export function getqueryfilters(req, res) {
	var filters = [{
		id: 'm_customer_name',
		label: 'Customer Name',
		type: 'string'
	}, {
		id: 'order_item_id',
		label: 'Order Item Id',
		type: 'double',
		validation: {
			min: 0,
			step: 0.01
		}
	}, {
		id: 'mrp',
		label: 'Mrp',
		type: 'double',
		validation: {
			min: 0,
			step: 0.01
		}
	}, {
		id: 'customer_id',
		label: 'Customer Id',
		type: 'double',
		validation: {
			min: 0,
			step: 0.01
		}
	}, {
		id: 'address',
		label: 'Store Address',
		type: 'string'
	}, {
		id: 'supplier_product_code',
		label: 'SKU Code',
		type: 'string'
	}];
	let promiseArray = [];
	AllFilterQuries.map((_record) => {
		promiseArray.push(filterQuery(_record));
	});
	Promise.all(promiseArray).then((record) => {
		filters = _.concat(filters, record);
		res.json({
			success: false,
			data: filters,
			parsedResponse: record,
			message: "Success"
		});
	})

}

export function getquerysave(req, res) {
	const _req_body = req.body;
	console.log("@@",`insert into dashboard_queries(query,query_name,user_id) VALUES( ${_req_body.query_string.sql.replace(/'/g, "''")},${_req_body.query_name},${_req_body.user_id})`);
	db.query(`insert into dashboard_queries(query,query_name,user_id) VALUES( '${_req_body.query_string.sql.replace(/'/g, "''")}','${_req_body.query_name}',${_req_body.user_id})`).then((result) => {
	res.json({
				success: true,
				message: "inserted query"
			});
	});
}
export function getuserqueries(req, res) {
	console.log(req.user,"@@");
	db.query(`select query_name as name,query,dashboard_queries_id as id from dashboard_queries where user_id= ${req.user.user_id}`).then((result) => {
	res.json({
				success: true,
				data:result[0],
				message: "getting queries"
			});
	});
}
export default {
	getqueryresults,
	getqueryfilters,
	getquerysave,
	getuserqueries
}