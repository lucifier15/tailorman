import path from 'path';
import Sequelize from 'sequelize';
import sequelizeConfig from '../sequelize_config';
let ENV = process.env.TAILORMAN_ENV;
if (ENV == 'PRODUCTION')
	ENV = 'production';
else
	ENV = 'development';
const config = sequelizeConfig[ENV];
//const basename = path.basename(module.filename);
const db = {};

const sequelize = new Sequelize(config.database, config.username, config.password, config);

// fs
//   .readdirSync(__dirname)
//   .filter((file) =>
//     (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
//   )
//   .forEach((file) => {
//     const model = sequelize.import(path.join(__dirname, file));
//     db[model.name] = model;
//   });

// Object.keys(db).forEach((modelName) => {
//   if (db[modelName].associate) {
//     db[modelName].associate(db);
//   }
// });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;