import deserializeUser from './deserializeUser';
import local from './local';
import google from './google';

export {
	deserializeUser,
	local,
	google
};

export default {
	deserializeUser,
	local,
	google
};