import Models from '../models';

const db = Models.sequelize;

export default (email, password, done) =>

db.query('Select * from "GetUser"(:in_email)', {
	raw: true,
	replacements: {
		in_email: email
	}
}).then(function(result) {
	const user = result[0][0];
	if (!user) return done(null, false, {
		message: 'The user does not exist'
	});
	return done(null, user);
}).catch((err) => {
	console.log(err);
	done(null, false, {
		message: "Something went wrong during authentication"
	});
});