import Models from '../models';
import {
	google_admin
} from '../controllers';
const db = Models.sequelize;

export default (req, accessToken, refreshToken, profile, done) =>
	google_admin.getRole(profile._json.emails[0].value).then((userData) => {
		if (!userData) {
			return done(null, false, {
				message: 'The user does not exist in our records. Please ask the administrator to enable your google account'
			});
		} else {
			if (userData.roles.store.length == 0 && userData.roles.workflow.length == 0) {
				return done(null, false, {
					message: 'The user does not have permissions to access the application.'
				});
			} else {
				db.query('select * from "GetUser"(:in_email)', {
					raw: true,
					replacements: {
						in_email: profile._json.emails[0].value
					}
				}).then((result) => {
					const user = result[0][0];
					if (!user) return done(null, false, {
						message: 'The user does not exist. Please ask the administrator to enable your google account'
					});
					user.stores = userData.stores;
					user.roles = userData.roles;
					return done(null, user);
				}).catch((err) => {
					console.log(err);
					done(null, false, {
						message: "Something went wrong during authentication"
					});
				});
			}
		}
	})

