//import { ENV } from '../../config/appConfig';
import sequelizeConfig from './sequelize_config';
const ENV = process.env.TAILORMAN_ENV;
let config = sequelizeConfig['development'];
if (ENV == 'PRODUCTION')
  config = sequelizeConfig['production'];
export const db = `${config.dialect}://${config.username}:${config.password}@${config.host}/${config.database}`;

export default {
  db
};