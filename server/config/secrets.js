/** Important **/
/** You should not be committing this file to GitHub **/
/** Repeat: DO! NOT! COMMIT! THIS! FILE! TO! YOUR! REPO! **/
export const sessionSecret = process.env.SESSION_SECRET || 'eqcnoa340ki5ekb9gvids4ksgqt9hf48';

export const google = {
	clientID: process.env.GOOGLE_CLIENTID || '542106466039-50ebo9g75dfa1l76mhq5qb1bk7gin6pk.apps.googleusercontent.com',
	clientSecret: process.env.GOOGLE_SECRET || 'COqerZTGZCPPRI2WFt-oAR_y',
	callbackURL: process.env.GOOGLE_CALLBACK || '/oauth2callback'
};

export const google_admin_local = {
	"type": "service_account",
	"project_id": "dev-store-ops",
	"private_key_id": "56cfc96da6c6a3767f2e8eeef62fe259c0308fc9",
	"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQD9E1Fb6Oo1261c\nKBkXFqfTEkKpMaGheHWZe1p4KEgx6Q/P6idZkWp1kJVh95T3AbPVaYQaqKNXd1nG\narbo2RYl/xXJqQjYwdGg/6qpnywESx6LgXkyMzZv2h3t620z7gl/8bcI/yxw0AOP\nnynwulfevox9k260RcGy6+ti1vXgbHkOULsC0t4O+ZI8lzHhPoQU0Ki328rPAtbV\n8cfXC7wUI0GJhDUix4VR3x6lEMGhA0wQTCu2pIbvj7ZDniksCGiqRh1UI6trZD6G\nDd63PoE5x3qTk5KZnujssD4ddAwIPf922vhpr6FV1pyXefj8KZ5Od2NZnq3y2zY4\nh/Ynw0rxAgMBAAECggEAVCymtPFFZWCJysszfTB15Yh/T5NIdVKBmukicDDFwoQI\nVFlHeU0aobvTU+BrVbTlQah03gEVQzRZUduQtuXtrfmoH4jvflkJBonDE+PzWgI6\nSKBp1T6fbpiXt9jEW69izYFyJbXp7CPj61M/Oehg7Z9nTyVIfwFsqbw6cIebBGem\nkc2MZNXqyJg5gcwMD4SdF0WJKaS0KexzXhsNskg2EVH0Owr+RcKMQOffI580mYGq\n3H/WIqSX93E6YjRkhIORtL8SHz36RmVWmDHnnAHNOnWoQw+wBvKZC8a1lPxZeCJt\nXiVNDdDkakZheICqAVnQ0inu8ZJisW4MJ6cW+1rZoQKBgQD+pOAKwGO3t5BFPR+I\n14XF9zSmmxnrSD8CCwyX3hP9692tT8H6W7cRvjunIqiJt1Mqd84K1YV1v5Nztcx+\njtfq1KhPoJAZ7fBFWt9WDaVWgAj2CjtK5dP0/d9vjrILlLjk0HwldLOlrXnmwOwf\nozew1y3J/Bo+dGiAhN1rteHoRQKBgQD+bE3sYqITu43S87R5Mn1VVdVQmMENpneS\nnoVPYmJ+UkTPJXNLC9QiFBJU3rZimzvP+Ldov4xB7Ol4p4eZXOaSK/NcjeBbiGc1\nHdWh7+HrX9H7Kv4Yaaf0igei5XRL+/cZX/HA25gPieNrUpI8QcObQb+jhKIlLce+\nHTtc30SQvQKBgQDW6DMlH8WIsWUS6kkI5kvCDaXjzhaL2UdD5CtWqFha/orxz9Q4\nBNt2IAScQ8XVpGCOANpljlq6S1qYVyAk100mpR7/RNUY7RTQk6bVK2MUo63saCou\nRdUieHfv4JAm8fcxNTrxKZLMCqJMuxkMgmVn29KLGzbS6s+fV4K/epsTuQKBgQCc\nvLAMrn7W1TZ7/PLzry0TQzuU2WYlOKZ1Rv8tdpji5V/KlXG/i9v12pOogUOu358r\ncjfbahXID2GNmPeqfVb2OelE3YqYGSIr0ONpTdTKIk/EAipjc8OqnNYzflP085a4\npSjT/PRirjnSRh4czQ7qOMkMvYr+iIODFZMaldkxqQKBgDxn5z6snOgl6nnYHJ5X\n5M73cz7RnbcvoU5e/Ej8UPHBP7vu2gW2QXdCdoyMOx/Nf4rKSN9qQrhsL+D68VXO\nmt3rBnRGpQSJYptnWXdBUzo9YNx3337m62F+F053QuYUTjUZzilC8WkAk+KcTC6/\nkEChuz+eoT9nZdZMNKGNAQjQ\n-----END PRIVATE KEY-----\n",
	"client_email": "dev-store-ops@dev-store-ops.iam.gserviceaccount.com",
	"client_id": "109123772820630123955",
	"auth_uri": "https://accounts.google.com/o/oauth2/auth",
	"token_uri": "https://accounts.google.com/o/oauth2/token",
	"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
	"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/dev-store-ops%40dev-store-ops.iam.gserviceaccount.com"
}

export const google_admin_prod = {
	"type": "service_account",
	"project_id": "store-ops",
	"private_key_id": "651e113d34aa58981f403dd17432f376a0481a30",
	"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC6nk48Uh30DLUl\nQ298EAu+SGLSkY23APzkz7wH5h4bDxD2tA02B7LOG9Dc9pAyhRLxcoIvu9lm946+\nfhAdDmVeN1xiqrXox4O6YngymwNiqK40hwx/8F81JL5iO+0fogrhwxXJHGDGekKs\nxOxatK7wbNQrg0PduwgBkmaP/JHPbmpxBWIVivrRzglOvPYgGLcK7iwQ2zSCiQnS\nrVJ22CghzZa/z0uK0lrezEdKs8CHhCR8YJGCrDGNo97zYIIG7P2vhRwFKj/W7hmT\nRcfPViYwDPLduy3iZZ/GeRb8+vpgGjkZdSJs/Oaax+DltVPCXh7DM1dXa30XKPgY\nHeJRIRDxAgMBAAECggEAEbKHitLiwk0eNJ1zihlfc7kbngptnQg781r74ufgvQIt\nwqeKhTAvtboPkZTUUeYqyLjqPaqb0Fbzs37y+j5uQIQRf83emGerR1alPW9T39xN\nQO/5sHxUGw8T+u7h6tkMHSLKAxts1gWsIwQ9wdBWsrgvWr0FB5cACF4g2dDC5x/R\nA4E4IHpo2CZlzDKoa3i4sdj3TV6+r+P5E7LjV4tkcVkxaYwbGld97cAWMQ4wga4l\nWOq4cfiU/bh6dhY7I5dhoNrhEkRq3KRLP29Wn2VgcsoK4ze9wkfQ6kKWlv1hOted\n2wlZN3Ce+ydt9jy1MIaeMUgJ7ASbKPCqFi6f3sUUEQKBgQDe5VqJC6PpTRceU8+k\n+F4HgHiK9olo3/gE9IdzjauegOssLQgN2mStpESxbHLJdflh9I9EI192oae+hRIl\n6XmuCzaKCf17c/l3TsRRYrTEAauDGUw3bUHhyWARlgTuueqjIgB3YtaAtGAXoJT4\nD869Hep39r+ulb5iidp/ieILTQKBgQDWVagk9y4RXy4LICUFxVwv/bmWdjbFot5q\n6Rso1Ixjiar7x/msOfsStmOrZ7D7BzetWx93mvvf231AfsD78TCyXbn1FHIgfIdR\ngP2b4KofUmb4LhpRHgny39Tl3hX1j9NfUiYwgZGEg5J0eOAf1nx8jBuN8PtBkdvQ\n9X+6X+aiNQKBgHOylXYBhsl47IEfe+7jxNscAnYPVnALoY/g5dZDOUwDGw9JKJ8Y\nN54yYD3ATyzngdUovpzdx5Wqf4nTRQJM2I5Q37RzTxIftuQJDLo6BB6hx0c7gGZc\n3g+3GZjHSDQqaPLYmWle8nzIJADfp7PDcMqVlrlCaWjT7Yj0iWgeSCkZAoGBAL/Y\n2fGEgmqwPEFfG2Rn/JVEUWsZEvuiRd2CG3pXjfXGkxJdM+Dx4Qcl1F7nKGX5Vt/H\nsYnYYt1D1H/3NPwOLl1pQXraOzFS5biRce+lkAELO/mnIo07CwKnUSQTvqoXe8ZV\nfLNsNnlbX6r7SvmUfcbpjfxmGC/fw9bs99A16PWFAoGAGZG1t/i3WphqvVS6h0XM\nDqn6BEObLXUM+dax9b5B0dI11FqaZI17BrT1PtVTIIx74jIKi1lZfbA6fiFWVPlD\nuY/XUAlpa2NaQ7JzrKswuRk8L15bm73r+az1Rot/LywGWC5s+Xgyo8iwYdis4K5a\nM29NBicLwP9VN/1F1hKzsIs=\n-----END PRIVATE KEY-----\n",
	"client_email": "store-ops@store-ops.iam.gserviceaccount.com",
	"client_id": "100060213778911004223",
	"auth_uri": "https://accounts.google.com/o/oauth2/auth",
	"token_uri": "https://accounts.google.com/o/oauth2/token",
	"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
	"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/store-ops%40store-ops.iam.gserviceaccount.com"
}

export const google_admin_uat = {
	"type": "service_account",
	"project_id": "uat-store-ops",
	"private_key_id": "82169fe8d47c0465af8ba29dbe0cbd38b1165663",
	"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC3a3UIEM2x161M\nDSaOnC8xuK8b6KchsRxWpzYTMg9TEKerP+8Ls7aT0QqPPe7+D+OzF3d+/GKiIx4S\n6Hni0J7Rz337xOMc+xSbaVjGaJveLavYRdCQoUJ0K+t9JfpR35v5G3YZMRJ8e/pi\njg7MArGpmtdtt3foU816T7D+/TLwwNmj9sVD2zD5jhMbYgTOcUUvmC7dJXxCFM/z\nC7E59JBvL93kOBtKCuc+11O/NdDCu7vZsLNIM/bUF9N1nt0MM9FI2dIhOgFt+qNf\n+nW4JtJvub7pp8jlavQedFZIVGlQksdWStQC/eqO8lb/bru/Kz4wcsT2ZP0+uKzq\nJ82PP0JfAgMBAAECggEALvCtLrc+LPlJPGFRMMX/vfXJupa7zv+IhRSd3iO9kEB+\n6HCgOJpxazrgPPBYrWRbnYWv6yukAmKCW8xrBQTm4EL9tuUYSl5nU66OLZiv6xg9\nFkgnrBvY6eojhg+mFDXpsMK6YpAQbxM7bOV2G/fEh+c8orOFWEu6Fkc2wMsDsmD4\n76k8e2EFHNbb8ZhAIphLCU3Pd3zZE9ZQDJnuB/sQdj88oPCF2B6u+JPbOS3N5B3x\norjQW0QvTYMrfLoi/uAnVaRjHnYPwXlwKW9LqLP8n89Y4H/q/3g8ukof5XJKd2s3\n1RMXlnJOvDfXNS49Wqz0Ix841LRP0YZEw9Omb7aeYQKBgQDtn1r2i9TAT0KA07Hz\nDScOREFturUMDOfCll4pnOeFaO7m2Y5t3dULGYgMOSEsNP04UqLDYOyOR7iJ3FiH\n93IACj671Sn4QC8OMLK71J1Fm3p6EzyggzHs6rwe3f5IW/p/t4J5Q8DRlEePYbiC\nYuUUNIG0ALDI5eSQuLhfqLv8HQKBgQDFmvOphwlsm3WgMYWr35DzjV0XoOClrkFy\nLdGx1XLAo/YRmPZhR0O7rRyrJfc6mG5iTc8wAlFSLTZODqg01DxhXa/F2Gh4LrFd\nlOHuo727LBq/f2eAjiHittICTgj1J04cRHBdiDMwUwjANoIbL4wWz97P0O6Qcxxs\noWb4IwBXqwKBgHxcSSP9PKRFFv4LTPdyK72ItWFzNKuQ2X56ad+HcZfduiJxO9x+\nG7CKdSxIffb/6IKgpPRx9nvkR01HV8IZG72vnvN49VUqajYYAILTHFhArdHT3Yf4\nAZoHo9rXvewqzygmZ4+FiRVmcCk3MHR0w4VWkYceUWdjAcNIGZ5e1bt1AoGAU4CN\nIsBLfTecVxtWLzEEIcdE5W04QQsH4OX3zLw0B6/BleIbizQALViT2sbqWDp9utDC\nkrkIAD/WyNlsvxZIhaR/WI2AZ/jjPnZhmuRakrOqge2moS5up002JX8wTdhUK6HI\nEd4yM2ODOSZPiIlpG2Kv9wWFKklaY5U42/Z+eI0CgYAbstEQtM1+pAtf6+hq2gI/\n67ruWFut8Q23+B/wMxJnWLNZpLeWofiq5CJ+yOxAvU6bxMVYRxMgC8k8HrGkiw/o\n8e2tGcW5Cm8gmHRYU4wcw0gTzdWkZW05F7uDIrausVXxc2rtDkWq1K14KvCTaXfl\napBwOKfBvRp53WjW+031fA==\n-----END PRIVATE KEY-----\n",
	"client_email": "uat-store-ops@uat-store-ops.iam.gserviceaccount.com",
	"client_id": "109225538139573265575",
	"auth_uri": "https://accounts.google.com/o/oauth2/auth",
	"token_uri": "https://accounts.google.com/o/oauth2/token",
	"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
	"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/uat-store-ops%40uat-store-ops.iam.gserviceaccount.com"
}

//AWS S3 Image uploader secret keys
export const AWSS3SecretKey = {
	region: process.env.AWS_REGION || 'ap-south-1',
	bucketName: process.env.AWS_BUCKETNAME || 'u-tm-so-img',	
	accessKeyID: process.env.AWS_ACCESSKEY_ID || 'AKIAIBODLHAQOTG2JDXA',
	secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY || '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'
};

export default {
	sessionSecret,
	google,
	google_admin_local,
	google_admin_prod,
	AWSS3SecretKey
};