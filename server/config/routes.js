/**
 * Routes for express app
 */
import passport from 'passport';
import unsupportedMessage from '../db/unsupportedMessage';
import fs from 'fs';
import {
	controllers,
	passport as passportConfig
} from '../db';

const usersController = controllers && controllers.users;
const listsController = controllers && controllers.lists;
const ordersController = controllers && controllers.orders;
const dashboardController = controllers && controllers.dashboard;

export default (app) => {
	// user routes
	if (usersController) {
		app.post('/login', usersController.login);
		app.post('/logout', usersController.logout);
		app.post('/user/stores', usersController.getStores);
	} else {
		console.warn(unsupportedMessage('users routes'));
	}

	if (passportConfig && passportConfig.google) {
		// google auth
		// Redirect the user to Google for authentication. When complete, Google
		// will redirect the user back to the application at
		// /auth/google/return
		// Authentication with google requires an additional scope param, for more info go
		// here https://developers.google.com/identity/protocols/OpenIDConnect#scope-param
		app.get('/auth/google', passport.authenticate('google', {
			scope: [
				'https://www.googleapis.com/auth/userinfo.profile',
				'https://www.googleapis.com/auth/userinfo.email'
			]
		}));

		// Google will redirect the user to this URL after authentication. Finish the
		// process by verifying the assertion. If valid, the user will be logged in.
		// Otherwise, the authentication has failed.
		app.get('/oauth2callback',
			passport.authenticate('google', {
				successRedirect: '/',
				failureRedirect: '/login'
			})
		);
	}


	if (listsController) {
		app.get('/list/tailor', listsController.getTailors);
		app.get('/list/priority', listsController.getPriorities);
		app.get('/list/paymentType', listsController.getPaymentTypes);
		app.get('/list/workflow', listsController.getWorkflows);
		app.get('/list/measurementField', listsController.getMeasurementFields);
		app.get('/list/measurementType', listsController.getMeasurementTypes);
		app.get('/list/style', listsController.getStyles);
		app.get('/list/itemType', listsController.getItemTypes);
		app.get('/list/fabric', listsController.searchFabrics);
		app.get('/list/customer', listsController.getCustomerList);
		app.get('/list/fabricDesignField', listsController.getFabricDesignFields);
		app.get('/list/customerSource', listsController.getCustomerSourceList);
		app.get('/list/occasion', listsController.getOccasions);
		app.get('/list/finishType', listsController.getFinishTypes);
		app.get('/list/salesman', listsController.getSalesmen);
		app.post('/list/workflowStage', listsController.getWorkFlowStageList);
		app.get('/list/workflow', listsController.getWorkFlowList);
		app.get('/list/searchRTW',listsController.getRTWList);
		app.get('/list/fetchSize',listsController.getSizeList);
		app.get('/list/fetchFit',listsController.getFitList);
		app.post('/list/inventoryTransfer',listsController.transferInventory)


	} else {
		console.warn(unsupportedMessage('lists routes'));
	}

	if (ordersController) {
		//fetch details
		app.get('/orders', ordersController.getCustomerOrders);
		app.get('/profiles', ordersController.getProfiles);
		app.get('/profile/values', ordersController.getMeasurementProfileValues);
		app.get('/order/item/fabricDesign', ordersController.getOrderItemFabricDesign);
		app.get('/order/itemList', ordersController.getOrderItems);
		app.get('/order/item/delete', ordersController.deleteOrderItem);
		app.get('/order/images', ordersController.getImages);
		app.get('/order/taxes', ordersController.getTaxes);
		app.post('/workorders', ordersController.getWorkOrders);
		app.post('/workflow/stages', ordersController.getWorkOrderStageList);
		app.post('/workflow/stage', ordersController.saveWorkFlowStage);
		app.get('/details/order', ordersController.getOrderDetails);
		app.get('/details/customer', ordersController.getCustomerDetails);
		app.get('/details/order/item', ordersController.getOrderItemDetails);
		app.get('/details/list/order', ordersController.getOrderItemList);
		app.get('/details/image', ordersController.getImagesOfType);
		app.get('/order/invoice', ordersController.testPhantom);
		app.get('/priority/upcharge', ordersController.getPriorityUpcharge);
		app.get('/workorder/measurements', ordersController.getWorkOrderMeasurementProfile);
		app.get('/orders/items/history', ordersController.getOrderHistory);
		app.post('/s3/image/getsignedurl', ordersController.getSignedUrl);
		app.get('/details/onlinemeasurement', ordersController.getOrderItemOnlineMeasurements);
		app.get('/details/onlinefabric', ordersController.getOrderItemOnlineFabricDetails);
		app.get('/details/getstylebucket', ordersController.getOrderItemOnlineStyleBucketDetails);

		//save details
		app.post('/order/customer', ordersController.saveCustomer);
		app.post('/order/checkcustomer', ordersController.checkCustomer);
		app.post('/order/customer/update', ordersController.updateCustomer);
		app.post('/order/details', ordersController.saveDetails);
		app.post('/order/item', ordersController.saveOrderItem);
		app.post('/order/altered/item', ordersController.saveAlteredOrderItem);
		app.post('/order/item/update', ordersController.updateOrderItem);
		app.post('/order/item/measurement', ordersController.saveMeasurement);
		app.post('/order/item/bulkmeasurement', ordersController.saveBulkMeasurement);
		app.post('/order/item/fabricDesign', ordersController.saveFabricDesign);
		app.post('/order/saveImage', ordersController.saveImage);
		app.post('/order/details/update', ordersController.updateDetails);
		app.post('/order/customer/profile', ordersController.saveProfile);
		app.post('/order/customer/profile/update', ordersController.updateProfile);
		app.get('/order/latestcomment', ordersController.getLatestItemTypeComment);
		app.get('/storeorderlist', ordersController.storetransactions);
		app.post('/stockupdate',ordersController.stockUpdate);		
	} else {
		console.warn(unsupportedMessage('orders routes'));
	}
	if(dashboardController){
		app.post('/dashboard/queryresult', dashboardController.getqueryresults);
		app.get('/dashboard/queryfilter', dashboardController.getqueryfilters);
		app.post('/dashboard/querysave', dashboardController.getquerysave);
		app.get('/dashboard/getuserqueries',dashboardController.getuserqueries);

	}
};